<%@page import="com.star.CommonMethod"%>
<%@page import="java.util.Date"%>
<%@page import="com.star.LogMessage"%>
<%@page import="com.star.DownloadServlet"%>
<%@page import="com.star.CommonMethod.*"%>

<%
	/*
		Created By : Nawab Alam
		Date : 18 Feb, 2025
		Description : DownloadFile.jsp
		Details : To generate the QDS file based on the Input Paramters from First Servlet and Download to client. 
	*/
%>

<%@ page import="java.io.*"%>
<%@ page import="javax.servlet.ServletException"%>
<%@ page import="javax.servlet.http.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Download Certificate</title>
<style>
.div-header {
	border: 1px solid lightgrey;
	padding: 10px;
	border-bottom: 0px;
}

table {
	text-align: left;
}

table, td, th {
	border: 1px solid lightgrey;
	border-collapse: collapse;
	padding: 10px;
}
</style>

</head>
<body>

	<%
		String TMP_PATH = "";
		String execPath = "";
		String tmplPath = "";

		if (System.getenv("SERVER") != null) {
			TMP_PATH = System.getenv("HOME") + "/bin/STAR_LOGS/";
			execPath = System.getenv("HOME") + "/bin/gencert_jade_linux_300000";
			//tmplPath = System.getenv("HOME") + "/bin/srdqds_jade.xml";
			tmplPath = System.getenv("HOME") + "/bin";
		} else {
			TMP_PATH = "C:\\Users\\NawabA\\Desktop\\tmp\\";
			execPath = TMP_PATH + "gencert_jade_linux_300000";
			tmplPath = TMP_PATH;
		}

		int ERROR = 0;
		String errMsg = "";
		String display = "none";

		//String virtualPath = request.getServletPath(); //The virtual path
		//String realPath = request.getRealPath(virtualPath);//The physical path
		//String directoryPath = realPath.substring(0, realPath.lastIndexOf('/') + 1);

		if (request.getMethod().equals("GET")) {

			errMsg = "";
			ERROR = 0;
			display = "none";

			CommonMethod cmnMthd = new CommonMethod();

			String cmpyId = cmnMthd.paramBlank(request.getParameter("cmpyId"));
			String qdsCtlNo = cmnMthd.paramBlank(request.getParameter("qdsCtlNo"));
			String selFlg = cmnMthd.paramBlank(request.getParameter("selFlg"));
			String chemList = cmnMthd.paramBlank(request.getParameter("chemList"));
			String jmyFlg = cmnMthd.paramBlank(request.getParameter("jmyFlg"));
			String impcFlg = cmnMthd.paramBlank(request.getParameter("impcFlg"));
			String micnFlg = cmnMthd.paramBlank(request.getParameter("micnFlg"));
			String htrmtFlg = cmnMthd.paramBlank(request.getParameter("htrmtFlg"));

			String tstSpcFlg = cmnMthd.paramBlank(request.getParameter("tstSpcFlg"));
			String mtlStdFlg = cmnMthd.paramBlank(request.getParameter("mtlStdFlg"));
			String etcFlg = cmnMthd.paramBlank(request.getParameter("etcFlg"));
			String jmyFlg2 = cmnMthd.paramBlank(request.getParameter("jmyFlg2"));

			String flNm = "star_jad_qds_cnfg_gencert_" + qdsCtlNo + "_" + new Date().getTime();
			String logFlNm = TMP_PATH + flNm + ".log";
			LogMessage.setLogFile(logFlNm);
			LogMessage.generateLogs();

			LogMessage.writeLogs("********* IN GET **********");

			LogMessage.writeLogs("TMP_PATH >> " + TMP_PATH);
			LogMessage.writeLogs("execPath >> " + execPath);
			LogMessage.writeLogs("tmplPath >> " + tmplPath);

			// LogMessage.writeLogs("virtualPath >> " + virtualPath);
			// LogMessage.writeLogs("realPath >> " + realPath);
			// LogMessage.writeLogs("directoryPath >> " + directoryPath);

			LogMessage.writeLogs("cmpyId >> " + cmpyId);
			LogMessage.writeLogs("qdsCtlNo >> " + qdsCtlNo);
			LogMessage.writeLogs("selFlg >> " + selFlg);
			LogMessage.writeLogs("chemList >> " + chemList);
			LogMessage.writeLogs("jmyFlg >> " + jmyFlg);
			LogMessage.writeLogs("impcFlg >> " + impcFlg);
			LogMessage.writeLogs("micnFlg >> " + micnFlg);
			LogMessage.writeLogs("htrmtFlg >> " + htrmtFlg);

			LogMessage.writeLogs("tstSpcFlg >> " + tstSpcFlg);
			LogMessage.writeLogs("mtlStdFlg >> " + mtlStdFlg);
			LogMessage.writeLogs("etcFlg >> " + etcFlg);
			LogMessage.writeLogs("jmyFlg2 >> " + jmyFlg2);

			if (qdsCtlNo.length() == 0) {
				errMsg = errMsg + "<tr><td>QDS Ctl No</td><td>qdsCtlNo</td></tr>";
				ERROR = 1;
			}

			// PDF FILE WILL GENERATE IN /tmp/ BY DEFAULT THROUGH COMMAND */
			String pdfFlNm = flNm + ".pdf";
			LogMessage.writeLogs("pdfFlNm >> " + "/tmp/" + pdfFlNm);

			if (ERROR == 0) {

				ProcessBuilder pb = new ProcessBuilder(execPath, cmpyId, qdsCtlNo, selFlg, chemList, jmyFlg,
						impcFlg, micnFlg, htrmtFlg, tstSpcFlg, mtlStdFlg, etcFlg, jmyFlg2, pdfFlNm, tmplPath);
				LogMessage.writeLogs("pb >> " + pb.command());
				Process pr = pb.start();
				int sts = pr.waitFor();
				LogMessage.writeLogs("sts >> " + sts);
				if (sts == 0) {
					if (pdfFlNm != null) {
						DownloadServlet downloadServlet = new DownloadServlet("/tmp/", pdfFlNm);
						downloadServlet.doGet(request, response);
					}
				}
			}

			else {
				display = "block";
			}
		}
	%>
	<div class='div-header' style='display:<%=display%>'>
		<b>Parameters not available in Request</b>
	</div>
	<table style='display:<%=display%>'>
		<thead>
			<tr>
				<th>Parameter Name</th>
				<th>Parameter Field</th>
			</tr>
		</thead>
		<tbody>
			<%=errMsg%>
		</tbody>
	</table>
</body>
</html>