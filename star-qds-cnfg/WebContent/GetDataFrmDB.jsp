<%@page import="java.util.Date"%>
<%@page import="com.star.auth.GetAuthCode"%>
<%@page
	import="com.invera.stratix.ws.exec.authentication.GatewayLoginResponseType"%>
<%@page import="com.star.auth.Authenticate"%>
<%@ page import="sun.misc.BASE64Decoder"%>
<%@ page import="sun.misc.BASE64Encoder"%>
<%@ page import="javax.crypto.Cipher"%>
<%@ page import="javax.crypto.spec.SecretKeySpec"%>
<%@ page import="javax.crypto.spec.IvParameterSpec"%>
<%@ page import="javax.crypto.BadPaddingException"%>
<%@ page import="javax.crypto.IllegalBlockSizeException"%>
<%@ page import="java.security.AlgorithmParameters"%>
<%@ page import="java.security.Key"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.util.StringTokenizer"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.Statement"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.google.gson.*"%>
<%@ page import="com.star.UpdateQDSInfo"%>
<%@ page import="com.star.QdsHeaderInput"%>
<%@ page import="java.io.*"%>
<%@ page import="com.star.*"%>
<%@ page import="com.ws.samples.handler.MessagesSOAPHandler"%>
<%!/* Vendor Price Information*/

	class ParameterInfo {
		private int entryMode = 0;
		//CSX-ENT-MTHD    1 Add
		//CSX-ENT-MTHD    2 Change
		//CSX-ENT-MTHD    3 Duplicate
		//CSX-ENT-MTHD    4 Inquiry

		private int tokenNo = 0;
		private String companyId = "";
		private String loginId = "";
		private String auth = "";
		private String qdsCtlNo = "";

		public int getEntryMode() {
			return entryMode;
		}

		public int getTokenNo() {
			return tokenNo;
		}

		public String getCompanyId() {
			return companyId;
		}

		public String getLoginId() {
			return loginId;
		}

		public String getAuth() {
			return auth;
		}

		public String getQdsCtlNo() {
			return qdsCtlNo;
		}

		public void setEntryMode(int entryMode) {
			this.entryMode = entryMode;
		}

		public void setTokenNo(int tokenNo) {
			this.tokenNo = tokenNo;
		}

		public void setCompanyId(String companyId) {
			this.companyId = companyId;
		}

		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}

		public void setAuth(String auth) {
			this.auth = auth;
		}

		public void setQdsCtlNo(String qdsCtlNo) {
			this.qdsCtlNo = qdsCtlNo;
		}

		public String toString() {
			return new StringBuffer().append("ParameterInfo<entryMode: ").append(this.entryMode).append(", tokenNo: ")
					.append(this.tokenNo).append(", companyId: ").append(this.companyId).append(", qdsCtlNo: ")
					.append(this.qdsCtlNo).append(", loginId: ").append(this.loginId).append(", auth: ")
					.append(this.auth).append(">").toString();
		}
	}

	String CIPHER_NAME = "Blowfish";
	String CIPHER_CONFIG = "CBC/PKCS5Padding";

	String SECRETKEY = "85C9D9182EDF7FE4C9108641B40DC2F2";
	String IV = "313C824124AE4239";

	String SERIALIZATION_DELIMITER = "^";
	String KEY_VALUE_DELIMITER = "=";

	String KEY_ENTRY_MODE = "EntMthd";
	String KEY_TOKEN_NO = "TknNo";
	String KEY_COMPANY_ID = "CmpyId";
	String KEY_LOGIN_ID = "LgnId";
	String KEY_AUTH = "Auth";
	String QDS_CTL_NO = "qdsCtlNo";

	/* HEIDTMAN TEST ENV */
	String exechost = "192.168.31.1";
	String execPort = "48800";
	String envSrvr = "tsthsp";
	String envClass = "TST";

	String outputVal = "";
	String pgmPath = "";

	private byte[] parseHexString(String hexString) {

		if (hexString != null) {

			if (hexString.length() % 2 == 1) {
				throw new IllegalArgumentException("Hex string parameter must contain an even number of characters.");
			}

			byte[] bytes = new byte[hexString.length() / 2];
			int end = hexString.length() - 2;
			int index = 0;
			try {
				for (int pos = 0; pos <= end; pos += 2) {
					bytes[index++] = (byte) Integer.parseInt(hexString.substring(pos, pos + 2), 16);
				}
			} catch (NumberFormatException e) {
				throw e;
			}
			return bytes;
		}

		return new byte[0];
	}

	private Cipher getDecrypter() {

		Cipher decrypter = null;

		try {

			byte[] keyBytes = parseHexString(SECRETKEY);
			byte[] ivBytes = parseHexString(IV);

			Key key = new SecretKeySpec(keyBytes, CIPHER_NAME);

			IvParameterSpec iv = new IvParameterSpec(ivBytes);
			AlgorithmParameters params = AlgorithmParameters.getInstance(CIPHER_NAME);
			params.init(iv);

			decrypter = Cipher.getInstance(CIPHER_NAME + "/" + CIPHER_CONFIG);
			decrypter.init(Cipher.DECRYPT_MODE, key, params);

		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}

		return decrypter;
	}

	private ParameterInfo deciferParamString(String encryptedParam) {
		String serializedString = null;
		try {
			Cipher decrypter = getDecrypter();
			byte[] ciphertext = new BASE64Decoder().decodeBuffer(encryptedParam);

			//synchronized(decrypter) { //no need to synchronize because it's a new one every time
			serializedString = new String(decrypter.doFinal(ciphertext));
			//}
		}

		catch (IOException ioe) {
			System.err.println(">>>>>>>>>>>> Encrypted JobOrder String that caused IOException:" + encryptedParam);
			throw new RuntimeException(ioe);
		} catch (BadPaddingException bpe) {
			System.err.println(
					">>>>>>>>>>>> Encrypted JobOrder String that caused BadPaddingException:" + encryptedParam);
			throw new RuntimeException(bpe);
		} catch (IllegalBlockSizeException ibe) {
			System.err.println(
					">>>>>>>>>>>> Encrypted JobOrder String that caused IllegalBlockSizeException:" + encryptedParam);
			throw new RuntimeException(ibe);
		}

		ParameterInfo parameterInfo = new ParameterInfo();

		StringTokenizer tokenizer = new StringTokenizer(serializedString, SERIALIZATION_DELIMITER);

		int tokenCount = tokenizer.countTokens();

		if (tokenCount == 0) {
			throw new IllegalArgumentException("Incomplete Exec Reference: " + serializedString);
		}

		while (tokenizer.hasMoreTokens()) {
			String keyValueToken = tokenizer.nextToken();

			if (keyValueToken.contains(KEY_AUTH)) {
				String value = keyValueToken.substring(KEY_AUTH.length() + 1, keyValueToken.length());
				parameterInfo.setAuth(value);

			} else if (keyValueToken.contains(KEY_COMPANY_ID)) {
				String value = keyValueToken.substring(KEY_COMPANY_ID.length() + 1, keyValueToken.length());
				parameterInfo.setCompanyId(value);
			} else if (keyValueToken.contains(KEY_LOGIN_ID)) {
				String value = keyValueToken.substring(KEY_LOGIN_ID.length() + 1, keyValueToken.length());
				parameterInfo.setLoginId(value);
			} else if (keyValueToken.contains(QDS_CTL_NO)) {
				String value = keyValueToken.substring(QDS_CTL_NO.length() + 1, keyValueToken.length());
				parameterInfo.setQdsCtlNo(value);
			}

			/* StringTokenizer valueTokenizer = new StringTokenizer(keyValueToken,	KEY_VALUE_DELIMITER);
			
			String key = valueTokenizer.nextToken();
			String value = valueTokenizer.nextToken();
			
			System.err.println("Key : " + key + ", Value : " + value );
			
			if (key.equalsIgnoreCase(KEY_ENTRY_MODE)) {
				parameterInfo.setEntryMode((value != null ? Integer.parseInt(value) : 0));
			} else if (key.equalsIgnoreCase(KEY_TOKEN_NO)) {
				parameterInfo.setTokenNo((value != null ? Integer.parseInt(value) : 0));
			} else if (key.equalsIgnoreCase(KEY_COMPANY_ID)) {
				parameterInfo.setCompanyId(value);
			} else if (key.equalsIgnoreCase(KEY_LOGIN_ID)) {
				parameterInfo.setLoginId(value);
			} else if (key.equalsIgnoreCase(KEY_AUTH)) {
				parameterInfo.setAuth(value);
			} */
		}

		return parameterInfo;
	}

	private List<String> getCmpyId(String usrId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String usrNm = "";
		String usrCmpy = "";

		List<String> userInList = new ArrayList<String>();

		try {

			con = DBConnection.getDBConnection();

			query = "select usr_usr_cmpy_id, usr_nm from mxrusr_rec where usr_lgn_id=?";

			ps = null;
			rs = null;

			ps = con.prepareStatement(query);

			ps.setString(1, usrId);

			rs = ps.executeQuery();

			if (rs.next()) {

				userInList.add(rs.getString(1));
				userInList.add(rs.getString(2));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
			throw new RuntimeException(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return userInList;
	}
	//Edited Shruti-30-05-23

	private String getElementList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String usrNm = "";
		String usrCmpy = "";
		JsonObject mainObject = new JsonObject();
		JsonArray array = new JsonArray();

		try {

			con = DBConnection.getDBConnection();

			query = "select che_chmel from scrche_rec  ";

			ps = null;
			rs = null;

			ps = con.prepareStatement(query);

			rs = ps.executeQuery();

			while (rs.next()) {

				JsonObject jsonObject = new JsonObject();

				jsonObject.addProperty("Element", rs.getString(1));

				array.add(jsonObject);

			}

			mainObject.add("chmList", array);

		} catch (SQLException sqle) {
			sqle.printStackTrace();
			throw new RuntimeException(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return mainObject.toString();
	}

	private QdsHeaderInput getQDSInformation(String qdsNo, String cmpyId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String usrNm = "";
		String usrCmpy = "";

		QdsHeaderInput headerInput = new QdsHeaderInput();

		try {

			con = DBConnection.getDBConnection();

			query = "select qds_whs, qds_mill, qds_heat, qds_qds_sts,qds_mill_ord_no, qds_ven_id, qds_apvd_flg, qds_orig_po_no, qds_orig_po_itm, qds_orig_po_dist, (select het_orig_zn from inhhet_rec where het_mill=qds_mill and het_heat=qds_heat) from mchqds_rec where qds_cmpy_id=? and qds_qds_ctl_no=?";

			ps = null;
			rs = null;

			ps = con.prepareStatement(query);

			ps.setString(1, cmpyId);
			ps.setInt(2, Integer.parseInt(qdsNo));

			rs = ps.executeQuery();

			if (rs.next()) {

				headerInput.setWhs(rs.getString(1));
				headerInput.setMill(rs.getString(2));
				headerInput.setHeat(rs.getString(3));
				headerInput.setStatus(rs.getString(4));
				headerInput.setMillOrdNo(rs.getString(5));
				headerInput.setVenId(rs.getString(6));
				headerInput.setApvdFlag(rs.getInt(7));
				headerInput.setOrigPoNo(rs.getInt(8));
				headerInput.setOrigPoItem(rs.getInt(9));
				headerInput.setOrigPoDistribution(rs.getInt(10));
				headerInput.setOrigZone(rs.getString(11));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
			throw new RuntimeException(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return headerInput;
	}%>
<%
	if (request.getMethod().equals("POST")) {

		String virtualPath = request.getServletPath(); //The virtual path 
		String realPath = request.getRealPath(virtualPath);//The physical path
		String directoryPath = realPath.substring(0, realPath.lastIndexOf('/') + 1);

		String newAuthToken = "";

		pgmPath = directoryPath;

		JsonObject mainObj = new JsonObject();

		String param = request.getParameter("param");
		String execFlg = request.getParameter("execFlg");
		String cmpyId = request.getParameter("cmpyId");
		String qdsCtlNo = request.getParameter("qdsCtlNo");
		String host = request.getParameter("host");
		String port = request.getParameter("port");
		String authToken = request.getParameter("token");
		String userId = request.getParameter("usrId");
		String lng = request.getParameter("lng");
		String cntry = request.getParameter("ctry");
		String nginxURL = request.getParameter("nginxURL");

		if (execFlg.equals("A")) {
			outputVal = GetDataFromDB.getQDSDetails(cmpyId, userId, qdsCtlNo, param, host, port, nginxURL,
					authToken);
		}

		/* GET OIR*/
		else if (execFlg.equals("EL")) {
			SendExternalReq externalReq = new SendExternalReq();

			JsonObject inpReq = new JsonObject();

			outputVal = getElementList();
		}

		/** GET CUSTOMER ID AND NAME*/
		else if (execFlg.equals("B")) {
			outputVal = GetDataFromDB.getCustomerList(cmpyId);
		}

		/* GENERATE JOMINY VALUES */
		else if (execFlg.equals("J")) {
			SendExternalReq externalReq = new SendExternalReq();

			String flag = request.getParameter("boronFlg");

			qdsCtlNo = request.getParameter("qdsCtlNo");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "A");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));
			inpReq.addProperty("boronFlg", Integer.parseInt(flag));
			System.out.println("***" + inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "A", "sfcjmy", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* GET OIR*/
		else if (execFlg.equals("R")) {

			System.out.println("********* execFlg= R *************");
			SendExternalReq externalReq = new SendExternalReq();

			//qdsCtlNo = request.getParameter("qdsCtlNo");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "R");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "R", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);

			System.out.println("** execFlg = R outputVal ***" + outputVal);
		}

		/* GET TEMPLATE DETAILS*/
		else if (execFlg.equals("T")) {
			SendExternalReq externalReq = new SendExternalReq();

			String cusId = request.getParameter("cusId");
			String tmplNm = request.getParameter("tmplNm");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "T");
			inpReq.addProperty("cusId", cusId);
			inpReq.addProperty("tmplNm", tmplNm);

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "T", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* CREATE QDS */
		else if (execFlg.equals("Q")) {
			UpdateQDSInfo qdsInfo = new UpdateQDSInfo();

			List<String> msgList = new ArrayList<String>();

			qdsCtlNo = request.getParameter("qdsCtlNo");

			ServiceReturn res = new ServiceReturn();

			QdsHeaderInput headerInput = getQDSInformation(qdsCtlNo, cmpyId);

			msgList = qdsInfo.updateQDS(host, port, authToken, userId, res, qdsCtlNo, headerInput, nginxURL);

			mainObj.addProperty("authToken", res.getAuthToekn());
			mainObj.addProperty("rtnStatus", 0);
			mainObj.addProperty("msg", msgList.get(0));
			outputVal = mainObj.toString();
		}

		/* DELETE QDS */
		else if (execFlg.equals("DT")) {
			UpdateQDSInfo qdsInfo = new UpdateQDSInfo();

			List<String> msgList = new ArrayList<String>();

			qdsCtlNo = request.getParameter("qdsCtlNo");

			JsonParser parser = new JsonParser();
			JsonArray qdsArray = (JsonArray) parser.parse(qdsCtlNo);

			ServiceReturn res = new ServiceReturn();

			for (int i = 0; i < qdsArray.size(); i++) {
				String qdsNo = qdsArray.get(i).getAsJsonObject().get("qdsCtlNo").getAsString();
				msgList = qdsInfo.deleteQDS(host, port, authToken, userId, res, qdsNo, nginxURL);
			}

			mainObj.addProperty("authToken", res.getAuthToekn());
			mainObj.addProperty("rtnStatus", 0);
			if (msgList.size() > 0) {
				mainObj.addProperty("msg", msgList.get(0));
			} else {
				mainObj.addProperty("msg", "");
			}
			outputVal = mainObj.toString();
		}

		/* GET CUSTOMER AND PRODUCT INFORMATION*/
		else if (execFlg.equals("G")) {
			SendExternalReq externalReq = new SendExternalReq();

			qdsCtlNo = request.getParameter("qdsCtlNo");
			String ordPfx = request.getParameter("ordPfx");
			String ordNo = request.getParameter("ordNo");
			String ordItm = request.getParameter("ordItm");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "G");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));
			inpReq.addProperty("ordPfx", ordPfx);
			inpReq.addProperty("ordNo", Integer.parseInt(ordNo));
			inpReq.addProperty("ordItm", Integer.parseInt(ordItm));

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "G", "sfcctm", "QDS", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* GET CUSTOMER AND PRODUCT INFORMATION*/
		else if (execFlg.equals("1")) {

			String TMP_PATH = "";
			if (System.getenv("SERVER") != null) {
				TMP_PATH = System.getenv("HOME") + "/bin/STAR_LOGS/";
			} else {
				TMP_PATH = "C:\\Users\\NawabA\\Desktop\\tmp\\";
			}

			String logFlNm = TMP_PATH + cmpyId + "_" + userId + "_1_" + new Date().getTime() + ".log";
			LogMessage.setLogFile(logFlNm);
			LogMessage.generateLogs();

			LogMessage.writeLogs("execFlg.equals(1)");
			SendExternalReq externalReq = new SendExternalReq();

			qdsCtlNo = request.getParameter("qdsCtlNo");
			String ordPfx = request.getParameter("ordPfx");
			String ordNo = request.getParameter("ordNo");
			String ordItm = request.getParameter("ordItm");
			String htrmtFlg = request.getParameter("htrmtFlg");
			String micnFlg = request.getParameter("mincnFlg");
			String impcFlg = request.getParameter("impcFlg");
			String jmyFlg = request.getParameter("jmyFlg");
			String tstSpcFlg = request.getParameter("tstSpcFlg");
			String mtlStdFlg = request.getParameter("mtlStdFlg");
			String etcFlg = request.getParameter("etcFlg");
			String jmyFlg2 = request.getParameter("jmyFlg2");
			String tmplNm = request.getParameter("tmplNm");
			String cusId = request.getParameter("cusId");
			String chmSelFlg = request.getParameter("chmSelFlg");
			String chemArr = request.getParameter("chemArr");

			LogMessage.writeLogs("qdsCtlNo >> " + qdsCtlNo);
			LogMessage.writeLogs("ordPfx >> " + ordPfx);
			LogMessage.writeLogs("ordNo >> " + ordNo);
			LogMessage.writeLogs("ordItm >> " + ordItm);
			LogMessage.writeLogs("htrmtFlg >> " + htrmtFlg);
			LogMessage.writeLogs("micnFlg >> " + micnFlg);
			LogMessage.writeLogs("impcFlg >> " + impcFlg);
			LogMessage.writeLogs("jmyFlg >> " + jmyFlg);
			LogMessage.writeLogs("tstSpcFlg >> " + tstSpcFlg);
			LogMessage.writeLogs("mtlStdFlg >> " + mtlStdFlg);
			LogMessage.writeLogs("etcFlg >> " + etcFlg);
			LogMessage.writeLogs("jmyFlg2 >> " + jmyFlg2);
			LogMessage.writeLogs("tmplNm >> " + tmplNm);
			LogMessage.writeLogs("cusId >> " + cusId);
			LogMessage.writeLogs("chmSelFlg >> " + chmSelFlg);
			LogMessage.writeLogs("chemArr >> " + chemArr);

			JsonParser parser = new JsonParser();
			JsonArray chemArray = (JsonArray) parser.parse(chemArr);

			System.out.println(htrmtFlg);

			JsonObject inpReq = new JsonObject();

			for (int i = 0; i < chemArray.size(); i++) {
				int k = i + 1;

				JsonObject chemObject = chemArray.get(i).getAsJsonObject();

				inpReq.addProperty("chmel" + k, chemObject.get("chemEl").getAsString());
			}

			inpReq.addProperty("prsMd", "1");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));
			inpReq.addProperty("ordPfx", ordPfx);
			inpReq.addProperty("ordNo", Integer.parseInt(ordNo));
			inpReq.addProperty("ordItm", Integer.parseInt(ordItm));
			inpReq.addProperty("tmplNm", tmplNm);
			inpReq.addProperty("cusId", cusId);
			inpReq.addProperty("chmSelFlg", chmSelFlg);

			inpReq.addProperty("htrmtFlg", Boolean.parseBoolean(htrmtFlg) ? 1 : 0);

			inpReq.addProperty("mincnFlg", Boolean.parseBoolean(micnFlg) ? 1 : 0);

			inpReq.addProperty("impcFlg", Boolean.parseBoolean(impcFlg) ? 1 : 0);

			inpReq.addProperty("jmyFlg", Boolean.parseBoolean(jmyFlg) ? 1 : 0);

			inpReq.addProperty("tstSpcFlg", Boolean.parseBoolean(tstSpcFlg) ? 1 : 0);

			inpReq.addProperty("mtlStdFlg", Boolean.parseBoolean(mtlStdFlg) ? 1 : 0);

			inpReq.addProperty("etcFlg", Boolean.parseBoolean(etcFlg) ? 1 : 0);

			inpReq.addProperty("jmyFlg2", Boolean.parseBoolean(jmyFlg2) ? 1 : 0);

			LogMessage.writeLogs("inpReq >> " + inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "1", "sfcctm", "QDS", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);

			LogMessage.writeLogs("outputVal >> " + outputVal);
		}

		/* GET CUSTOMER AND PRODUCT INFORMATION*/
		else if (execFlg.equals("2")) {
			SendExternalReq externalReq = new SendExternalReq();

			qdsCtlNo = request.getParameter("qdsCtlNo");
			String htrmtFlg = request.getParameter("htrmtFlg");
			String micnFlg = request.getParameter("mincnFlg");
			String impcFlg = request.getParameter("impcFlg");
			String jmyFlg = request.getParameter("jmyFlg");
			String tmplNm = request.getParameter("tmplNm");
			String cusId = request.getParameter("cusId");
			String chmSelFlg = request.getParameter("chmSelFlg");

			String chemArr = request.getParameter("chemArr");

			JsonParser parser = new JsonParser();
			JsonArray chemArray = (parser.parse(chemArr)).getAsJsonArray();

			System.out.println(htrmtFlg);

			JsonObject inpReq = new JsonObject();

			for (int i = 0; i < chemArray.size(); i++) {
				int k = i + 1;

				JsonObject chemObject = chemArray.get(i).getAsJsonObject();

				inpReq.addProperty("chmel" + k, chemObject.get("chemEl").getAsString());
			}

			inpReq.addProperty("prsMd", "2");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));
			inpReq.addProperty("tmplNm", tmplNm);
			inpReq.addProperty("cusId", cusId);
			inpReq.addProperty("chmSelFlg", chmSelFlg);

			if (Boolean.parseBoolean(htrmtFlg) == false) {
				inpReq.addProperty("htrmtFlg", 0);
			} else {
				inpReq.addProperty("htrmtFlg", 1);
			}

			if (Boolean.parseBoolean(micnFlg) == false) {
				inpReq.addProperty("mincnFlg", 0);
			} else {
				inpReq.addProperty("mincnFlg", 1);
			}

			if (Boolean.parseBoolean(impcFlg) == false) {
				inpReq.addProperty("impcFlg", 0);
			} else {
				inpReq.addProperty("impcFlg", 1);
			}

			if (Boolean.parseBoolean(jmyFlg) == false) {
				inpReq.addProperty("jmyFlg", 0);
			} else {
				inpReq.addProperty("jmyFlg", 1);
			}

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "2", "sfcctm", "QDS", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* GET OIR*/
		else if (execFlg.equals("RT")) {
			SendExternalReq externalReq = new SendExternalReq();

			qdsCtlNo = request.getParameter("qdsCtlNo");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "R");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "R", "sfctag", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* GET OIR*/
		else if (execFlg.equals("AT")) {
			SendExternalReq externalReq = new SendExternalReq();

			qdsCtlNo = request.getParameter("qdsCtlNo");

			String tagList = request.getParameter("tagList");

			JsonParser parser = new JsonParser();
			JsonArray tagArray = (JsonArray) parser.parse(tagList);

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "A");
			inpReq.addProperty("qdsCtlNo", Integer.parseInt(qdsCtlNo));
			inpReq.add("FldTbl", tagArray);

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */
			outputVal = externalReq.callService(host, port, authToken, "A", "sfctag", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		//Edited By Shruti 25-05-23
		/* Sav check List*/
		else if (execFlg.equals("V")) {
			SendExternalReq externalReq = new SendExternalReq();

			String cusId = request.getParameter("cusId");
			String chmSelFlg = request.getParameter("chmSelFlg");
			String htrmtFlg = request.getParameter("htrmtFlg");
			String impcFlg = request.getParameter("impcFlg");
			String jmyFlg = request.getParameter("jmyFlg");
			String jmyFlg2 = request.getParameter("jmyFlg2");
			String mincnFlg = request.getParameter("mincnFlg");
			String exCertFlg = request.getParameter("exCertFlg");
			String tstSpecFlg = request.getParameter("tstSpecFlg");
			String mtlStdFlg = request.getParameter("mtlStdFlg");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "C");
			inpReq.addProperty("cusId", cusId);
			inpReq.addProperty("chmSelFlg", Integer.parseInt(chmSelFlg));
			inpReq.addProperty("htrmtFlg", Integer.parseInt(htrmtFlg));
			inpReq.addProperty("impcFlg", Integer.parseInt(impcFlg));
			inpReq.addProperty("jmyFlg", Integer.parseInt(jmyFlg));
			inpReq.addProperty("jmyFlg2", Integer.parseInt(jmyFlg2));
			inpReq.addProperty("mincnFlg", Integer.parseInt(mincnFlg));
			inpReq.addProperty("tstSpecFlg", Integer.parseInt(tstSpecFlg));
			inpReq.addProperty("mtlStdFlg", Integer.parseInt(mtlStdFlg));

			String chemArr = request.getParameter("chemArr");

			JsonParser parser = new JsonParser();
			JsonArray chemArray = (parser.parse(chemArr)).getAsJsonArray();

			System.out.println(htrmtFlg);

			/*  for(int i = 0; i < chemArray.size() ;i++)
			 {
			 	int k = i +1 ;
			 	
			 	JsonObject chemObject = chemArray.get(i).getAsJsonObject();
			 	
			 	 inpReq.addProperty("chmel" + k, chemObject.get("chemEl").getAsString());
			 } */

			for (int i = 0; i < 20; i++) {
				int k = i + 1;
				String chmel = i < chemArray.size() ? chemArray.get(i).getAsString() : "";
				inpReq.addProperty("chmel" + k, chmel);
			}
			/* save CHEMISTRY INFORMATION */

			outputVal = externalReq.callService(host, port, authToken, "A", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		// Read check list
		else if (execFlg.equals("S")) {
			SendExternalReq externalReq = new SendExternalReq();

			String cusId = request.getParameter("cusId");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "B");
			inpReq.addProperty("cusId", cusId);

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */

			outputVal = externalReq.callService(host, port, authToken, "A", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		// Read check list
		else if (execFlg.equals("W")) {
			SendExternalReq externalReq = new SendExternalReq();

			String ordPfx = request.getParameter("ordPfx");
			String ordNo = request.getParameter("ordNo");
			String ordItm = request.getParameter("ordItm");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "B");
			inpReq.addProperty("ordPfx", ordPfx);
			inpReq.addProperty("ordNo", Integer.parseInt(ordNo));
			inpReq.addProperty("ordItm", Integer.parseInt(ordItm));

			System.out.println(inpReq.toString());

			/* GET CHEMISTRY INFORMATION */

			outputVal = externalReq.callService(host, port, authToken, "A", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/* Save check List*/
		else if (execFlg.equals("WA")) {
			SendExternalReq externalReq = new SendExternalReq();

			String ordPfx = request.getParameter("ordPfx");

			String ordNo = request.getParameter("ordNo");
			String ordItm = request.getParameter("ordItm");
			String chmSelFlg = request.getParameter("chmSelFlg");
			String htrmtFlg = request.getParameter("htrmtFlg");
			String impcFlg = request.getParameter("impcFlg");
			String jmyFlg = request.getParameter("jmyFlg");
			String jmyFlg2 = request.getParameter("jmyFlg2");
			String mincnFlg = request.getParameter("mincnFlg");
			String exCertFlg = request.getParameter("exCertFlg");
			String tstSpecFlg = request.getParameter("tstSpecFlg");
			String mtlStdFlg = request.getParameter("mtlStdFlg");

			JsonObject inpReq = new JsonObject();

			inpReq.addProperty("prsMd", "3");
			inpReq.addProperty("ordPfx", ordPfx);
			inpReq.addProperty("ordNo", Integer.parseInt(ordNo));
			inpReq.addProperty("ordItm", Integer.parseInt(ordItm));
			inpReq.addProperty("chmSelFlg", Integer.parseInt(chmSelFlg));
			inpReq.addProperty("htrmtFlg", Integer.parseInt(htrmtFlg));
			inpReq.addProperty("impcFlg", Integer.parseInt(impcFlg));
			inpReq.addProperty("jmyFlg", Integer.parseInt(jmyFlg));
			inpReq.addProperty("jmyFlg2", Integer.parseInt(jmyFlg2));
			inpReq.addProperty("mincnFlg", Integer.parseInt(mincnFlg));
			inpReq.addProperty("tstSpecFlg", Integer.parseInt(tstSpecFlg));
			inpReq.addProperty("mtlStdFlg", Integer.parseInt(mtlStdFlg));

			String chemArr = request.getParameter("chemArr");

			JsonParser parser = new JsonParser();
			JsonArray chemArray = (parser.parse(chemArr)).getAsJsonArray();

			System.out.println(htrmtFlg);

			/*  for(int i = 0; i < chemArray.size() ;i++)
			 {
			 	int k = i +1 ;
			 	
			 	JsonObject chemObject = chemArray.get(i).getAsJsonObject();
			 	
			 	 inpReq.addProperty("chmel" + k, chemObject.get("chemEl").getAsString());
			 } */

			for (int i = 0; i < 20; i++) {
				int k = i + 1;
				String chmel = i < chemArray.size() ? chemArray.get(i).getAsString() : "";
				inpReq.addProperty("chmel" + k, chmel);
			}
			/* save CHEMISTRY INFORMATION */

			outputVal = externalReq.callService(host, port, authToken, "A", "sfcctm", "WMN", userId, cmpyId,
					lng, cntry, inpReq.toString(), nginxURL);
		}

		/** Login Call */
		else if (execFlg.equals("I")) {

			String encryptedParam = request.getParameter("param");
			String appUrl = request.getParameter("appUrl");

			System.out.println("App URL--->" + appUrl);
			System.out.println("PARAM --->" + encryptedParam);

			ParameterInfo parameterInfo = deciferParamString(encryptedParam);
			cmpyId = parameterInfo.getCompanyId();
			String lgnId = parameterInfo.getLoginId();
			qdsCtlNo = parameterInfo.getQdsCtlNo();
			String auth = parameterInfo.getAuth();

			outputVal = GetDataFromDB.callLogin(cmpyId, lgnId, qdsCtlNo, auth, appUrl);
		}

		// ADDITION OF METAL STANDARD IN INVEX
		else if (execFlg.equals("AM")) {

			param = request.getParameter("param");
			System.out.println("param >> " + param);

			outputVal = GetDataFromDB.updateMtlStdInInvex(cmpyId, userId, authToken, qdsCtlNo, param, host,
					port, nginxURL);

			LogMessage.writeLogs("outputVal >> " + outputVal);
		}

		else {
			outputVal = "";
		}

		System.out.println(
				"IN POST, execFlg : " + execFlg + ", param : " + param + " , outputVal -----> " + outputVal);
	}
%>
<%=outputVal%>