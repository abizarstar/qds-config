<%/*
	Created By : Abizar Hasan
	Date : 29/04/2021
	Description : callPgmAndGetJSON
	Details : To Send Request and read File Output
*/
%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.*,java.util.*,javax.servlet.*"%>

<%!
String ReportPath = "/tmp/";
%>

<%	 

	String execPath = "";
	String args = "";
	String reportName = "";
	String errLogFileName = "";
	String JSONData = "";

	execPath = request.getParameter("pgmPath");
	args = request.getParameter("args");
	reportName = request.getParameter("rptName");
	
	errLogFileName = reportName.replace(".dat", "_")+System.nanoTime()/1000+".log";
	
	 File errLogFile = new File(ReportPath+errLogFileName); 
	
	 PrintWriter pw = new PrintWriter(new FileOutputStream(errLogFile)); 

	try{
		ProcessBuilder pb = new ProcessBuilder(execPath,args,reportName);
		Process pr = pb.start();

		StringBuilder sb = new StringBuilder();
		
		if( pr.waitFor() == 0){
			File file = new File(ReportPath+reportName);
			 BufferedReader reader = new BufferedReader(new FileReader(file));

			String line = "";

			while((line = reader.readLine())!= null){
				sb.append(line);
			} 
	
			JSONData=sb.toString();
			
			}
		else{
		pw.println("Some error occurred in the Report Program. Please contact the administrator");
		}
	}
	catch (Throwable t) {
	pw.println(">>>>> ERROR: " + t.getMessage());
			
	}
	finally {
	}
	
	pw.close();
	
	//JSONData = "{\"output\": \"@Recipe<br>B-LG<br>@DataName<br>1000264031-000<br>@Gauge<br>0.243<br>@Width<br>50.5<br>@Customer<br>WO5159<br>@WidthMonNominalMM<br>1282.7<br>@WidthMonTolMM<br>.0130\"}";
	
	 BufferedReader br = new BufferedReader(new FileReader(errLogFile));
	
	if (br.readLine() == null){
		errLogFile.delete();
	} 
	JSONData = JSONData.replaceAll(",\n]", "]");	
	JSONData = JSONData.replaceAll(",]", "]");	
	JSONData = JSONData.replaceAll(",\n}", "}");	
	JSONData = JSONData.replaceAll(",}", "}");
	JSONData = JSONData.replaceAll("\t", " ");
	//JSONData = "[" +JSONData+"]";
%>
<%=JSONData%>

