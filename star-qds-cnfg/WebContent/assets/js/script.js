/*
	Created By   :- Yogesh Bhade
	Created Date :- 26 April 2021
	Description  :- Javascript For Production Centers
*/
//var execPath = "/u/tsthsp/bin/cstm_heid_amtk_sys_pg";
//var execPath = "cstm_get_cpl_arctag_pg";

var pgmNmAmtekInfo = "Amtek_info";
var execPath = "/u/tsthsp/bin/cstm_heid_amtk_sys_pg";
var dbUrl = 'GetDataFrmDB.jsp';


$(document).ready(function(){
	
	if(localStorage.getItem("starHdtQdsAuthToken") != null && localStorage.getItem("starHdtQdsAuthToken") != "" )
	{
	
		$("#loginUsrNm").html(localStorage.getItem("starHdtQdsUserNm"));
	
		if( $.isFunction( $.fn.select2 ) ){
			$(".select2").select2();
		}
		
		
		 setMonthYear();
		
		var d = new Date();
		var year = d.getFullYear() ;
		var month = d.getMonth()+1; 
		
		if(String(month).length == 1)
		{
			month = "0" + month;
		}
		
		console.log(month);
		
		console.log(String(month).length);
		console.log(year);
		
		$("#monthSel").val(month). trigger("change");
		$("#yearSel").val(year). trigger("change");
		
		//getInitialOptions();
		
		//getInitialData();
		//getCustomer();
		
		
		$("#initScreen").show();
		$("#configScreen").hide();
		$("#qdsNoClnt").html(localStorage.getItem("starHdtQdsCtlNo"));
		// Get the full URL of the current page
		
         //Edited By Shruti 26-05-23
		var url = window.location.href;

		// Extract the file name from the URL
		var fileNa = url.substring(url.lastIndexOf("/") + 1);

		// Remove any query parameters or fragments from the file name
		var	fileName = fileNa.split("?")[0].split("#")[0];

		// Output the file name
		console.log(fileName);
		if (fileName=="index2.html"){
			//if(localStorage.getItem("starHdtQdsAuthToken") != null && localStorage.getItem("starHdtQdsAuthToken") != "" ){
			$("#configScreenMenu").show();
			   getCustomerIndex(); 
			   getElements()
			 
			   
			//}
			   
		}
		
		if (fileName=="index3.html"){
			//if(localStorage.getItem("starHdtQdsAuthToken") != null && localStorage.getItem("starHdtQdsAuthToken") != "" ){
			$("#config2ScreenMenu").show();
		
		
			   getElements()
			 
			   
			//}
			   
		}
			
	//Till here
		
		
	}
	else
	{
		//window.location.replace("login.html");
	}

    
	
	$("#textRefresh").click(function(){
		
		 getInitialData();
	
	});
	
	$("#linkTags").click(function(){
		
		getTagList();
	
	});
	
	$("#saveLinkTag").click(function(){
		
		saveLinkTags();
	
	});
	
	$('#updMtlStd').click(function() {
        // Get the values of the selected checkboxes
        var selectedChemistry = $('#chem').is(':checked');
        //console.log("selectedChemistry "+selectedChemistry);
        //var selectedTestSpec = $('#tstSpec').is(':checked');
        
        // Print or use the values
        if (selectedChemistry) {
            //console.log("Chemistry is selected");
            
            // Read the value inside the span with id 'qdsNoClnt'
		    //var qdsNoClntValue = $('#qdsNoClnt').text();
		    
		    // Print or use the value as needed
		    //console.log("QDS No Client: " + qdsNoClntValue);
		    
		    updateMetalStds("1","0");
		    
            
        } else {
            console.log("Chemistry is not selected");
            Swal.fire({
				type: 'warning',
				title: 'Oops...',
				text: 'Please select the chemistry validation before proceeding!',
			})    
        }

        /*if (selectedTestSpec) {
            console.log("Test Specifications is selected");
        } else {
            console.log("Test Specifications is not selected");
        }*/

        // You can perform further logic or actions based on the selected values
    });
	
	
	$("#getSoButton").click(function(){
		var soNo = $("#soNo").val();
		if (soNo!=undefined &&soNo!=null && soNo!=""){
			console.log("%%Inside get so  buton click%");
			getSoDtails(soNo);
		}

	});
	
	
	$("#save2LinkTag").click(function(){
		
		saveLinkChk();
	
	});
	
	$("#printQDS").click(function(){
		
		var chemStr = '';
		var chmSelFlg = '';
		
		$("#chemDivTbl input[type='checkbox'][name='select-chem']:checked").each(function(index, row){
			
			chemStr += $(row).closest("tr").attr("data-chem-el") + "%7C"; 
			
		});
		
		chemStr = chemStr.substring(0, chemStr.length - 3);
		
		
		if($("#selectAllChem").prop("checked") == true) {	
			chmSelFlg = 'A';
		}		
		else if($("#selectAllChem").prop("checked") == false && chemStr.length > 0) {
			chmSelFlg = 'P';
		}	
		else {
			chmSelFlg = 'N';
		}

		var heatCheck = $("#heatCheck").prop("checked") ? 1 : 0;
		
		var microCheck = $("#microCheck").prop("checked") ? 1 : 0;
		
		var impackCheck = $("#impactCheck").prop("checked") ? 1 : 0;
		
		var jominy1Check = $("#jominy1Check").prop("checked") ? 1 : 0;
		
		var tstSpcChk = $("#tstSpcChk").prop("checked") ? 1 : 0;
		
		var mtlStdChk = $("#mtlStdChk").prop("checked") ? 1 : 0;
		
		var certChk = $("#certChk").prop("checked") ? 1 : 0;
		
		var jominy2Check = $("#jominy2Check").prop("checked") ? 1 : 0;

		
		
		var urlParams = new URLSearchParams(window.location.search);
		
		var qdsNo = localStorage.getItem("starHdtQdsCtlNo");
		
		var fmtQdsNo = '';
		
		for(var i = 10; i > qdsNo.length; i--)
		{
			fmtQdsNo += "0" ;
		}
		
		fmtQdsNo = fmtQdsNo + qdsNo;
		
		var rootAppName = "/" + window.location.pathname.split("/")[1];
		
		if (!rootAppName.includes("star-jad-qds-cnfg")) {
		    rootAppName = rootAppName + "/star-jad-qds-cnfg";
		}
		
		var docUrl = rootAppName + "/DownloadFile.jsp?cmpyId=" + localStorage.getItem("starHdtQdsCmpy") + "&qdsCtlNo=" + fmtQdsNo;  		
		docUrl += "&selFlg=" + chmSelFlg + "&chemList=" + chemStr + "&jmyFlg=" + jominy1Check + "&impcFlg=" + impackCheck;
		docUrl += "&micnFlg=" + microCheck + "&htrmtFlg="+ heatCheck + "&tstSpcFlg="+ tstSpcChk + "&mtlStdFlg="+ mtlStdChk;
		docUrl += "&etcFlg="+ certChk + "&jmyFlg2="+ jominy2Check;
		
		console.log("docUrl >> " + docUrl);
			
		window.open(docUrl, '_blank');
		
	});
	
	$("#qdsConfig").click(function(){
		
		$("#configScreen").show();	
		$("#initScreen").hide();
		
		$("#tmplNmLstDiv").hide();
		$("#tmplNmDiv").show();
		
		$("#tmpltTypText").html("(New)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		
		getCustomer();
	
	});
	
	$("#tmplType").click(function(){
	
		console.log($(this).prop("checked"));
		
		if($(this).prop("checked") == true)
		{
			$("#tmpltTypText").html("(New)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			$("#tmplNmLstDiv").hide();
			$("#tmplNmDiv").show();
			
		}
		else
		{
			$("#tmpltTypText").html("(Existing)");
			$("#tmplNmLstDiv").show();
			$("#tmplNmDiv").hide();
		}
	
	});
	
	
	$("#tmplNmLst").change(function(){
		
		if( $(this).val() != "" && $(this).val() != null && $(this).val() != undefined && $("#cusId").val() != "" && $("#cusId").val() != null && $("#cusId").val() != undefined){
			getTemplate($(this).val(), $("#cusId").val());
		}
		
		console.log($(this).val());
	
	});
	
	$("#createQDS").click(function(){
		createNewQDS()
	});
	
	$(".click-app-back").click(function(){
	
		$("#configScreen").hide();	
		$("#initScreen").show();
		
		$("#selectAllChem").trigger("click");
				
		 $("#heatCheck").prop( "checked" ,false);
		 $("#microCheck").prop( "checked",false);
		 $("#impactCheck").prop( "checked",false );
		 $("#jominy1Check").prop( "checked",false );
		 $("#jominy2Check").prop( "checked",false );
		
	});
	const boron = document.querySelector('#boron');
	const nonBoron = document.querySelector('#Non-Boron');
	//const submitBtn = document.querySelector('#jominypopup');
	var bflag = 0;
	 
	boron.addEventListener('change', (event) => {
	  if (event.target.checked) {
	    console.log('boron');
	    bflag=1;
	    console.log( bflag);
	  }
	});

	nonBoron.addEventListener('change', (event) => {
		
	  if (event.target.checked) {
		  bflag=0;
	    console.log('non-boron ');
	    console.log(bflag);
	  }
	});
	

	$("#generateJominy").click(function(){
		console.log('InsideSubmit');
		generateJominy(bflag);
	});
	
	
	$(document).on("keyup", ".decimal", function(e) {
        var val = 0;

		if ( $(this).val() == ".") {
			$(this).val("0.");
		}
		
		var afterDecimal = 2, beforeDecimal = 10;
		
		var count = $(this).val().split("-").length - 1;
		
		if(e.key == '-')
		{
			var resultStr = '';
			var firstOccuranceIndex = $(this).val().search(/\-/) + 1; // Index of first occurance of (.)
			
			if($(this).val().charAt(0) == '-')
			{
				resultStr = $(this).val().substr(0, firstOccuranceIndex) + $(this).val().slice(firstOccuranceIndex).replace(/\-/g, ''); // Splitting into two string and replacing all the dots (.'s) in the second string
			}
			else
			{
				resultStr = $(this).val().replace(/\-/g, ''); // Splitting into two string and replacing all the dots (.'s) in the second string
			}
			
			$(this).val(resultStr);
		}
		
		
		$(this).val(
			$(this).val()
          .replace(/[^-{1}\d.]/g, '')           
          .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
          .replace(/(\..*)\./g, '$1')
          .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1')
		)
    });
	
	$("#chemDivTbl").on("change", "#selectAllChem", function(){
		if( $(this).prop("checked") == true ){
			$("#chemDivTbl tbody input[type='checkbox'][name='select-chem']").prop("checked", "checked");
		}else{
			$("#chemDivTbl tbody input[type='checkbox'][name='select-chem']").prop("checked", false);
		}
	});
	
	$("#tagDivTbl").on("change", "#selectAllLinkTag", function(){
		if( $(this).prop("checked") == true ){
			$("#tagDivTbl tbody input[type='checkbox'][name='select-tag']").prop("checked", "checked");
		}else{
			$("#tagDivTbl tbody input[type='checkbox'][name='select-tag']").prop("checked", false);
		}
	});
	
	$("#chemDivTbl").on("change", "input[type='checkbox'][name='select-chem']", function(){
	
		if( $("#chemDivTbl tbody input[type='checkbox'][name='select-chem']").length == $("#chemDivTbl tbody input[type='checkbox'][name='select-chem']:checked").length ){
			$("#selectAllChem").prop("checked", true);
		}else{
			$("#selectAllChem").prop("checked", false);
		}
	});
	
	
	$("#ordNo, #ordItm").focusout(function(){
	
		var ordNo = $("#ordNo").val();
		var ordItm = $("#ordItm").val();
		
	    if( ordNo != "" && ordNo != null && ordNo != undefined && ordItm != "" && ordItm != null && ordItm != undefined){
	    	
	    	getCustomerProduct();
	    }
	});
	


	
	$("#cusId").change(function(){
		//var temp="";
		//getTemplate(temp, $("#cusId").val());
		$("#ordNo").val("");
		$("#ordItm").val("");
	});
	
	
	//25-05-23
	
	

//
	$("#save").click(function(){
		
		var ordNo = $("#ordNo").val();
		var ordItm = $("#ordItm").val();
		var tmplNm = $("#tmplNm").val();
		var chemArr = [];
		
		if( tmplNm == "" || tmplNm == null || tmplNm == undefined )
		{	
			var tmplNmFrmLst = $("#tmplNmLst").val();
			
			var arrTmpl = tmplNmFrmLst.split("_");
		
			tmplNm = arrTmpl[1];
		}
		
		
		$("#searchResults, .action-btn").hide();
		
		$("#chemDivTbl input[type='checkbox'][name='select-chem']:checked").each(function(index, row){
			
			var chemVal = {
					chemEl: $(row).closest("tr").attr("data-chem-el"),
				};
								
				chemArr.push(chemVal);
			
		});
		
		if( ordNo == "" || ordNo == null || ordNo == undefined )
		{
			saveDataCustomer(chemArr);
		}
		else
		{
			saveData(chemArr, tmplNm);	
		}
		
		console.log(chemArr);

	});

	
	
	$("#logout").click(function(){
		
		localStorage.removeItem("starHdtQdsAuthToken");
	    localStorage.removeItem("starHdtQdsUserId");
	    localStorage.removeItem("starHdtQdsUserNm");
	    localStorage.removeItem("starHdtQdsCmpy");
	    localStorage.removeItem("starHdtQdsAppHost");
	    localStorage.removeItem("starHdtQdsAppPort");
	    localStorage.removeItem("starHdtQdsCtlNo");	
	    localStorage.removeItem("starHdtQdsBaseLng");  
		localStorage.removeItem("starHdtQdsBaseCtry");  
		localStorage.removeItem("starHdtQdsNginxURL");
		window.location.replace("login.html");
	
	});
	
	
	
	
	if($("#chemDivTbl table tbody input[type='checkbox'][name='chem-check']:checked").length>=20){
		Swal.fire({
			type: 'warning',
			title: 'Oops...',
			text: 'Cannot select more than 20 Elements',
		})
}
	
	
	
});
	





function updateMetalStds(chmFlg,mtlSpecFlg){
	$("#mainLoader").show();
	
	$.ajax({
		data: {
			execFlg: "AM", // Add Metal standard
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			tagList: "",
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			param: chmFlg + "/" + mtlSpecFlg,
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				var arrResponse = JSON.parse(response.trim());
				console.log("arrResponse.output >> "+arrResponse.output);
				
				console.log("arrResponse.updateStatus >> "+arrResponse.updateStatus);
				console.log("arrResponse.msg >> "+arrResponse.msg);
				
				if (arrResponse.updateStatus === 0 && arrResponse.msg.length === 0) {
				    Swal.fire({
				        type: 'success',
				        title: 'Wow...',
				        html: 'Metal standards updated successfully.'
				    });
				} 
				
				else if (arrResponse.updateStatus === 2 && arrResponse.msg.length > 0) {
				    Swal.fire({
				        type: 'info',
				        html: 'The QDS chemistry provided does not match any entries in the metal standards chemistry.'
				    });
				}
				
				else {
				    Swal.fire({
				        type: 'warning',
				        title: 'Oops...',
				        html: 'An error occurred:<br>' + arrResponse.msg
				    });
				}

			}	
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}









/**
 *	This function is validating for valid JSON String
 */
function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

/**
 *	This function is used to allow only numbers in textbox
 */
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateDate(date) {

	var dateMMDDYYYRegex = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$";
    if(date.match(dateMMDDYYYRegex) && isValidDate(date))	
	{
		return true;
	}
	else
	{
		return false;
	}
    
    /*var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/ ;
    return dateRegex.test(date);*/
}

function validateTime(time) {
    var timeRegex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/ ;
    return timeRegex.test(time);
}

 function isValidDate(s) {
       var bits = s.split('/');
       var d = new Date(bits[2], bits[0] - 1, bits[1]);
       return d && (d.getMonth() + 1) == bits[0] && d.getDate() == Number(bits[1]);
    }

function setMonthYear(){
	
	var option = '';
	
	option += '<option value="01"> Jan </option>';
	option += '<option value="02"> Feb </option>';
	option += '<option value="03"> Mar </option>';
	option += '<option value="04"> Apr </option>';
	option += '<option value="05"> May </option>';
	option += '<option value="06"> Jun </option>';
	option += '<option value="07"> Jul </option>';
	option += '<option value="08"> Aug </option>';
	option += '<option value="09"> Sep </option>';
	option += '<option value="10"> Oct </option>';
	option += '<option value="11"> Nov </option>';
	option += '<option value="12"> Dec </option>';
	
	$("#monthSel").html(option);
	
	option = '';
	
	for(var i =0; i <=10 ; i++)
	{	
		var k = 2020;
		
		k = k + i;
	
		option += '<option value='+ k +'>'+ k +'</option>';
	}
	
	$("#yearSel").html(option);

}



function getInitialData(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "A",
			param: "CHM",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var chmList = arrResponse.output.fldTbl;
				var loopLimit = chmList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr data-chem-el = '"+ chmList[i].chmel +"' data-chem-el-val = '"+ chmList[i].fmtRslt +"'><td><input type='checkbox' class='chem-check' name='select-chem'/></td><td>"+ chmList[i].chmel +"</td><td>" + chmList[i].fmtRslt +"</td></tr>";
					
				}
				
				$("#chemDivTblBody").html(tblRows);
				
				$("#selectAllChem").trigger("click");
				
				 $("#heatCheck").prop( "checked" ,true);
				 $("#microCheck").prop( "checked",true );
				 $("#impactCheck").prop( "checked",true );
				 $("#jominy1Check").prop( "checked",true );
				 $("#jominy2Check").prop( "checked",true );
				 $("#mtlStdChk").prop( "checked",true );
				 $("#tstSpcChk").prop( "checked",true );
				 $("#certChk").prop( "checked",true );
				 
				
				getOIR();	
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	/* GET HEAT TREATMENT */
	$.ajax({
		data: {
			execFlg: "A",
			param: "HT",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var htTrmntList = arrResponse.output.fldTbl;
				var loopLimit = htTrmntList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr><td>"+ htTrmntList[i].htrmt +"</td><td>" + htTrmntList[i].htrmtTmpStr +"</td><td>" + htTrmntList[i].compTmStr +"</td><td>" + htTrmntList[i].cmthd +"</td><td>" + htTrmntList[i].cooltTmpStr +"</td></tr>";
					
				}
				
				$("#heatTrtmtDivTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	
	/* GET IMPACT TEST */
	$.ajax({
		data: {
			execFlg: "A",
			param: "IMP",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var chmList = arrResponse.output.fldTbl;
				var loopLimit = chmList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr><td>"+ chmList[i].impcTstTyp +"</td><td>" + chmList[i].tstDirn +"</td><td>" + chmList[i].tstUm +"</td><td>" + chmList[i].tmpMsrStr +"</td><td>" + chmList[i].impcRslt1Str +"</td><td>" + chmList[i].impcRslt2Str +"</td><td>" + chmList[i].impcRslt3Str +"</td></tr>";
					
				}
				
				$("#impactDivTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	/* GET JOMINY */
	$.ajax({
		data: {
			execFlg: "A",
			param: "JMY",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var jmyList = arrResponse.output.fldTbl;
				var loopLimit = jmyList.length;
				
				tblRows += "<thead><tr>";
				
				tblRows += "<th>" + arrResponse.output.jmyTstTyp + "</th>";
				
				for( var i=0; i<loopLimit; i++ ){
				
					tblRows += "<th>" + jmyList[i].rdgposStr + "</th>";
				}
				
				tblRows += "</tr></thead> <tbody id='jominyTblBody1'><tr><td></td>";
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<td>"+ jmyList[i].fmtRslt +"</td>";
				}
				
				tblRows += "</tr></tbody>";
				
				$("#jominyTbl1").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	/* GET MICROINCLUSION */
	$.ajax({
		data: {
			execFlg: "A",
			param: "MIC",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var microList = arrResponse.output.fldTbl;
				var loopLimit = microList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr><td class='text-center'>"+ microList[i].tnrsAStr +"</td><td class='text-center'>" + microList[i].tkrsAStr +"</td><td class='text-center'>" + microList[i].tnrsBStr +"</td><td class='text-center'>" + microList[i].tkrsBStr +"</td><td class='text-center'>" + microList[i].tnrsCStr +"</td><td class='text-center'>" + microList[i].tkrsCStr +"</td><td class='text-center'>" + microList[i].tnrsDStr +"</td><td class='text-center'>" + microList[i].tkrsDStr +"</td></tr>";
					
				}
				
				$("#microTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	
	/* GET JOMINY 2 */
	$.ajax({
		data: {
			execFlg: "A",
			param: "JMY2",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var jmyList = arrResponse.output.fldTbl;
				var loopLimit = jmyList.length;
				
				tblRows += "<thead><tr>";
				
				tblRows += "<th>" + arrResponse.output.jmyTstTyp + "</th>";
				
				for( var i=0; i<loopLimit; i++ ){
				
					tblRows += "<th>" + jmyList[i].rdgposStr + "</th>";
				}
				
				tblRows += "</tr></thead> <tbody id='jominyTblBody1'><tr><td></td>";
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<td>"+ jmyList[i].fmtRslt +"</td>";
				}
				
				tblRows += "</tr></tbody>";
				
				$("#jominyTbl2").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	/* GET TEST SPECIFICATIONS */
	$.ajax({
		data: {
			execFlg: "A",
			param: "TST",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var tstList = arrResponse.output.fldTbl;
				var loopLimit = tstList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr><td>"+ tstList[i].tstAbbrStr +"</td><td>" + tstList[i].rsltStr +"</td><td>" + tstList[i].tstDirn +"</td></tr>";
					
				}
				
				$("#testTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	/* GET METAL STANDARDS */
	$.ajax({
		data: {
			execFlg: "A",
			param: "MTL",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var metalList = arrResponse.output.fldTbl;
				var loopLimit = metalList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					//tblRows +=	"<tr><td >"+ metalList[i].rsltStr +"</td><td>" + metalList[i].mtchgChmStr +"</td><td >" + metalList[i].mtchgTstStr +"</td><td >" + metalList[i].mtchgJmyStr +"</td><td >" + metalList[i].certByStr +"</td></tr>";
					
					tblRows +=	"<tr><td >"+ metalList[i].rsltStr +"</td><td>" + metalList[i].mtchgChm +"</td><td >" + metalList[i].mtchgTst +"</td><td >" + metalList[i].mtchgJmy +"</td><td >" + metalList[i].certBySctr +"</td></tr>";
					
					
				}
				
				$("#mtlStndrdTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	
	/* GET EXTERNAL CERTIFICATES */
	$.ajax({
		data: {
			execFlg: "A",
			param: "CERT",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var certList = arrResponse.output.fldTbl;
				var loopLimit = certList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr><td >"+ certList[i].certTyp +"</td><td >" + certList[i].sdo +"</td><td >" + certList[i].imgNoStr +"</td><td >" + certList[i].stsStr +"</td></tr>";
					
				}
				
				$("#certTblBody").html(tblRows);
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
}
// By Shruti 26-05-23 //From here
function getCustomerIndex(){
	$("#configScreenMenu").show();
	//$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "B",
			param: "",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var cusList = arrResponse.cusFldTbl;
				
				var optionData = "<option value=''>Select</option>";
				for( var i=0; i<cusList.length; i++ ){
					optionData += "<option value='"+ cusList[i].cusId2 +"'>("+ cusList[i].cusId2 +") " + cusList[i].cusNm + "</option>";
				}
				$("#cusId1").html(optionData);
				
            }

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
	
	$("#cusId1").on('change', function() {
		getCustomerChk($("#cusId1").val());
  	});

}


function getCustomerChk(cusId1) {
    console.log(cusId1);
    var htrmtFlg ="" ;
    var impcFlg ="";
    var jmyFlg = "";
    var jmyFlg2 = "";
    var mincnFlg = "";
    var exCertFlg = "";
    var tstSpecFlg ="";
    var mtlStdFlg = "";
    
    
    $.ajax({
        data: {
            execFlg: "S",
            cmpyId: localStorage.getItem("starHdtQdsCmpy"),
            qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
            usrId: localStorage.getItem("starHdtQdsUserId"),
            host: localStorage.getItem("starHdtQdsAppHost"),
            port: localStorage.getItem("starHdtQdsAppPort"),
            token: localStorage.getItem("starHdtQdsAuthToken"),
            lng: localStorage.getItem("starHdtQdsBaseLng"),
            ctry: localStorage.getItem("starHdtQdsBaseCtry"),
            cusId: cusId1,
            nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
        },
        url: dbUrl,
        type: 'POST',
        dataType: 'text',
        success: function (response) {
            if (IsJsonString(response)) {
                var arrResponse = JSON.parse(response.trim());

                if (arrResponse.output.impcFlg == 1) {
                    $("#impactCheck").prop("checked", true);
                } else {
                    $("#impactCheck").prop("checked", false);
                }

                if (arrResponse.output.htrmtFlg == 1) {
                    $("#heatCheck").prop("checked", true);
                } else {
                    $("#heatCheck").prop("checked", false);
                }

                if (arrResponse.output.jmyFlg == 1) {
                    $("#jominy1Check").prop("checked", true);
                } else {
                    $("#jominy1Check").prop("checked", false);
                }
                if (arrResponse.output.mincnFlg == 1) {
                    $("#microCheck").prop("checked", true);
                } else {
                    $("#microCheck").prop("checked", false);
                }
                if (arrResponse.output.exCertFlg == 1) {
                    $("#extCheck").prop("checked", true);
                }
                else 
                {
                    $("#extCheck").prop("checked", false);
                }
                if (arrResponse.output.tstSpecFlg == 1) {
                    $("#tstCheck").prop("checked", true);
                } else {
                    $("#tstCheck").prop("checked", false);
                }
                if (arrResponse.output.jmyFlg2 == 1) {
                    $("#jmy2Check").prop("checked", true);
                } else {
                    $("#jmy2Check").prop("checked", false);
                }
                if (arrResponse.output.mtlStdFlg == 1) {
                    $("#mtlCheck").prop("checked", true);
                } else {
                    $("#mtlCheck").prop("checked", false);
                }

                $("#myButton").click(function () {
                    console.log("Button clicked!");

                    if (cusId1 != "" && cusId1 != null && cusId1 != undefined) {
                         htrmtFlg = ($("#heatCheck").prop("checked")) ? 1 : 0;
                         impcFlg = ($("#impactCheck").prop("checked")) ? 1 : 0;
                         jmyFlg = ($("#jominy1Check").prop("checked")) ? 1 : 0;
                         jmyFlg2 = ($("#jmy2Check").prop("checked")) ? 1 : 0;
                         mincnFlg = ($("#microCheck").prop("checked")) ? 1 : 0;
                        exCertFlg = ($("#extCheck").prop("checked")) ? 1 : 0;
                        tstSpecFlg = ($("#tstCheck").prop("checked")) ? 1 : 0;
                        mtlStdFlg = ($("#mtlCheck").prop("checked")) ? 1 : 0;

                        handleSelectedElements(function (selectedElements) {
                            var chmSelFlg = (selectedElements.length > 0) ? 1 : 0;
                          
                            console.log("values", cusId1, htrmtFlg, impcFlg, jmyFlg, jmyFlg2, mincnFlg, exCertFlg, tstSpecFlg, mtlStdFlg,selectedElements);
                        });
                    }
                    saveCheckList(cusId1, chmSelFlg, htrmtFlg, impcFlg, jmyFlg, jmyFlg2, mincnFlg, exCertFlg, tstSpecFlg, mtlStdFlg, selectedElements);

                });

            }

        },
        error: function (data) {
            $("#mainLoader").hide();
            console.log(data);
        }
    });
}

function getElements() {
	
    $.ajax({
        data: {
            execFlg: "EL",
            cmpyId: localStorage.getItem("starHdtQdsCmpy"),
            qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
            usrId: localStorage.getItem("starHdtQdsUserId"),
            host: localStorage.getItem("starHdtQdsAppHost"),
            port: localStorage.getItem("starHdtQdsAppPort"),
            token: localStorage.getItem("starHdtQdsAuthToken"),
            lng: localStorage.getItem("starHdtQdsBaseLng"),
            ctry: localStorage.getItem("starHdtQdsBaseCtry"),
            nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
        },
        url: dbUrl,
        type: 'POST',
        dataType: 'text',
        success: function (response) {
            if (IsJsonString(response)) {
                var tblRows = '';
                var arrResponse = JSON.parse(response.trim());
                var elmntList = arrResponse.chmList;
                var loopLimit = elmntList.length;
                var selectedElements = [];
                for (var i = 0; i < loopLimit; i++) {
                    tblRows += "<tr data-chem-el='" + elmntList[i].Element.trim() + "'><td><input type='checkbox' class='chem-check' name='select-chem'/></td><td>" + elmntList[i].Element + "</td></tr>";
                    selectedElements.push(elmntList[i].Element);
                   // console.log("Element:",selectedElements);
                }
                $("#chemDivTblBody").html(tblRows);
                handleSelectedElements();
                
              
            }
        },
        error: function (data) {
            $("#mainLoader").hide();
            console.log(data);
        }
    });
}

function handleSelectedElements(callback) {
    var selectedElements = [];

    // Loop through the checkboxes and check if they are selected
    $(".chem-check").each(function () {
        if ($(this).prop("checked")) {
            var element = $(this).closest("tr").data("chem-el");
            selectedElements.push(element);
        }
    });

    // Call the callback function with the selectedElements argument if provided
    if (callback) {
        callback(selectedElements);
    } else {
        // Perform actions with the selected elements directly if no callback is provided
        saveSelectedElements(selectedElements);
    }
}

function saveSelectedElements(selectedElements) {
    // Process the selected elements or perform actions with them
  
    $("#chemDivTbl table tbody input[type='checkbox'][name='chem-check']:checked").each(function(index, checkbox) {
    	  console.log("Selected elements:", selectedElements);
    	  console.log($(checkbox).closest("tr").attr("data-chem-el"))  ;
	});
}

function getCustomer(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "B",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var cusList = arrResponse.cusFldTbl;
				var loopLimit = cusList.length;
				var optionData = "<option value=''>Select</option>";
				
				for( var i=0; i<loopLimit; i++ ){
					optionData += "<option value='"+ cusList[i].cusId2 +"'>("+ cusList[i].cusId2 +") " + cusList[i].cusNm + "</option>";
				}
				$("#cusId").html(optionData);
				
                }
			
			getQDSInformation();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function getTagList(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "RT",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
				var tblRows = '';
				var arrResponse = JSON.parse(response.trim());
				var tagList = arrResponse.output.FldTbl;
				var loopLimit = tagList.length;
				
				for( var i=0; i<loopLimit; i++ ){
					
					tblRows +=	"<tr data-itm-ctl-no = '"+ tagList[i].itmCtlNo +"'><td><input type='checkbox' class='tag-check' name='select-tag'/></td><td>"+ tagList[i].tagNo +"</td></tr>";
				}
				
				$("#tagDivTblBody").html(tblRows);
			}
			
			$("#editLinkTagModal").modal("show");
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function getQDSInformation(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "A",
			param: "QDS",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				var arrResponse = JSON.parse(response.trim());
				console.log("arrResponse >> "+arrResponse.output);
				
				if (arrResponse && arrResponse.output && arrResponse.output.apvdFlg === 0 && arrResponse.output.qdsTyp === 'H') {
				    $("#createQDS").show();
				    
				    $("#heatNo").html(arrResponse.output.millHeatStr);
					$("#qdsType").html("(" + arrResponse.output.qdsTypDesc + ")");
					$("#qdsStatus").html(arrResponse.output.apvdFlgStr);
				    
				} else {
				    $("#createQDS").hide();
				}
	
				
				
				
				//arrResponse.output.qdsTypDesc
				//arrResponse.output.apvdFlg
			}
			
			getInitialData();
			

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}
//Edited on 12-06-23

function getSoDtails(soNo) {
    console.log(soNo);
   var soNoParts = soNo.split('-');

    var ordPfx = soNoParts[0];
    var ordNo = soNoParts[1];
    var ordItm = soNoParts[2];
    
    
    var htrmtFlg = "";
    var impcFlg ="";
    var jmyFlg = "";
    var jmyFlg2 = "";
    var mincnFlg ="";
    var exCertFlg ="";
    var tstSpecFlg ="";
    var mtlStdFlg = "";

    console.log("ordPfx: ", ordPfx);
    console.log("ordNo: ", ordNo);
    console.log("ordItm: ", ordItm);

   

        $.ajax({
            data: {
                execFlg: "W",
                cmpyId: localStorage.getItem("starHdtQdsCmpy"),
                qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
                usrId: localStorage.getItem("starHdtQdsUserId"),
                host: localStorage.getItem("starHdtQdsAppHost"),
                port: localStorage.getItem("starHdtQdsAppPort"),
                token: localStorage.getItem("starHdtQdsAuthToken"),
                lng: localStorage.getItem("starHdtQdsBaseLng"),
                ctry: localStorage.getItem("starHdtQdsBaseCtry"),
                ordPfx: ordPfx,
                ordNo:ordNo,
                ordItm:ordItm,
                nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
            },
            url: dbUrl,
            type: 'POST',
            dataType: 'text',
            success: function (response) {
                if (IsJsonString(response)) {
                    var arrResponse = JSON.parse(response.trim());
                    console.log("response"+arrResponse);

                    if (arrResponse.output.impcFlg == 1) {
                        $("#impactCheck").prop("checked", true);
                    } else {
                        $("#impactCheck").prop("checked", false);
                    }

                    if (arrResponse.output.htrmtFlg == 1) {
                        $("#heatCheck").prop("checked", true);
                    } else {
                        $("#heatCheck").prop("checked", false);
                    }

                    if (arrResponse.output.jmyFlg == 1) {
                        $("#jominy1Check").prop("checked", true);
                    } else {
                        $("#jominy1Check").prop("checked", false);
                    }
                    if (arrResponse.output.mincnFlg == 1) {
                        $("#microCheck").prop("checked", true);
                    } else {
                        $("#microCheck").prop("checked", false);
                    }
                    if (arrResponse.output.exCertFlg == 1) {
                        $("#extCheck").prop("checked", true);
                    } else {
                        $("#extCheck").prop("checked", false);
                    }
                    if (arrResponse.output.tstSpecFlg == 1) {
                        $("#tstCheck").prop("checked", true);
                    } else {
                        $("#tstCheck").prop("checked", false);
                    }
                    if (arrResponse.output.jmyFlg2 == 1) {
                        $("#jmy2Check").prop("checked", true);
                    } else {
                        $("#jmy2Check").prop("checked", false);
                    }
                    if (arrResponse.output.mtlStdFlg == 1) {
                        $("#mtlCheck").prop("checked", true);
                    } else {
                        $("#mtlCheck").prop("checked", false);
                    }
                    if (arrResponse.output.cusId !="") {
                    	$("#cusIdLabel").text(arrResponse.output.cusId);
                     
                    } else {
                    	$("#cusIdLabel").text("");
                    }

                    $("#mySaveButton").click(function () {
                        console.log("Button save clicked!");

                        if (soNo != "" && soNo != null && soNo != undefined) {
                            htrmtFlg = ($("#heatCheck").prop("checked")) ? 1 : 0;
                            impcFlg = ($("#impactCheck").prop("checked")) ? 1 : 0;
                            jmyFlg = ($("#jominy1Check").prop("checked")) ? 1 : 0;
                            jmyFlg2 = ($("#jmy2Check").prop("checked")) ? 1 : 0;
                            mincnFlg = ($("#microCheck").prop("checked")) ? 1 : 0;
                            exCertFlg = ($("#extCheck").prop("checked")) ? 1 : 0;
                            tstSpecFlg = ($("#tstCheck").prop("checked")) ? 1 : 0;
                            mtlStdFlg = ($("#mtlCheck").prop("checked")) ? 1 : 0;

                            handleSelectedElements(function (selectedElements) {
                                var chmSelFlg = (selectedElements.length > 0) ? 1 : 0;
                               
                                console.log("values", ordPfx,ordNo,ordItm, htrmtFlg, impcFlg, jmyFlg, jmyFlg2, mincnFlg, exCertFlg, tstSpecFlg, mtlStdFlg);
                            });
                           
                        }
                        saveSoCheckList(ordPfx,ordNo,ordItm, chmSelFlg, htrmtFlg, impcFlg, jmyFlg, jmyFlg2, mincnFlg, exCertFlg, tstSpecFlg, mtlStdFlg, selectedElements);

                    });

                }

            },
            error: function (data) {
                $("#mainLoader").hide();
                console.log(data);
            }
        });
    }

//Edited on 12-06-23
function saveSoCheckList(ordPfx,ordNo,ordItm,chmSelFlg,htrmtFlg,impcFlg,jmyFlg,jmyFlg2,mincnFlg,exCertFlg,tstSpecFlg,mtlStdFlg,selectedElements){

	
	  console.log("Inside savechecklist")  ;
     var tempselectedElements=[];
      	  $("#chemDiv table tbody input[type='checkbox'][name='select-chem']:checked").each(function(index, checkbox) {
          	  //console.log("Selected elements:", selectedElements);
          	  console.log("Inside savechecklist",$(checkbox).closest("tr").attr("data-chem-el"))  ;
          	  tempselectedElements.push($(checkbox).closest("tr").attr("data-chem-el"));
      	});
       let originalChemElem=[];
    for(let i=0;i<20;i++){
  	  if(tempselectedElements[i]==undefined || tempselectedElements[i]==null || tempselectedElements[i]==""){
  		  originalChemElem.push("");
  	  }
  	  else {
  		  originalChemElem.push(tempselectedElements[i]);
  	  }
    }
	
	$.ajax({
		data: {
			 execFlg: "WA",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			ordPfx : ordPfx,
			ordNo:ordNo,
			ordItm:ordItm,
			chmSelFlg,chmSelFlg,
			htrmtFlg: htrmtFlg,
		    impcFlg: impcFlg,
		    jmyFlg:jmyFlg,
		    jmyFlg2:jmyFlg2,
		    mincnFlg:mincnFlg,
		    exCertFlg:exCertFlg,
		    tstSpecFlg:tstSpecFlg,
		    mtlStdFlg,mtlStdFlg,
		    chemArr:JSON.stringify(originalChemElem),
		    
		    
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
		
			if (IsJsonString(response)) {
					
					var arrResponse = JSON.parse(response.trim());
					
					if(arrResponse.output.rtnSts == 0)
					{
						Swal.fire({
							type: 'success',
							title: 'Success',
							text: 'Record saved Successfully',
						})
					}
					else if(arrResponse.output.rtnSts != 0)
					{
						Swal.fire({
							type: 'warning',
							title: 'Oops...',
							text: 'Error while Saving Record, ',
						})
					}
									
		    	$("#soNo").val("");
				
				$("#heatCheck").prop( "checked", false );
				
				$("#impactCheck").prop( "checked", false );
				
				$("#jominy1Check").prop( "checked", false );
				
				$("#microCheck").prop( "checked", false );
				
				$("#extCheck").prop( "checked", false );
				$("#tstCheck").prop( "checked", false );
				$("#jmy2Check").prop( "checked", false );
				$("#mtlCheck").prop( "checked", false );
				 $("#chemDiv table tbody input[type='checkbox'][name='select-chem']:checked").each(function (index, checkbox) {
		                $(checkbox).prop('checked', false);
		            });
				
			}
			
			
			//$("#mainLoader").hide();F
       
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}



function getTemplate(tmplNm, cusId){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "T",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			cusId: cusId,
			tmplNm: tmplNm,
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				var arrResponse = JSON.parse(response.trim());
				
				if(arrResponse.output.htrmtFlg == 1)
				{
					$("#heatCheck").prop( "checked", true );
				}
				else
				{
					$("#heatCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.impcFlg == 1)
				{
					$("#impactCheck").prop( "checked", true );
				}
				else
				{
					$("#impactCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.jmyFlg == 1)
				{
					$("#jominy1Check").prop( "checked", true );
				}
				else
				{
					$("#jominy1Check").prop( "checked", false );
				}
				if(arrResponse.output.jmyFlg2 == 1)
				{
					$("#jominy2Check").prop( "checked", true );
				}
				else
				{
					$("#jominy2Check").prop( "checked", false );
				}
				if(arrResponse.output.mincnFlg == 1)
				{
					$("#microCheck").prop( "checked", true );
				}
				else
				{
					$("#microCheck").prop( "checked", false );
				}
				if(arrResponse.output.exCertFlg == 1)
				{
					$("#certChk").prop( "checked", true );
				}
				else
				{
					$("#certChk").prop( "checked", false );
				}
				if(arrResponse.output.tstSpecFlg == 1)
				{
					$("#tstSpcChk").prop( "checked", true );
				}
				else
				{
					$("#tstSpcChk").prop( "checked", false );
				}
				if(arrResponse.output.mtlStdFlg == 1)
				{
					$("#mtlStdChk").prop( "checked", true );
				}
				else
				{
					$("#mtlStdChk").prop( "checked", false );
				}
				
				//$("#selectAllChem").trigger("click");
				
				var chemValue = [arrResponse.output.chmel1, arrResponse.output.chmel2, arrResponse.output.chmel3, arrResponse.output.chmel4, arrResponse.output.chmel5, arrResponse.output.chmel6, arrResponse.output.chmel7, arrResponse.output.chmel8, arrResponse.output.chmel9, arrResponse.output.chmel10, arrResponse.output.chmel11, arrResponse.output.chmel12, arrResponse.output.chmel13, arrResponse.output.chmel14, arrResponse.output.chmel15, arrResponse.output.chmel16, arrResponse.output.chmel17, arrResponse.output.chmel18, arrResponse.output.chmel19, arrResponse.output.chmel20];
				
				$("#selectAllChem").prop("checked", false);
				
				$("#chemDivTbl tbody input[type='checkbox'][name='select-chem']").prop("checked", false);
				
				$("#chemDivTbl input[type='checkbox'][name='select-chem']").each(function(index, row){
		
					for(var j = 0; j < chemValue.length; j++)
					{
						if($(row).closest("tr").attr("data-chem-el")  == chemValue[j])
						{
							$(row).closest("tr").find("td input[type='checkbox'][name='select-chem']").prop("checked", true);
						}
					}  
		
				});
				
				
				if(arrResponse.output.ordNo == 0)
				{
					/*$("#ordNo").val("");
					$("#ordItm").val("");*/
					
				}
				
				//$("#prdNm").html(arrResponse.output.prdStrng1);
				
			
			}
			
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function createNewQDS(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "Q",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
				
				localStorage.setItem("starHdtVenAuthToken", arrResponse.authToken); 
				
				
				
			}
			
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}


function deleteQDS(qdsNo){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "DT",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: JSON.stringify(qdsNo),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
				
				localStorage.setItem("starHdtVenAuthToken", arrResponse.authToken); 
				
				
				
			}
			
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}
//Edit-25-05-23



function saveLinkTags(){
	
	var linkTag = [];
		
	$("#tagDivTbl input[type='checkbox'][name='select-tag']:checked").each(function(index, row){
			
			var ctlNo = {
					itmCtlNo: parseInt($(row).closest("tr").attr("data-itm-ctl-no")),
				};
								
				linkTag.push(ctlNo);
			
		});
	
	
	$("#mainLoader").show();
	
	
	$.ajax({
		data: {
			execFlg: "AT",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			tagList: JSON.stringify(linkTag),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
				
				if(arrResponse.output.rtnSts == 0)
				{
					Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Record saved Successfully',
					})
					
					var qdsCtlNoArr = [];
					
					for(var i=0; i < arrResponse.output.FldTbl.length; i++)
					{
						
					var qdsVal = {
						qdsCtlNo: arrResponse.output.FldTbl[i].qdsCtlNo,
					};
					
						qdsCtlNoArr.push(qdsVal);
					}
					
					deleteQDS(qdsCtlNoArr);
					
					$("#editLinkTagModal").modal("hide");
				}
				else if(arrResponse.output.rtnSts != 0)
				{
					Swal.fire({
						type: 'warning',
						title: 'Oops...',
						html: 'Error while updating Record, please contact Admin' + '<br>' + arrResponse.output.msg
					})
				}
			}
			
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function generateJominy(flag){
	console.log('Inside Generate jominy');
	console.log(flag);
	$("#mainLoader").show();
	$.ajax({
		data: {
			
			execFlg: "J",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo:localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token:localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry:localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			boronFlg:flag,
			
			
			//Edit 08-05-23
		
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
								
				if(arrResponse.output.rtnSts == 0)
				{
					Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Record updated Successfully',
						
					})
					getInitialData();
				}
				else if(arrResponse.output.rtnSts != 0)
				{
					Swal.fire({
						type: 'warning',
						title: 'Oops...',
						text: 'Error while updating Record, please contact Admin',
					})
				}
				
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function saveData(chemArr, tmplNm){

	var chmSelFlg = '';
	
	console.log(chemArr.length);
	
	if($("#selectAllChem").prop("checked") == true)
	{
		chmSelFlg = 'A';
	}
	else if($("#selectAllChem").prop("checked") == false && chemArr.length > 0)
	{
		chmSelFlg = 'P';
	}
	else
	{
		chmSelFlg = 'N';
	}
	
	
	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "1",
			chemArr:JSON.stringify(chemArr),
			chmSelFlg : chmSelFlg,
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			htrmtFlg: $("#heatCheck").prop("checked"),
			mincnFlg: $("#microCheck").prop("checked"),
			impcFlg: $("#impactCheck").prop("checked"),
			jmyFlg: $("#jominy1Check").prop("checked"),
			tstSpcFlg: $("#tstSpcChk").prop("checked"),
			mtlStdFlg: $("#mtlStdChk").prop("checked"),
			etcFlg: $("#certChk").prop("checked"),
			jmyFlg2: $("#jominy2Check").prop("checked"),	
			tmplNm: $("#tmplNm").val(),
			ordPfx : $("#ordPfx").val(),
			ordNo: $("#ordNo").val(),
			ordItm: $("#ordItm").val(),
			cusId:  $("#cusId").val(),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
			
			var arrResponse = JSON.parse(response.trim());
			
			if(arrResponse.output.rtnSts == 0)
				{
					
					if(tmplNm == "" || tmplNm == null || tmplNm == undefined )
					{
						Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Record saved Successfully',
						})
						
						$("#mainLoader").hide();
					}
					else
					{
						saveDataCustomer(chemArr);
					}
				
					/*Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Record saved Successfully',
					})*/
				}
				else if(arrResponse.output.rtnSts != 0)
				{
					Swal.fire({
						type: 'warning',
						title: 'Oops...',
						text: 'Error while updating Record, please contact Admin',
					})
					
					$("#mainLoader").hide();
				}
								
			}
			
			
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}


function saveDataCustomer(chemArr){
	
	$("#tmplNmLst").next().find(".select2-selection").removeClass("border-danger");
	$("#cusId").next().find(".select2-selection").removeClass("border-danger");
	$("#tmplNm").removeClass("border-danger");
	
	var heatCheck = $("#heatCheck").prop( "checked" );
	var microCheck = $("#microCheck").prop( "checked" );
	var impackCheck = $("#impactCheck").prop( "checked" );
	var jominy1Check = $("#jominy1Check").prop( "checked" );
	var jominy2Check = $("#jominy2Check").prop( "checked" );
	
	var cusId = $("#cusId").val();
	var tmplNm = $("#tmplNm").val();
	var ordPfx = $("#ordPfx").val();
	var ordNo = $("#ordNo").val();
	var ordItm = $("#ordItm").val();
	var chmSelFlg = '';
	var errFlg = false;
	
	if( tmplNm == "" || tmplNm == null || tmplNm == undefined )
	{
		var tmplNmFrmLst = $("#tmplNmLst").val();
			
		var arrTmpl = tmplNmFrmLst.split("_");
		
		tmplNm = arrTmpl[1];
	}
	
	console.log(chemArr.length);
	
	if($("#selectAllChem").prop("checked") == true)
	{
		chmSelFlg = 'A';
	}
	else if($("#selectAllChem").prop("checked") == false && chemArr.length > 0)
	{
		chmSelFlg = 'P';
	}
	else
	{
		chmSelFlg = 'N';
	}
	
	if( tmplNm == "" || tmplNm == null || tmplNm == undefined )
	{
		$("#tmplNmLst").next().find(".select2-selection").addClass("border-danger");
		$("#tmplNm").addClass("border-danger");
		errFlg = true;
	}
	
	if( cusId == "" || cusId == null || cusId == undefined )
	{
		$("#cusId").next().find(".select2-selection").addClass("border-danger");
		errFlg = true;
	}
	
	if(errFlg == true)
	{
		return;
	}
	
	
	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "2",
			chemArr:JSON.stringify(chemArr),
			chmSelFlg : chmSelFlg,
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			htrmtFlg: heatCheck,
			mincnFlg: microCheck,
			impcFlg: impackCheck,
			jmyFlg: jominy1Check,
			tmplNm: tmplNm,
			cusId: cusId,
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
				
				if(arrResponse.output.rtnSts == 0)
				{
					Swal.fire({
						type: 'success',
						title: 'Success',
						text: 'Record saved Successfully',
					})
				}
				else if(arrResponse.output.rtnSts != 0)
				{
					Swal.fire({
						type: 'warning',
						title: 'Oops...',
						text: 'Error while updating Record, please contact Admin',
					})
				}
								
			}
			
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

/* GET OIR INFORMATION FROM QDS */
function getOIR(){

	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "R",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
		
			if (IsJsonString(response)) {
				
				var optionData = '';
				
				var arrResponse = JSON.parse(response.trim());
								
				$("#cusId").val(arrResponse.output.cusId).trigger("change");
				
				$("#prdNm").html(arrResponse.output.prdStrng1);
				
				var fldTblList = arrResponse.output.fldTbl;
				var loopLimit = fldTblList.length;
				
				optionData = "<option value=''>Select</option>";
				
				for( var i=0; i<loopLimit; i++ ){
					optionData += "<option value='"+ fldTblList[i].cusTmplNm +"'>" + fldTblList[i].cusTmplNm + "</option>";
				}
				$("#tmplNmLst").html(optionData);
				
				if(loopLimit > 0)
				{			
					$("#tmplType").prop("checked", true); 
					$("#tmplType").trigger("click");
				}
				else
					
				{
					$("#tmplType").prop("checked", false);
					$("#tmplType").trigger("click");
				}
				
		
				
				if(arrResponse.output.ordNo == 0)
				{
					$("#ordNo").val("");
				}
				else
				{
					$("#ordNo").val(arrResponse.output.ordNo);
				}
				
				if(arrResponse.output.ordItm != 0)
				{
					$("#ordItm").val(arrResponse.output.ordItm);
				}
				else
				{
					$("#ordItm").val("");
				}
				
				
				if(arrResponse.output.htrmtFlg == 1)
				{
					$("#heatCheck").prop( "checked", true );
				}
				else
				{
					$("#heatCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.impcFlg == 1)
				{
					$("#impactCheck").prop( "checked", true );
				}
				else
				{
					$("#impactCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.jmyFlg == 1)
				{
					$("#jominy1Check").prop( "checked", true );
				}
				else
				{
					$("#jominy1Check").prop( "checked", false );
				}
				
				if(arrResponse.output.mincnFlg == 1)
				{
					$("#microCheck").prop( "checked", true );
				}
				else
				{
					$("#microCheck").prop( "checked", false );
				}
				
				
				
				//$("#selectAllChem").trigger("click");
				
				var chemValue = [arrResponse.output.chmel1, arrResponse.output.chmel2, arrResponse.output.chmel3, arrResponse.output.chmel4, arrResponse.output.chmel5, arrResponse.output.chmel6, arrResponse.output.chmel7, arrResponse.output.chmel8, arrResponse.output.chmel9, arrResponse.output.chmel10, arrResponse.output.chmel11, arrResponse.output.chmel12, arrResponse.output.chmel13, arrResponse.output.chmel14, arrResponse.output.chmel15, arrResponse.output.chmel16, arrResponse.output.chmel17, arrResponse.output.chmel18, arrResponse.output.chmel19, arrResponse.output.chmel20];
				
				$("#selectAllChem").prop("checked", false);
				
				$("#chemDivTbl tbody input[type='checkbox'][name='select-chem']").prop("checked", false);
				
				$("#chemDivTbl input[type='checkbox'][name='select-chem']").each(function(index, row){
		
					for(var j = 0; j < chemValue.length; j++)
					{
						if($(row).closest("tr").attr("data-chem-el")  == chemValue[j])
						{
							$(row).closest("tr").find("td input[type='checkbox'][name='select-chem']").prop("checked", true);
						}
					}  
		
				});
				
				
			}
			
		
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

//Edited by Shruti 26-05-23

function saveCheckList(cusId1,chmSelFlg,htrmtFlg,impcFlg,jmyFlg,jmyFlg2,mincnFlg,exCertFlg,tstSpecFlg,mtlStdFlg,selectedElements){
	

	  console.log("Inside savechecklist")  ;
       var tempselectedElements=[];
        	  $("#chemDiv table tbody input[type='checkbox'][name='select-chem']:checked").each(function(index, checkbox) {
            	  //console.log("Selected elements:", selectedElements);
            	  console.log("Inside savechecklist",$(checkbox).closest("tr").attr("data-chem-el"))  ;
            	  tempselectedElements.push($(checkbox).closest("tr").attr("data-chem-el"));
        	});
         let originalChemElem=[];
      for(let i=0;i<20;i++){
    	  if(tempselectedElements[i]==undefined || tempselectedElements[i]==null || tempselectedElements[i]==""){
    		  originalChemElem.push("");
    	  }
    	  else {
    		  originalChemElem.push(tempselectedElements[i]);
    	  }
      }
	
	$.ajax({
		data: {
			 execFlg: "V",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			cusId : cusId1,
			chmSelFlg,chmSelFlg,
			htrmtFlg: htrmtFlg,
		    impcFlg: impcFlg,
		    jmyFlg:jmyFlg,
		    jmyFlg2:jmyFlg2,
		    mincnFlg:mincnFlg,
		    exCertFlg:exCertFlg,
		    tstSpecFlg:tstSpecFlg,
		    mtlStdFlg:mtlStdFlg,
		    chemArr:JSON.stringify(originalChemElem),
		    
		    
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
		
			if (IsJsonString(response)) {
					
					var arrResponse = JSON.parse(response.trim());
					
					if(arrResponse.output.rtnSts == 0)
					{
						Swal.fire({
							type: 'success',
							title: 'Success',
							text: 'Record saved Successfully',
						})
					}
					else if(arrResponse.output.rtnSts != 0)
					{
						Swal.fire({
							type: 'warning',
							title: 'Oops...',
							text: 'Error while Saving Record, ',
						})
					}
									
		    	$("#cusId1").val("");
				
				$("#heatCheck").prop( "checked", false );
				
				$("#impactCheck").prop( "checked", false );
				
				$("#jominy1Check").prop( "checked", false );
				
				$("#microCheck").prop( "checked", false );
				
				$("#extCheck").prop( "checked", false );
				$("#tstCheck").prop( "checked", false );
				$("#jmy2Check").prop( "checked", false );
				$("#mtlCheck").prop( "checked", false );
				 $("#chemDiv table tbody input[type='checkbox'][name='select-chem']:checked").each(function (index, checkbox) {
		                $(checkbox).prop('checked', false);
		            });
				
			}
			
			getCustomerIndex();
			//$("#mainLoader").hide();F
         
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}


/* GET CUSTOMER AND PRODUCT INFORMATION INFORMATION FROM QDS */
function getCustomerProduct(){
	
	var ordPfx = $("#ordPfx").val();
	var ordNo = $("#ordNo").val();
	var ordItm = $("#ordItm").val();
	
	$.ajax({
		data: {
			execFlg: "G",
			cmpyId: localStorage.getItem("starHdtQdsCmpy"),
			qdsCtlNo: localStorage.getItem("starHdtQdsCtlNo"),
			usrId: localStorage.getItem("starHdtQdsUserId"), 
			host: localStorage.getItem("starHdtQdsAppHost"),
			port: localStorage.getItem("starHdtQdsAppPort"),
			token: localStorage.getItem("starHdtQdsAuthToken"),
			lng: localStorage.getItem("starHdtQdsBaseLng"),
			ctry: localStorage.getItem("starHdtQdsBaseCtry"),
			nginxURL: localStorage.getItem("starHdtQdsNginxURL"),
			ordPfx : ordPfx,
			ordNo: ordNo,
			ordItm: ordItm
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(response){
		
			if (IsJsonString(response)) {
				
				var arrResponse = JSON.parse(response.trim());
				
				$("#cusId").val(arrResponse.output.cusId).trigger("change");
				
				$("#prdNm").html(arrResponse.output.prdStrng1);
				
				if(arrResponse.output.ordNo == 0)
				{
					$("#ordNo").val("");
				}
				else
				{
					$("#ordNo").val(arrResponse.output.ordNo);
				}
				
				if(arrResponse.output.ordItm != 0)
				{
					$("#ordItm").val(arrResponse.output.ordItm);
				}
				else
				{
					$("#ordItm").val("");
				}
				if(arrResponse.output.htrmtFlg == 1)
				{
					$("#heatCheck").prop( "checked", true );
				}
				else
				{
					$("#heatCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.impcFlg == 1)
				{
					$("#impactCheck").prop( "checked", true );
				}
				else
				{
					$("#impactCheck").prop( "checked", false );
				}
				
				if(arrResponse.output.jmyFlg == 1)
				{
					$("#jominy1Check").prop( "checked", true );
				}
				else
				{
					$("#jominy1Check").prop( "checked", false );
				}
				if(arrResponse.output.jmyFlg2 == 1)
				{
					$("#jominy2Check").prop( "checked", true );
				}
				else
				{
					$("#jominy2Check").prop( "checked", false );
				}
				if(arrResponse.output.mincnFlg == 1)
				{
					$("#microCheck").prop( "checked", true );
				}
				else
				{
					$("#microCheck").prop( "checked", false );
				}
				if(arrResponse.output.exCertFlg == 1)
				{
					$("#certChk").prop( "checked", true );
				}
				else
				{
					$("#certChk").prop( "checked", false );
				}
				if(arrResponse.output.tstSpecFlg == 1)
				{
					$("#tstSpcChk").prop( "checked", true );
				}
				else
				{
					$("#tstSpcChk").prop( "checked", false );
				}
				if(arrResponse.output.mtlStdFlg == 1)
				{
					$("#mtlStdChk").prop( "checked", true );
				}
				else
				{
					$("#mtlStdChk").prop( "checked", false );
				}
			
				
			}
			
		
			$("#mainLoader").hide();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function hideAllEditRow(){
	$("#allDataTbl .update-shift, #allDataTbl .cancel-edit, #allDataTbl .form-control").hide();
	$("#allDataTbl span, #allDataTbl .edit-shift").show();
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}