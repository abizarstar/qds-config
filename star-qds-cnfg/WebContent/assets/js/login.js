
var dbUrl = 'GetDataFrmDB.jsp';

$(function(){
	
	if(localStorage.getItem("starHdtAuthToken") != null && localStorage.getItem("starHdtAuthToken") != "" )
	{
		window.location.replace("index.html");
	}
	
	var urlParams = new URLSearchParams(window.location.search);
	console.log("urlParams >> "+urlParams);
	
	var urlWithData = document.location.href;
	console.log("urlWithData >> "+urlWithData);
	
	var nginxURL = "";
	if(urlWithData.includes("invex.cloud")){
		nginxURL = urlWithData.split("invex.cloud")[0]+"invex.cloud/" + window.location.pathname.split("/")[1];
	}
	
	else if(urlWithData.includes("auxinvex")){
		var fullURL = urlWithData;
		var baseUrl = new URL(fullURL).origin;
		nginxURL = baseUrl+"/" + window.location.pathname.split("/")[1];
	}
	
	console.log("nginxURL >> "+nginxURL);
	
	console.log(urlParams.has('param'))
	
	if(urlParams.has('param')){
	
		var inpParam = urlParams.get('param');
		var url = window.location.origin;
		var urlArr = url.split("//");
		
		// passing ip and port if app is not running on nginxURL
		if(nginxURL.trim().length == 0){
			nginxURL = urlArr[1];
		}
		
		console.log("nginxURL in param >> "+nginxURL);
		
		//callLogin(inpParam, urlArr[1]);
		
		callLogin(inpParam, nginxURL);	
	}

    
    $("#password").keyup(function(event){
    	if (event.keyCode === 13) {
    		$("#btnLogin").trigger("click");
    	}
    });
    
    
	$("#btnLogin").click(function(){
	
		$("#mainLoader").show();
		$("#btnLogin").hide();
		$("#loadingLoginWrapper").show();
		
		$("#formLogin .form-control").removeClass("border-danger");
		$("#loginError").html("");
		
		var error = false;
		
		var userId = $("#userId").val();
		var password = $("#password").val();
		
		if( userId == undefined || userId == "" || userId.trim().length == 0 ){
			error = true;
			$("#userId").addClass("border-danger");
		}
		
		if( password == undefined || password == "" || password.trim().length == 0 ){
			error = true;
			$("#password").addClass("border-danger");
		}
		
		
		if( error == false ){
			
			$.ajax({
				type: 'POST',
				dataType: 'json',
		        url: dbUrl,
		        data: {
					execFlg: "H",
					param: userId + "|" + password,
				},
		        success: function(response, textStatus, jqXHR) {
		            console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            
		          if( response.rtnSts == 1)
		            {
		            	if( response.msgList != undefined && response.msgList != undefined ){
		            		var loopLimit = response.msgList.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-3 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li><h5>'+ response.msgList[i].msg +'</h5></li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	customAlertTop(errList);
		         
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            	$("#mainLoader").hide();
		            }
		            else
		            {		            
		            	localStorage.setItem("starHdtQdsAuthToken", response.authToken);
		            	localStorage.setItem("starHdtQdsUserId", response.userId);
		            	localStorage.setItem("starHdtQdsUserNm", response.userNm);
		            	localStorage.setItem("starHdtQdsCmpy", response.cmpy);
		            	localStorage.setItem("starHdtQdsAppHost",response.server);
		            	localStorage.setItem("starHdtQdsAppPort", response.port);
		            	localStorage.setItem("starHdtQdsCtlNo", response.qdsCtlNo);	   
		            	localStorage.setItem("starHdtQdsBaseLng", response.baseLanguage);  
		            	localStorage.setItem("starHdtQdsBaseCtry", response.baseCountry);           	
		        
		            	
		            	//redirectToDashboard();
		            	
		            	//window.location.replace("upld-invc.html");
		            	window.location.replace("index.html");
		            	
		            	$("#mainLoader").hide();
		            }
		        }
		    });
		}else{
			
			var errList = '<ul class="m-3 pl-1">';
			errList += '<li><h5>'+'Please enter User Id/Password' +'</h5></li>';					
			errList += '</ul>';
			 
			customAlertTop(errList, "bg-warning");
			$("#mainLoader").hide();
			$("#loadingLoginWrapper").hide();
		    $("#btnLogin").show();
		}
	});
	
});


function callLogin(inpParam, nginxURL){

	$("#mainLoader").show();
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
	    url: dbUrl,
	    data: {
			execFlg: "I",
			param: inpParam,
			appUrl: nginxURL,
		},
        success: function(response, textStatus, jqXHR) {         
		    console.log(response);
		    console.log(textStatus);
		    console.log(jqXHR.status); 
		    
            if( response.rtnSts == 1){
		            
	            if( response.msgList != undefined && response.msgList != undefined ){
        			var loopLimit = response.msgList.length;
	            	if( loopLimit > 0 ){
	            		errList = '<ul class="m-3 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li><h5>'+ response.msgList[i].msg +'</h5></li>';
						}
						errList += '</ul>';
	            	}
	             }
		            	
        		 customAlertTop(errList);
		         
				$("#loadingLoginWrapper").hide();
            	$("#btnLogin").show();
            	$("#mainLoader").hide();
            }
            
            else {		            
	        	localStorage.setItem("starHdtQdsAuthToken", response.authToken);
	        	localStorage.setItem("starHdtQdsUserId", response.userId);
	        	localStorage.setItem("starHdtQdsUserNm", response.userNm);
	        	localStorage.setItem("starHdtQdsCmpy", response.cmpy);
	        	localStorage.setItem("starHdtQdsAppHost",response.server);
	        	localStorage.setItem("starHdtQdsAppPort", response.port);
	        	localStorage.setItem("starHdtQdsCtlNo", response.qdsCtlNo);
	        	localStorage.setItem("starHdtQdsBaseLng", response.baseLanguage);  
	        	localStorage.setItem("starHdtQdsBaseCtry", response.baseCountry); 
	        	localStorage.setItem("starHdtQdsNginxURL", response.nginxURL);             	
		        
            	window.location.replace("index.html");
		            	
            	$("#mainLoader").hide();
            }
        }
    });
}


function customAlertTop(errList, divClass){

	$(".notify").toggleClass("active-alert");		            	
	$(".notify").addClass(divClass);
	$("#notifyType").html(errList);
	
	 setTimeout(function(){
	    $(".notify").removeClass("active-alert");
	  },3000);
		         
}