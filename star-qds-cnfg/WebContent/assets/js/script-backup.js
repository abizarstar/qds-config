/*
	Created By   :- Yogesh Bhade
	Created Date :- 26 April 2021
	Description  :- Javascript For Production Centers
*/
//var execPath = "/u/starcph/bin/cstm_get_cpl_arctag_pg";
//var execPath = "cstm_get_cpl_arctag_pg";

var pgmNmAmtekInfo = "Amtek_info";
var execPath = "/u/starsoft/bin/test";
var dbUrl = 'GetDataFrmDB.jsp';

$(document).ready(function(){
	
	var urlParams = new URLSearchParams(window.location.search);
	
	if(urlParams.has('param')){
		var inpParam = urlParams.get('param');
		var mastCoilUrl = "http://172.20.35.65:60768/customer/cust-prod-pwc/prodPwc.jsp?param=" + inpParam;
		$("#masterCoilLink").attr("href", mastCoilUrl);
	}
	
	
	
	$(".click-app-back").click(function() {
	
		$(".sec-wrk-shft").addClass("d-none");
		$(".home").show();
	});
	
	$(".click-app").click(function() {
	
	
	if($(this).attr("data-page") == 'wrk-shft')
	{		
		$(".sec-wrk-shft").removeClass("d-none");
		$(".home").hide();
		
		getInitialOptions();
	
		$("#appPath").val( window.location.pathname.split("/")[2] + "/" + window.location.search );
		
		if( $('.select2').length > 0 ){
			$('.select2').select2();
		}
		
		$("body").on("keypress", ".number", function(evt){
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		});
	
		var fullDate = new Date()
		console.log(fullDate);
		//Thu May 19 2011 17:25:38 GMT+1000 {}
		 
		//convert month to 2 digits
		var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
		 
		var currentDate = twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear();
		console.log(currentDate);
		
		$("#startDate").val(currentDate);
		
		$(".end-span").hide();
		
		setInterval(function(){
			var dt = new Date();
			var hour = dt.getHours().toString();
			var minute = dt.getMinutes().toString();
			var second = dt.getSeconds().toString();
			
			hour = (hour.length == 1) ? "0" + hour : hour;
			minute = (minute.length == 1) ? "0" + minute : minute;
			second = (second.length == 1) ? "0" + second : second;
			
			var time = hour + ":" + minute;
			$("#startTime").val(time);
		}, 1000);
		
	}
	else if($(this).attr("data-page") == 'logix-plc')
	{
		
	}
	else if($(this).attr("data-page") == 'logix-amtek')
	{
		getInitialOptionsPLC();
	}
	
	});
	
	
	$("#startShift").click(function() {
		$("#mainLoader").show();

		$.ajax({
			data: {
				execFlg: "B",
				param: $("#companyId").val() + "|" + $("#jobPfx").val() + "|" + $("#jobNo").val() + "|" + $("#jobItm").val() + "|" + $("#jobSbItm").val() + "|" + $("#operator").val() + "|" + $("#shiftNo").val() + "|" + $("#location").val() + "|" + $("#line").val() + "|" + $("#startDate").val() + "|" + $("#startTime").val()
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(out){
				console.log(out);
	
				if (IsJsonString(out)) {
					jsonOutput = JSON.parse(out);
					if( jsonOutput.insertStatus == 1 ){
						swal.fire({
							text: "Shift started.",
							type: 'success',
						});
						
						getShiftList();
					}else if( jsonOutput.insertStatus == 2 ){
						swal.fire({
							title: 'error',
							text: "This shift is already exist.",
							type: 'error',
						});
					}
				}
				
				$("#mainLoader").hide();
			},
			error: function(data){
				$("#mainLoader").hide();
				console.log(data);
			}
		});
	});
	
	$("#endShift").click(function() {
		$("#mainLoader").show();

		$.ajax({
			data: {
				execFlg: "D",
				param: $("#companyId").val() + "|" + $("#jobPfx").val() + "|" + $("#jobNo").val() + "|" + $("#jobItm").val() + "|" + $("#jobSbItm").val() + "|" + $("#operator").val() + "|" + $("#shiftNo").val() + "|" + $("#location").val() + "|" + $("#line").val() + "|" + $("#startDate").val() + "|" + $("#startTime").val()
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(out){
				console.log(out);
	
				if (IsJsonString(out)) {
					jsonOutput = JSON.parse(out);
					if( jsonOutput.updateStatus == 1 ){
						swal.fire({
							text: "Shift ended.",
							type: 'success',
						});
						
						getShiftList();
					}
				}
				
				$("#mainLoader").hide();
			},
			error: function(data){
				$("#mainLoader").hide();
				console.log(data);
			}
		});
	});
	
});

/**
 *	This function is validating for valid JSON String
 */
function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

/**
 *	This function is used to allow only numbers in textbox
 */
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function getInitialOptions(){
	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "A",
			param: $("#companyId").val() + "|" + $("#jobPfx").val() + "|" + $("#jobNo").val() 
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(out){
			if (IsJsonString(out)) {
				output = JSON.parse(out);
				var max = output.whsList.length;
				var optionData = "<option value=''>Select</option>";
				for( var i=0; i<max; i++ ){
					optionData += "<option value='"+ output.whsList[i].whs +"'>("+ output.whsList[i].whs +") " + output.whsList[i].whsNm + "</option>";
				}
				$("#location").html(optionData);
				$("#location").val( output.whs ).trigger("change");
				
				var max = output.usrList.length;
				var optionData = "<option value=''>Select</option>";
				for( var i=0; i<max; i++ ){
					optionData += "<option value='"+ output.usrList[i].usrId +"'>("+ output.usrList[i].usrId +") " + output.usrList[i].usrNm + "</option>";
				}
				$("#operator").html(optionData);
				$("#operator").val( $("#loginId").val() ).trigger("change");
				
				var max = output.pwcList.length;
				var optionData = "<option value=''>Select</option>";
				for( var i=0; i<max; i++ ){
					optionData += "<option value='"+ output.pwcList[i].pwc +"'>" + output.pwcList[i].pwc + "</option>";
				}
				$("#line").html(optionData);
				$("#line").val( output.pwc ).trigger("change");
				
			}
			
			getShiftList();

		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}


function getShiftList(){
	$("#mainLoader").show();

	$.ajax({
		data: {
			execFlg: "C",
			param: $("#companyId").val() + "|" + $("#jobPfx").val() + "|" + $("#jobNo").val() + "|" + $("#jobItm").val() + "|" + $("#jobSbItm").val() + "|" + $("#operator").val()
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(out){
			console.log(out);

			if (IsJsonString(out)) {
				jsonOutput = JSON.parse(out);
				var shiftArr = jsonOutput.existShiftList;
				var maxRow = shiftArr.length;
				var tblRow = '', endDate = '', endTime = '', runningShiftNo = 0;

				tblRow = (maxRow==0) ? '<tr><td colspan="8" class="text-center">No shift found</td></tr>' : '';			

				for( var i=0; i<maxRow; i++ ){
					endDate = (shiftArr[i].endDate != null) ? shiftArr[i].endDate : ""; 
					endTime = (shiftArr[i].endTime != null) ? shiftArr[i].endTime : ""; 
					
					if( shiftArr[i].endDate == null || shiftArr[i].endTime == null ){
						runningShiftNo = shiftArr[i].shiftNo;
					}
					
					tblRow += '<tr data-shift-no="' + shiftArr[i].shiftNo + '">'+
								'<td>' + shiftArr[i].operator + '</td>'+
								'<td>' + shiftArr[i].location + '</td>'+
								'<td>' + shiftArr[i].line + '</td>'+
								'<td>' + shiftArr[i].shiftNo + '</td>'+
								'<td>' + shiftArr[i].startDate + '</td>'+
								'<td>' + shiftArr[i].startTime + '</td>'+
								'<td>' + endDate + '</td>'+
								'<td>' + endTime + '</td>'+
								'<td>' + shiftArr[i].dur + '</td>'+
								
							'</tr>';
				}
				$("#dataTbl tbody").html(tblRow);
				
				if( runningShiftNo > 0 ){
					$("#startShift").hide();
					$("#endShift").show();
					
					$("#shiftNo").val(runningShiftNo).trigger("change");
					$("#shiftNo").attr("disabled", "disabled");
					
					$(".end-span").show();
					$(".start-span").hide();
				}else{
					$("#startShift").show();
					$("#endShift").hide();
					
					$("#shiftNo").val(1).trigger("change");
					$("#shiftNo").removeAttr("disabled", "disabled");
					
					$(".start-span").show();
					$(".end-span").hide();
				}
			}
			
			$("#mainLoader").hide();
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	});
}


function getInitialOptionsPLC(){
	$("#mainLoader").show();
	$.ajax({
		data: {
			execFlg: "A",
			param: $("#companyId").val() + "|" + $("#jobPfx").val() + "|" + $("#jobNo").val() 
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(out){
			if (IsJsonString(out)) {
				output = JSON.parse(out);
				generateAmtekPLCData(output.pwc, output.whs, $("#jobNo").val());
				
			}
			
			$("#mainLoader").hide();
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	}); 
}

function generateAmtekPLCData(pwc, whs, jobNo){
	$("#mainLoader").show();
	
	var arguments = pwc + "|" + whs + "|" + jobNo;
	var d = new Date();
	var n = d.getTime();
	var reportName = pgmNmAmtekInfo + n + ".dat";
		
	$.ajax({
		data: {
			pgmPath: execPath,
			args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(out){
			
			console.log(out);
			if(IsJsonString(out))
			{
				var arrResponse = JSON.parse(out);
				
				$("#jobNoModal").val(jobNo);
				$("#pwcModal").val(pwc);
				$("#whsModal").val(whs);
				
				$("#strOutput").html(arrResponse.output);
				
				$("#amtekModal").modal("show");
				
			}
			
			$("#mainLoader").hide();
		},
		error: function(data){
		  console.log(data);
		  $("#loader-gif").hide();
		}
	});
}

 function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
   }