/*sftotm HEADER FILE */
#ifndef _sftotm
#define _sftotm
#ifndef OTM_CMPY_ID_FIELD_POSITION
#define OTM_CMPY_ID_FIELD_POSITION  0
#endif
#ifndef CMPY_ID_SIZE
#define CMPY_ID_SIZE  3
#endif
#ifndef OTM_QDS_CTL_NO_FIELD_POSITION
#define OTM_QDS_CTL_NO_FIELD_POSITION  3
#endif
#ifndef QDS_CTL_NO_SIZE
#define QDS_CTL_NO_SIZE  10
#endif
#ifndef QDS_CTL_NO_DCML_SIZE
#define QDS_CTL_NO_DCML_SIZE  0
#endif
#ifndef OTM_ORD_PFX_FIELD_POSITION
#define OTM_ORD_PFX_FIELD_POSITION  13
#endif
#ifndef ORD_PFX_SIZE
#define ORD_PFX_SIZE  2
#endif
#ifndef OTM_ORD_NO_FIELD_POSITION
#define OTM_ORD_NO_FIELD_POSITION  15
#endif
#ifndef ORD_NO_SIZE
#define ORD_NO_SIZE  8
#endif
#ifndef OTM_ORD_ITM_FIELD_POSITION
#define OTM_ORD_ITM_FIELD_POSITION  23
#endif
#ifndef ORD_ITM_SIZE
#define ORD_ITM_SIZE  3
#endif
#ifndef OTM_CHM_SEL_FLG_FIELD_POSITION
#define OTM_CHM_SEL_FLG_FIELD_POSITION  26
#endif
#ifndef CHM_SEL_FLG_SIZE
#define CHM_SEL_FLG_SIZE  1
#endif
#ifndef OTM_JMY_FLG_FIELD_POSITION
#define OTM_JMY_FLG_FIELD_POSITION  27
#endif
#ifndef JMY_FLG_SIZE
#define JMY_FLG_SIZE  1
#endif
#ifndef JMY_FLG_DCML_SIZE
#define JMY_FLG_DCML_SIZE  0
#endif
#ifndef OTM_IMPC_FLG_FIELD_POSITION
#define OTM_IMPC_FLG_FIELD_POSITION  28
#endif
#ifndef IMPC_FLG_SIZE
#define IMPC_FLG_SIZE  1
#endif
#ifndef IMPC_FLG_DCML_SIZE
#define IMPC_FLG_DCML_SIZE  0
#endif
#ifndef OTM_MINCN_FLG_FIELD_POSITION
#define OTM_MINCN_FLG_FIELD_POSITION  29
#endif
#ifndef MINCN_FLG_SIZE
#define MINCN_FLG_SIZE  1
#endif
#ifndef MINCN_FLG_DCML_SIZE
#define MINCN_FLG_DCML_SIZE  0
#endif
#ifndef OTM_HTRMT_FLG_FIELD_POSITION
#define OTM_HTRMT_FLG_FIELD_POSITION  30
#endif
#ifndef HTRMT_FLG_SIZE
#define HTRMT_FLG_SIZE  1
#endif
#ifndef HTRMT_FLG_DCML_SIZE
#define HTRMT_FLG_DCML_SIZE  0
#endif
#ifndef OTM_JMY_FLG2_FIELD_POSITION
#define OTM_JMY_FLG2_FIELD_POSITION  31
#endif
#ifndef JMY_FLG2_SIZE
#define JMY_FLG2_SIZE  1
#endif
#ifndef JMY_FLG2_DCML_SIZE
#define JMY_FLG2_DCML_SIZE  0
#endif
#ifndef OTM_EX_CERT_FLG_FIELD_POSITION
#define OTM_EX_CERT_FLG_FIELD_POSITION  32
#endif
#ifndef EX_CERT_FLG_SIZE
#define EX_CERT_FLG_SIZE  1
#endif
#ifndef EX_CERT_FLG_DCML_SIZE
#define EX_CERT_FLG_DCML_SIZE  0
#endif
#ifndef OTM_TST_SPEC_FLG_FIELD_POSITION
#define OTM_TST_SPEC_FLG_FIELD_POSITION  33
#endif
#ifndef TST_SPEC_FLG_SIZE
#define TST_SPEC_FLG_SIZE  1
#endif
#ifndef TST_SPEC_FLG_DCML_SIZE
#define TST_SPEC_FLG_DCML_SIZE  0
#endif
#ifndef OTM_MTL_STD_FLG_FIELD_POSITION
#define OTM_MTL_STD_FLG_FIELD_POSITION  34
#endif
#ifndef MTL_STD_FLG_SIZE
#define MTL_STD_FLG_SIZE  1
#endif
#ifndef MTL_STD_FLG_DCML_SIZE
#define MTL_STD_FLG_DCML_SIZE  0
#endif
#ifndef OTM_CHMEL_1_FIELD_POSITION
#define OTM_CHMEL_1_FIELD_POSITION  35
#endif
#ifndef CHMEL_1_SIZE
#define CHMEL_1_SIZE  3
#endif
#ifndef OTM_CHMEL_2_FIELD_POSITION
#define OTM_CHMEL_2_FIELD_POSITION  38
#endif
#ifndef CHMEL_2_SIZE
#define CHMEL_2_SIZE  3
#endif
#ifndef OTM_CHMEL_3_FIELD_POSITION
#define OTM_CHMEL_3_FIELD_POSITION  41
#endif
#ifndef CHMEL_3_SIZE
#define CHMEL_3_SIZE  3
#endif
#ifndef OTM_CHMEL_4_FIELD_POSITION
#define OTM_CHMEL_4_FIELD_POSITION  44
#endif
#ifndef CHMEL_4_SIZE
#define CHMEL_4_SIZE  3
#endif
#ifndef OTM_CHMEL_5_FIELD_POSITION
#define OTM_CHMEL_5_FIELD_POSITION  47
#endif
#ifndef CHMEL_5_SIZE
#define CHMEL_5_SIZE  3
#endif
#ifndef OTM_CHMEL_6_FIELD_POSITION
#define OTM_CHMEL_6_FIELD_POSITION  50
#endif
#ifndef CHMEL_6_SIZE
#define CHMEL_6_SIZE  3
#endif
#ifndef OTM_CHMEL_7_FIELD_POSITION
#define OTM_CHMEL_7_FIELD_POSITION  53
#endif
#ifndef CHMEL_7_SIZE
#define CHMEL_7_SIZE  3
#endif
#ifndef OTM_CHMEL_8_FIELD_POSITION
#define OTM_CHMEL_8_FIELD_POSITION  56
#endif
#ifndef CHMEL_8_SIZE
#define CHMEL_8_SIZE  3
#endif
#ifndef OTM_CHMEL_9_FIELD_POSITION
#define OTM_CHMEL_9_FIELD_POSITION  59
#endif
#ifndef CHMEL_9_SIZE
#define CHMEL_9_SIZE  3
#endif
#ifndef OTM_CHMEL_10_FIELD_POSITION
#define OTM_CHMEL_10_FIELD_POSITION  62
#endif
#ifndef CHMEL_10_SIZE
#define CHMEL_10_SIZE  3
#endif
#ifndef OTM_CHMEL_11_FIELD_POSITION
#define OTM_CHMEL_11_FIELD_POSITION  65
#endif
#ifndef CHMEL_11_SIZE
#define CHMEL_11_SIZE  3
#endif
#ifndef OTM_CHMEL_12_FIELD_POSITION
#define OTM_CHMEL_12_FIELD_POSITION  68
#endif
#ifndef CHMEL_12_SIZE
#define CHMEL_12_SIZE  3
#endif
#ifndef OTM_CHMEL_13_FIELD_POSITION
#define OTM_CHMEL_13_FIELD_POSITION  71
#endif
#ifndef CHMEL_13_SIZE
#define CHMEL_13_SIZE  3
#endif
#ifndef OTM_CHMEL_14_FIELD_POSITION
#define OTM_CHMEL_14_FIELD_POSITION  74
#endif
#ifndef CHMEL_14_SIZE
#define CHMEL_14_SIZE  3
#endif
#ifndef OTM_CHMEL_15_FIELD_POSITION
#define OTM_CHMEL_15_FIELD_POSITION  77
#endif
#ifndef CHMEL_15_SIZE
#define CHMEL_15_SIZE  3
#endif
#ifndef OTM_CHMEL_16_FIELD_POSITION
#define OTM_CHMEL_16_FIELD_POSITION  80
#endif
#ifndef CHMEL_16_SIZE
#define CHMEL_16_SIZE  3
#endif
#ifndef OTM_CHMEL_17_FIELD_POSITION
#define OTM_CHMEL_17_FIELD_POSITION  83
#endif
#ifndef CHMEL_17_SIZE
#define CHMEL_17_SIZE  3
#endif
#ifndef OTM_CHMEL_18_FIELD_POSITION
#define OTM_CHMEL_18_FIELD_POSITION  86
#endif
#ifndef CHMEL_18_SIZE
#define CHMEL_18_SIZE  3
#endif
#ifndef OTM_CHMEL_19_FIELD_POSITION
#define OTM_CHMEL_19_FIELD_POSITION  89
#endif
#ifndef CHMEL_19_SIZE
#define CHMEL_19_SIZE  3
#endif
#ifndef OTM_CHMEL_20_FIELD_POSITION
#define OTM_CHMEL_20_FIELD_POSITION  92
#endif
#ifndef CHMEL_20_SIZE
#define CHMEL_20_SIZE  3
#endif
#define sizesftotm 95
#define ssizesftotm "00095"
struct sftotm_rec {
	char cmpy_id[3];
	char qds_ctl_no[10];
	char ord_pfx[2];
	char ord_no[8];
	char ord_itm[3];
	char chm_sel_flg[1];
	char jmy_flg[1];
	char impc_flg[1];
	char mincn_flg[1];
	char htrmt_flg[1];
	char jmy_flg2[1];
	char ex_cert_flg[1];
	char tst_spec_flg[1];
	char mtl_std_flg[1];
	char chmel_1[3];
	char chmel_2[3];
	char chmel_3[3];
	char chmel_4[3];
	char chmel_5[3];
	char chmel_6[3];
	char chmel_7[3];
	char chmel_8[3];
	char chmel_9[3];
	char chmel_10[3];
	char chmel_11[3];
	char chmel_12[3];
	char chmel_13[3];
	char chmel_14[3];
	char chmel_15[3];
	char chmel_16[3];
	char chmel_17[3];
	char chmel_18[3];
	char chmel_19[3];
	char chmel_20[3];
}; 
#endif
