/****************************************************************************************
*                      <sfctag - Return tags >                                        *
*****************************************************************************************/

/****************************************************************************************
* Created By:  Mohammad Yasir                                                           *
* Date Created: 01 February 2022                                                        *
* Release:      20.0                                                                    *
* eCIM Call #:                                                                          *
* Description:  Created New Service for Return tags                                     *
*                                                                                       *
\****************************************************************************************/
/* Lint Comments */
/* NOTUSED*/
/* END Lint Comments */

#include "syscom.h"
#include "sfctag.h"
#include "mchqds.h"
#include "sccct1.h"
#include "xcti29.h"
#include "xcti28.h"
#include "xcti30.h"
#include "xcti00.h"
#include "injitd.h"
#include "mccqds.h"
#include "xczpec.h"

/**************** CONSTANTS ****************/
#define PGM_NM									"sfctag"
#define PRS_MD_ADD								'A'

/**********Variable definition**************/

static struct mchqds_rec pvtMchqds;

static char pvtsHeat[HEAT_SIZE+1] = "";
static char pvtsItmCtlNo[ITM_CTL_NO_SIZE+1] = "";
static int pvtiCtr = 0;

/**************** Function Prototypes ****************/
static void pvtReadMchqds(char *sQdsCtlNo);
static void pvtCallSccct1(void);
static void pvtInsRecXcti29(void);
static void pvtInsRecXcti28(char *sItmCtlNo);
static void pvtCreateXcti00(void);
static void pvtRtnTag(void);
static void pvtCrtRec(void);
static void pvtInsRecXcti30(void);
static void pvtChgQdsHeat(void);
static void pvtCallMccqds(char *sPrsMd);
static void pvtReadInjitd(void);
static void pvtCallXczpec(void);

/*------------------------------------------------------------------------*/
/* This is the main function which accepts the input message from the     */
/* client program and returns output.                                     */
/*------------------------------------------------------------------------*/

int sfctag(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	if (setjmp(pvtReturnJmp) != 0)
	{
		return 0;
	}

	pvtInitVars(rqst);
	c_writelnkarea(PGM_NM, (char *)&sfctagInpRec, SFCTAG_INP_SSIZE);

	pvtInitLnkVars();
	CHK_SVC_RTN_STS

	pvtBusinessLogic();
	pvtMainLogicEnd();

	return 0;
}

/*------------------------------------------------------------------------*\
 * Function to initialize variables
\*------------------------------------------------------------------------*/
static void pvtInitVars(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = 0;
	pvtInMainLogicEnd[0] = 'N';

	/*** I/P initialization ***/
	pvtLnkArea = rqst;
	memcpy((char *)&sfctagInpRec, rqst->data, SFCTAG_INP_SIZE);
	
	/*** O/P initialization ***/
	
	INIT_SFCTAG_OUTPUT
	INIT_SCCCT1_OUTPUT
	INIT_MCCQDS_OUTPUT
	INIT_XCZPEC_OUTPUT
	
	pvtOutStr = rqst->data + rqst->len;

	svcInitVars(PGM_NM);
	
	memset(&pvtMchqds, ' ', sizemchqds);
	svcCallPlnsqlio(TRACE_INFO, "mchqds", "1", "1", INL, (char *)&pvtMchqds, ssizemchqds, STXCONT, 1);

	memset(pvtsItmCtlNo, '\0', sizeof(pvtsItmCtlNo));
	memset(pvtsItmCtlNo, '0', ITM_CTL_NO_SIZE);
	pvtiCtr = 0;

	return;
}

/* Function to Initialize request message's numeric fields if not set by the client */
static void pvtInitLnkVars(void)
{
	int iCtr = 0;
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = svcVldNumeric(sfctagInpRec.clnt_dttm, CLNT_DTTM_SIZE, "CLNT-DTTM", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfctagInpRec.qds_ctl_no, QDS_CTL_NO_SIZE, "QDS-CTL-NO", 0);
	CHK_EXCPT_STS
	
	for (iCtr=0; iCtr<1000; iCtr++)
	{
		pvtiRtnSts = svcVldNumeric(sfctagInpRec.fld_tbl[iCtr].itm_ctl_no, ITM_CTL_NO_SIZE, "ITM-CTL-NO", 0);
		CHK_EXCPT_STS
	}
	
	return;
}

static void pvtBusinessLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtValidateInput();
	CHK_FAIL_STS

	pvtProgramLogic();
	return;
}

/*------------------------------------------------------------------------*\
 * Function to validate
\*------------------------------------------------------------------------*/
static void pvtValidateInput(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	if (pvtLnkArea->len < SFCTAG_INP_SIZE)
	{
		svcUserlog(TRACE_INFO, "Length of Input message is less than the expected length %d < %d", pvtLnkArea->len, SFCTAG_INP_SIZE);
		MAIN_LOGIC_RETURN
	}
	
	/* if process mode is applicable */
	if(sfctagInpRec.prs_md[0] != PRS_MD_ADD && sfctagInpRec.prs_md[0] != PRS_MD_RD)
	{
		svcLogErrMsg(1, 1, "0000001500", "PRS-MD");
		MAIN_LOGIC_RETURN
	}

	pvtReadMchqds(sfctagInpRec.qds_ctl_no);
	CHK_FAIL_STS

	return;
}

static void pvtProgramLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	switch (sfctagInpRec.prs_md[0])
	{
		case PRS_MD_ADD:

			pvtChgQdsHeat();
			CHK_FAIL_STS

			pvtCrtRec();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_RD:
			pvtRtnTag();
			CHK_FAIL_STS
			break;
		default:
			break;
	}
	return;
}

static void pvtCrtRec(void)
{
	int iCtr=0;
	svcSetDbgInfo(TRACE_INFO, "");

	for (iCtr=0; iCtr<1000; iCtr++)
	{
		if (memcmp(sfctagInpRec.fld_tbl[iCtr].itm_ctl_no, ZEROS, ITM_CTL_NO_SIZE) == 0)
		{
			break;
		}
		memset(pvtsItmCtlNo, '0', ITM_CTL_NO_SIZE);
		memcpy(pvtsItmCtlNo, sfctagInpRec.fld_tbl[iCtr].itm_ctl_no, ITM_CTL_NO_SIZE);

		pvtCallSccct1();
		CHK_FAIL_STS

		pvtInsRecXcti28(pvtsItmCtlNo);
		CHK_FAIL_STS

		pvtInsRecXcti29();
		CHK_FAIL_STS

		pvtInsRecXcti30();
		CHK_FAIL_STS

		pvtCreateXcti00();
		CHK_FAIL_STS

		pvtCallXczpec();
		CHK_FAIL_STS
	}

	return;
}

static void pvtInsRecXcti30(void)
{
	struct xcti30_rec xcti30;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK

	memset(&xcti30, ' ', sizexcti30);
	svcCallPlnsqlio(TRACE_INFO, "xcti30", "4", "1", INL, (char *)&xcti30, ssizexcti30, STXCONT, 1);
	
	memcpy(xcti30.intchg_pfx, "XI", 2);
	memcpy(xcti30.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	svcIntToSrvrValStr(xcti30.intchg_itm, INTCHG_ITM_SIZE, 1, INTCHG_ITM_SIZE);
	memcpy(xcti30.mill, pvtMchqds.mill, MILL_SIZE);
	memcpy(xcti30.heat, pvtsHeat, strnlen(pvtsHeat, HEAT_SIZE));

	svcCallPlnsqlio(TRACE_INFO, "xcti30", "4", "1", INS, (char *)&xcti30, ssizexcti30, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK
	
	return;
}

static void pvtRtnTag(void)
{
	int iCtr = 0;
	int position = 0;
	char sTmpSqlioStr[2501] = "";
	svcSetDbgInfo(TRACE_INFO, "");

	memset(sTmpSqlioStr, '\0', sizeof(sTmpSqlioStr));
	sprintf(sTmpSqlioStr, "SELECT prd_itm_ctl_no, prd_tag_no FROM intprd_rec WHERE prd_cmpy_id ='%-.*s' AND prd_mill = '%-.*s' and prd_heat = '%-.*s' order by prd_tag_no",CMPY_ID_SIZE, g_CmpyId, MILL_SIZE, pvtMchqds.mill, HEAT_SIZE, pvtMchqds.heat);

	svcCallPlnsqlio(TRACE_INFO, "STSQL1", "2", "1", OWN, sTmpSqlioStr, "01500", STXCONT, 0);
	CHK_SQL_IO_STS
	if(svciSqlIoSts == 0)
	{
		for(iCtr = 0; iCtr <= 1000; )
		{
			position = 0;
			
			if (svcCallPlnsqlio(TRACE_INFO, "STSQL1", "2", "1", RDN, sTmpSqlioStr, "01500", STXCONT, 0) != 0)
			{
					break;
			}
			
			if (iCtr == 1000)
			{
				break;
			}

			memcpy(sfctagOutRec.fld_tbl[iCtr].itm_ctl_no, sTmpSqlioStr+position, ITM_CTL_NO_SIZE);
			position += ITM_CTL_NO_SIZE+1;
			memcpy(sfctagOutRec.fld_tbl[iCtr].tag_no , sTmpSqlioStr+position, TAG_NO_SIZE);
			position += TAG_NO_SIZE;

			iCtr++;
		}
		svcCallPlnsqlio(TRACE_INFO, "STSQL1", "2", "1", CLS, sTmpSqlioStr, "01500", STXCONT, 0);
	}

	/*memcpy(sfctagOutRec.qds_ctl_no, sfctagInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);*/

	return;
}

static void pvtInsRecXcti28(char *sItmCtlNo)
{
	struct xcti28_rec xcti28;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xcti28, ' ', sizexcti28);
	svcCallPlnsqlio(TRACE_INFO, "xcti28", "5", "1", INL, (char *)&xcti28, ssizexcti28, STXCONT, 1);
	
	memcpy(xcti28.intchg_pfx, "XI", 2);
	memcpy(xcti28.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	memcpy(xcti28.itm_ctl_no, sItmCtlNo, ITM_CTL_NO_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "xcti28", "5", "1", INS, (char *)&xcti28, ssizexcti28, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK
	
	return;
}

static void pvtInsRecXcti29(void)
{
	char sDttm[18] = "";
	struct xcti29_rec xcti29;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xcti29, ' ', sizexcti29);
	svcCallPlnsqlio(TRACE_INFO, "xcti29", "5", "1", INL, (char *)&xcti29, ssizexcti29, STXCONT, 1);
	
	memcpy(xcti29.intchg_pfx, "XI", 2);
	memcpy(xcti29.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	svcIntToSrvrValStr(xcti29.intchg_itm, INTCHG_ITM_SIZE, 1, INTCHG_ITM_SIZE);

	/* CRTD-DTTM */
	memset(sDttm, '\0', sizeof(sDttm));
	c_GetUTDateTm(sDttm);
	memcpy(xcti29.crtd_dtts, sDttm, CRTD_DTTS_SIZE);
	memcpy(xcti29.crtd_dtms, sDttm+CRTD_DTTS_SIZE, CRTD_DTMS_SIZE);
	memcpy(xcti29.upd_dtts, ZEROS, UPD_DTTS_SIZE);
	memcpy(xcti29.upd_dtms, ZEROS, UPD_DTMS_SIZE);

	memcpy(xcti29.adj_typ, "03", 2);
	memcpy(xcti29.adj_rsn, "AJ", 2);
	/*memcpy(xcti29.adj_rmk, "TESTING", 7);*/
	memcpy(xcti29.sts_cd, "N", 1);

	svcCallPlnsqlio(TRACE_INFO, "xcti29", "5", "1", INS, (char *)&xcti29, ssizexcti29, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK
	
	return;
}

/* Function to insert record in xcti00 table */
static void pvtCreateXcti00(void)
{
	char sDttm[18] = "";
	struct xcti00_rec xcti00;

	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xcti00, ' ', sizexcti00);
	svcCallPlnsqlio(TRACE_INFO, "xcti00", "3", "1", INL, (char *)&xcti00, ssizexcti00, STXCONT, 1);
	
	memcpy(xcti00.intchg_pfx, "XI", 2);
	memcpy(xcti00.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	memcpy(xcti00.evnt, "PRG", 3);
	
	memcpy(xcti00.usr_id, sfctagInpRec.clnt_lgn_id, USR_ID_SIZE);
	
	/* CRTD-DTTM */
	memset(sDttm, '\0', sizeof(sDttm));
	c_GetUTDateTm(sDttm);
	memcpy(xcti00.crtd_dtts, sDttm, CRTD_DTTS_SIZE);
	memcpy(xcti00.crtd_dtms, sDttm+CRTD_DTTS_SIZE, CRTD_DTMS_SIZE);
	memcpy(xcti00.upd_dtts, ZEROS, UPD_DTTS_SIZE);
	memcpy(xcti00.upd_dtms, ZEROS, UPD_DTMS_SIZE);
	memcpy(xcti00.sts_cd, "N", 1);
	memcpy(xcti00.ssn_log_ctl_no, ZEROS, SSN_LOG_CTL_NO_SIZE);
	/*memcpy(xcti00.intrf_cl, "E", 1);*/
	memcpy(xcti00.intrf_cl, "C", 1);

	svcCallPlnsqlio(TRACE_INFO, "xcti00", "3", "1", INS, (char *)&xcti00, ssizexcti00, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK

	return;
}

static void pvtCallSccct1(void)
{
	char sInpStr[SCCCT1_INP_SIZE + 1] = "";
	char sOutStr[SCCCT1_OUT_SIZE + 1] = "";
	
	svcSetDbgInfo(TRACE_INFO, "");

	INIT_SCCCT1_INPUT
	INIT_SCCCT1_OUTPUT
	
	memcpy(sccct1InpRec.apln, "IN", 2);
	memcpy(sccct1InpRec.ref_pfx, "QC", 2);
	memcpy(sccct1InpRec.ctl_no_lgth, "08", 2);
	
	memset(sInpStr, '\0', sizeof(sInpStr));
	memset(sOutStr, '\0', sizeof(sOutStr));

	memcpy(sInpStr, (char *)&sccct1InpRec, SCCCT1_INP_SIZE);
	if (svcCallSvc("sccct1", sInpStr, SCCCT1_INP_SIZE, sOutStr, SCCCT1_OUT_SIZE, "") == 0)
	{
		if (memcmp(sOutStr, "0001", RTN_STS_SIZE) == 0)
		{
			MAIN_LOGIC_RETURN
		}
		else
		{
			memcpy(&sccct1OutRec, sOutStr, SCCCT1_OUT_SIZE);
			SET_SVC_RTN_STS(sccct1OutRec)
			CHK_FAIL_STS
		}
	}
	else
	{
		MAIN_LOGIC_RETURN
	}
	
	/* Set data in output string */
	memcpy(&sccct1OutRec, sOutStr, SCCCT1_OUT_SIZE);
	
	return;
}

static void pvtReadMchqds(char *sQdsCtlNo)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtMchqds, ' ', sizemchqds);
	svcCallPlnsqlio(TRACE_INFO, "mchqds", "1", "1", INL, (char *)&pvtMchqds, ssizemchqds, STXCONT, 1);
	
	memcpy(pvtMchqds.qds_ctl_no, sQdsCtlNo, QDS_CTL_NO_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "mchqds", "1", "1", RD, (char *)&pvtMchqds, ssizemchqds, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtCallXczpec(void)
{
	char sInpStr[XCZPEC_INP_SIZE + 1] = "";
	char sOutStr[XCZPEC_OUT_SIZE + 1] = "";

	svcSetDbgInfo(TRACE_INFO, "");
	
	/* Call COBOL service XCZPEC passing the following input linkage data. */
	INIT_XCZPEC_INPUT
	INIT_XCZPEC_OUTPUT

	memcpy(xczpecInpRec.intchg_pfx, "XI", 2);
	memcpy(xczpecInpRec.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);

	memset(sInpStr, '\0', sizeof(sInpStr));
	memset(sOutStr, '\0', sizeof(sOutStr));

	memcpy(sInpStr, (char *)&xczpecInpRec, XCZPEC_INP_SIZE);
	if (svcCallSvc("xczpec", sInpStr, XCZPEC_INP_SIZE, sOutStr, XCZPEC_OUT_SIZE, "") == 0)
	{
		if(memcmp(sOutStr, "0001", RTN_STS_SIZE) == 0)
		{
			MAIN_LOGIC_RETURN
		}

		/* Set data in output string */
		memcpy(&xczpecOutRec, sOutStr, XCZPEC_OUT_SIZE);
		SET_SVC_RTN_STS(xczpecOutRec)
		CHK_FAIL_STS
	}
	else
	{
		MAIN_LOGIC_RETURN
	}

	if (memcmp(xczpecOutRec.rtn_sts, ZEROS, RTN_STS_SIZE) == 0)
	{
		/*Read INJITD to get the Product QDS created for the tag when its heat is changed */
		pvtReadInjitd();
		CHK_FAIL_STS
	}

	return;
}

static void pvtCallMccqds(char *sPrsMd)
{
	char sInpStrMccqds[MCCQDS_INP_SIZE + 1] = "";
	char sOutStrMccqds[MCCQDS_OUT_SIZE + 1] = "";

	svcSetDbgInfo(TRACE_INFO, "");

	INIT_MCCQDS_INPUT


	memcpy(mccqdsInpRec.prs_md, sPrsMd, 1);
	memcpy(mccqdsInpRec.qds_ctl_no, sfctagInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);

	if (sPrsMd[0] == 'R')
	{

		memcpy(mccqdsInpRec.whs, mccqdsOutRec.whs, WHS_SIZE);
		memcpy(mccqdsInpRec.mill, mccqdsOutRec.mill, MILL_SIZE);
		memcpy(mccqdsInpRec.heat, pvtsHeat, strnlen(pvtsHeat, HEAT_SIZE));
		memcpy(mccqdsInpRec.orig_zn, mccqdsOutRec.orig_zn, ORIG_ZN_SIZE);
		memcpy(mccqdsInpRec.unknw_heat, mccqdsOutRec.unknw_heat, UNKNW_HEAT_SIZE);
		memcpy(mccqdsInpRec.mill_ord_no, mccqdsOutRec.mill_ord_no, MILL_ORD_NO_SIZE);
		memcpy(mccqdsInpRec.ven_id, mccqdsOutRec.ven_id, VEN_ID_SIZE);
		memcpy(mccqdsInpRec.qds_sts, mccqdsOutRec.qds_sts, QDS_STS_SIZE);
		memcpy(mccqdsInpRec.qds_typ, mccqdsOutRec.qds_typ, QDS_TYP_SIZE);
		memcpy(mccqdsInpRec.orig_po_pfx, mccqdsOutRec.orig_po_pfx, ORIG_PO_PFX_SIZE);
		memcpy(mccqdsInpRec.orig_po_no, mccqdsOutRec.orig_po_no, ORIG_PO_NO_SIZE);
		memcpy(mccqdsInpRec.orig_po_itm, mccqdsOutRec.orig_po_itm, ORIG_PO_ITM_SIZE);
		memcpy(mccqdsInpRec.orig_po_dist, mccqdsOutRec.orig_po_dist, ORIG_PO_DIST_SIZE);

		INIT_MCCQDS_OUTPUT
	}
	else
	{
		INIT_MCCQDS_OUTPUT
	}
	
	memset(sInpStrMccqds, '\0', sizeof(sInpStrMccqds));
	memset(sOutStrMccqds, '\0', sizeof(sOutStrMccqds));

	memcpy(sInpStrMccqds, (char *)&mccqdsInpRec, MCCQDS_INP_SIZE);
	
	if (svcCallSvc("mccqds", sInpStrMccqds, MCCQDS_INP_SIZE, sOutStrMccqds, MCCQDS_OUT_SIZE, "F") == 0)
	{
		/* Set data in output string */
		memcpy(&mccqdsOutRec, sOutStrMccqds, MCCQDS_OUT_SIZE);
		SET_SVC_RTN_STS(mccqdsOutRec)
		CHK_FAIL_STS
	}
	else
	{
		MAIN_LOGIC_RETURN
	}
	
	return;
}


static void pvtChgQdsHeat(void)
{
	/*char sHeat[HEAT_SIZE+1] = "";*/
	char sQdsCtlNo[QDS_CTL_NO_SIZE*2+1] = "";

	svcSetDbgInfo(TRACE_INFO, "");

	svcFmtIntNoSym(sQdsCtlNo, sizeof(sQdsCtlNo), sfctagInpRec.qds_ctl_no, QDS_CTL_NO_SIZE, 0);
	sprintf(pvtsHeat, "Z%s", sQdsCtlNo);

	/*RTrim(sHeat, sizeof(sHeat), pvtMchqds.heat, HEAT_SIZE);
	if (strcmp(sHeat, pvtsHeat) == 0)
	{ Heat already changed for Product QDS 
		return;
	}*/

	/* Read QDS info */
	pvtCallMccqds("4");
	CHK_FAIL_STS

	/* Change Heat */
	pvtCallMccqds("R");
	CHK_FAIL_STS

	return;
}

static void pvtReadInjitd(void)
{
	struct injitd_rec injitd;
	
	svcSetDbgInfo(TRACE_INFO, "");

	svcBuildSqlioStr(TRACE_INFO, "injitd", "1", "1", OWN, ssizeinjitd);

	sprintf(startOfData, "SELECT * FROM injitd_rec WHERE itd_cmpy_id = '%-.*s' AND itd_ref_pfx = 'AJ' AND itd_itm_ctl_no = %-.*s AND itd_heat = '%s'", CMPY_ID_SIZE, g_CmpyId, ITM_CTL_NO_SIZE, pvtsItmCtlNo, pvtsHeat);

	plnsqlio(svcPgmNm,sqlioStr);

	if(memcmp(sqlioStr+17,"00000000",8) == 0)
	{
		memcpy(&injitd, sqlioStr+25, sizeinjitd);
		if (memcmp(injitd.qds_ctl_no, ZEROS, QDS_CTL_NO_SIZE) != 0)
		{
			/*svcUserlog(TRACE_INFO, "Itm Ctl: %-.*s QDS: %-.*s", ITM_CTL_NO_SIZE, pvtsItmCtlNo, QDS_CTL_NO_SIZE, injitd.qds_ctl_no);*/
			memcpy(sfctagOutRec.fld_tbl[pvtiCtr++].qds_ctl_no, injitd.qds_ctl_no, QDS_CTL_NO_SIZE);
		}
	}

	svcBuildSqlioStr(TRACE_INFO, "injitd", "1", "1", CLS, ssizeinjitd);
	plnsqlio(PGM_NM,sqlioStr);

}

static void pvtMainLogicEnd(void)
{
	if (*pvtInMainLogicEnd != 'Y')
	{
		SET_OUT_RTN_STS(sfctagOutRec)
		memcpy(pvtOutStr, (char *)&sfctagOutRec, SFCTAG_OUT_SIZE);
		svcTerminationRtn(pvtInMainLogicEnd, PGM_NM, pvtOutStr, SFCTAG_OUT_SSIZE, pvtiRtnSts, pvtReturnJmp);
	}
	return;

}
