#ifndef _xczpev
#define _xczpev
#define XCZPEV_INP_SIZE             108
#define XCZPEV_INP_SSIZE             "00108"
#define XCZPEV_OUT_SIZE             4
#define XCZPEV_OUT_SSIZE             "00004"
#ifndef CLNT_PGM_NM_SIZE
#define CLNT_PGM_NM_SIZE        14
#endif
#ifndef CLNT_LGN_ID_SIZE
#define CLNT_LGN_ID_SIZE        8
#endif
#ifndef CLNT_CTY_SIZE
#define CLNT_CTY_SIZE        3
#endif
#ifndef CLNT_LNG_SIZE
#define CLNT_LNG_SIZE        2
#endif
#ifndef CLNT_HOST_NM_SIZE
#define CLNT_HOST_NM_SIZE        15
#endif
#ifndef CLNT_TTY_NM_SIZE
#define CLNT_TTY_NM_SIZE        15
#endif
#ifndef CLNT_PID_SIZE
#define CLNT_PID_SIZE        10
#endif
#ifndef CLNT_DTTM_SIZE
#define CLNT_DTTM_SIZE        17
#endif
#ifndef SVC_PGM_NM_SIZE
#define SVC_PGM_NM_SIZE        14
#endif
#ifndef INTCHG_PFX_SIZE
#define INTCHG_PFX_SIZE        2
#endif
#ifndef INTCHG_NO_SIZE
#define INTCHG_NO_SIZE        8
#endif
#ifndef RTN_STS_SIZE
#define RTN_STS_SIZE        4
#endif
static struct xczpev_inp_rec {
     char clnt_pgm_nm[14];
     char clnt_lgn_id[8];
     char clnt_cty[3];
     char clnt_lng[2];
     char clnt_host_nm[15];
     char clnt_tty_nm[15];
     char clnt_pid[10];
     char clnt_dttm[17];
     char svc_pgm_nm[14];
     char intchg_pfx[2];
     char intchg_no[8];
}xczpevInpRec;
static struct xczpev_out_rec {
     char rtn_sts[4];
}xczpevOutRec;
#define INIT_XCZPEV_INPUT \
{\
     memset(xczpevInpRec.clnt_pgm_nm   ,' ',14);\
     memset(xczpevInpRec.clnt_lgn_id   ,' ',8);\
     memset(xczpevInpRec.clnt_cty      ,' ',3);\
     memset(xczpevInpRec.clnt_lng      ,' ',2);\
     memset(xczpevInpRec.clnt_host_nm  ,' ',15);\
     memset(xczpevInpRec.clnt_tty_nm   ,' ',15);\
     memset(xczpevInpRec.clnt_pid      ,' ',10);\
     memset(xczpevInpRec.clnt_dttm     ,'0',17);\
     memset(xczpevInpRec.svc_pgm_nm    ,' ',14);\
     memset(xczpevInpRec.intchg_pfx    ,' ',2);\
     memset(xczpevInpRec.intchg_no     ,'0',8);\
}
#define INIT_XCZPEV_OUTPUT \
{\
     memset(xczpevOutRec.rtn_sts       ,'0',4);\
}
#endif