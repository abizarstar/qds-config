
if [ "$SERVER" = "INFORMIX" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
then
  echo "INDEXDBS not defined. Aborting.."
  exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'"\
|dbaccess $DATABASE|tail -2|head -1`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=pmd
  fi
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
dbaccess $DATABASE <<!
  create view informix.sftdcu_rec as 
  select * from $PRI_DATABASE@$PRI_IFXSERVER:sftdcu_rec;
  grant all on sftdcu_rec to public as informix;
!
else

#Create the table
dbaccess $DATABASE <<!
	set lock mode to wait;
	CREATE TABLE informix.sftdcu_rec	(
		DCU_CMPY_ID             nchar(3)  NOT NULL,
		DCU_CUS_ID              char(8)  NOT NULL,
		DCU_CHM_SEL_FLG         nchar(1)  NOT NULL,
		DCU_JMY_FLG             decimal(1,0)  NOT NULL,
		DCU_IMPC_FLG            decimal(1,0)  NOT NULL,
		DCU_MINCN_FLG           decimal(1,0)  NOT NULL,
		DCU_HTRMT_FLG           decimal(1,0)  NOT NULL,
		DCU_JMY_FLG2            decimal(1,0)  NOT NULL,
		DCU_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		DCU_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		DCU_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		DCU_CHMEL_1             nchar(3)  NOT NULL,
		DCU_CHMEL_2             nchar(3)  NOT NULL,
		DCU_CHMEL_3             nchar(3)  NOT NULL,
		DCU_CHMEL_4             nchar(3)  NOT NULL,
		DCU_CHMEL_5             nchar(3)  NOT NULL,
		DCU_CHMEL_6             nchar(3)  NOT NULL,
		DCU_CHMEL_7             nchar(3)  NOT NULL,
		DCU_CHMEL_8             nchar(3)  NOT NULL,
		DCU_CHMEL_9             nchar(3)  NOT NULL,
		DCU_CHMEL_10            nchar(3)  NOT NULL,
		DCU_CHMEL_11            nchar(3)  NOT NULL,
		DCU_CHMEL_12            nchar(3)  NOT NULL,
		DCU_CHMEL_13            nchar(3)  NOT NULL,
		DCU_CHMEL_14            nchar(3)  NOT NULL,
		DCU_CHMEL_15            nchar(3)  NOT NULL,
		DCU_CHMEL_16            nchar(3)  NOT NULL,
		DCU_CHMEL_17            nchar(3)  NOT NULL,
		DCU_CHMEL_18            nchar(3)  NOT NULL,
		DCU_CHMEL_19            nchar(3)  NOT NULL,
		DCU_CHMEL_20            nchar(3)  NOT NULL
) lock mode ROW;
!

#Create the Primary Key Index and Constraint
dbaccess $DATABASE <<!
set lock mode to wait;
create unique index DCU_KEY on sftdcu_rec( DCU_CMPY_ID, DCU_CUS_ID ) in $INDEXDBS;
alter table sftdcu_rec add constraint PRIMARY KEY( DCU_CMPY_ID, DCU_CUS_ID ) constraint sftdcu_pk;
!

fi #if not IWD

fi

if [ "$SERVER" = "POSTGRES" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
  then
    echo "INDEXDBS not defined. Aborting.."
    exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'" $DATABASE`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=pmd
  fi
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
psql $DATABASE <<!
  create foreign table sftdcu_rec (
		DCU_CMPY_ID             nchar(3),
		DCU_CUS_ID              char(8),
		DCU_CHM_SEL_FLG         nchar(1),
		DCU_JMY_FLG             decimal(1,0),
		DCU_IMPC_FLG            decimal(1,0),
		DCU_MINCN_FLG           decimal(1,0),
		DCU_HTRMT_FLG           decimal(1,0),
		DCU_JMY_FLG2            decimal(1,0),
		DCU_EX_CERT_FLG         decimal(1,0),
		DCU_TST_SPEC_FLG        decimal(1,0),
		DCU_MTL_STD_FLG         decimal(1,0),
		DCU_CHMEL_1             nchar(3),
		DCU_CHMEL_2             nchar(3),
		DCU_CHMEL_3             nchar(3),
		DCU_CHMEL_4             nchar(3),
		DCU_CHMEL_5             nchar(3),
		DCU_CHMEL_6             nchar(3),
		DCU_CHMEL_7             nchar(3),
		DCU_CHMEL_8             nchar(3),
		DCU_CHMEL_9             nchar(3),
		DCU_CHMEL_10            nchar(3),
		DCU_CHMEL_11            nchar(3),
		DCU_CHMEL_12            nchar(3),
		DCU_CHMEL_13            nchar(3),
		DCU_CHMEL_14            nchar(3),
		DCU_CHMEL_15            nchar(3),
		DCU_CHMEL_16            nchar(3),
		DCU_CHMEL_17            nchar(3),
		DCU_CHMEL_18            nchar(3),
		DCU_CHMEL_19            nchar(3),
		DCU_CHMEL_20            nchar(3)
server ${PRI_PGHOST}${PRI_PGPORT}
options (table_name 'sftdcu_rec');
!
else

#Create the table
psql $DATABASE <<!
	CREATE TABLE sftdcu_rec	(
		DCU_CMPY_ID             nchar(3)  NOT NULL,
		DCU_CUS_ID              char(8)  NOT NULL,
		DCU_CHM_SEL_FLG         nchar(1)  NOT NULL,
		DCU_JMY_FLG             decimal(1,0)  NOT NULL,
		DCU_IMPC_FLG            decimal(1,0)  NOT NULL,
		DCU_MINCN_FLG           decimal(1,0)  NOT NULL,
		DCU_HTRMT_FLG           decimal(1,0)  NOT NULL,
		DCU_JMY_FLG2            decimal(1,0)  NOT NULL,
		DCU_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		DCU_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		DCU_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		DCU_CHMEL_1             nchar(3)  NOT NULL,
		DCU_CHMEL_2             nchar(3)  NOT NULL,
		DCU_CHMEL_3             nchar(3)  NOT NULL,
		DCU_CHMEL_4             nchar(3)  NOT NULL,
		DCU_CHMEL_5             nchar(3)  NOT NULL,
		DCU_CHMEL_6             nchar(3)  NOT NULL,
		DCU_CHMEL_7             nchar(3)  NOT NULL,
		DCU_CHMEL_8             nchar(3)  NOT NULL,
		DCU_CHMEL_9             nchar(3)  NOT NULL,
		DCU_CHMEL_10            nchar(3)  NOT NULL,
		DCU_CHMEL_11            nchar(3)  NOT NULL,
		DCU_CHMEL_12            nchar(3)  NOT NULL,
		DCU_CHMEL_13            nchar(3)  NOT NULL,
		DCU_CHMEL_14            nchar(3)  NOT NULL,
		DCU_CHMEL_15            nchar(3)  NOT NULL,
		DCU_CHMEL_16            nchar(3)  NOT NULL,
		DCU_CHMEL_17            nchar(3)  NOT NULL,
		DCU_CHMEL_18            nchar(3)  NOT NULL,
		DCU_CHMEL_19            nchar(3)  NOT NULL,
		DCU_CHMEL_20            nchar(3)  NOT NULL
);
!

#Create the Primary Key Index and Constraint
psql $DATABASE <<!
alter table sftdcu_rec add constraint DCU_KEY PRIMARY KEY( DCU_CMPY_ID, DCU_CUS_ID ) using index tablespace $INDEXDBS;
!

fi #if not IWD

fi
