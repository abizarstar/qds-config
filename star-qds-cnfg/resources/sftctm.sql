
if [ "$SERVER" = "INFORMIX" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
then
  echo "INDEXDBS not defined. Aborting.."
  exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'"\
|dbaccess $DATABASE|tail -2|head -1`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=pmd
  fi
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
dbaccess $DATABASE <<!
  create view informix.sftctm_rec as 
  select * from $PRI_DATABASE@$PRI_IFXSERVER:sftctm_rec;
  grant all on sftctm_rec to public as informix;
!
else

#Create the table
dbaccess $DATABASE <<!
	set lock mode to wait;
	CREATE TABLE informix.sftctm_rec	(
		CTM_CMPY_ID             nchar(3)  NOT NULL,
		CTM_CUS_ID              char(8)  NOT NULL,
		CTM_CUS_TMPL_NM         nchar(60)  NOT NULL,
		CTM_CHM_SEL_FLG         nchar(1)  NOT NULL,
		CTM_JMY_FLG             decimal(1,0)  NOT NULL,
		CTM_IMPC_FLG            decimal(1,0)  NOT NULL,
		CTM_MINCN_FLG           decimal(1,0)  NOT NULL,
		CTM_HTRMT_FLG           decimal(1,0)  NOT NULL,
		CTM_JMY_FLG2            decimal(1,0)  NOT NULL,
		CTM_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		CTM_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		CTM_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		CTM_CHMEL_1             nchar(3)  NOT NULL,
		CTM_CHMEL_2             nchar(3)  NOT NULL,
		CTM_CHMEL_3             nchar(3)  NOT NULL,
		CTM_CHMEL_4             nchar(3)  NOT NULL,
		CTM_CHMEL_5             nchar(3)  NOT NULL,
		CTM_CHMEL_6             nchar(3)  NOT NULL,
		CTM_CHMEL_7             nchar(3)  NOT NULL,
		CTM_CHMEL_8             nchar(3)  NOT NULL,
		CTM_CHMEL_9             nchar(3)  NOT NULL,
		CTM_CHMEL_10            nchar(3)  NOT NULL,
		CTM_CHMEL_11            nchar(3)  NOT NULL,
		CTM_CHMEL_12            nchar(3)  NOT NULL,
		CTM_CHMEL_13            nchar(3)  NOT NULL,
		CTM_CHMEL_14            nchar(3)  NOT NULL,
		CTM_CHMEL_15            nchar(3)  NOT NULL,
		CTM_CHMEL_16            nchar(3)  NOT NULL,
		CTM_CHMEL_17            nchar(3)  NOT NULL,
		CTM_CHMEL_18            nchar(3)  NOT NULL,
		CTM_CHMEL_19            nchar(3)  NOT NULL,
		CTM_CHMEL_20            nchar(3)  NOT NULL
) lock mode ROW;
!

#Create the Primary Key Index and Constraint
dbaccess $DATABASE <<!
set lock mode to wait;
create unique index CTM_KEY on sftctm_rec( CTM_CMPY_ID, CTM_CUS_ID, CTM_CUS_TMPL_NM ) in $INDEXDBS;
alter table sftctm_rec add constraint PRIMARY KEY( CTM_CMPY_ID, CTM_CUS_ID, CTM_CUS_TMPL_NM ) constraint sftctm_pk;
!

fi #if not IWD

fi

if [ "$SERVER" = "POSTGRES" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
  then
    echo "INDEXDBS not defined. Aborting.."
    exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'" $DATABASE`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=pmd
  fi
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
psql $DATABASE <<!
  create foreign table sftctm_rec (
		CTM_CMPY_ID             char(3),
		CTM_CUS_ID              char(8),
		CTM_CUS_TMPL_NM         char(60),
		CTM_CHM_SEL_FLG         char(1),
		CTM_JMY_FLG             decimal(1,0),
		CTM_IMPC_FLG            decimal(1,0),
		CTM_MINCN_FLG           decimal(1,0),
		CTM_HTRMT_FLG           decimal(1,0),
		CTM_JMY_FLG2            decimal(1,0),
		CTM_EX_CERT_FLG         decimal(1,0),
		CTM_TST_SPEC_FLG        decimal(1,0),
		CTM_MTL_STD_FLG         decimal(1,0),
		CTM_CHMEL_1             char(3),
		CTM_CHMEL_2             char(3),
		CTM_CHMEL_3             char(3),
		CTM_CHMEL_4             char(3),
		CTM_CHMEL_5             char(3),
		CTM_CHMEL_6             char(3),
		CTM_CHMEL_7             char(3),
		CTM_CHMEL_8             char(3),
		CTM_CHMEL_9             char(3),
		CTM_CHMEL_10            char(3),
		CTM_CHMEL_11            char(3),
		CTM_CHMEL_12            char(3),
		CTM_CHMEL_13            char(3),
		CTM_CHMEL_14            char(3),
		CTM_CHMEL_15            char(3),
		CTM_CHMEL_16            char(3),
		CTM_CHMEL_17            char(3),
		CTM_CHMEL_18            char(3),
		CTM_CHMEL_19            char(3),
		CTM_CHMEL_20            char(3)
server ${PRI_PGHOST}${PRI_PGPORT}
options (table_name 'sftctm_rec');
!
else

#Create the table
psql $DATABASE <<!
	CREATE TABLE sftctm_rec	(
		CTM_CMPY_ID             char(3)  NOT NULL,
		CTM_CUS_ID              char(8)  NOT NULL,
		CTM_CUS_TMPL_NM         char(60)  NOT NULL,
		CTM_CHM_SEL_FLG         char(1)  NOT NULL,
		CTM_JMY_FLG             decimal(1,0)  NOT NULL,
		CTM_IMPC_FLG            decimal(1,0)  NOT NULL,
		CTM_MINCN_FLG           decimal(1,0)  NOT NULL,
		CTM_HTRMT_FLG           decimal(1,0)  NOT NULL,
		CTM_JMY_FLG2            decimal(1,0)  NOT NULL,
		CTM_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		CTM_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		CTM_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		CTM_CHMEL_1             char(3)  NOT NULL,
		CTM_CHMEL_2             char(3)  NOT NULL,
		CTM_CHMEL_3             char(3)  NOT NULL,
		CTM_CHMEL_4             char(3)  NOT NULL,
		CTM_CHMEL_5             char(3)  NOT NULL,
		CTM_CHMEL_6             char(3)  NOT NULL,
		CTM_CHMEL_7             char(3)  NOT NULL,
		CTM_CHMEL_8             char(3)  NOT NULL,
		CTM_CHMEL_9             char(3)  NOT NULL,
		CTM_CHMEL_10            char(3)  NOT NULL,
		CTM_CHMEL_11            char(3)  NOT NULL,
		CTM_CHMEL_12            char(3)  NOT NULL,
		CTM_CHMEL_13            char(3)  NOT NULL,
		CTM_CHMEL_14            char(3)  NOT NULL,
		CTM_CHMEL_15            char(3)  NOT NULL,
		CTM_CHMEL_16            char(3)  NOT NULL,
		CTM_CHMEL_17            char(3)  NOT NULL,
		CTM_CHMEL_18            char(3)  NOT NULL,
		CTM_CHMEL_19            char(3)  NOT NULL,
		CTM_CHMEL_20            char(3)  NOT NULL
);
!

#Create the Primary Key Index and Constraint
psql $DATABASE <<!
alter table sftctm_rec add constraint CTM_KEY PRIMARY KEY( CTM_CMPY_ID, CTM_CUS_ID, CTM_CUS_TMPL_NM ) using index tablespace $INDEXDBS;
!

fi #if not IWD

fi
