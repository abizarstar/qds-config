/*sftctm HEADER FILE */
#ifndef _sftctm
#define _sftctm
#ifndef CTM_CMPY_ID_FIELD_POSITION
#define CTM_CMPY_ID_FIELD_POSITION  0
#endif
#ifndef CMPY_ID_SIZE
#define CMPY_ID_SIZE  3
#endif
#ifndef CTM_CUS_ID_FIELD_POSITION
#define CTM_CUS_ID_FIELD_POSITION  3
#endif
#ifndef CUS_ID_SIZE
#define CUS_ID_SIZE  8
#endif
#ifndef CUS_ID_DCML_SIZE
#define CUS_ID_DCML_SIZE  0
#endif
#ifndef CTM_CUS_TMPL_NM_FIELD_POSITION
#define CTM_CUS_TMPL_NM_FIELD_POSITION  11
#endif
#ifndef CUS_TMPL_NM_SIZE
#define CUS_TMPL_NM_SIZE  60
#endif
#ifndef CTM_CHM_SEL_FLG_FIELD_POSITION
#define CTM_CHM_SEL_FLG_FIELD_POSITION  71
#endif
#ifndef CHM_SEL_FLG_SIZE
#define CHM_SEL_FLG_SIZE  1
#endif
#ifndef CTM_JMY_FLG_FIELD_POSITION
#define CTM_JMY_FLG_FIELD_POSITION  72
#endif
#ifndef JMY_FLG_SIZE
#define JMY_FLG_SIZE  1
#endif
#ifndef JMY_FLG_DCML_SIZE
#define JMY_FLG_DCML_SIZE  0
#endif
#ifndef CTM_IMPC_FLG_FIELD_POSITION
#define CTM_IMPC_FLG_FIELD_POSITION  73
#endif
#ifndef IMPC_FLG_SIZE
#define IMPC_FLG_SIZE  1
#endif
#ifndef IMPC_FLG_DCML_SIZE
#define IMPC_FLG_DCML_SIZE  0
#endif
#ifndef CTM_MINCN_FLG_FIELD_POSITION
#define CTM_MINCN_FLG_FIELD_POSITION  74
#endif
#ifndef MINCN_FLG_SIZE
#define MINCN_FLG_SIZE  1
#endif
#ifndef MINCN_FLG_DCML_SIZE
#define MINCN_FLG_DCML_SIZE  0
#endif
#ifndef CTM_HTRMT_FLG_FIELD_POSITION
#define CTM_HTRMT_FLG_FIELD_POSITION  75
#endif
#ifndef HTRMT_FLG_SIZE
#define HTRMT_FLG_SIZE  1
#endif
#ifndef HTRMT_FLG_DCML_SIZE
#define HTRMT_FLG_DCML_SIZE  0
#endif
#ifndef CTM_JMY_FLG2_FIELD_POSITION
#define CTM_JMY_FLG2_FIELD_POSITION  76
#endif
#ifndef JMY_FLG2_SIZE
#define JMY_FLG2_SIZE  1
#endif
#ifndef JMY_FLG2_DCML_SIZE
#define JMY_FLG2_DCML_SIZE  0
#endif
#ifndef CTM_EX_CERT_FLG_FIELD_POSITION
#define CTM_EX_CERT_FLG_FIELD_POSITION  77
#endif
#ifndef EX_CERT_FLG_SIZE
#define EX_CERT_FLG_SIZE  1
#endif
#ifndef EX_CERT_FLG_DCML_SIZE
#define EX_CERT_FLG_DCML_SIZE  0
#endif
#ifndef CTM_TST_SPEC_FLG_FIELD_POSITION
#define CTM_TST_SPEC_FLG_FIELD_POSITION  78
#endif
#ifndef TST_SPEC_FLG_SIZE
#define TST_SPEC_FLG_SIZE  1
#endif
#ifndef TST_SPEC_FLG_DCML_SIZE
#define TST_SPEC_FLG_DCML_SIZE  0
#endif
#ifndef CTM_MTL_STD_FLG_FIELD_POSITION
#define CTM_MTL_STD_FLG_FIELD_POSITION  79
#endif
#ifndef MTL_STD_FLG_SIZE
#define MTL_STD_FLG_SIZE  1
#endif
#ifndef MTL_STD_FLG_DCML_SIZE
#define MTL_STD_FLG_DCML_SIZE  0
#endif
#ifndef CTM_CHMEL_1_FIELD_POSITION
#define CTM_CHMEL_1_FIELD_POSITION  80
#endif
#ifndef CHMEL_1_SIZE
#define CHMEL_1_SIZE  3
#endif
#ifndef CTM_CHMEL_2_FIELD_POSITION
#define CTM_CHMEL_2_FIELD_POSITION  83
#endif
#ifndef CHMEL_2_SIZE
#define CHMEL_2_SIZE  3
#endif
#ifndef CTM_CHMEL_3_FIELD_POSITION
#define CTM_CHMEL_3_FIELD_POSITION  86
#endif
#ifndef CHMEL_3_SIZE
#define CHMEL_3_SIZE  3
#endif
#ifndef CTM_CHMEL_4_FIELD_POSITION
#define CTM_CHMEL_4_FIELD_POSITION  89
#endif
#ifndef CHMEL_4_SIZE
#define CHMEL_4_SIZE  3
#endif
#ifndef CTM_CHMEL_5_FIELD_POSITION
#define CTM_CHMEL_5_FIELD_POSITION  92
#endif
#ifndef CHMEL_5_SIZE
#define CHMEL_5_SIZE  3
#endif
#ifndef CTM_CHMEL_6_FIELD_POSITION
#define CTM_CHMEL_6_FIELD_POSITION  95
#endif
#ifndef CHMEL_6_SIZE
#define CHMEL_6_SIZE  3
#endif
#ifndef CTM_CHMEL_7_FIELD_POSITION
#define CTM_CHMEL_7_FIELD_POSITION  98
#endif
#ifndef CHMEL_7_SIZE
#define CHMEL_7_SIZE  3
#endif
#ifndef CTM_CHMEL_8_FIELD_POSITION
#define CTM_CHMEL_8_FIELD_POSITION  101
#endif
#ifndef CHMEL_8_SIZE
#define CHMEL_8_SIZE  3
#endif
#ifndef CTM_CHMEL_9_FIELD_POSITION
#define CTM_CHMEL_9_FIELD_POSITION  104
#endif
#ifndef CHMEL_9_SIZE
#define CHMEL_9_SIZE  3
#endif
#ifndef CTM_CHMEL_10_FIELD_POSITION
#define CTM_CHMEL_10_FIELD_POSITION  107
#endif
#ifndef CHMEL_10_SIZE
#define CHMEL_10_SIZE  3
#endif
#ifndef CTM_CHMEL_11_FIELD_POSITION
#define CTM_CHMEL_11_FIELD_POSITION  110
#endif
#ifndef CHMEL_11_SIZE
#define CHMEL_11_SIZE  3
#endif
#ifndef CTM_CHMEL_12_FIELD_POSITION
#define CTM_CHMEL_12_FIELD_POSITION  113
#endif
#ifndef CHMEL_12_SIZE
#define CHMEL_12_SIZE  3
#endif
#ifndef CTM_CHMEL_13_FIELD_POSITION
#define CTM_CHMEL_13_FIELD_POSITION  116
#endif
#ifndef CHMEL_13_SIZE
#define CHMEL_13_SIZE  3
#endif
#ifndef CTM_CHMEL_14_FIELD_POSITION
#define CTM_CHMEL_14_FIELD_POSITION  119
#endif
#ifndef CHMEL_14_SIZE
#define CHMEL_14_SIZE  3
#endif
#ifndef CTM_CHMEL_15_FIELD_POSITION
#define CTM_CHMEL_15_FIELD_POSITION  122
#endif
#ifndef CHMEL_15_SIZE
#define CHMEL_15_SIZE  3
#endif
#ifndef CTM_CHMEL_16_FIELD_POSITION
#define CTM_CHMEL_16_FIELD_POSITION  125
#endif
#ifndef CHMEL_16_SIZE
#define CHMEL_16_SIZE  3
#endif
#ifndef CTM_CHMEL_17_FIELD_POSITION
#define CTM_CHMEL_17_FIELD_POSITION  128
#endif
#ifndef CHMEL_17_SIZE
#define CHMEL_17_SIZE  3
#endif
#ifndef CTM_CHMEL_18_FIELD_POSITION
#define CTM_CHMEL_18_FIELD_POSITION  131
#endif
#ifndef CHMEL_18_SIZE
#define CHMEL_18_SIZE  3
#endif
#ifndef CTM_CHMEL_19_FIELD_POSITION
#define CTM_CHMEL_19_FIELD_POSITION  134
#endif
#ifndef CHMEL_19_SIZE
#define CHMEL_19_SIZE  3
#endif
#ifndef CTM_CHMEL_20_FIELD_POSITION
#define CTM_CHMEL_20_FIELD_POSITION  137
#endif
#ifndef CHMEL_20_SIZE
#define CHMEL_20_SIZE  3
#endif
#define sizesftctm 140
#define ssizesftctm "00140"
struct sftctm_rec {
	char cmpy_id[3];
	char cus_id[8];
	char cus_tmpl_nm[60];
	char chm_sel_flg[1];
	char jmy_flg[1];
	char impc_flg[1];
	char mincn_flg[1];
	char htrmt_flg[1];
	char jmy_flg2[1];
	char ex_cert_flg[1];
	char tst_spec_flg[1];
	char mtl_std_flg[1];
	char chmel_1[3];
	char chmel_2[3];
	char chmel_3[3];
	char chmel_4[3];
	char chmel_5[3];
	char chmel_6[3];
	char chmel_7[3];
	char chmel_8[3];
	char chmel_9[3];
	char chmel_10[3];
	char chmel_11[3];
	char chmel_12[3];
	char chmel_13[3];
	char chmel_14[3];
	char chmel_15[3];
	char chmel_16[3];
	char chmel_17[3];
	char chmel_18[3];
	char chmel_19[3];
	char chmel_20[3];
}; 
#endif
