#ifndef _sfcctm
#define _sfcctm
#define SFCCTM_INP_SIZE             249
#define SFCCTM_INP_SSIZE             "00249"
#define SFCCTM_OUT_SIZE             11054
#define SFCCTM_OUT_SSIZE             "11054"
#ifndef CLNT_PGM_NM_SIZE
#define CLNT_PGM_NM_SIZE        14
#endif
#ifndef CLNT_LGN_ID_SIZE
#define CLNT_LGN_ID_SIZE        8
#endif
#ifndef CLNT_CTY_SIZE
#define CLNT_CTY_SIZE        3
#endif
#ifndef CLNT_LNG_SIZE
#define CLNT_LNG_SIZE        2
#endif
#ifndef CLNT_HOST_NM_SIZE
#define CLNT_HOST_NM_SIZE        15
#endif
#ifndef CLNT_TTY_NM_SIZE
#define CLNT_TTY_NM_SIZE        15
#endif
#ifndef CLNT_PID_SIZE
#define CLNT_PID_SIZE        10
#endif
#ifndef CLNT_DTTM_SIZE
#define CLNT_DTTM_SIZE        17
#endif
#ifndef SVC_PGM_NM_SIZE
#define SVC_PGM_NM_SIZE        14
#endif
#ifndef PRS_MD_SIZE
#define PRS_MD_SIZE        1
#endif
#ifndef QDS_CTL_NO_SIZE
#define QDS_CTL_NO_SIZE        10
#endif
#ifndef ORD_PFX_SIZE
#define ORD_PFX_SIZE        2
#endif
#ifndef ORD_NO_SIZE
#define ORD_NO_SIZE        8
#endif
#ifndef ORD_ITM_SIZE
#define ORD_ITM_SIZE        3
#endif
#ifndef CUS_ID_SIZE
#define CUS_ID_SIZE        8
#endif
#ifndef TMPL_NM_SIZE
#define TMPL_NM_SIZE  50
#endif
#ifndef CHM_SEL_FLG_SIZE
#define CHM_SEL_FLG_SIZE  1
#endif
#ifndef CHMEL_1_SIZE
#define CHMEL_1_SIZE  3
#endif
#ifndef CHMEL_2_SIZE
#define CHMEL_2_SIZE  3
#endif
#ifndef CHMEL_3_SIZE
#define CHMEL_3_SIZE  3
#endif
#ifndef CHMEL_4_SIZE
#define CHMEL_4_SIZE  3
#endif
#ifndef CHMEL_5_SIZE
#define CHMEL_5_SIZE  3
#endif
#ifndef CHMEL_6_SIZE
#define CHMEL_6_SIZE  3
#endif
#ifndef CHMEL_7_SIZE
#define CHMEL_7_SIZE  3
#endif
#ifndef CHMEL_8_SIZE
#define CHMEL_8_SIZE  3
#endif
#ifndef CHMEL_9_SIZE
#define CHMEL_9_SIZE  3
#endif
#ifndef CHMEL_10_SIZE
#define CHMEL_10_SIZE  3
#endif
#ifndef CHMEL_11_SIZE
#define CHMEL_11_SIZE  3
#endif
#ifndef CHMEL_12_SIZE
#define CHMEL_12_SIZE  3
#endif
#ifndef CHMEL_13_SIZE
#define CHMEL_13_SIZE  3
#endif
#ifndef CHMEL_14_SIZE
#define CHMEL_14_SIZE  3
#endif
#ifndef CHMEL_15_SIZE
#define CHMEL_15_SIZE  3
#endif
#ifndef CHMEL_16_SIZE
#define CHMEL_16_SIZE  3
#endif
#ifndef CHMEL_17_SIZE
#define CHMEL_17_SIZE  3
#endif
#ifndef CHMEL_18_SIZE
#define CHMEL_18_SIZE  3
#endif
#ifndef CHMEL_19_SIZE
#define CHMEL_19_SIZE  3
#endif
#ifndef CHMEL_20_SIZE
#define CHMEL_20_SIZE  3
#endif
#ifndef JMY_FLG_SIZE
#define JMY_FLG_SIZE  1
#endif
#ifndef JMY_FLG_DCML_SIZE
#define JMY_FLG_DCML_SIZE  0
#endif
#ifndef IMPC_FLG_SIZE
#define IMPC_FLG_SIZE  1
#endif
#ifndef IMPC_FLG_DCML_SIZE
#define IMPC_FLG_DCML_SIZE  0
#endif
#ifndef MINCN_FLG_SIZE
#define MINCN_FLG_SIZE  1
#endif
#ifndef MINCN_FLG_DCML_SIZE
#define MINCN_FLG_DCML_SIZE  0
#endif
#ifndef HTRMT_FLG_SIZE
#define HTRMT_FLG_SIZE  1
#endif
#ifndef HTRMT_FLG_DCML_SIZE
#define HTRMT_FLG_DCML_SIZE  0
#endif
#ifndef RTN_STS_SIZE
#define RTN_STS_SIZE        4
#endif
#ifndef CUS_TMPL_NM_SIZE
#define CUS_TMPL_NM_SIZE  60
#endif
#ifndef CUS_ID_2_SIZE
#define CUS_ID_2_SIZE  8
#endif
#ifndef CUS_NM_SIZE
#define CUS_NM_SIZE  15
#endif
#ifndef CUS_ID_STR_SIZE
#define CUS_ID_STR_SIZE        46
#endif
#ifndef PRD_STRNG_SIZE
#define PRD_STRNG_SIZE        300
#endif
#ifndef CE_VAL_STR_SIZE
#define CE_VAL_STR_SIZE        7
#endif
#ifndef DI_VAL_STR_SIZE
#define DI_VAL_STR_SIZE        7
#endif
struct sfcctm_out_fld_tbl       {
     char cus_tmpl_nm[60];
};
struct sfcctm_cus_out_fld_tbl       {
     char cus_id_2[8];
     char cus_nm[15];
};
static struct sfcctm_inp_rec {
     char clnt_pgm_nm[14];
     char clnt_lgn_id[8];
     char clnt_cty[3];
     char clnt_lng[2];
     char clnt_host_nm[15];
     char clnt_tty_nm[15];
     char clnt_pid[10];
     char clnt_dttm[17];
     char svc_pgm_nm[14];
     char prs_md[1];
     char qds_ctl_no[10];
     char ord_pfx[2];
     char ord_no[8];
     char ord_itm[3];
     char cus_id[8];
     char tmpl_nm[50];
     char chm_sel_flg[1];
     char chmel_1[3];
     char chmel_2[3];
     char chmel_3[3];
     char chmel_4[3];
     char chmel_5[3];
     char chmel_6[3];
     char chmel_7[3];
     char chmel_8[3];
     char chmel_9[3];
     char chmel_10[3];
     char chmel_11[3];
     char chmel_12[3];
     char chmel_13[3];
     char chmel_14[3];
     char chmel_15[3];
     char chmel_16[3];
     char chmel_17[3];
     char chmel_18[3];
     char chmel_19[3];
     char chmel_20[3];
     char jmy_flg[1];
     char impc_flg[1];
     char mincn_flg[1];
     char htrmt_flg[1];
     char jmy_flg2[1];
     char ex_cert_flg[1];
     char tst_spec_flg[1];
     char mtl_std_flg[1];
}sfcctmInpRec;
static struct sfcctm_out_rec {
     char rtn_sts[4];
     char ord_pfx[2];
     char ord_no[8];
     char ord_itm[3];
     char cus_id[8];
     char cus_id_str[46];
     char prd_strng[300];
     char chm_sel_flg[1];
     char chmel_1[3];
     char chmel_2[3];
     char chmel_3[3];
     char chmel_4[3];
     char chmel_5[3];
     char chmel_6[3];
     char chmel_7[3];
     char chmel_8[3];
     char chmel_9[3];
     char chmel_10[3];
     char chmel_11[3];
     char chmel_12[3];
     char chmel_13[3];
     char chmel_14[3];
     char chmel_15[3];
     char chmel_16[3];
     char chmel_17[3];
     char chmel_18[3];
     char chmel_19[3];
     char chmel_20[3];
     char jmy_flg[1];
     char impc_flg[1];
     char mincn_flg[1];
     char htrmt_flg[1];
     char ce_val_str[7];
     char di_val_str[7];
     char jmy_flg2[1];
     char ex_cert_flg[1];
     char tst_spec_flg[1];
     char mtl_std_flg[1];
     struct sfcctm_out_fld_tbl        fld_tbl[100];
     struct sfcctm_cus_out_fld_tbl    cus_fld_tbl[200];
}sfcctmOutRec;
#define INIT_SFCCTM_INPUT \
{\
     memset(sfcctmInpRec.clnt_pgm_nm   ,' ',14);\
     memset(sfcctmInpRec.clnt_lgn_id   ,' ',8);\
     memset(sfcctmInpRec.clnt_cty      ,' ',3);\
     memset(sfcctmInpRec.clnt_lng      ,' ',2);\
     memset(sfcctmInpRec.clnt_host_nm  ,' ',15);\
     memset(sfcctmInpRec.clnt_tty_nm   ,' ',15);\
     memset(sfcctmInpRec.clnt_pid      ,' ',10);\
     memset(sfcctmInpRec.clnt_dttm     ,'0',17);\
     memset(sfcctmInpRec.svc_pgm_nm    ,' ',14);\
     memset(sfcctmInpRec.prs_md        ,' ',1);\
     memset(sfcctmInpRec.qds_ctl_no    ,'0',10);\
     memset(sfcctmInpRec.ord_pfx       ,' ', 2);\
     memset(sfcctmInpRec.ord_no        ,'0', 8);\
     memset(sfcctmInpRec.ord_itm       ,'0', 3);\
     memset(sfcctmInpRec.cus_id        ,' ', 8);\
     memset(sfcctmInpRec.tmpl_nm       ,' ', 50);\
     memset(sfcctmInpRec.chm_sel_flg   ,' ', 1);\
     memset(sfcctmInpRec.chmel_1       ,' ', 3);\
     memset(sfcctmInpRec.chmel_2       ,' ', 3);\
     memset(sfcctmInpRec.chmel_3       ,' ', 3);\
     memset(sfcctmInpRec.chmel_4       ,' ', 3);\
     memset(sfcctmInpRec.chmel_5       ,' ', 3);\
     memset(sfcctmInpRec.chmel_6       ,' ', 3);\
     memset(sfcctmInpRec.chmel_7       ,' ', 3);\
     memset(sfcctmInpRec.chmel_8       ,' ', 3);\
     memset(sfcctmInpRec.chmel_9       ,' ', 3);\
     memset(sfcctmInpRec.chmel_10      ,' ', 3);\
     memset(sfcctmInpRec.chmel_11      ,' ', 3);\
     memset(sfcctmInpRec.chmel_12      ,' ', 3);\
     memset(sfcctmInpRec.chmel_13      ,' ', 3);\
     memset(sfcctmInpRec.chmel_14      ,' ', 3);\
     memset(sfcctmInpRec.chmel_15      ,' ', 3);\
     memset(sfcctmInpRec.chmel_16      ,' ', 3);\
     memset(sfcctmInpRec.chmel_17      ,' ', 3);\
     memset(sfcctmInpRec.chmel_18      ,' ', 3);\
     memset(sfcctmInpRec.chmel_19      ,' ', 3);\
     memset(sfcctmInpRec.chmel_20      ,' ', 3);\
     memset(sfcctmInpRec.jmy_flg       ,'0', 1);\
     memset(sfcctmInpRec.impc_flg      ,'0', 1);\
     memset(sfcctmInpRec.mincn_flg     ,'0', 1);\
     memset(sfcctmInpRec.htrmt_flg     ,'0', 1);\
     memset(sfcctmInpRec.jmy_flg2      ,'0', 1);\
     memset(sfcctmInpRec.ex_cert_flg   ,'0', 1);\
     memset(sfcctmInpRec.tst_spec_flg  ,'0', 1);\
     memset(sfcctmInpRec.mtl_std_flg   ,'0', 1);\
}
#define INIT_SFCCTM_OUTPUT \
{\
     int sfcctmCtr; \
     for (sfcctmCtr=0; sfcctmCtr<100; sfcctmCtr++) \
    { \
        memset(sfcctmOutRec.fld_tbl[sfcctmCtr].cus_tmpl_nm, ' ',60);\
    } \
     for (sfcctmCtr=0; sfcctmCtr<200; sfcctmCtr++) \
    { \
        memset(sfcctmOutRec.cus_fld_tbl[sfcctmCtr].cus_id_2, ' ',8);\
        memset(sfcctmOutRec.cus_fld_tbl[sfcctmCtr].cus_nm, ' ',15);\
    } \
     memset(sfcctmOutRec.rtn_sts       ,'0',4);\
     memset(sfcctmOutRec.ord_pfx       ,' ', 2);\
     memset(sfcctmOutRec.ord_no        ,'0', 8);\
     memset(sfcctmOutRec.ord_itm       ,'0', 3);\
     memset(sfcctmOutRec.cus_id        ,' ', 8);\
     memset(sfcctmOutRec.cus_id_str    ,' ', 46);\
     memset(sfcctmOutRec.prd_strng     ,' ', 300);\
     memset(sfcctmOutRec.chm_sel_flg   ,' ', 1);\
     memset(sfcctmOutRec.chmel_1       ,' ', 3);\
     memset(sfcctmOutRec.chmel_2       ,' ', 3);\
     memset(sfcctmOutRec.chmel_3       ,' ', 3);\
     memset(sfcctmOutRec.chmel_4       ,' ', 3);\
     memset(sfcctmOutRec.chmel_5       ,' ', 3);\
     memset(sfcctmOutRec.chmel_6       ,' ', 3);\
     memset(sfcctmOutRec.chmel_7       ,' ', 3);\
     memset(sfcctmOutRec.chmel_8       ,' ', 3);\
     memset(sfcctmOutRec.chmel_9       ,' ', 3);\
     memset(sfcctmOutRec.chmel_10      ,' ', 3);\
     memset(sfcctmOutRec.chmel_11      ,' ', 3);\
     memset(sfcctmOutRec.chmel_12      ,' ', 3);\
     memset(sfcctmOutRec.chmel_13      ,' ', 3);\
     memset(sfcctmOutRec.chmel_14      ,' ', 3);\
     memset(sfcctmOutRec.chmel_15      ,' ', 3);\
     memset(sfcctmOutRec.chmel_16      ,' ', 3);\
     memset(sfcctmOutRec.chmel_17      ,' ', 3);\
     memset(sfcctmOutRec.chmel_18      ,' ', 3);\
     memset(sfcctmOutRec.chmel_19      ,' ', 3);\
     memset(sfcctmOutRec.chmel_20      ,' ', 3);\
     memset(sfcctmOutRec.jmy_flg       ,'0', 1);\
     memset(sfcctmOutRec.impc_flg      ,'0', 1);\
     memset(sfcctmOutRec.mincn_flg     ,'0', 1);\
     memset(sfcctmOutRec.htrmt_flg     ,'0', 1);\
     memset(sfcctmOutRec.ce_val_str    ,' ', 7);\
     memset(sfcctmOutRec.di_val_str    ,' ', 7);\
     memset(sfcctmOutRec.jmy_flg2      ,'0', 1);\
     memset(sfcctmOutRec.ex_cert_flg   ,'0', 1);\
     memset(sfcctmOutRec.tst_spec_flg  ,'0', 1);\
     memset(sfcctmOutRec.mtl_std_flg   ,'0', 1);\
}
#endif