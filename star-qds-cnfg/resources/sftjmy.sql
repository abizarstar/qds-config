
if [ "$SERVER" = "INFORMIX" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
then
  echo "INDEXDBS not defined. Aborting.."
  exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'"\
|dbaccess $DATABASE|tail -2|head -1`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=pmd
  fi
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
dbaccess $DATABASE <<!
  create view informix.sftjmy_rec as 
  select * from $PRI_DATABASE@$PRI_IFXSERVER:sftjmy_rec;
  grant all on sftjmy_rec to public as informix;
!
else

#Create the table
dbaccess $DATABASE <<!
	set lock mode to wait;
	CREATE TABLE informix.sftjmy_rec	(
		JMY_CMPY_ID             nchar(3)  NOT NULL,
		JMY_QDS_CTL_NO          decimal(10,0)  NOT NULL,
		JMY_ORD_PFX             nchar(2)  NOT NULL,
		JMY_ORD_NO              decimal(8,0)  NOT NULL,
		JMY_ORD_ITM             decimal(3,0)  NOT NULL,
		JMY_ORD_RLS_NO          decimal(2,0)  NOT NULL,
		JMY_SEL_FLG             nchar(1)  NOT NULL,
		JMY_JMY_TMPL            nchar(3)  NOT NULL,
		JMY_JMY_FLG             decimal(1,0)  NOT NULL,
		JMY_IMPC_FLG            decimal(1,0)  NOT NULL,
		JMY_MINCN_FLG           decimal(1,0)  NOT NULL,
		JMY_HTRMT_FLG           decimal(1,0)  NOT NULL,
		JMY_CHMEL_1             nchar(3)  NOT NULL,
		JMY_CHMEL_2             nchar(3)  NOT NULL,
		JMY_CHMEL_3             nchar(3)  NOT NULL,
		JMY_CHMEL_4             nchar(3)  NOT NULL,
		JMY_CHMEL_5             nchar(3)  NOT NULL,
		JMY_CHMEL_6             nchar(3)  NOT NULL,
		JMY_CHMEL_7             nchar(3)  NOT NULL,
		JMY_CHMEL_8             nchar(3)  NOT NULL,
		JMY_CHMEL_9             nchar(3)  NOT NULL,
		JMY_CHMEL_10            nchar(3)  NOT NULL,
		JMY_CHMEL_11            nchar(3)  NOT NULL,
		JMY_CHMEL_12            nchar(3)  NOT NULL,
		JMY_CHMEL_13            nchar(3)  NOT NULL,
		JMY_CHMEL_14            nchar(3)  NOT NULL,
		JMY_CHMEL_15            nchar(3)  NOT NULL,
		JMY_CHMEL_16            nchar(3)  NOT NULL,
		JMY_CHMEL_17            nchar(3)  NOT NULL,
		JMY_CHMEL_18            nchar(3)  NOT NULL,
		JMY_CHMEL_19            nchar(3)  NOT NULL,
		JMY_CHMEL_20            nchar(3)  NOT NULL
) lock mode ROW;
!

#Create the Primary Key Index and Constraint
dbaccess $DATABASE <<!
set lock mode to wait;
create unique index JMY_KEY on sftjmy_rec( JMY_CMPY_ID, JMY_QDS_CTL_NO ) in $INDEXDBS;
alter table sftjmy_rec add constraint PRIMARY KEY( JMY_CMPY_ID, JMY_QDS_CTL_NO ) constraint sftjmy_pk;
!

fi #if not IWD

fi

if [ "$SERVER" = "POSTGRES" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
  then
    echo "INDEXDBS not defined. Aborting.."
    exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'" $DATABASE`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=pmd
  fi
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
psql $DATABASE <<!
  create foreign table sftjmy_rec (
		JMY_CMPY_ID             char(3),
		JMY_QDS_CTL_NO          decimal(10,0),
		JMY_ORD_PFX             char(2),
		JMY_ORD_NO              decimal(8,0),
		JMY_ORD_ITM             decimal(3,0),
		JMY_ORD_RLS_NO          decimal(2,0),
		JMY_SEL_FLG             char(1),
		JMY_JMY_TMPL            char(3),
		JMY_JMY_FLG             decimal(1,0),
		JMY_IMPC_FLG            decimal(1,0),
		JMY_MINCN_FLG           decimal(1,0),
		JMY_HTRMT_FLG           decimal(1,0),
		JMY_CHMEL_1             char(3),
		JMY_CHMEL_2             char(3),
		JMY_CHMEL_3             char(3),
		JMY_CHMEL_4             char(3),
		JMY_CHMEL_5             char(3),
		JMY_CHMEL_6             char(3),
		JMY_CHMEL_7             char(3),
		JMY_CHMEL_8             char(3),
		JMY_CHMEL_9             char(3),
		JMY_CHMEL_10            char(3),
		JMY_CHMEL_11            char(3),
		JMY_CHMEL_12            char(3),
		JMY_CHMEL_13            char(3),
		JMY_CHMEL_14            char(3),
		JMY_CHMEL_15            char(3),
		JMY_CHMEL_16            char(3),
		JMY_CHMEL_17            char(3),
		JMY_CHMEL_18            char(3),
		JMY_CHMEL_19            char(3),
		JMY_CHMEL_20            char(3)
server ${PRI_PGHOST}${PRI_PGPORT}
options (table_name 'sftjmy_rec');
!
else

#Create the table
psql $DATABASE <<!
	CREATE TABLE sftjmy_rec	(
		JMY_CMPY_ID             char(3)  NOT NULL,
		JMY_QDS_CTL_NO          decimal(10,0)  NOT NULL,
		JMY_ORD_PFX             char(2)  NOT NULL,
		JMY_ORD_NO              decimal(8,0)  NOT NULL,
		JMY_ORD_ITM             decimal(3,0)  NOT NULL,
		JMY_ORD_RLS_NO          decimal(2,0)  NOT NULL,
		JMY_SEL_FLG             char(1)  NOT NULL,
		JMY_JMY_TMPL            char(3)  NOT NULL,
		JMY_JMY_FLG             decimal(1,0)  NOT NULL,
		JMY_IMPC_FLG            decimal(1,0)  NOT NULL,
		JMY_MINCN_FLG           decimal(1,0)  NOT NULL,
		JMY_HTRMT_FLG           decimal(1,0)  NOT NULL,
		JMY_CHMEL_1             char(3)  NOT NULL,
		JMY_CHMEL_2             char(3)  NOT NULL,
		JMY_CHMEL_3             char(3)  NOT NULL,
		JMY_CHMEL_4             char(3)  NOT NULL,
		JMY_CHMEL_5             char(3)  NOT NULL,
		JMY_CHMEL_6             char(3)  NOT NULL,
		JMY_CHMEL_7             char(3)  NOT NULL,
		JMY_CHMEL_8             char(3)  NOT NULL,
		JMY_CHMEL_9             char(3)  NOT NULL,
		JMY_CHMEL_10            char(3)  NOT NULL,
		JMY_CHMEL_11            char(3)  NOT NULL,
		JMY_CHMEL_12            char(3)  NOT NULL,
		JMY_CHMEL_13            char(3)  NOT NULL,
		JMY_CHMEL_14            char(3)  NOT NULL,
		JMY_CHMEL_15            char(3)  NOT NULL,
		JMY_CHMEL_16            char(3)  NOT NULL,
		JMY_CHMEL_17            char(3)  NOT NULL,
		JMY_CHMEL_18            char(3)  NOT NULL,
		JMY_CHMEL_19            char(3)  NOT NULL,
		JMY_CHMEL_20            char(3)  NOT NULL
);
!

#Create the Primary Key Index and Constraint
psql $DATABASE <<!
alter table sftjmy_rec add constraint JMY_KEY PRIMARY KEY( JMY_CMPY_ID, JMY_QDS_CTL_NO ) using index tablespace $INDEXDBS;
!

fi #if not IWD

fi
