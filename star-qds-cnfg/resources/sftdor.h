/*sftdor HEADER FILE */
#ifndef _sftdor
#define _sftdor
#ifndef DOR_CMPY_ID_FIELD_POSITION
#define DOR_CMPY_ID_FIELD_POSITION  0
#endif
#ifndef CMPY_ID_SIZE
#define CMPY_ID_SIZE  3
#endif
#ifndef DOR_DOR_PFX_FIELD_POSITION
#define DOR_DOR_PFX_FIELD_POSITION  3
#endif
#ifndef DOR_PFX_SIZE
#define DOR_PFX_SIZE  2
#endif
#ifndef DOR_DOR_NO_FIELD_POSITION
#define DOR_DOR_NO_FIELD_POSITION  5
#endif
#ifndef DOR_NO_SIZE
#define DOR_NO_SIZE  8
#endif
#ifndef DOR_DOR_ITM_FIELD_POSITION
#define DOR_DOR_ITM_FIELD_POSITION  13
#endif
#ifndef DOR_ITM_SIZE
#define DOR_ITM_SIZE  3
#endif
#ifndef DOR_CHM_SEL_FLG_FIELD_POSITION
#define DOR_CHM_SEL_FLG_FIELD_POSITION  16
#endif
#ifndef CHM_SEL_FLG_SIZE
#define CHM_SEL_FLG_SIZE  1
#endif
#ifndef DOR_JMY_FLG_FIELD_POSITION
#define DOR_JMY_FLG_FIELD_POSITION  17
#endif
#ifndef JMY_FLG_SIZE
#define JMY_FLG_SIZE  1
#endif
#ifndef JMY_FLG_DCML_SIZE
#define JMY_FLG_DCML_SIZE  0
#endif
#ifndef DOR_IMPC_FLG_FIELD_POSITION
#define DOR_IMPC_FLG_FIELD_POSITION  18
#endif
#ifndef IMPC_FLG_SIZE
#define IMPC_FLG_SIZE  1
#endif
#ifndef IMPC_FLG_DCML_SIZE
#define IMPC_FLG_DCML_SIZE  0
#endif
#ifndef DOR_MINCN_FLG_FIELD_POSITION
#define DOR_MINCN_FLG_FIELD_POSITION  19
#endif
#ifndef MINCN_FLG_SIZE
#define MINCN_FLG_SIZE  1
#endif
#ifndef MINCN_FLG_DCML_SIZE
#define MINCN_FLG_DCML_SIZE  0
#endif
#ifndef DOR_HTRMT_FLG_FIELD_POSITION
#define DOR_HTRMT_FLG_FIELD_POSITION  20
#endif
#ifndef HTRMT_FLG_SIZE
#define HTRMT_FLG_SIZE  1
#endif
#ifndef HTRMT_FLG_DCML_SIZE
#define HTRMT_FLG_DCML_SIZE  0
#endif
#ifndef DOR_JMY_FLG2_FIELD_POSITION
#define DOR_JMY_FLG2_FIELD_POSITION  21
#endif
#ifndef JMY_FLG2_SIZE
#define JMY_FLG2_SIZE  1
#endif
#ifndef JMY_FLG2_DCML_SIZE
#define JMY_FLG2_DCML_SIZE  0
#endif
#ifndef DOR_EX_CERT_FLG_FIELD_POSITION
#define DOR_EX_CERT_FLG_FIELD_POSITION  22
#endif
#ifndef EX_CERT_FLG_SIZE
#define EX_CERT_FLG_SIZE  1
#endif
#ifndef EX_CERT_FLG_DCML_SIZE
#define EX_CERT_FLG_DCML_SIZE  0
#endif
#ifndef DOR_TST_SPEC_FLG_FIELD_POSITION
#define DOR_TST_SPEC_FLG_FIELD_POSITION  23
#endif
#ifndef TST_SPEC_FLG_SIZE
#define TST_SPEC_FLG_SIZE  1
#endif
#ifndef TST_SPEC_FLG_DCML_SIZE
#define TST_SPEC_FLG_DCML_SIZE  0
#endif
#ifndef DOR_MTL_STD_FLG_FIELD_POSITION
#define DOR_MTL_STD_FLG_FIELD_POSITION  24
#endif
#ifndef MTL_STD_FLG_SIZE
#define MTL_STD_FLG_SIZE  1
#endif
#ifndef MTL_STD_FLG_DCML_SIZE
#define MTL_STD_FLG_DCML_SIZE  0
#endif
#ifndef DOR_CHMEL_1_FIELD_POSITION
#define DOR_CHMEL_1_FIELD_POSITION  25
#endif
#ifndef CHMEL_1_SIZE
#define CHMEL_1_SIZE  3
#endif
#ifndef DOR_CHMEL_2_FIELD_POSITION
#define DOR_CHMEL_2_FIELD_POSITION  28
#endif
#ifndef CHMEL_2_SIZE
#define CHMEL_2_SIZE  3
#endif
#ifndef DOR_CHMEL_3_FIELD_POSITION
#define DOR_CHMEL_3_FIELD_POSITION  31
#endif
#ifndef CHMEL_3_SIZE
#define CHMEL_3_SIZE  3
#endif
#ifndef DOR_CHMEL_4_FIELD_POSITION
#define DOR_CHMEL_4_FIELD_POSITION  34
#endif
#ifndef CHMEL_4_SIZE
#define CHMEL_4_SIZE  3
#endif
#ifndef DOR_CHMEL_5_FIELD_POSITION
#define DOR_CHMEL_5_FIELD_POSITION  37
#endif
#ifndef CHMEL_5_SIZE
#define CHMEL_5_SIZE  3
#endif
#ifndef DOR_CHMEL_6_FIELD_POSITION
#define DOR_CHMEL_6_FIELD_POSITION  40
#endif
#ifndef CHMEL_6_SIZE
#define CHMEL_6_SIZE  3
#endif
#ifndef DOR_CHMEL_7_FIELD_POSITION
#define DOR_CHMEL_7_FIELD_POSITION  43
#endif
#ifndef CHMEL_7_SIZE
#define CHMEL_7_SIZE  3
#endif
#ifndef DOR_CHMEL_8_FIELD_POSITION
#define DOR_CHMEL_8_FIELD_POSITION  46
#endif
#ifndef CHMEL_8_SIZE
#define CHMEL_8_SIZE  3
#endif
#ifndef DOR_CHMEL_9_FIELD_POSITION
#define DOR_CHMEL_9_FIELD_POSITION  49
#endif
#ifndef CHMEL_9_SIZE
#define CHMEL_9_SIZE  3
#endif
#ifndef DOR_CHMEL_10_FIELD_POSITION
#define DOR_CHMEL_10_FIELD_POSITION  52
#endif
#ifndef CHMEL_10_SIZE
#define CHMEL_10_SIZE  3
#endif
#ifndef DOR_CHMEL_11_FIELD_POSITION
#define DOR_CHMEL_11_FIELD_POSITION  55
#endif
#ifndef CHMEL_11_SIZE
#define CHMEL_11_SIZE  3
#endif
#ifndef DOR_CHMEL_12_FIELD_POSITION
#define DOR_CHMEL_12_FIELD_POSITION  58
#endif
#ifndef CHMEL_12_SIZE
#define CHMEL_12_SIZE  3
#endif
#ifndef DOR_CHMEL_13_FIELD_POSITION
#define DOR_CHMEL_13_FIELD_POSITION  61
#endif
#ifndef CHMEL_13_SIZE
#define CHMEL_13_SIZE  3
#endif
#ifndef DOR_CHMEL_14_FIELD_POSITION
#define DOR_CHMEL_14_FIELD_POSITION  64
#endif
#ifndef CHMEL_14_SIZE
#define CHMEL_14_SIZE  3
#endif
#ifndef DOR_CHMEL_15_FIELD_POSITION
#define DOR_CHMEL_15_FIELD_POSITION  67
#endif
#ifndef CHMEL_15_SIZE
#define CHMEL_15_SIZE  3
#endif
#ifndef DOR_CHMEL_16_FIELD_POSITION
#define DOR_CHMEL_16_FIELD_POSITION  70
#endif
#ifndef CHMEL_16_SIZE
#define CHMEL_16_SIZE  3
#endif
#ifndef DOR_CHMEL_17_FIELD_POSITION
#define DOR_CHMEL_17_FIELD_POSITION  73
#endif
#ifndef CHMEL_17_SIZE
#define CHMEL_17_SIZE  3
#endif
#ifndef DOR_CHMEL_18_FIELD_POSITION
#define DOR_CHMEL_18_FIELD_POSITION  76
#endif
#ifndef CHMEL_18_SIZE
#define CHMEL_18_SIZE  3
#endif
#ifndef DOR_CHMEL_19_FIELD_POSITION
#define DOR_CHMEL_19_FIELD_POSITION  79
#endif
#ifndef CHMEL_19_SIZE
#define CHMEL_19_SIZE  3
#endif
#ifndef DOR_CHMEL_20_FIELD_POSITION
#define DOR_CHMEL_20_FIELD_POSITION  82
#endif
#ifndef CHMEL_20_SIZE
#define CHMEL_20_SIZE  3
#endif
#define sizesftdor 85
#define ssizesftdor "00085"
struct sftdor_rec {
	char cmpy_id[3];
	char ord_pfx[2];
	char ord_no[8];
	char ord_itm[3];
	char chm_sel_flg[1];
	char jmy_flg[1];
	char impc_flg[1];
	char mincn_flg[1];
	char htrmt_flg[1];
	char jmy_flg2[1];
	char ex_cert_flg[1];
	char tst_spec_flg[1];
	char mtl_std_flg[1];
	char chmel_1[3];
	char chmel_2[3];
	char chmel_3[3];
	char chmel_4[3];
	char chmel_5[3];
	char chmel_6[3];
	char chmel_7[3];
	char chmel_8[3];
	char chmel_9[3];
	char chmel_10[3];
	char chmel_11[3];
	char chmel_12[3];
	char chmel_13[3];
	char chmel_14[3];
	char chmel_15[3];
	char chmel_16[3];
	char chmel_17[3];
	char chmel_18[3];
	char chmel_19[3];
	char chmel_20[3];
}; 
#endif
