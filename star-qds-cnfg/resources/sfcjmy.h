#ifndef _sfcjmy
#define _sfcjmy
#define SFCJMY_INP_SIZE             110
#define SFCJMY_INP_SSIZE             "00110"
#define SFCJMY_OUT_SIZE             4
#define SFCJMY_OUT_SSIZE             "00004"
#ifndef CLNT_PGM_NM_SIZE
#define CLNT_PGM_NM_SIZE        14
#endif
#ifndef CLNT_LGN_ID_SIZE
#define CLNT_LGN_ID_SIZE        8
#endif
#ifndef CLNT_CTY_SIZE
#define CLNT_CTY_SIZE        3
#endif
#ifndef CLNT_LNG_SIZE
#define CLNT_LNG_SIZE        2
#endif
#ifndef CLNT_HOST_NM_SIZE
#define CLNT_HOST_NM_SIZE        15
#endif
#ifndef CLNT_TTY_NM_SIZE
#define CLNT_TTY_NM_SIZE        15
#endif
#ifndef CLNT_PID_SIZE
#define CLNT_PID_SIZE        10
#endif
#ifndef CLNT_DTTM_SIZE
#define CLNT_DTTM_SIZE        17
#endif
#ifndef SVC_PGM_NM_SIZE
#define SVC_PGM_NM_SIZE        14
#endif
#ifndef PRS_MD_SIZE
#define PRS_MD_SIZE        1
#endif
#ifndef QDS_CTL_NO_SIZE
#define QDS_CTL_NO_SIZE        10
#endif
#ifndef BORON_FLG_SIZE
#define BORON_FLG_SIZE        1
#endif
#ifndef RTN_STS_SIZE
#define RTN_STS_SIZE        4
#endif
static struct sfcjmy_inp_rec {
     char clnt_pgm_nm[14];
     char clnt_lgn_id[8];
     char clnt_cty[3];
     char clnt_lng[2];
     char clnt_host_nm[15];
     char clnt_tty_nm[15];
     char clnt_pid[10];
     char clnt_dttm[17];
     char svc_pgm_nm[14];
     char prs_md[1];
     char qds_ctl_no[10];
     char boron_flg[1];
}sfcjmyInpRec;
static struct sfcjmy_out_rec {
     char rtn_sts[4];
}sfcjmyOutRec;
#define INIT_SFCJMY_INPUT \
{\
     memset(sfcjmyInpRec.clnt_pgm_nm   ,' ',14);\
     memset(sfcjmyInpRec.clnt_lgn_id   ,' ',8);\
     memset(sfcjmyInpRec.clnt_cty      ,' ',3);\
     memset(sfcjmyInpRec.clnt_lng      ,' ',2);\
     memset(sfcjmyInpRec.clnt_host_nm  ,' ',15);\
     memset(sfcjmyInpRec.clnt_tty_nm   ,' ',15);\
     memset(sfcjmyInpRec.clnt_pid      ,' ',10);\
     memset(sfcjmyInpRec.clnt_dttm     ,'0',17);\
     memset(sfcjmyInpRec.svc_pgm_nm    ,' ',14);\
     memset(sfcjmyInpRec.prs_md        ,' ',1);\
     memset(sfcjmyInpRec.qds_ctl_no    ,'0',10);\
     memset(sfcjmyInpRec.boron_flg     ,'0',1);\
}
#define INIT_SFCJMY_OUTPUT \
{\
     memset(sfcjmyOutRec.rtn_sts       ,'0',4);\
}
#endif