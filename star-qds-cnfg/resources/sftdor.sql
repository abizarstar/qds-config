
if [ "$SERVER" = "INFORMIX" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
then
  echo "INDEXDBS not defined. Aborting.."
  exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'"\
|dbaccess $DATABASE|tail -2|head -1`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=pmd
  fi
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
dbaccess $DATABASE <<!
  create view informix.sftdor_rec as 
  select * from $PRI_DATABASE@$PRI_IFXSERVER:sftdor_rec;
  grant all on sftdor_rec to public as informix;
!
else

#Create the table
dbaccess $DATABASE <<!
	set lock mode to wait;
	CREATE TABLE informix.sftdor_rec	(
		DOR_CMPY_ID             nchar(3)  NOT NULL,
		DOR_ORD_PFX             nchar(2)  NOT NULL,
		DOR_ORD_NO              decimal(8,0)  NOT NULL,
		DOR_ORD_ITM             decimal(3,0)  NOT NULL,
		DOR_CHM_SEL_FLG         nchar(1)  NOT NULL,
		DOR_JMY_FLG             decimal(1,0)  NOT NULL,
		DOR_IMPC_FLG            decimal(1,0)  NOT NULL,
		DOR_MINCN_FLG           decimal(1,0)  NOT NULL,
		DOR_HTRMT_FLG           decimal(1,0)  NOT NULL,
		DOR_JMY_FLG2            decimal(1,0)  NOT NULL,
		DOR_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		DOR_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		DOR_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		DOR_CHMEL_1             nchar(3)  NOT NULL,
		DOR_CHMEL_2             nchar(3)  NOT NULL,
		DOR_CHMEL_3             nchar(3)  NOT NULL,
		DOR_CHMEL_4             nchar(3)  NOT NULL,
		DOR_CHMEL_5             nchar(3)  NOT NULL,
		DOR_CHMEL_6             nchar(3)  NOT NULL,
		DOR_CHMEL_7             nchar(3)  NOT NULL,
		DOR_CHMEL_8             nchar(3)  NOT NULL,
		DOR_CHMEL_9             nchar(3)  NOT NULL,
		DOR_CHMEL_10            nchar(3)  NOT NULL,
		DOR_CHMEL_11            nchar(3)  NOT NULL,
		DOR_CHMEL_12            nchar(3)  NOT NULL,
		DOR_CHMEL_13            nchar(3)  NOT NULL,
		DOR_CHMEL_14            nchar(3)  NOT NULL,
		DOR_CHMEL_15            nchar(3)  NOT NULL,
		DOR_CHMEL_16            nchar(3)  NOT NULL,
		DOR_CHMEL_17            nchar(3)  NOT NULL,
		DOR_CHMEL_18            nchar(3)  NOT NULL,
		DOR_CHMEL_19            nchar(3)  NOT NULL,
		DOR_CHMEL_20            nchar(3)  NOT NULL
) lock mode ROW;
!

#Create the Primary Key Index and Constraint
dbaccess $DATABASE <<!
set lock mode to wait;
create unique index DOR_KEY on sftdor_rec( DOR_CMPY_ID, DOR_ORD_PFX, DOR_ORD_NO, DOR_ORD_ITM ) in $INDEXDBS;
alter table sftdor_rec add constraint PRIMARY KEY( DOR_CMPY_ID, DOR_ORD_PFX, DOR_ORD_NO, DOR_ORD_ITM ) constraint sftdor_pk;
!

fi #if not IWD

fi

if [ "$SERVER" = "POSTGRES" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
  then
    echo "INDEXDBS not defined. Aborting.."
    exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'" $DATABASE`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=pmd
  fi
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
psql $DATABASE <<!
  create foreign table sftdor_rec (
		DOR_CMPY_ID             nchar(3),
		DOR_ORD_PFX             nchar(2),
		DOR_ORD_NO              decimal(8,0),
		DOR_ORD_ITM             decimal(3,0),
		DOR_CHM_SEL_FLG         nchar(1),
		DOR_JMY_FLG             decimal(1,0),
		DOR_IMPC_FLG            decimal(1,0),
		DOR_MINCN_FLG           decimal(1,0),
		DOR_HTRMT_FLG           decimal(1,0),
		DOR_JMY_FLG2            decimal(1,0),
		DOR_EX_CERT_FLG         decimal(1,0),
		DOR_TST_SPEC_FLG        decimal(1,0),
		DOR_MTL_STD_FLG         decimal(1,0),
		DOR_CHMEL_1             nchar(3),
		DOR_CHMEL_2             nchar(3),
		DOR_CHMEL_3             nchar(3),
		DOR_CHMEL_4             nchar(3),
		DOR_CHMEL_5             nchar(3),
		DOR_CHMEL_6             nchar(3),
		DOR_CHMEL_7             nchar(3),
		DOR_CHMEL_8             nchar(3),
		DOR_CHMEL_9             nchar(3),
		DOR_CHMEL_10            nchar(3),
		DOR_CHMEL_11            nchar(3),
		DOR_CHMEL_12            nchar(3),
		DOR_CHMEL_13            nchar(3),
		DOR_CHMEL_14            nchar(3),
		DOR_CHMEL_15            nchar(3),
		DOR_CHMEL_16            nchar(3),
		DOR_CHMEL_17            nchar(3),
		DOR_CHMEL_18            nchar(3),
		DOR_CHMEL_19            nchar(3),
		DOR_CHMEL_20            nchar(3)
server ${PRI_PGHOST}${PRI_PGPORT}
options (table_name 'sftdor_rec');
!
else

#Create the table
psql $DATABASE <<!
	CREATE TABLE sftdor_rec	(
		DOR_CMPY_ID             nchar(3)  NOT NULL,
		DOR_ORD_PFX             nchar(2)  NOT NULL,
		DOR_ORD_NO              decimal(8,0)  NOT NULL,
		DOR_ORD_ITM             decimal(3,0)  NOT NULL,
		DOR_CHM_SEL_FLG         nchar(1)  NOT NULL,
		DOR_JMY_FLG             decimal(1,0)  NOT NULL,
		DOR_IMPC_FLG            decimal(1,0)  NOT NULL,
		DOR_MINCN_FLG           decimal(1,0)  NOT NULL,
		DOR_HTRMT_FLG           decimal(1,0)  NOT NULL,
		DOR_JMY_FLG2            decimal(1,0)  NOT NULL,
		DOR_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		DOR_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		DOR_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		DOR_CHMEL_1             nchar(3)  NOT NULL,
		DOR_CHMEL_2             nchar(3)  NOT NULL,
		DOR_CHMEL_3             nchar(3)  NOT NULL,
		DOR_CHMEL_4             nchar(3)  NOT NULL,
		DOR_CHMEL_5             nchar(3)  NOT NULL,
		DOR_CHMEL_6             nchar(3)  NOT NULL,
		DOR_CHMEL_7             nchar(3)  NOT NULL,
		DOR_CHMEL_8             nchar(3)  NOT NULL,
		DOR_CHMEL_9             nchar(3)  NOT NULL,
		DOR_CHMEL_10            nchar(3)  NOT NULL,
		DOR_CHMEL_11            nchar(3)  NOT NULL,
		DOR_CHMEL_12            nchar(3)  NOT NULL,
		DOR_CHMEL_13            nchar(3)  NOT NULL,
		DOR_CHMEL_14            nchar(3)  NOT NULL,
		DOR_CHMEL_15            nchar(3)  NOT NULL,
		DOR_CHMEL_16            nchar(3)  NOT NULL,
		DOR_CHMEL_17            nchar(3)  NOT NULL,
		DOR_CHMEL_18            nchar(3)  NOT NULL,
		DOR_CHMEL_19            nchar(3)  NOT NULL,
		DOR_CHMEL_20            nchar(3)  NOT NULL
);
!

#Create the Primary Key Index and Constraint
psql $DATABASE <<!
alter table sftdor_rec add constraint DOR_KEY PRIMARY KEY( DOR_CMPY_ID, DOR_ORD_PFX, DOR_ORD_NO, DOR_ORD_ITM ) using index tablespace $INDEXDBS;
!

fi #if not IWD

fi
