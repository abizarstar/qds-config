
if [ "$SERVER" = "INFORMIX" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
then
  echo "INDEXDBS not defined. Aborting.."
  exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'"\
|dbaccess $DATABASE|tail -2|head -1`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=pmd
  fi
  VAR1=`echo "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'"\
|dbaccess $DATABASE|tail -2|head -1`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d @`
  VAR5=`echo $VAR3|cut -f 2 -d @`
  if test "$VAR4" = "$DATABASE" -a "$VAR5" = "$INFORMIXSERVER"
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
dbaccess $DATABASE <<!
  create view informix.sftotm_rec as 
  select * from $PRI_DATABASE@$PRI_IFXSERVER:sftotm_rec;
  grant all on sftotm_rec to public as informix;
!
else

#Create the table
dbaccess $DATABASE <<!
	set lock mode to wait;
	CREATE TABLE informix.sftotm_rec	(
		OTM_CMPY_ID             nchar(3)  NOT NULL,
		OTM_QDS_CTL_NO          decimal(10,0)  NOT NULL,
		OTM_ORD_PFX             nchar(2)  NOT NULL,
		OTM_ORD_NO              decimal(8,0)  NOT NULL,
		OTM_ORD_ITM             decimal(3,0)  NOT NULL,
		OTM_CHM_SEL_FLG         nchar(1)  NOT NULL,
		OTM_JMY_FLG             decimal(1,0)  NOT NULL,
		OTM_IMPC_FLG            decimal(1,0)  NOT NULL,
		OTM_MINCN_FLG           decimal(1,0)  NOT NULL,
		OTM_HTRMT_FLG           decimal(1,0)  NOT NULL,
		OTM_JMY_FLG2            decimal(1,0)  NOT NULL,
		OTM_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		OTM_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		OTM_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		OTM_CHMEL_1             nchar(3)  NOT NULL,
		OTM_CHMEL_2             nchar(3)  NOT NULL,
		OTM_CHMEL_3             nchar(3)  NOT NULL,
		OTM_CHMEL_4             nchar(3)  NOT NULL,
		OTM_CHMEL_5             nchar(3)  NOT NULL,
		OTM_CHMEL_6             nchar(3)  NOT NULL,
		OTM_CHMEL_7             nchar(3)  NOT NULL,
		OTM_CHMEL_8             nchar(3)  NOT NULL,
		OTM_CHMEL_9             nchar(3)  NOT NULL,
		OTM_CHMEL_10            nchar(3)  NOT NULL,
		OTM_CHMEL_11            nchar(3)  NOT NULL,
		OTM_CHMEL_12            nchar(3)  NOT NULL,
		OTM_CHMEL_13            nchar(3)  NOT NULL,
		OTM_CHMEL_14            nchar(3)  NOT NULL,
		OTM_CHMEL_15            nchar(3)  NOT NULL,
		OTM_CHMEL_16            nchar(3)  NOT NULL,
		OTM_CHMEL_17            nchar(3)  NOT NULL,
		OTM_CHMEL_18            nchar(3)  NOT NULL,
		OTM_CHMEL_19            nchar(3)  NOT NULL,
		OTM_CHMEL_20            nchar(3)  NOT NULL
) lock mode ROW;
!

#Create the Primary Key Index and Constraint
dbaccess $DATABASE <<!
set lock mode to wait;
create unique index OTM_KEY on sftotm_rec( OTM_CMPY_ID, OTM_QDS_CTL_NO, OTM_ORD_PFX, OTM_ORD_NO, OTM_ORD_ITM ) in $INDEXDBS;
alter table sftotm_rec add constraint PRIMARY KEY( OTM_CMPY_ID, OTM_QDS_CTL_NO, OTM_ORD_PFX, OTM_ORD_NO, OTM_ORD_ITM ) constraint sftotm_pk;
!

fi #if not IWD

fi

if [ "$SERVER" = "POSTGRES" ]
then

#Determine where to create the indexes
if test "$INDEXDBS" = "" 
  then
    echo "INDEXDBS not defined. Aborting.."
    exit
fi
echo "Indexes will be created in $INDEXDBS"


#Determine the Database Type
VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='ETP'" $DATABASE`
VAR2=`echo $VAR1|cut -f 2 -d " "`
if test "$VAR2" = "GL"
  then DBTYPE=gld
elif test "$VAR2" = "EXC"
  then DBTYPE=exc
elif test "$VAR2" = "MCM"
  then DBTYPE=mcm
elif test "$VAR2" = "STX"
  then DBTYPE=""
else
  echo "Database type unknown! Exiting...."
  exit 1
fi
if test "$VAR2" = "STX"
then
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='PMD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=pmd
  fi
  VAR1=`psql -t -c "select env_idfr_val from sccenv_rec where env_env_idfr='IWD'" $DATABASE`
  VAR3=`echo $VAR1|cut -f 2 -d " "`
  VAR4=`echo $VAR3|cut -f 1 -d "@"`
  if test "$VAR4" = "$DATABASE" 
    then DBTYPE=iwd
  fi
fi
if test "$DBTYPE" = ""
then
  echo "Database type unknown! Exiting...."
exit 1
fi
echo "Database is type $DBTYPE"

#In an IW database, create this table as a view
if test "$DBTYPE" = "iwd"
then
psql $DATABASE <<!
  create foreign table sftotm_rec (
		OTM_CMPY_ID             char(3),
		OTM_QDS_CTL_NO          decimal(10,0),
		OTM_ORD_PFX             char(2),
		OTM_ORD_NO              decimal(8,0),
		OTM_ORD_ITM             decimal(3,0),
		OTM_CHM_SEL_FLG         char(1),
		OTM_JMY_FLG             decimal(1,0),
		OTM_IMPC_FLG            decimal(1,0),
		OTM_MINCN_FLG           decimal(1,0),
		OTM_HTRMT_FLG           decimal(1,0),
		OTM_JMY_FLG2            decimal(1,0),
		OTM_EX_CERT_FLG         decimal(1,0),
		OTM_TST_SPEC_FLG        decimal(1,0),
		OTM_MTL_STD_FLG         decimal(1,0),
		OTM_CHMEL_1             char(3),
		OTM_CHMEL_2             char(3),
		OTM_CHMEL_3             char(3),
		OTM_CHMEL_4             char(3),
		OTM_CHMEL_5             char(3),
		OTM_CHMEL_6             char(3),
		OTM_CHMEL_7             char(3),
		OTM_CHMEL_8             char(3),
		OTM_CHMEL_9             char(3),
		OTM_CHMEL_10            char(3),
		OTM_CHMEL_11            char(3),
		OTM_CHMEL_12            char(3),
		OTM_CHMEL_13            char(3),
		OTM_CHMEL_14            char(3),
		OTM_CHMEL_15            char(3),
		OTM_CHMEL_16            char(3),
		OTM_CHMEL_17            char(3),
		OTM_CHMEL_18            char(3),
		OTM_CHMEL_19            char(3),
		OTM_CHMEL_20            char(3)
server ${PRI_PGHOST}${PRI_PGPORT}
options (table_name 'sftotm_rec');
!
else

#Create the table
psql $DATABASE <<!
	CREATE TABLE sftotm_rec	(
		OTM_CMPY_ID             char(3)  NOT NULL,
		OTM_QDS_CTL_NO          decimal(10,0)  NOT NULL,
		OTM_ORD_PFX             char(2)  NOT NULL,
		OTM_ORD_NO              decimal(8,0)  NOT NULL,
		OTM_ORD_ITM             decimal(3,0)  NOT NULL,
		OTM_CHM_SEL_FLG         char(1)  NOT NULL,
		OTM_JMY_FLG             decimal(1,0)  NOT NULL,
		OTM_IMPC_FLG            decimal(1,0)  NOT NULL,
		OTM_MINCN_FLG           decimal(1,0)  NOT NULL,
		OTM_HTRMT_FLG           decimal(1,0)  NOT NULL,
		OTM_JMY_FLG2            decimal(1,0)  NOT NULL,
		OTM_EX_CERT_FLG         decimal(1,0)  NOT NULL,
		OTM_TST_SPEC_FLG        decimal(1,0)  NOT NULL,
		OTM_MTL_STD_FLG         decimal(1,0)  NOT NULL,
		OTM_CHMEL_1             char(3)  NOT NULL,
		OTM_CHMEL_2             char(3)  NOT NULL,
		OTM_CHMEL_3             char(3)  NOT NULL,
		OTM_CHMEL_4             char(3)  NOT NULL,
		OTM_CHMEL_5             char(3)  NOT NULL,
		OTM_CHMEL_6             char(3)  NOT NULL,
		OTM_CHMEL_7             char(3)  NOT NULL,
		OTM_CHMEL_8             char(3)  NOT NULL,
		OTM_CHMEL_9             char(3)  NOT NULL,
		OTM_CHMEL_10            char(3)  NOT NULL,
		OTM_CHMEL_11            char(3)  NOT NULL,
		OTM_CHMEL_12            char(3)  NOT NULL,
		OTM_CHMEL_13            char(3)  NOT NULL,
		OTM_CHMEL_14            char(3)  NOT NULL,
		OTM_CHMEL_15            char(3)  NOT NULL,
		OTM_CHMEL_16            char(3)  NOT NULL,
		OTM_CHMEL_17            char(3)  NOT NULL,
		OTM_CHMEL_18            char(3)  NOT NULL,
		OTM_CHMEL_19            char(3)  NOT NULL,
		OTM_CHMEL_20            char(3)  NOT NULL
);
!

#Create the Primary Key Index and Constraint
psql $DATABASE <<!
alter table sftotm_rec add constraint OTM_KEY PRIMARY KEY( OTM_CMPY_ID, OTM_QDS_CTL_NO, OTM_ORD_PFX, OTM_ORD_NO, OTM_ORD_ITM ) using index tablespace $INDEXDBS;
!

fi #if not IWD

fi
