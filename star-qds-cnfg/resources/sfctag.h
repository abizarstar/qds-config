#ifndef _sfctag
#define _sfctag
#define SFCTAG_INP_SIZE             10109
#define SFCTAG_INP_SSIZE             "10109"
#define SFCTAG_OUT_SIZE             32004
#define SFCTAG_OUT_SSIZE             "32004"
#ifndef CLNT_PGM_NM_SIZE
#define CLNT_PGM_NM_SIZE        14
#endif
#ifndef CLNT_LGN_ID_SIZE
#define CLNT_LGN_ID_SIZE        8
#endif
#ifndef CLNT_CTY_SIZE
#define CLNT_CTY_SIZE        3
#endif
#ifndef CLNT_LNG_SIZE
#define CLNT_LNG_SIZE        2
#endif
#ifndef CLNT_HOST_NM_SIZE
#define CLNT_HOST_NM_SIZE        15
#endif
#ifndef CLNT_TTY_NM_SIZE
#define CLNT_TTY_NM_SIZE        15
#endif
#ifndef CLNT_PID_SIZE
#define CLNT_PID_SIZE        10
#endif
#ifndef CLNT_DTTM_SIZE
#define CLNT_DTTM_SIZE        17
#endif
#ifndef SVC_PGM_NM_SIZE
#define SVC_PGM_NM_SIZE        14
#endif
#ifndef PRS_MD_SIZE
#define PRS_MD_SIZE        1
#endif
#ifndef QDS_CTL_NO_SIZE
#define QDS_CTL_NO_SIZE        10
#endif
#ifndef ITM_CTL_NO_SIZE
#define ITM_CTL_NO_SIZE        10
#endif
#ifndef RTN_STS_SIZE
#define RTN_STS_SIZE        4
#endif
#ifndef ITM_CTL_NO_SIZE
#define ITM_CTL_NO_SIZE        10
#endif
#ifndef TAG_NO_SIZE
#define TAG_NO_SIZE        12
#endif
#ifndef QDS_CTL_NO_SIZE
#define QDS_CTL_NO_SIZE        10
#endif
struct sfctag_inp_fld_tbl       {
     char itm_ctl_no[10];
};
struct sfctag_out_fld_tbl       {
     char itm_ctl_no[10];
     char tag_no[12];
     char qds_ctl_no[10];
};
static struct sfctag_inp_rec {
     char clnt_pgm_nm[14];
     char clnt_lgn_id[8];
     char clnt_cty[3];
     char clnt_lng[2];
     char clnt_host_nm[15];
     char clnt_tty_nm[15];
     char clnt_pid[10];
     char clnt_dttm[17];
     char svc_pgm_nm[14];
     char prs_md[1];
     char qds_ctl_no[10];
     struct sfctag_inp_fld_tbl        fld_tbl[1000];
}sfctagInpRec;
static struct sfctag_out_rec {
     char rtn_sts[4];
     struct sfctag_out_fld_tbl        fld_tbl[1000];
}sfctagOutRec;
#define INIT_SFCTAG_INPUT \
{\
     int iCtr; \
     for (iCtr=0; iCtr<1000; iCtr++) \
    { \
        memset(sfctagInpRec.fld_tbl[iCtr].itm_ctl_no, '0',10);\
    } \
     memset(sfctagInpRec.clnt_pgm_nm   ,' ',14);\
     memset(sfctagInpRec.clnt_lgn_id   ,' ',8);\
     memset(sfctagInpRec.clnt_cty      ,' ',3);\
     memset(sfctagInpRec.clnt_lng      ,' ',2);\
     memset(sfctagInpRec.clnt_host_nm  ,' ',15);\
     memset(sfctagInpRec.clnt_tty_nm   ,' ',15);\
     memset(sfctagInpRec.clnt_pid      ,' ',10);\
     memset(sfctagInpRec.clnt_dttm     ,'0',17);\
     memset(sfctagInpRec.svc_pgm_nm    ,' ',14);\
     memset(sfctagInpRec.prs_md        ,' ',1);\
     memset(sfctagInpRec.qds_ctl_no    ,'0',10);\
}
#define INIT_SFCTAG_OUTPUT \
{\
     memset(sfctagOutRec.rtn_sts       ,'0',4);\
     int iCtr; \
     for (iCtr=0; iCtr<1000; iCtr++) \
    { \
        memset(sfctagOutRec.fld_tbl[iCtr].itm_ctl_no, '0',10);\
        memset(sfctagOutRec.fld_tbl[iCtr].tag_no, ' ',12);\
        memset(sfctagOutRec.fld_tbl[iCtr].qds_ctl_no, '0',10);\
    } \
}
#endif