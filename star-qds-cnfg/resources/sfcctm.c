/****************************************************************************************
*                            Custom Template Service                                    *
*****************************************************************************************/

/****************************************************************************************
* Created By:  Shilpa Singhai                                                           *
* Date Created: Nov 30, 2021                                                            *
* Release:      20.0                                                                    *
* eCIM Call #:                                                                          *
* Description:  Created New Service for Custom Template Service                         *
*                                                                                       *
\****************************************************************************************/
/* Lint Comments */
/* NOTUSED*/
/* END Lint Comments */

#include "syscom.h"
#include "sfcctm.h"
#include "sftotm.h"
#include "sftctm.h"
#include "intpcr.h"
#include "ortorh.h"
#include "tctipd.h"
#include "arrcus.h"
#include "inrprm.h"
#include "mchqcm.h"
#include "sftdcu.h"
#include "sftdor.h"

/**************** CONSTANTS ****************/

#define PGM_NM									"sfcctm"
#define PRS_MD_FND_OIR								'R'
#define PRS_MD_GET_CUS_ID							'G'
#define PRS_MD_LNK_OIR_TMPL							'1'
#define PRS_MD_GET_CUS_TMPL_INFO					'T'
#define PRS_MD_LNK_CUS_TMPL							'2'
#define PRS_MD_UPD_CUS								'C'
#define PRS_MD_UPD_ORD								'3'
#define PRS_MD_DFLT									'B'
#define PRS_MD_READ									'D'

/**********Variable definition**************/

static struct tctipd_rec pvtTctipd;
static struct intprd_rec pvtIntprd;
static struct ortorh_rec pvtOrtorh;
static struct arrcus_rec pvtArrcus;
static struct inrprm_rec pvtInrprm;
static struct sftotm_rec pvtSftotm;
static struct sftctm_rec pvtSftctm;
static struct mchqcm_rec pvtMchqcm;
static struct sftdor_rec pvtSftdor;
static struct sftdcu_rec pvtSftdcu;
static char pvtsOrdPfx[ORD_PFX_SIZE+1] = "";
static char pvtsOrdNo[ORD_NO_SIZE+1] = "";
static char pvtsOrdItm[ORD_ITM_SIZE+1] = "";
static double pvtdCe = 0.0;
static double pvtdDi = 0.0;


/**************** Function Prototypes ****************/

static void pvtReadTctipd(void);
static void pvtGetFrstIntpcr(void);
static void pvtReadIntprd(char *sItmCtlNo);
static void pvtFindOIR(void);
static void pvtReadOrtorh(void);
static void pvtReadArrcus(void);
static void pvtReadInrprm(void);
static void pvtLinkOIRTemplate(void);
static void pvtLinkCustomerTemplate(void);
static void pvtReadMchqcm(char *sQdsCtlNo);
static void pvtReadSftctm(void);
static void pvtCusTmplInfo(void);
static void pvtGetCusIdPrd(void);
static void pvtUpdCus(void);
static void pvtReadSftdcu(char *sCusId);
static void pvtUpdOrd(void);
static void pvtDflt(void);
static void pvtRtnRec(void);
static void pvtReadSftdor(void);
static void pvtRtnTmplNm(char *sCusId);
static void pvtRtnOirVal(void);

/*------------------------------------------------------------------------*/
/* This is the main function which accepts the input message from the     */
/* client program and returns output.                                     */
/*------------------------------------------------------------------------*/

int sfcctm(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	if (setjmp(pvtReturnJmp) != 0)
	{
		return 0;
	}

	pvtInitVars(rqst);
	c_writelnkarea(PGM_NM, (char *)&sfcctmInpRec, SFCCTM_INP_SSIZE);

	pvtInitLnkVars();
	CHK_SVC_RTN_STS

	pvtBusinessLogic();
	pvtMainLogicEnd();

	return 0;
}

/*------------------------------------------------------------------------*\
 * Function to initialize variables
\*------------------------------------------------------------------------*/

static void pvtInitVars(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = 0;
	pvtInMainLogicEnd[0] = 'N';

	/*** I/P initialization ***/
	pvtLnkArea = rqst;
	memcpy((char *)&sfcctmInpRec, rqst->data, SFCCTM_INP_SIZE);
	
	/*** O/P initialization ***/
	
	INIT_SFCCTM_OUTPUT
	
	pvtOutStr = rqst->data + rqst->len;

	svcInitVars(PGM_NM);
	
	memset(&pvtTctipd, ' ', sizetctipd);
	svcCallPlnsqlio(TRACE_INFO, "tctipd", "1", "1", INL, (char *)&pvtTctipd, ssizetctipd, STXCONT, 1);
	
	memset(&pvtIntprd, ' ', sizeintprd);
	svcCallPlnsqlio(TRACE_INFO, "intprd", "1", "1", INL, (char *)&pvtIntprd, ssizeintprd, STXCONT, 1);
	
	memset(&pvtOrtorh, ' ', sizeortorh);
	svcCallPlnsqlio(TRACE_INFO, "ortorh", "1", "1", INL, (char *)&pvtOrtorh, ssizeortorh, STXCONT, 1);
	
	memset(&pvtArrcus, ' ', sizearrcus);
	svcCallPlnsqlio(TRACE_INFO, "arrcus", "1", "1", INL, (char *)&pvtArrcus, ssizearrcus, STXCONT, 1);
	
	memset(&pvtInrprm, ' ', sizeinrprm);
	svcCallPlnsqlio(TRACE_INFO, "inrprm", "1", "1", INL, (char *)&pvtInrprm, ssizeinrprm, STXCONT, 0);
	
	memset(&pvtSftotm, ' ', sizesftotm);
	svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", INL, (char *)&pvtSftotm, ssizesftotm, STXCONT, 1);
	
	memset(&pvtSftctm, ' ', sizesftctm);
	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", INL, (char *)&pvtSftctm, ssizesftctm, STXCONT, 1);
	
	memset(&pvtMchqcm, ' ', sizemchqcm);
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", INL, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	
	memset(pvtsOrdPfx, '\0', sizeof(pvtsOrdPfx));
	memset(pvtsOrdPfx, ' ', ORD_PFX_SIZE);
	
	memset(pvtsOrdNo, '\0', sizeof(pvtsOrdNo));
	memset(pvtsOrdNo, '0', ORD_NO_SIZE);
	
	memset(pvtsOrdItm, '\0', sizeof(pvtsOrdItm));
	memset(pvtsOrdItm, '0', ORD_ITM_SIZE);
	
	pvtdCe = 0.0;
	pvtdDi = 0.0;
	
	return;
}

/* Function to Initialize request message's numeric fields if not set by the client */

static void pvtInitLnkVars(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.clnt_dttm, CLNT_DTTM_SIZE, "CLNT-DTTM", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.ord_no, ORD_NO_SIZE, "ORD-NO", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.ord_itm, ORD_ITM_SIZE, "ORD-ITM", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE, "QDS-CTL-NO", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.jmy_flg, JMY_FLG_SIZE, "JMY-FLG", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.impc_flg, IMPC_FLG_SIZE, "IMPC-FLG", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE, "MINCN-FLG", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE, "HTRMT-FLG", 0);
	CHK_EXCPT_STS
	
	return;
}

static void pvtBusinessLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtValidateInput();
	CHK_FAIL_STS

	pvtProgramLogic();
	return;
}

/*------------------------------------------------------------------------*\
 * Function to validate
\*------------------------------------------------------------------------*/

static void pvtValidateInput(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	if (pvtLnkArea->len < SFCCTM_INP_SIZE)
	{
		svcUserlog(TRACE_INFO, "Length of Input message is less than the expected length %d < %d", pvtLnkArea->len, SFCCTM_INP_SIZE);
		MAIN_LOGIC_RETURN
	}
	
	/* if process mode is applicable */
	if(sfcctmInpRec.prs_md[0] != PRS_MD_FND_OIR && sfcctmInpRec.prs_md[0] != PRS_MD_GET_CUS_ID && sfcctmInpRec.prs_md[0] != PRS_MD_LNK_OIR_TMPL && sfcctmInpRec.prs_md[0] != PRS_MD_GET_CUS_TMPL_INFO && sfcctmInpRec.prs_md[0] != PRS_MD_LNK_CUS_TMPL && sfcctmInpRec.prs_md[0] != PRS_MD_UPD_CUS && sfcctmInpRec.prs_md[0] != PRS_MD_UPD_ORD && sfcctmInpRec.prs_md[0] != PRS_MD_DFLT && sfcctmInpRec.prs_md[0] != PRS_MD_READ)
	{
		svcLogErrMsg(1, 1, "0000001500", "PRS-MD");
		MAIN_LOGIC_RETURN
	}

	return;
}

static void pvtProgramLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	switch (sfcctmInpRec.prs_md[0])
	{
		case PRS_MD_FND_OIR:
			pvtFindOIR();
			CHK_FAIL_STS
			pvtGetCusIdPrd();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_GET_CUS_ID:
			memcpy(pvtsOrdPfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
			memcpy(pvtsOrdNo, sfcctmInpRec.ord_no, ORD_NO_SIZE);
			memcpy(pvtsOrdItm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
			pvtRtnOirVal();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_LNK_OIR_TMPL:
			pvtLinkOIRTemplate();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_LNK_CUS_TMPL:
			pvtLinkCustomerTemplate();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_GET_CUS_TMPL_INFO:
			pvtCusTmplInfo();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_UPD_CUS:
			pvtUpdCus();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_UPD_ORD:
			memcpy(pvtsOrdPfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
			memcpy(pvtsOrdNo, sfcctmInpRec.ord_no, ORD_NO_SIZE);
			memcpy(pvtsOrdItm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
			pvtUpdOrd();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_DFLT:
			pvtDflt();
			CHK_FAIL_STS
			break;
			
		case PRS_MD_READ:
			pvtRtnRec();
			CHK_FAIL_STS
			break;
			
		default:
			break;
	}
	
	return;
}

static void pvtRtnOirVal(void)
{
	int iOffset = 0;
	int iCtr=0;

	svcSetDbgInfo(TRACE_INFO, "");

	pvtReadSftdor();
	CHK_FAIL_STS
	if (svciSqlIoSts == 0)
	{
		/*CHMEL_1 to 20*/
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdor.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		
		memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdor.chm_sel_flg, CHM_SEL_FLG_SIZE);
		memcpy(sfcctmOutRec.jmy_flg, pvtSftdor.jmy_flg, JMY_FLG_SIZE);
		memcpy(sfcctmOutRec.impc_flg, pvtSftdor.impc_flg, IMPC_FLG_SIZE);
		memcpy(sfcctmOutRec.mincn_flg, pvtSftdor.mincn_flg, MINCN_FLG_SIZE);
		memcpy(sfcctmOutRec.htrmt_flg, pvtSftdor.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(sfcctmOutRec.jmy_flg2, pvtSftdor.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdor.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdor.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdor.mtl_std_flg, MTL_STD_FLG_SIZE);
	}
	else if (svciSqlIoSts == 2)
	{
		pvtReadTctipd();
		CHK_FAIL_STS
		if (svciSqlIoSts == 0)
		{
			pvtReadSftdcu(pvtTctipd.cus_ven_id);
			CHK_FAIL_STS
			if (svciSqlIoSts == 0)
			{
				/*CHMEL_1 to 20*/
				for (iCtr = 0; iCtr < 20; iCtr++)
				{
					memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdcu.chmel_1+iOffset, CHMEL_1_SIZE);
					iOffset = iOffset+CHMEL_1_SIZE;
				}
				
				memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdcu.chm_sel_flg, CHM_SEL_FLG_SIZE);
				memcpy(sfcctmOutRec.jmy_flg, pvtSftdcu.jmy_flg, JMY_FLG_SIZE);
				memcpy(sfcctmOutRec.impc_flg, pvtSftdcu.impc_flg, IMPC_FLG_SIZE);
				memcpy(sfcctmOutRec.mincn_flg, pvtSftdcu.mincn_flg, MINCN_FLG_SIZE);
				memcpy(sfcctmOutRec.htrmt_flg, pvtSftdcu.htrmt_flg, HTRMT_FLG_SIZE);
				memcpy(sfcctmOutRec.jmy_flg2, pvtSftdcu.jmy_flg2, JMY_FLG2_SIZE);
				memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdcu.ex_cert_flg, EX_CERT_FLG_SIZE);
				memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdcu.tst_spec_flg, TST_SPEC_FLG_SIZE);
				memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdcu.mtl_std_flg, MTL_STD_FLG_SIZE);
			}
		}
	}

	pvtGetCusIdPrd();
	CHK_FAIL_STS

	return;
}

static void pvtRtnRec(void)
{
	int iOffset = 0;
	int iCtr = 0;
	svcSetDbgInfo(TRACE_INFO, "");

	pvtReadSftdcu(sfcctmInpRec.cus_id);
	CHK_FAIL_STS
	if (svciSqlIoSts == 0)
	{
		/*CHMEL_1 to 20*/
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdcu.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		
		memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdcu.chm_sel_flg, CHM_SEL_FLG_SIZE);
		memcpy(sfcctmOutRec.jmy_flg, pvtSftdcu.jmy_flg, JMY_FLG_SIZE);
		memcpy(sfcctmOutRec.impc_flg, pvtSftdcu.impc_flg, IMPC_FLG_SIZE);
		memcpy(sfcctmOutRec.mincn_flg, pvtSftdcu.mincn_flg, MINCN_FLG_SIZE);
		memcpy(sfcctmOutRec.htrmt_flg, pvtSftdcu.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(sfcctmOutRec.jmy_flg2, pvtSftdcu.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdcu.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdcu.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdcu.mtl_std_flg, MTL_STD_FLG_SIZE);
	}

	return;
}

static void pvtDflt(void)
{
	int iOffset = 0;
	int iCtr = 0;
	char sCusIdStr[CUS_ID_STR_SIZE+1]= "";
	char sCusId[CUS_VEN_ID_SIZE+1] = "";
	char sCusLongNm[CUS_LONG_NM_SIZE+1] = "";
	svcSetDbgInfo(TRACE_INFO, "");

	if (memcmp(sfcctmInpRec.ord_pfx, SPACES, ORD_PFX_SIZE) == 0)
	{
		pvtReadSftdcu(sfcctmInpRec.cus_id);
		CHK_FAIL_STS
		if (svciSqlIoSts == 0)
		{
			/*CHMEL_1 to 20*/
			for (iCtr = 0; iCtr < 20; iCtr++)
			{
				memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdcu.chmel_1+iOffset, CHMEL_1_SIZE);
				iOffset = iOffset+CHMEL_1_SIZE;
			}
			
			memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdcu.chm_sel_flg, CHM_SEL_FLG_SIZE);
			memcpy(sfcctmOutRec.jmy_flg, pvtSftdcu.jmy_flg, JMY_FLG_SIZE);
			memcpy(sfcctmOutRec.impc_flg, pvtSftdcu.impc_flg, IMPC_FLG_SIZE);
			memcpy(sfcctmOutRec.mincn_flg, pvtSftdcu.mincn_flg, MINCN_FLG_SIZE);
			memcpy(sfcctmOutRec.htrmt_flg, pvtSftdcu.htrmt_flg, HTRMT_FLG_SIZE);
			memcpy(sfcctmOutRec.jmy_flg2, pvtSftdcu.jmy_flg2, JMY_FLG2_SIZE);
			memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdcu.ex_cert_flg, EX_CERT_FLG_SIZE);
			memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdcu.tst_spec_flg, TST_SPEC_FLG_SIZE);
			memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdcu.mtl_std_flg, MTL_STD_FLG_SIZE);
		}
	}
	else
	{
		memcpy(pvtsOrdPfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
		memcpy(pvtsOrdNo, sfcctmInpRec.ord_no, ORD_NO_SIZE);
		memcpy(pvtsOrdItm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
		pvtReadTctipd();
		CHK_FAIL_STS
		if (svciSqlIoSts == 0)
		{
			/*ORD-PFX*/
			memcpy(sfcctmOutRec.ord_pfx, pvtsOrdPfx, ORD_PFX_SIZE);
			
			/*ORD-NO*/
			memcpy(sfcctmOutRec.ord_no, pvtsOrdNo, ORD_NO_SIZE);
			
			/*ORD-ITM*/
			memcpy(sfcctmOutRec.ord_itm, pvtsOrdItm, ORD_ITM_SIZE);

			/*CUS-ID*/
			memcpy(sfcctmOutRec.cus_id, pvtTctipd.cus_ven_id, CUS_VEN_ID_SIZE);
			
			/*CUS-ID-STR*/
			pvtReadArrcus();
			CHK_FAIL_STS
			if (svciSqlIoSts == 0)
			{
				memset(sCusIdStr, '\0', sizeof(sCusIdStr));
				memset(sCusId, '\0', sizeof(sCusId));
				memset(sCusLongNm, '\0', sizeof(sCusLongNm));
				RTrim(sCusId, sizeof(sCusId), pvtTctipd.cus_ven_id, CUS_VEN_ID_SIZE);
				RTrim(sCusLongNm, sizeof(sCusLongNm), pvtArrcus.cus_long_nm, CUS_LONG_NM_SIZE);
				svcConcat4(sCusIdStr, sizeof(sCusIdStr), "(", 1, sCusId, (int)strlen(sCusId), ") ", 2, sCusLongNm, (int)strlen(sCusLongNm));
				memcpy(sfcctmOutRec.cus_id_str, sCusIdStr, (int)strnlen(sCusIdStr, CUS_ID_STR_SIZE));
			}

			pvtReadSftdor();
			CHK_FAIL_STS
			if (svciSqlIoSts == 0)
			{
				/*CHMEL_1 to 20*/
				for (iCtr = 0; iCtr < 20; iCtr++)
				{
					memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdor.chmel_1+iOffset, CHMEL_1_SIZE);
					iOffset = iOffset+CHMEL_1_SIZE;
				}
				
				memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdor.chm_sel_flg, CHM_SEL_FLG_SIZE);
				memcpy(sfcctmOutRec.jmy_flg, pvtSftdor.jmy_flg, JMY_FLG_SIZE);
				memcpy(sfcctmOutRec.impc_flg, pvtSftdor.impc_flg, IMPC_FLG_SIZE);
				memcpy(sfcctmOutRec.mincn_flg, pvtSftdor.mincn_flg, MINCN_FLG_SIZE);
				memcpy(sfcctmOutRec.htrmt_flg, pvtSftdor.htrmt_flg, HTRMT_FLG_SIZE);
				memcpy(sfcctmOutRec.jmy_flg2, pvtSftdor.jmy_flg2, JMY_FLG2_SIZE);
				memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdor.ex_cert_flg, EX_CERT_FLG_SIZE);
				memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdor.tst_spec_flg, TST_SPEC_FLG_SIZE);
				memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdor.mtl_std_flg, MTL_STD_FLG_SIZE);
			}
			else
			{
				pvtReadSftdcu(pvtTctipd.cus_ven_id);
				CHK_FAIL_STS
				if (svciSqlIoSts == 0)
				{
					/*CHMEL_1 to 20*/
					for (iCtr = 0; iCtr < 20; iCtr++)
					{
						memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftdcu.chmel_1+iOffset, CHMEL_1_SIZE);
						iOffset = iOffset+CHMEL_1_SIZE;
					}
					
					memcpy(sfcctmOutRec.chm_sel_flg, pvtSftdcu.chm_sel_flg, CHM_SEL_FLG_SIZE);
					memcpy(sfcctmOutRec.jmy_flg, pvtSftdcu.jmy_flg, JMY_FLG_SIZE);
					memcpy(sfcctmOutRec.impc_flg, pvtSftdcu.impc_flg, IMPC_FLG_SIZE);
					memcpy(sfcctmOutRec.mincn_flg, pvtSftdcu.mincn_flg, MINCN_FLG_SIZE);
					memcpy(sfcctmOutRec.htrmt_flg, pvtSftdcu.htrmt_flg, HTRMT_FLG_SIZE);
					memcpy(sfcctmOutRec.jmy_flg2, pvtSftdcu.jmy_flg2, JMY_FLG2_SIZE);
					memcpy(sfcctmOutRec.ex_cert_flg, pvtSftdcu.ex_cert_flg, EX_CERT_FLG_SIZE);
					memcpy(sfcctmOutRec.tst_spec_flg, pvtSftdcu.tst_spec_flg, TST_SPEC_FLG_SIZE);
					memcpy(sfcctmOutRec.mtl_std_flg, pvtSftdcu.mtl_std_flg, MTL_STD_FLG_SIZE);
				}
			}
		}
	}

	return;
}

static void pvtUpdOrd(void)
{
	int iOffset = 0;
	int iCtr = 0;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK

	pvtReadSftdor();
	CHK_FAIL_STS
	if(svciSqlIoSts == 2)
	{
		memset(&pvtSftdor, ' ', sizesftdor);
		svcCallPlnsqlio(TRACE_INFO, "sftdor", "1", "1", INL, (char *)&pvtSftdor, ssizesftdor, STXCONT, 1);
		
		memcpy(pvtSftdor.ord_pfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
		memcpy(pvtSftdor.ord_no, sfcctmInpRec.ord_no, ORD_NO_SIZE);
		memcpy(pvtSftdor.ord_itm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftdor.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftdor.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftdor.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftdor.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftdor.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftdor.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftdor.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftdor.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftdor.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftdor", "1", "1", INS, (char *)&pvtSftdor, ssizesftdor, STXERR, 1);
		CHK_SQL_IO_STS
	}
	else
	{
		memcpy(pvtSftdor.ord_pfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
		memcpy(pvtSftdor.ord_no, sfcctmInpRec.ord_no, ORD_NO_SIZE);
		memcpy(pvtSftdor.ord_itm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftdor.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftdor.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftdor.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftdor.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftdor.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftdor.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftdor.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftdor.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftdor.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftdor", "1", "1", UPD, (char *)&pvtSftdor, ssizesftdor, STXERR, 1);
		CHK_SQL_IO_STS
	}

	COMMIT_WORK

	return;
}

static void pvtReadSftdor(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	memset(&pvtSftdor, ' ', sizesftdor);
	svcCallPlnsqlio(TRACE_INFO, "sftdor", "1", "1", INL, (char *)&pvtSftdor, ssizesftdor, STXCONT, 1);

	memcpy(pvtSftdor.ord_pfx, pvtsOrdPfx, ORD_PFX_SIZE);
	memcpy(pvtSftdor.ord_no, pvtsOrdNo, ORD_NO_SIZE);
	memcpy(pvtSftdor.ord_itm, pvtsOrdItm, ORD_ITM_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "sftdor", "1", "1", RD, (char *)&pvtSftdor, ssizesftdor, STXCONT, 1);
	CHK_SQL_IO_STS

	return;
}

static void pvtUpdCus(void)
{
	int iOffset = 0;
	int iCtr = 0;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK

	pvtReadSftdcu(sfcctmInpRec.cus_id);
	CHK_FAIL_STS
	if(svciSqlIoSts == 2)
	{
		memset(&pvtSftdcu, ' ', sizesftdcu);
		svcCallPlnsqlio(TRACE_INFO, "sftdcu", "1", "1", INL, (char *)&pvtSftdcu, ssizesftdcu, STXCONT, 1);
		
		memcpy(pvtSftdcu.cus_id, sfcctmInpRec.cus_id, CUS_ID_SIZE);
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftdcu.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftdcu.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftdcu.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftdcu.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftdcu.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftdcu.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftdcu.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftdcu.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftdcu.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftdcu", "1", "1", INS, (char *)&pvtSftdcu, ssizesftdcu, STXERR, 1);
		CHK_SQL_IO_STS
	}
	else
	{
		memcpy(pvtSftdcu.cus_id, sfcctmInpRec.cus_id, CUS_ID_SIZE);
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftdcu.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftdcu.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftdcu.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftdcu.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftdcu.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftdcu.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftdcu.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftdcu.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftdcu.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftdcu", "1", "1", UPD, (char *)&pvtSftdcu, ssizesftdcu, STXERR, 1);
		CHK_SQL_IO_STS
	}

	COMMIT_WORK

	return;
}

static void pvtReadSftdcu(char *sCusId)
{
	svcSetDbgInfo(TRACE_INFO, "");

	memset(&pvtSftdcu, ' ', sizesftdcu);
	svcCallPlnsqlio(TRACE_INFO, "sftdcu", "1", "1", INL, (char *)&pvtSftdcu, ssizesftdcu, STXCONT, 1);

	memcpy(pvtSftdcu.cus_id, sCusId, CUS_ID_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "sftdcu", "1", "1", RD, (char *)&pvtSftdcu, ssizesftdcu, STXCONT, 1);
	CHK_SQL_IO_STS

	return;
}

static void pvtFindOIR(void)
{
	char sOutRtnMsg[69] = "";
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(sOutRtnMsg, '\0', sizeof(sOutRtnMsg));
	
	pvtGetFrstIntpcr();
	CHK_FAIL_STS
	
	pvtReadOrtorh();
	CHK_FAIL_STS
	if (svciSqlIoSts == 0)
	{
		if(pvtOrtorh.ord_typ[0] != 'N' && pvtOrtorh.ord_typ[0] != 'J' && pvtOrtorh.ord_typ[0] != 'B' && pvtOrtorh.ord_typ[0] != 'D')
		{
			memcpy(sOutRtnMsg, "Template can only be linked for orders with order types N,J, B and D", 68);
			svcLogErrMsg(0, 0, "0000000000", sOutRtnMsg);
			MAIN_LOGIC_RETURN
		}
	}
	return;
}

static void pvtReadOrtorh(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	memset(&pvtOrtorh, ' ', sizeortorh);
	svcCallPlnsqlio(TRACE_INFO, "ortorh", "1", "1", INL, (char *)&pvtOrtorh, ssizeortorh, STXCONT, 1);

	memcpy(pvtOrtorh.ord_pfx, pvtsOrdPfx, ORD_PFX_SIZE);
	memcpy(pvtOrtorh.ord_no, pvtsOrdNo, ORD_NO_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "ortorh", "1", "1", RD, (char *)&pvtOrtorh, ssizeortorh, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtGetFrstIntpcr(void)
{
	char sOutRtnMsg[63] = "";
	struct intpcr_rec intpcr;
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(sOutRtnMsg, '\0', sizeof(sOutRtnMsg));
	
	memset(&intpcr, ' ', sizeintpcr);
	svcCallPlnsqlio(TRACE_INFO, "intpcr", "1", "3", INL, (char *)&intpcr, ssizeintpcr, STXCONT, 1);
	
	memcpy(intpcr.qds_ctl_no, sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "intpcr", "1", "3", SGE, (char *)&intpcr, ssizeintpcr, STXCONT, 1);
	CHK_SQL_IO_STS

	svcCallPlnsqlio(TRACE_INFO, "intpcr", "1", "3", RDN, (char *)&intpcr, ssizeintpcr, STXCONT, 1);

	CHK_SQL_IO_STS

	if (svciSqlIoSts == 0 && memcmp(intpcr.qds_ctl_no, sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE) == 0)
	{
		pvtReadIntprd(intpcr.itm_ctl_no);
		CHK_FAIL_STS
		if (svciSqlIoSts == 0)
		{
			if(memcmp(pvtIntprd.ord_ffm, SPACES, ORD_FFM_SIZE) == 0)
			{
				memcpy(sOutRtnMsg, "QDS cannot be linked with order.Please enter sales order Item.", 62);
				svcLogErrMsg(0, 0, "0000000000", sOutRtnMsg);
				MAIN_LOGIC_RETURN
			}
			
			memcpy(pvtsOrdPfx, pvtIntprd.ord_ffm, ORD_PFX_SIZE);
			memcpy(pvtsOrdNo, pvtIntprd.ord_ffm+ORD_PFX_SIZE, ORD_NO_SIZE);
			memcpy(pvtsOrdItm, pvtIntprd.ord_ffm+ORD_PFX_SIZE+ORD_NO_SIZE, ORD_ITM_SIZE);
		}
	}

	svcCallPlnsqlio(TRACE_INFO, "intpcr", "1", "3", CLS, (char *)&intpcr, ssizeintpcr, STXCONT, 1);
	
	return;
}

static void pvtReadIntprd(char *sItmCtlNo)
{	
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtIntprd, ' ', sizeintprd);
	svcCallPlnsqlio(TRACE_INFO, "intprd", "1", "1", INL, (char *)&pvtIntprd, ssizeintprd, STXCONT, 1);
	
	memcpy(pvtIntprd.itm_ctl_no, sItmCtlNo, ITM_CTL_NO_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "intprd", "1", "1", RD, (char *)&pvtIntprd, ssizeintprd, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtGetCusIdPrd(void)
{
	char sCusIdStr[CUS_ID_STR_SIZE+1]= "";
	char sCusId[CUS_VEN_ID_SIZE+1] = "";
	char sCusLongNm[CUS_LONG_NM_SIZE+1] = "";
	char sFrm[FRM_SIZE+1] = "";
	char sGrd[GRD_SIZE+1] = "";
	char sSize[SIZE_SIZE+1] = "";
	char sFnsh[FNSH_SIZE+1] = "";
	char sWdth[21] = "";
	char sLgth[21] = "";
	char sProdStrng[PRD_STRNG_SIZE+1]= "";
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	pvtReadTctipd();
	CHK_FAIL_STS
	if (svciSqlIoSts == 0)
	{
		/*ORD-PFX*/
		memcpy(sfcctmOutRec.ord_pfx, pvtsOrdPfx, ORD_PFX_SIZE);
		
		/*ORD-NO*/
		memcpy(sfcctmOutRec.ord_no, pvtsOrdNo, ORD_NO_SIZE);
		
		/*ORD-ITM*/
		memcpy(sfcctmOutRec.ord_itm, pvtsOrdItm, ORD_ITM_SIZE);
		
		/*CUS-ID*/
		memcpy(sfcctmOutRec.cus_id, pvtTctipd.cus_ven_id, CUS_VEN_ID_SIZE);
		
		/*CUS-ID-STR*/
		pvtReadArrcus();
		CHK_FAIL_STS
		if (svciSqlIoSts == 0)
		{
			memset(sCusIdStr, '\0', sizeof(sCusIdStr));
			memset(sCusId, '\0', sizeof(sCusId));
			memset(sCusLongNm, '\0', sizeof(sCusLongNm));
			RTrim(sCusId, sizeof(sCusId), pvtTctipd.cus_ven_id, CUS_VEN_ID_SIZE);
			RTrim(sCusLongNm, sizeof(sCusLongNm), pvtArrcus.cus_long_nm, CUS_LONG_NM_SIZE);
			svcConcat4(sCusIdStr, sizeof(sCusIdStr), "(", 1, sCusId, (int)strlen(sCusId), ") ", 2, sCusLongNm, (int)strlen(sCusLongNm));
			memcpy(sfcctmOutRec.cus_id_str, sCusIdStr, (int)strnlen(sCusIdStr, CUS_ID_STR_SIZE));
		}
		
		/*PRD-STRNG*/
		memset(sFrm, '\0', sizeof(sFrm));
		memset(sGrd, '\0', sizeof(sGrd));
		memset(sSize, '\0', sizeof(sSize));
		memset(sFnsh, '\0', sizeof(sFnsh));
		memset(sWdth, '\0', sizeof(sWdth));
		memset(sLgth, '\0', sizeof(sLgth));
		memset(sProdStrng, '\0', sizeof(sProdStrng));
		memcpy(sFrm, pvtTctipd.frm, FRM_SIZE);
		memcpy(sGrd, pvtTctipd.grd, GRD_SIZE);
		memcpy(sSize, pvtTctipd.size, SIZE_SIZE);
		memcpy(sFnsh, pvtTctipd.fnsh, FNSH_SIZE);
		RemoveEndSpaces(sFrm);
		RemoveEndSpaces(sGrd);
		RemoveEndSpaces(sSize);
		RemoveEndSpaces(sFnsh);
		
		pvtReadInrprm();
		CHK_FAIL_STS
		
		if(memcmp(pvtTctipd.wdth, ZEROS, WDTH_SIZE) != 0)
		{
			svcFmtDimension(sWdth, 20, "", 0, "", 0, 0, pvtTctipd.wdth, WDTH_SIZE, WDTH_DCML_SIZE, "WDT", svcScrcsc.bas_msr, pvtInrprm.dim_seg, "   ", pvtInrprm.lgth_disp_fmt, "0", "0","0","0");
		}

		if(memcmp(pvtTctipd.lgth, ZEROS, LGTH_SIZE) !=0 )
		{
			svcFmtDimension(sLgth, 20, "", 0, "", 0, 0, pvtTctipd.lgth, LGTH_SIZE, LGTH_DCML_SIZE, "LGT", svcScrcsc.bas_msr, pvtInrprm.dim_seg, "   ", pvtInrprm.lgth_disp_fmt, "0", "0","0","0");
		}
		
		if(sFrm[0] !='\0')
		{
			strcpy(sProdStrng, sFrm);
		}
	
		if(sGrd[0]!='\0')
		{
			if(sProdStrng[0] !='\0')
			{
				strcat(sProdStrng, "-");
			}
			strcat(sProdStrng, sGrd);
		}
	
		if(sSize[0] !='\0')
		{
			if(sProdStrng[0] !='\0')
			{
				strcat(sProdStrng, "-");
			}
			strcat(sProdStrng, sSize);
		} 
		
		if(sFnsh[0] !='\0')
		{
			if(sProdStrng[0] !='\0')
			{
				strcat(sProdStrng, "-");
			}
			strcat(sProdStrng, sFnsh);
		} 
		
		if(sWdth[0] !='\0')
		{
			if(sProdStrng[0] !='\0')
			{
				strcat(sProdStrng, "-");
			}
			strcat(sProdStrng, sWdth);
		} 
		
		if(sLgth[0] !='\0')
		{
			if(sProdStrng[0] !='\0')
			{
				strcat(sProdStrng, "-");
			}
			strcat(sProdStrng, sLgth);
		}

		memcpy(sfcctmOutRec.prd_strng, sProdStrng, (int)strnlen(sProdStrng, PRD_STRNG_SIZE));

		pvtRtnTmplNm(pvtTctipd.cus_ven_id);
		CHK_FAIL_STS
	}

	return;
}


static void pvtRtnTmplNm(char *sCusId)
{
	int iCtr = 0;

	struct sftctm_rec sftctm;
	
	svcSetDbgInfo(TRACE_INFO, "");

	/*CUS-TMPL-NM*/
	memset(&sftctm, ' ', sizesftctm);
	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", INL, (char *)&sftctm, ssizesftctm, STXCONT, 1);

	memcpy(sftctm.cus_id, sCusId, CUS_ID_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", SGE, (char *)&pvtSftctm, ssizesftctm, STXCONT, 1);
	CHK_SQL_IO_STS
	
	if(svciSqlIoSts != 0)
	{
		return;
	}
	
	while(1)
	{
		if(svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", RDN, (char *)&sftctm, ssizesftctm, STXCONT, 1) != 0 || memcmp(sftctm.cus_id, pvtTctipd.cus_ven_id, CUS_ID_SIZE) != 0)
		{
			CHK_SQL_IO_STS
			break;
		}

		memcpy(sfcctmOutRec.fld_tbl[iCtr].cus_tmpl_nm, sftctm.cus_tmpl_nm, CUS_TMPL_NM_SIZE);

		iCtr++;
		
		if(iCtr == 20)
		{
			break;
		}
	}

	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", CLS, (char *)&sftctm, ssizesftctm, STXCONT, 1);

/*	if (iCtr == 1)
	{
		pvtCusTmplInfo();
		CHK_FAIL_STS
	}*/

	return;
}

static void pvtReadInrprm(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	memset(&pvtInrprm, ' ', sizeinrprm);
	svcCallPlnsqlio(TRACE_INFO, "inrprm", "1", "1", INL, (char *)&pvtInrprm, ssizeinrprm, STXCONT, 0);
	
	memcpy(pvtInrprm.frm, pvtTctipd.frm, FRM_SIZE);
	memcpy(pvtInrprm.grd, pvtTctipd.grd, GRD_SIZE);
	memcpy(pvtInrprm.size, pvtTctipd.size, SIZE_SIZE);
	memcpy(pvtInrprm.fnsh, pvtTctipd.fnsh, FNSH_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "inrprm", "1", "1", RD, (char *)&pvtInrprm, ssizeinrprm, STXCONT, 0);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtReadArrcus(void)
{	
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtArrcus, ' ', sizearrcus);
	svcCallPlnsqlio(TRACE_INFO, "arrcus", "1", "1", INL, (char *)&pvtArrcus, ssizearrcus, STXCONT, 1);
	
	memcpy(pvtArrcus.cus_id, pvtTctipd.cus_ven_id, CUS_VEN_ID_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "arrcus", "1", "1", RD, (char *)&pvtArrcus, ssizearrcus, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtReadTctipd(void)
{	
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtTctipd, ' ', sizetctipd);
	svcCallPlnsqlio(TRACE_INFO, "tctipd", "1", "1", INL, (char *)&pvtTctipd, ssizetctipd, STXCONT, 1);
	
	memcpy(pvtTctipd.ref_pfx, pvtsOrdPfx, REF_PFX_SIZE);
	memcpy(pvtTctipd.ref_no, pvtsOrdNo, REF_NO_SIZE);
	memcpy(pvtTctipd.ref_itm, pvtsOrdItm, REF_ITM_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "tctipd", "1", "1", RD, (char *)&pvtTctipd, ssizetctipd, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtLinkOIRTemplate(void)
{
	int iCtr = 0;
	int iOffset = 0;
	int iNbrCe =0;
	double dC = 0.0; 
	double dMn = 0.0; 
	double dCr = 0.0; 
	double dMo = 0.0; 
	double dV = 0.0; 
	double dCu = 0.0; 
	double dNi = 0.0; 
	double dSi = 0.0;
	char sValStr[DI_VAL_STR_SIZE*4+3]= "";
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	pvtReadMchqcm(sfcctmInpRec.qds_ctl_no);
	CHK_FAIL_STS
	if(svciSqlIoSts == 0)
	{
		for (iCtr = 0, iOffset = 0; iCtr < 20;)
		{
			if(memcmp(pvtMchqcm.chmel_1+iOffset, "C  ", CHMEL_1_SIZE) == 0)
			{
				dC = 0.54 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mn ", CHMEL_1_SIZE) == 0)
			{
				dMn = 3.3333 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cr ", CHMEL_1_SIZE) == 0)
			{
				dCr = 2.16 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mo ", CHMEL_1_SIZE) == 0)
			{
				dMo = 3 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "V  ", CHMEL_1_SIZE) == 0)
			{
				dV = 1.73 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Ni ", CHMEL_1_SIZE) == 0)
			{
				dNi = 0.363 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cu ", CHMEL_1_SIZE) == 0)
			{
				dCu = 0.365 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Si ", CHMEL_1_SIZE) == 0)
			{
				dSi = 0.7 * svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
		
			if((iNbrCe == 8) || memcmp(pvtMchqcm.chmel_1+iOffset, SPACES, CHMEL_1_SIZE) == 0)
			{
				break;
			}
			
			iCtr++;

			iOffset += QCM_CHMEL_2_FIELD_POSITION - QCM_CHMEL_1_FIELD_POSITION;
		}
	
		if(dC != 0.0)
		{
			pvtdDi = dC;
		}
	
		if(dSi != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dSi;
			}
			else
			{
				pvtdDi = dSi;
			}
		}
	
		if(dMn != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dMn;
			}
			else
			{
				pvtdDi = dMn;
			}
		}
	
		if(dCr != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dCr;
			}
			else
			{
				pvtdDi = dCr;
			}
		}
	
		if(dMo != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dMo;
			}
			else
			{
				pvtdDi = dMo;
			}
		}
	
		if(dNi != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dNi;
			}
			else
			{
				pvtdDi = dNi;
			}
		}
	
		if(dCu != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dCu;
			}
			else
			{
				pvtdDi = dCu;
			}
		}
	
		if(dV != 0.0)
		{
			if(pvtdDi != 0.0)
			{
				pvtdDi = pvtdDi * dV;
			}
			else
			{
				pvtdDi = dV;
			}
		}
		
		dC =  0.0;  
		dMn = 0.0; 
		dCr = 0.0; 
		dMo = 0.0; 
		dV =  0.0; 
		dCu = 0.0; 
		dNi = 0.0; 
		dSi = 0.0;
		iNbrCe = 0;
	
		for (iCtr = 0, iOffset = 0; iCtr < 20;)
		{
			if(memcmp(pvtMchqcm.chmel_1+iOffset, "C  ", CHMEL_1_SIZE) == 0)
			{
				dC = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mn ", CHMEL_1_SIZE) == 0)
			{
				dMn = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cr ", CHMEL_1_SIZE) == 0)
			{
				dCr = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mo ", CHMEL_1_SIZE) == 0)
			{
				dMo = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "V  ", CHMEL_1_SIZE) == 0)
			{
				dV = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Ni ", CHMEL_1_SIZE) == 0)
			{
				dNi = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cu ", CHMEL_1_SIZE) == 0)
			{
				dCu = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
		
			if((iNbrCe == 7) || memcmp(pvtMchqcm.chmel_1+iOffset, SPACES, CHMEL_1_SIZE) == 0)
			{
				break;
			}
			
			iCtr++;

			iOffset += QCM_CHMEL_2_FIELD_POSITION - QCM_CHMEL_1_FIELD_POSITION;
		}
	
		if(dC != 0.0)
		{
			pvtdCe = dC;
		}
	
		if(dMn != 0.0)
		{
			pvtdCe = pvtdCe + (dMn/6);
		}
	
		if(dCr != 0.0 || dMo != 0.0 || dV != 0.0)
		{
			pvtdCe = pvtdCe + ((dCr + dMo + dV)/5) ;
		}
	
		if(dNi != 0.0 || dCu != 0.0)
		{
			pvtdCe = pvtdCe + ((dNi+dCu)/15);
		}
	}

	/*CE-VAL-STR*/
	memset(sValStr, '\0', sizeof(sValStr));
	svcDblToStr(sValStr, sizeof(sValStr), pvtdCe, 2, 1, 0);
	memcpy(sfcctmOutRec.ce_val_str, sValStr, (int)strnlen(sValStr, CE_VAL_STR_SIZE));
	
	/*DE-VAL-STR*/
	memset(sValStr, '\0', sizeof(sValStr));
	svcDblToStr(sValStr, sizeof(sValStr), pvtdDi, 2, 1, 0);
	memcpy(sfcctmOutRec.di_val_str, sValStr, (int)strnlen(sValStr, DI_VAL_STR_SIZE));
	
	memset(&pvtSftotm, ' ', sizesftotm);
	svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", INL, (char *)&pvtSftotm, ssizesftotm, STXCONT, 1);
	
	memcpy(pvtSftotm.qds_ctl_no, sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);
	memcpy(pvtSftotm.ord_pfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
	memcpy(pvtSftotm.ord_no, sfcctmInpRec.ord_no, ORD_NO_SIZE);
	memcpy(pvtSftotm.ord_itm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", RD, (char *)&pvtSftotm, ssizesftotm, STXCONT, 1);
	CHK_SQL_IO_STS
	
	if(svciSqlIoSts == 2)
	{
		memset(&pvtSftotm, ' ', sizesftotm);
		svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", INL, (char *)&pvtSftotm, ssizesftotm, STXCONT, 1);
		
		memcpy(pvtSftotm.qds_ctl_no, sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);
		memcpy(pvtSftotm.ord_pfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
		memcpy(pvtSftotm.ord_no, sfcctmInpRec.ord_no, ORD_NO_SIZE);
		memcpy(pvtSftotm.ord_itm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
		memcpy(pvtSftotm.chm_sel_flg, sfcctmInpRec.chm_sel_flg, CHM_SEL_FLG_SIZE);
		
		iCtr = 0;
		iOffset = 0;
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftotm.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftotm.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftotm.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftotm.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftotm.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftotm.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftotm.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftotm.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftotm.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", INS, (char *)&pvtSftotm, ssizesftotm, STXERR, 1);
		CHK_SQL_IO_STS
	}
	else
	{
		memcpy(pvtSftotm.qds_ctl_no, sfcctmInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);
		memcpy(pvtSftotm.ord_pfx, sfcctmInpRec.ord_pfx, ORD_PFX_SIZE);
		memcpy(pvtSftotm.ord_no, sfcctmInpRec.ord_no, ORD_NO_SIZE);
		memcpy(pvtSftotm.ord_itm, sfcctmInpRec.ord_itm, ORD_ITM_SIZE);
		memcpy(pvtSftotm.chm_sel_flg, sfcctmInpRec.chm_sel_flg, CHM_SEL_FLG_SIZE);
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftotm.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftotm.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftotm.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftotm.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftotm.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftotm.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftotm.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftotm.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftotm.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftotm", "1", "1", UPD, (char *)&pvtSftotm, ssizesftotm, STXERR, 1);
		CHK_SQL_IO_STS
	}
	
	COMMIT_WORK
	
	return;
}

static void pvtCusTmplInfo(void)
{
	int iCtr = 0;
	int iOffset = 0;
	svcSetDbgInfo(TRACE_INFO, "");
	
	pvtReadSftctm();
	CHK_FAIL_STS
	
	if(svciSqlIoSts == 0)
	{
		/*SEL-FLG*/
		memcpy(sfcctmOutRec.chm_sel_flg, pvtSftctm.chm_sel_flg, CHM_SEL_FLG_SIZE);
		
		/*CHMEL_1 to 20*/
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(sfcctmOutRec.chmel_1+iOffset, pvtSftctm.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		
		/*JMY-FLG*/
		memcpy(sfcctmOutRec.jmy_flg, pvtSftctm.jmy_flg, JMY_FLG_SIZE);
		
		/*IMPC-FLG*/
		memcpy(sfcctmOutRec.impc_flg, pvtSftctm.impc_flg, IMPC_FLG_SIZE);
		
		/*MICN-FLG*/
		memcpy(sfcctmOutRec.mincn_flg, pvtSftctm.mincn_flg, MINCN_FLG_SIZE);
		
		/*HTRMT-FLG*/
		memcpy(sfcctmOutRec.htrmt_flg, pvtSftctm.htrmt_flg, HTRMT_FLG_SIZE);

		memcpy(sfcctmOutRec.jmy_flg2, pvtSftctm.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(sfcctmOutRec.ex_cert_flg, pvtSftctm.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(sfcctmOutRec.tst_spec_flg, pvtSftctm.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(sfcctmOutRec.mtl_std_flg, pvtSftctm.mtl_std_flg, MTL_STD_FLG_SIZE);
	}
	
	return;
}

static void pvtReadMchqcm(char *sQdsCtlNo)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtMchqcm, ' ', sizemchqcm);
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", INL, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	
	memcpy(pvtMchqcm.qds_ctl_no, sQdsCtlNo, QDS_CTL_NO_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", RD, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtReadSftctm(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtSftctm, ' ', sizesftctm);
	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", INL, (char *)&pvtSftctm, ssizesftctm, STXCONT, 1);
	
	memcpy(pvtSftctm.cus_id, sfcctmInpRec.cus_id, CUS_ID_SIZE);
	memcpy(pvtSftctm.cus_tmpl_nm, sfcctmInpRec.tmpl_nm, TMPL_NM_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", RD, (char *)&pvtSftctm, ssizesftctm, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtLinkCustomerTemplate(void)
{
	int iCtr = 0;
	int iOffset = 0;
	char sCusTmplNm[CUS_TMPL_NM_SIZE+1]= "";
	char sTmplNm[TMPL_NM_SIZE+1] = "";
	char sCusId[CUS_ID_SIZE+1] = "";
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	pvtReadSftctm();
	CHK_FAIL_STS
	
	if(svciSqlIoSts == 2)
	{
		memset(&pvtSftctm, ' ', sizesftctm);
		svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", INL, (char *)&pvtSftctm, ssizesftctm, STXCONT, 1);
		
		memcpy(pvtSftctm.cus_id, sfcctmInpRec.cus_id, CUS_ID_SIZE);
		memcpy(pvtSftctm.chm_sel_flg, sfcctmInpRec.chm_sel_flg, CHM_SEL_FLG_SIZE);
		
		memset(sCusId, '\0', sizeof(sCusId));
		RTrim(sCusId, sizeof(sCusId), sfcctmInpRec.cus_id, CUS_ID_SIZE);
		memset(sTmplNm, '\0', sizeof(sTmplNm));
		memset(sCusTmplNm, '\0', sizeof(sCusTmplNm));
		RTrim(sTmplNm, sizeof(sTmplNm), sfcctmInpRec.tmpl_nm, TMPL_NM_SIZE);
		svcConcat3(sCusTmplNm, sizeof(sCusTmplNm), sCusId, (int)strlen(sCusId), "_", 1, sTmplNm, (int)strlen(sTmplNm));
		memcpy(pvtSftctm.cus_tmpl_nm, sCusTmplNm, (int)strnlen(sCusTmplNm, CUS_TMPL_NM_SIZE));
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftctm.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftctm.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftctm.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftctm.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftctm.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftctm.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftctm.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftctm.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftctm.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", INS, (char *)&pvtSftctm, ssizesftctm, STXERR, 1);
		CHK_SQL_IO_STS
	}
	else
	{
		memcpy(pvtSftctm.cus_id, sfcctmInpRec.cus_id, CUS_ID_SIZE);
		memcpy(pvtSftctm.chm_sel_flg, sfcctmInpRec.chm_sel_flg, CHM_SEL_FLG_SIZE);
		
		memset(sCusId, '\0', sizeof(sCusId));
		RTrim(sCusId, sizeof(sCusId), sfcctmInpRec.cus_id, CUS_ID_SIZE);
		memset(sTmplNm, '\0', sizeof(sTmplNm));
		memset(sCusTmplNm, '\0', sizeof(sCusTmplNm));
		RTrim(sTmplNm, sizeof(sTmplNm), sfcctmInpRec.tmpl_nm, TMPL_NM_SIZE);
		svcConcat3(sCusTmplNm, sizeof(sCusTmplNm), sCusId, (int)strlen(sCusId), "_", 1, sTmplNm, (int)strlen(sTmplNm));
		memcpy(pvtSftctm.cus_tmpl_nm, sCusTmplNm, (int)strnlen(sCusTmplNm, CUS_TMPL_NM_SIZE));
		
		for (iCtr = 0; iCtr < 20; iCtr++)
		{
			memcpy(pvtSftctm.chmel_1+iOffset, sfcctmInpRec.chmel_1+iOffset, CHMEL_1_SIZE);
			iOffset = iOffset+CHMEL_1_SIZE;
		}
		memcpy(pvtSftctm.jmy_flg, sfcctmInpRec.jmy_flg, JMY_FLG_SIZE);
		memcpy(pvtSftctm.impc_flg, sfcctmInpRec.impc_flg, IMPC_FLG_SIZE);
		memcpy(pvtSftctm.mincn_flg, sfcctmInpRec.mincn_flg, MINCN_FLG_SIZE);
		memcpy(pvtSftctm.htrmt_flg, sfcctmInpRec.htrmt_flg, HTRMT_FLG_SIZE);
		memcpy(pvtSftctm.jmy_flg2, sfcctmInpRec.jmy_flg2, JMY_FLG2_SIZE);
		memcpy(pvtSftctm.ex_cert_flg, sfcctmInpRec.ex_cert_flg, EX_CERT_FLG_SIZE);
		memcpy(pvtSftctm.tst_spec_flg, sfcctmInpRec.tst_spec_flg, TST_SPEC_FLG_SIZE);
		memcpy(pvtSftctm.mtl_std_flg, sfcctmInpRec.mtl_std_flg, MTL_STD_FLG_SIZE);
		
		svcCallPlnsqlio(TRACE_INFO, "sftctm", "1", "1", UPD, (char *)&pvtSftctm, ssizesftctm, STXERR, 1);
		CHK_SQL_IO_STS
	}
	
	COMMIT_WORK
	
	return;
}

static void pvtMainLogicEnd(void)
{
	if (*pvtInMainLogicEnd != 'Y')
	{
		SET_OUT_RTN_STS(sfcctmOutRec)
		memcpy(pvtOutStr, (char *)&sfcctmOutRec, SFCCTM_OUT_SIZE);
		svcTerminationRtn(pvtInMainLogicEnd, PGM_NM, pvtOutStr, SFCCTM_OUT_SSIZE, pvtiRtnSts, pvtReturnJmp);
	}
	return;

}