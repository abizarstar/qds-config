/****************************************************************************************
*                      <scfjmy - Jominy Update >                                        *
*****************************************************************************************/

/****************************************************************************************
* Created By:  Shilpa Singhai                                                           *
* Date Created: Oct 26, 2021                                                            *
* Release:      20.0                                                                    *
* eCIM Call #:                                                                          *
* Description:  Created New Service for Jominy Update                                   *
*                                                                                       *
\****************************************************************************************/
/* Lint Comments */
/* NOTUSED*/
/* END Lint Comments */

#include "syscom.h"
#include "sfcjmy.h"
#include "mchqcm.h"
#include "sccct1.h"
#include "xctq10.h"
#include "xctq01.h"
#include "xcti00.h"

/**************** CONSTANTS ****************/
#define PGM_NM									"sfcjmy"
#define PRS_MD_ADD								'A'


/**********Variable definition**************/
static struct mchqcm_rec pvtMchqcm;

static double pvtdDi = 0.0;
static double pvtdJ1 = 0.0;
static double pvtdJ2 = 0.0;
static double pvtdJ3 = 0.0;
static double pvtdJ4 = 0.0;
static double pvtdJ5 = 0.0;
static double pvtdJ6 = 0.0;
static double pvtdJ7 = 0.0;
static double pvtdJ8 = 0.0;
static double pvtdJ9 = 0.0;
static double pvtdJ10 = 0.0;
static double pvtdJ12 = 0.0;
static double pvtdJ14 = 0.0;
static double pvtdJ16 = 0.0;
static double pvtdJ18 = 0.0;
static double pvtdJ20 = 0.0;
static double pvtdJ24 = 0.0;
static double pvtdJ28 = 0.0;
static double pvtdJ32 = 0.0;
static double pvtdC = 0.0;
/**************** Function Prototypes ****************/
static void pvtReadMchqcm(char *sQdsCtlNo);
static void pvtCallSccct1(void);
static void pvtInsRecXctq10(void);
static void pvtFndJominyValuesForBoronGrades(void);
static void pvtFndJominyValuesForNonBoronGrades(void);
static void pvtAddJominyValues(void);
static void pvtInsRecXctq01(void);
static void pvtCreateXcti00(void);
static void pvtRndCMnCuSiMoVal(double *dChem);
static void pvtCalDiVal(double dC, double dMn, double dSi, double dNi, double dCr, double dMo, double dCu, double dV);
static void pvtRndJmyVal(double *dJmyVal);

/*------------------------------------------------------------------------*/
/* This is the main function which accepts the input message from the     */
/* client program and returns output.                                     */
/*------------------------------------------------------------------------*/

int sfcjmy(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	if (setjmp(pvtReturnJmp) != 0)
	{
		return 0;
	}

	pvtInitVars(rqst);
	c_writelnkarea(PGM_NM, (char *)&sfcjmyInpRec, SFCJMY_INP_SSIZE);

	pvtInitLnkVars();
	CHK_SVC_RTN_STS

	pvtBusinessLogic();
	pvtMainLogicEnd();

	return 0;
}

/*------------------------------------------------------------------------*\
 * Function to initialize variables
\*------------------------------------------------------------------------*/
static void pvtInitVars(TPSVCINFO *rqst)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = 0;
	pvtInMainLogicEnd[0] = 'N';

	/*** I/P initialization ***/
	pvtLnkArea = rqst;
	memcpy((char *)&sfcjmyInpRec, rqst->data, SFCJMY_INP_SIZE);
	
	/*** O/P initialization ***/
	
	INIT_SFCJMY_OUTPUT
	INIT_SCCCT1_OUTPUT
	
	pvtOutStr = rqst->data + rqst->len;

	svcInitVars(PGM_NM);
	
	memset(&pvtMchqcm, ' ', sizemchqcm);
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", INL, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	
	pvtdDi = 0.0;
	pvtdJ1 = 0.0;
	pvtdJ2 = 0.0;
	pvtdJ3 = 0.0;
	pvtdJ4 = 0.0;
	pvtdJ5 = 0.0;
	pvtdJ6 = 0.0;
	pvtdJ7 = 0.0;
	pvtdJ8 = 0.0;
	pvtdJ9 = 0.0;
	pvtdJ10 = 0.0;
	pvtdJ12 = 0.0;
	pvtdJ14 = 0.0;
	pvtdJ16 = 0.0;
	pvtdJ18 = 0.0;
	pvtdJ20 = 0.0;
	pvtdJ24 = 0.0;
	pvtdJ28 = 0.0;
	pvtdJ32 = 0.0;
	
	return;
}

/* Function to Initialize request message's numeric fields if not set by the client */
static void pvtInitLnkVars(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtiRtnSts = svcVldNumeric(sfcjmyInpRec.clnt_dttm, CLNT_DTTM_SIZE, "CLNT-DTTM", 0);
	CHK_EXCPT_STS
	
	pvtiRtnSts = svcVldNumeric(sfcjmyInpRec.qds_ctl_no, QDS_CTL_NO_SIZE, "QDS-CTL-NO", 0);
	CHK_EXCPT_STS
	
	return;
}

static void pvtBusinessLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");

	pvtValidateInput();
	CHK_FAIL_STS

	pvtProgramLogic();
	return;
}

/*------------------------------------------------------------------------*\
 * Function to validate
\*------------------------------------------------------------------------*/
static void pvtValidateInput(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	if (pvtLnkArea->len < SFCJMY_INP_SIZE)
	{
		svcUserlog(TRACE_INFO, "Length of Input message is less than the expected length %d < %d", pvtLnkArea->len, SFCJMY_INP_SIZE);
		MAIN_LOGIC_RETURN
	}
	
	/* if process mode is applicable */
	if(sfcjmyInpRec.prs_md[0] != PRS_MD_ADD)
	{
		svcLogErrMsg(1, 1, "0000001500", "PRS-MD");
		MAIN_LOGIC_RETURN
	}

	return;
}

static void pvtProgramLogic(void)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	switch (sfcjmyInpRec.prs_md[0])
	{
		case PRS_MD_ADD:
		
			pvtAddJominyValues();
			CHK_FAIL_STS
			
			break;
			
		default:
			break;
	}
	return;
}

static void pvtRndJmyVal(double *dJmyVal)
{
	double dJomRem = 0;
	double dCalVal = 0;
	double dInpVal = 0;
	double dJomRem1 = 0;

	svcSetDbgInfo(TRACE_INFO, "");

	dInpVal = *dJmyVal;

	dJomRem = dInpVal*10;
	dJomRem1 = (dJomRem - (dJomRem / 5 * 5));

	if (dJomRem1 == 0 || fabs(dJomRem1) < 0.001)  /* divisible by 5 */
	{
		dJomRem = dInpVal;

		dJomRem1 = (dJomRem - (dJomRem / 2 * 2));
		if (dJomRem1 == 0 || fabs(dJomRem1) < 0.001)  /* even */
		{
			dCalVal = (((dInpVal*10)-1)/10);  /* round down */
		}
		else  /* odd */
		{
			dCalVal = (((dInpVal*10)+1)/10);  /* round up */
		}
	}
	else
	{
		dCalVal = dInpVal;  /* normal rounding */
	}

	/* Round value */
	dCalVal = (int)floor(dCalVal+0.5);

	*dJmyVal = dCalVal;

	return;
}

static void pvtRndCMnCuSiMoVal(double *dChem)
{
	double dTmpVal = 0;
	double dRem5 = 0;
	double dRem2 = 0;
	double dCalVal = 0;
	double dInpVal = 0;

	svcSetDbgInfo(TRACE_INFO, "");

	dInpVal = *dChem;

	dTmpVal=dInpVal*1000;
	dRem5 = (dTmpVal - ((dTmpVal/5)*5));

	dTmpVal=dInpVal*100;
	dRem2 = (dTmpVal - ((dTmpVal/2)*2));

	if (dRem5 == 0)
	{
		if (dRem2 != 0)
		{
			dCalVal = (((dInpVal*1000)-1)/1000);
		}
		else
		{
			dCalVal = (((dInpVal*1000)+1)/1000);
		}
	}
	else
	{
		dCalVal = dInpVal;
	}

	/* Truncate to two digits after decimal */
	dCalVal = floor(dCalVal*100)/100;

	*dChem = dCalVal;

	return;
}

static void pvtAddJominyValues(void)
{
	int iCtr = 0;
	int iOffset = 0;
	int iNbrCe =0;
	double dC = 0.0; 
	double dMn = 0.0; 
	double dCr = 0.0; 
	double dMo = 0.0; 
	double dV = 0.0; 
	double dCu = 0.0; 
	double dNi = 0.0; 
	double dSi = 0.0;
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	pvtReadMchqcm(sfcjmyInpRec.qds_ctl_no);
	CHK_FAIL_STS
	if(svciSqlIoSts == 0)
	{
		for (iCtr = 0, iOffset = 0; iCtr < 20;)
		{
			if(memcmp(pvtMchqcm.chmel_1+iOffset, "C  ", CHMEL_1_SIZE) == 0)
			{
				pvtdC = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				dC = pvtdC;
				pvtRndCMnCuSiMoVal(&dC);
				CHK_FAIL_STS
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mn ", CHMEL_1_SIZE) == 0)
			{
				dMn = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				pvtRndCMnCuSiMoVal(&dMn);
				CHK_FAIL_STS
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cr ", CHMEL_1_SIZE) == 0)
			{
				dCr = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Mo ", CHMEL_1_SIZE) == 0)
			{
				dMo = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				pvtRndCMnCuSiMoVal(&dMo);
				CHK_FAIL_STS
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "V  ", CHMEL_1_SIZE) == 0)
			{
				dV = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Ni ", CHMEL_1_SIZE) == 0)
			{
				dNi = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Cu ", CHMEL_1_SIZE) == 0)
			{
				dCu = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				pvtRndCMnCuSiMoVal(&dCu);
				CHK_FAIL_STS
				iNbrCe++;
			}
			else if(memcmp(pvtMchqcm.chmel_1+iOffset, "Si ", CHMEL_1_SIZE) == 0)
			{
				dSi = svcGetIntVal(pvtMchqcm.chm_val_1+iOffset, CHM_VAL_1_SIZE, CHM_VAL_1_DCML_SIZE);
				pvtRndCMnCuSiMoVal(&dSi);
				CHK_FAIL_STS
				iNbrCe++;
			}
		
			if((iNbrCe == 8) || memcmp(pvtMchqcm.chmel_1+iOffset, SPACES, CHMEL_1_SIZE) == 0)
			{
				break;
			}
			
			iCtr++;

			iOffset += QCM_CHMEL_2_FIELD_POSITION - QCM_CHMEL_1_FIELD_POSITION;
		}

		pvtCalDiVal(dC, dMn, dSi, dNi, dCr, dMo, dCu, dV);
		CHK_FAIL_STS
	}
	
	if(sfcjmyInpRec.boron_flg[0] == '1')
	{
		pvtFndJominyValuesForBoronGrades();
		CHK_FAIL_STS
	}
	else
	{
		pvtFndJominyValuesForNonBoronGrades();
		CHK_FAIL_STS
	}
	
	pvtCallSccct1();
	CHK_FAIL_STS

	pvtInsRecXctq10();
	CHK_FAIL_STS

	pvtInsRecXctq01();
	CHK_FAIL_STS

	pvtCreateXcti00();
	CHK_FAIL_STS
	
	return;
}

static void pvtCalDiVal(double dC, double dMn, double dSi, double dNi, double dCr, double dMo, double dCu, double dV)
{
	double dFC, dFMn, dFSi, dFNi, dFCr, dFMo, dFCu, dFV = 0;

	svcSetDbgInfo(TRACE_INFO, "");

	if (dC > 0.91)
	{
		pvtdDi = -1;
	}
	else if ( dC > 0.76)
	{
		dFC = (0.062 + 0.409*dC - 0.135*dC*dC);
	}
	else if (dC > 0.66)
	{
		dFC = (0.143 + 0.2*dC);
	}
	else if ( dC > 0.56)
	{
		dFC = (0.115 + 0.268*dC - 0.038*dC*dC);
	}
	else if (dC > 0.4)
	{
		dFC = (0.171 + 0.001*dC + 0.265*dC*dC);
	}
	else
	{
		dFC = (0.54*dC);
	}

	/* Round to four digits after decimal */
	dFC = floor((dFC*10000)+0.5)/10000;

	if (pvtdDi == -1)
	{
		return;
	}
	else
	{
		if (dMn > 1.96 )
		{
			pvtdDi = -1;
		}
		else if (dMn > 1.21)
		{
			dFMn = (5.10*dMn - 1.12);
		}
		else
		{
			dFMn = (3.3333*dMn + 1.00);
		}

		/* Round to four digits after decimal */
		dFMn = floor((dFMn*10000)+0.5)/10000;

		if (pvtdDi == -1)
		{
			return;
		}
	}

	if (dSi > 2.01 )
	{
		dFSi = 1;
	}
	else
	{
		dFSi = (1 + 0.7*dSi);
	}

	/* Round to four digits after decimal */
	dFSi = floor((dFSi*10000)+0.5)/10000;

	if (dNi > 2.01 )
	{
		dFNi = 1;
	}
	else
	{
		dFNi = (1 + 0.363*dNi);
	}

	/* Round to four digits after decimal */
	dFNi = floor((dFNi*10000)+0.5)/10000;

	if (dCr > 1.76 )
	{
		dFCr = 1;
	}
	else
	{
		dFCr = (1 + 2.16*dCr);
	}

	/* Round to four digits after decimal */
	dFCr = floor((dFCr*10000)+0.5)/10000;

	if (dMo > 0.56 )
	{
		dFMo = 1;
	}
	else
	{
		dFMo = (1 + 3.0*dMo);
	}

	/* Round to four digits after decimal */
	dFMo = floor((dFMo*10000)+0.5)/10000;

	if (dCu > 0.56 )
	{
		dFCu = 1;
	}
	else
	{
		dFCu = (1 + 0.365*dCu);
	}

	/* Round to four digits after decimal */
	dFCu = floor((dFCu*10000)+0.5)/10000;

	if (dV > 0.21 )
	{
		dFV = 1;
	}
	else
	{
		dFV = (1 + 1.73*dV);
	}

	/* Round to four digits after decimal */
	dFV = floor((dFV*10000)+0.5)/10000;

	/* Calculate DI value */
	pvtdDi = (dFC*dFMn*dFSi*dFNi*dFCr*dFMo*dFCu*dFV);

	/* Round to four digits after decimal */
	pvtdDi = floor((pvtdDi*10000)+0.5)/10000;

	return;
}

static void pvtInsRecXctq01(void)
{
	struct xctq01_rec xctq01;
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xctq01, ' ', sizexctq01);
	svcCallPlnsqlio(TRACE_INFO, "xctq01", "5", "1", INL, (char *)&xctq01, ssizexctq01, STXCONT, 1);
	
	memcpy(xctq01.intchg_pfx, "XI", 2);
	memcpy(xctq01.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	memcpy(xctq01.sts_cd, SPACES, STS_CD_SIZE);
	memcpy(xctq01.ent_md, "C", 1);
	memcpy(xctq01.intrm_tbl, "xctq10", 6);
	memcpy(xctq01.qds_ctl_no, sfcjmyInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);

	svcCallPlnsqlio(TRACE_INFO, "xctq01", "5", "1", INS, (char *)&xctq01, ssizexctq01, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK
	
	return;
}

/* Function to insert record in xcti00 table */
static void pvtCreateXcti00(void)
{
	char sDttm[18] = "";
	struct xcti00_rec xcti00;

	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xcti00, ' ', sizexcti00);
	svcCallPlnsqlio(TRACE_INFO, "xcti00", "3", "1", INL, (char *)&xcti00, ssizexcti00, STXCONT, 1);
	
	memcpy(xcti00.intchg_pfx, "XI", 2);
	memcpy(xcti00.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	memcpy(xcti00.evnt, "QDG", 3);
	
	memcpy(xcti00.usr_id, sfcjmyInpRec.clnt_lgn_id, USR_ID_SIZE);
	
	/* CRTD-DTTM */
	memset(sDttm, '\0', sizeof(sDttm));
	c_GetUTDateTm(sDttm);
	memcpy(xcti00.crtd_dtts, sDttm, CRTD_DTTS_SIZE);
	memcpy(xcti00.crtd_dtms, sDttm+CRTD_DTTS_SIZE, CRTD_DTMS_SIZE);
	memcpy(xcti00.upd_dtts, ZEROS, UPD_DTTS_SIZE);
	memcpy(xcti00.upd_dtms, ZEROS, UPD_DTMS_SIZE);
	memcpy(xcti00.sts_cd, "N", 1);
	memcpy(xcti00.ssn_log_ctl_no, ZEROS, SSN_LOG_CTL_NO_SIZE);
	memcpy(xcti00.intrf_cl, "E", 1);
	
	svcCallPlnsqlio(TRACE_INFO, "xcti00", "3", "1", INS, (char *)&xcti00, ssizexcti00, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK

	return;
}

static void pvtFndJominyValuesForBoronGrades(void)
{
	double dB3 = 0;
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	dB3 = 33.087 + (50.723 * pvtdC) + (33.662 * pvtdC * pvtdC) - (2.7048 * pvtdC * pvtdC * pvtdC) - (107.02 * pvtdC * pvtdC * pvtdC * pvtdC) + (43.523 * pvtdC * pvtdC * pvtdC * pvtdC * pvtdC);

	pvtdJ1 = dB3;

	pvtRndJmyVal(&pvtdJ1);
	CHK_FAIL_STS

	if(pvtdDi > 2.5)
	{
		pvtdJ2 = dB3;
	}
	else
	{
		pvtdJ2 = dB3 / (26.3659 - (63.9376 * pvtdDi) + (64.5141 *pvtdDi * pvtdDi) - (32.4046 *pvtdDi * pvtdDi * pvtdDi) + (8.08566 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.801282* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ2);
	CHK_FAIL_STS
	
	if(pvtdDi > 2.9)
	{
		pvtdJ3 = dB3;
	}
	else
	{
		pvtdJ3 = dB3 / (11.1118 - (23.185 * pvtdDi) + (21.5865 * pvtdDi * pvtdDi) - (10.0461 *pvtdDi * pvtdDi * pvtdDi) + (2.32282 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.212967 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ3);
	CHK_FAIL_STS
	
	if(pvtdDi > 3.5)
	{
		pvtdJ4 = dB3;
	}
	else
	{
		pvtdJ4 = dB3 / (28.50611 - (46.7043  * pvtdDi) + (31.90431 * pvtdDi * pvtdDi) - (10.91263 * pvtdDi * pvtdDi * pvtdDi) + (1.8657 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.12747 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ4);
	CHK_FAIL_STS
	
	if(pvtdDi > 4.4)
	{
		pvtdJ5 = dB3;
	}
	else
	{
		pvtdJ5 = dB3 / (24.56368 - (33.70604 * pvtdDi) + (19.34623 * pvtdDi * pvtdDi) - (5.52132 * pvtdDi * pvtdDi * pvtdDi) + (0.78088 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.0437473 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ5);
	CHK_FAIL_STS
	
	if(pvtdDi > 4.9)
	{
		pvtdJ6 = dB3;
	}
	else
	{
		pvtdJ6 = dB3 / (5.32872 + (1.00334 * pvtdDi) - (3.67571 * pvtdDi * pvtdDi) + (1.70752* pvtdDi * pvtdDi * pvtdDi) - (0.31024 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.0201755* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ6);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.2)
	{
		pvtdJ7 = dB3;
	}
	else
	{
		pvtdJ7 = dB3 /(5.34598 + (0.9881 * pvtdDi) - (3.15067 * pvtdDi * pvtdDi) + (1.33727 * pvtdDi * pvtdDi * pvtdDi) - (0.22285 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.0133182* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ7);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.6)
	{
		pvtdJ8 = dB3;
	}
	else
	{
		pvtdJ8 = dB3 / (2.61397 + (4.69073 * pvtdDi) - (4.71553 * pvtdDi * pvtdDi) + (1.58031 *pvtdDi * pvtdDi * pvtdDi) - (0.22844 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.01219 *pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ8);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.8)
	{
		pvtdJ9 = dB3;
	}
	else
	{
		pvtdJ9 = dB3 / (3.80939 + (2.96448 * pvtdDi) - (3.58847 * pvtdDi * pvtdDi) + (1.22906*pvtdDi * pvtdDi * pvtdDi) - (0.1773 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.00938121*pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ9);
	CHK_FAIL_STS
	
	if(pvtdDi > 6.1)
	{
		pvtdJ10 = dB3; 
	}
	else
	{
		pvtdJ10 = dB3 / (11.75138 - (8.15904 * pvtdDi) + (2.57305 * pvtdDi * pvtdDi) - (0.42384* pvtdDi * pvtdDi * pvtdDi) + (0.03679 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00135613 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ10);
	CHK_FAIL_STS
	
	if(pvtdDi > 6.6)
	{
		pvtdJ12 = dB3; 
	}
	else
	{
		pvtdJ12 = dB3 / (10.94580 - (6.42904 * pvtdDi) + (1.72900 * pvtdDi * pvtdDi) - (0.24187 * pvtdDi * pvtdDi * pvtdDi) + (0.01769 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000547832 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ12);
	CHK_FAIL_STS
	
	if(pvtdDi > 6.9)
	{
		pvtdJ14 =  dB3; 
	}
	else
	{
		pvtdJ14 = dB3 / (14.86832 - (10.1637 * pvtdDi) + (3.327 * pvtdDi * pvtdDi) - (0.5948 *pvtdDi * pvtdDi * pvtdDi) + (0.0563926 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00221015 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ14);
	CHK_FAIL_STS
	
	pvtdJ16 = dB3 / (14.10267 - (7.94906 * pvtdDi) + (1.93841 * pvtdDi * pvtdDi) - (0.22357 * pvtdDi * pvtdDi * pvtdDi) + (0.0108383 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00010342* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ16);
	CHK_FAIL_STS
	
	pvtdJ18 = dB3 / (11.29531 - (4.46248 * pvtdDi) + (0.41286 * pvtdDi * pvtdDi) + (0.09097 * pvtdDi * pvtdDi * pvtdDi) - (0.020345 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.00109529* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ18);
	CHK_FAIL_STS
	
	pvtdJ20 = dB3 / (7.14752 + (0.355 * pvtdDi) - (1.61359 * pvtdDi * pvtdDi) + (0.49403 * pvtdDi * pvtdDi * pvtdDi) - (0.0587857 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.00250946* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ20);
	CHK_FAIL_STS
	
	pvtdJ24 = dB3 / (12.3738 - (4.5069 * pvtdDi) + (0.29009 * pvtdDi * pvtdDi) + (0.12299 * pvtdDi * pvtdDi * pvtdDi) - (0.02325 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.00117 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ24);
	CHK_FAIL_STS
	
	pvtdJ28 = dB3/ (27.50991 - (20.45946 * pvtdDi) + (6.9758 * pvtdDi * pvtdDi) - (1.25184 * pvtdDi * pvtdDi * pvtdDi) + (0.11543 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00432751 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ28);
	CHK_FAIL_STS
	
	pvtdJ32 = dB3 / (43.35623 - (35.3426 * pvtdDi) + (12.58238 * pvtdDi * pvtdDi) - (2.29821* pvtdDi * pvtdDi * pvtdDi) + (0.21196 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00785122 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ32);
	CHK_FAIL_STS
	
	return;
}

static void pvtFndJominyValuesForNonBoronGrades(void)
{
	double dC3 = 0.0;
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	dC3 = 33.087 + (50.723 * pvtdC) + (33.662 * pvtdC * pvtdC) - (2.7048 * pvtdC * pvtdC * pvtdC) - (107.02 * pvtdC * pvtdC * pvtdC * pvtdC) + (43.523 * pvtdC * pvtdC * pvtdC * pvtdC * pvtdC);
	
	pvtdJ1 = dC3;

	pvtRndJmyVal(&pvtdJ1);
	CHK_FAIL_STS
	
	if(pvtdDi > 2.1)
	{
		pvtdJ2 = dC3;
	}
	else
	{
		pvtdJ2 = dC3 / (4.68961 - (11.00832 * pvtdDi) + (13.83314 * pvtdDi * pvtdDi) - (8.80283 * pvtdDi * pvtdDi * pvtdDi) + (2.78698 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.3488 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ2);
	CHK_FAIL_STS
	
	if(pvtdDi > 3.1)
	{
		pvtdJ3 = dC3;
	}
	else
	{
		pvtdJ3 = dC3 / (2.34904 - (0.28254 * pvtdDi) - (1.42995 * pvtdDi * pvtdDi) + (1.16697 * pvtdDi * pvtdDi * pvtdDi) - (0.33813 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.03403 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ3);
	CHK_FAIL_STS
	
	if(pvtdDi > 4.1)
	{
		pvtdJ4 = dC3;
	}
	else
	{
		pvtdJ4 = dC3 / (5.66795 - (6.14648 * pvtdDi) + (3.52874 * pvtdDi * pvtdDi) - (1.06026 * pvtdDi * pvtdDi * pvtdDi) + (0.16301 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.01015 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ4);
	CHK_FAIL_STS
	
	if(pvtdDi > 4.4)
	{
		pvtdJ5 = dC3;
	}
	else
	{
		pvtdJ5 = dC3 / (4.52902 - (2.90739 * pvtdDi) + (0.986508 * pvtdDi * pvtdDi) - (0.163586 * pvtdDi * pvtdDi * pvtdDi) + (0.012095 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000257202 * pvtdDi * pvtdDi * pvtdDi * pvtdDi* pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ5);
	CHK_FAIL_STS
	
	if(pvtdDi > 5)
	{
		pvtdJ6 = dC3;
	}
	else
	{
		pvtdJ6 = dC3 / (4.39436 - (2.16072 * pvtdDi) + (0.56027 * pvtdDi * pvtdDi) - (0.08145 *pvtdDi * pvtdDi * pvtdDi) + (0.0084 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000530827* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ6);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.3)
	{
		pvtdJ7 = dC3;
	}
	else
	{
		pvtdJ7 = dC3 / (4.15002 - (1.43154 * pvtdDi) + (0.00235893 * pvtdDi * pvtdDi) + (0.112947 * pvtdDi * pvtdDi * pvtdDi) - (0.0237546 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.00150903 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ7);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.6)
	{
		pvtdJ8 = dC3;
	}
	else
	{
		pvtdJ8 = dC3 / (4.44473 - (1.79085 * pvtdDi) + (0.24617 * pvtdDi * pvtdDi) + (0.03378* pvtdDi * pvtdDi * pvtdDi) - (0.01189 *pvtdDi * pvtdDi * pvtdDi * pvtdDi) + (0.000841843* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ8);
	CHK_FAIL_STS
	
	if(pvtdDi > 5.8)
	{
		pvtdJ9 = dC3;
	}
	else
	{
		pvtdJ9 = dC3 / (4.95421 - (2.43521 * pvtdDi) + (0.62983 * pvtdDi * pvtdDi) - (0.07914* pvtdDi * pvtdDi * pvtdDi) + (0.00399154 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.0000120363 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ9);
	CHK_FAIL_STS
	
	if(pvtdDi > 6.1)
	{
		pvtdJ10 = dC3;
	}
	else
	{
		pvtdJ10 = dC3 / (5.3161 - (2.80977 * pvtdDi) + (0.84183 * pvtdDi * pvtdDi) - (0.141781* pvtdDi * pvtdDi * pvtdDi) + (0.0130138 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000512388 * pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ10);
	CHK_FAIL_STS
	
	if(pvtdDi > 6.6)
	{
		pvtdJ12 = dC3;
	}
	else
	{
		pvtdJ12 = dC3 / (5.63649 - (2.89264 * pvtdDi) + (0.90309 * pvtdDi * pvtdDi) - (0.17297* pvtdDi * pvtdDi * pvtdDi) + (0.01881 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00086593* pvtdDi * pvtdDi *pvtdDi * pvtdDi * pvtdDi));
	}

	pvtRndJmyVal(&pvtdJ12);
	CHK_FAIL_STS
	
	pvtdJ14 = dC3 / (5.83176 - (2.99646 * pvtdDi) + (0.94088 * pvtdDi * pvtdDi) - (0.17734 *pvtdDi * pvtdDi * pvtdDi) + (0.0183885 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000790018* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ14);
	CHK_FAIL_STS
	
	pvtdJ16 = dC3 / (6.06952 - (3.15198 * pvtdDi) + (0.99297 * pvtdDi * pvtdDi) - (0.1801 * pvtdDi * pvtdDi * pvtdDi) + (0.0172029 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.000664079*pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ16);
	CHK_FAIL_STS

	pvtdJ18 = dC3 / (7.32018 - (4.60605 * pvtdDi) + (1.68442 * pvtdDi * pvtdDi) - (0.338443 * pvtdDi * pvtdDi * pvtdDi) + (0.0345114 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00138927*pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ18);
	CHK_FAIL_STS
	
	pvtdJ20 = dC3 / (7.81382 - (5.10022 * pvtdDi) + (1.92141 * pvtdDi * pvtdDi) - (0.394591 * pvtdDi * pvtdDi * pvtdDi) + (0.040784 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00165327* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ20);
	CHK_FAIL_STS
	
	pvtdJ24 = dC3 / (9.18138 - (6.69048 * pvtdDi) + (2.75891 * pvtdDi * pvtdDi) - (0.611613 * pvtdDi * pvtdDi * pvtdDi) + (0.0677165 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.0029307* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ24);
	CHK_FAIL_STS
	
	pvtdJ28 = dC3 / (9.27904 - (6.21461* pvtdDi) + (2.33158 * pvtdDi * pvtdDi) - (0.46972 * pvtdDi * pvtdDi * pvtdDi) + (0.0472654 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.00186035*pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ28);
	CHK_FAIL_STS
	
	pvtdJ32 = dC3 / (8.62857 - (5.16125 * pvtdDi) + (1.81214 * pvtdDi * pvtdDi) - (0.35489* pvtdDi * pvtdDi * pvtdDi) + (0.035687 * pvtdDi * pvtdDi * pvtdDi * pvtdDi) - (0.001434* pvtdDi * pvtdDi * pvtdDi * pvtdDi * pvtdDi));

	pvtRndJmyVal(&pvtdJ32);
	CHK_FAIL_STS
	
	return;
}

static void pvtCallSccct1(void)
{
	char sInpStr[SCCCT1_INP_SIZE + 1] = "";
	char sOutStr[SCCCT1_OUT_SIZE + 1] = "";
	
	svcSetDbgInfo(TRACE_INFO, "");

	INIT_SCCCT1_INPUT
	INIT_SCCCT1_OUTPUT
	
	memcpy(sccct1InpRec.apln, "IN", 2);
	memcpy(sccct1InpRec.ref_pfx, "QC", 2);
	memcpy(sccct1InpRec.ctl_no_lgth, "08", 2);
	
	memset(sInpStr, '\0', sizeof(sInpStr));
	memset(sOutStr, '\0', sizeof(sOutStr));

	memcpy(sInpStr, (char *)&sccct1InpRec, SCCCT1_INP_SIZE);
	if (svcCallSvc("sccct1", sInpStr, SCCCT1_INP_SIZE, sOutStr, SCCCT1_OUT_SIZE, "") == 0)
	{
		if (memcmp(sOutStr, "0001", RTN_STS_SIZE) == 0)
		{
			MAIN_LOGIC_RETURN
		}
		else
		{
			memcpy(&sccct1OutRec, sOutStr, SCCCT1_OUT_SIZE);
			SET_SVC_RTN_STS(sccct1OutRec)
			CHK_FAIL_STS
		}
	}
	else
	{
		MAIN_LOGIC_RETURN
	}
	
	/* Set data in output string */
	memcpy(&sccct1OutRec, sOutStr, SCCCT1_OUT_SIZE);
	
	return;
}

static void pvtInsRecXctq10(void)
{
	struct xctq10_rec xctq10;
	
	svcSetDbgInfo(TRACE_INFO, "");
	
	BEGIN_WORK
	
	memset(&xctq10, ' ', sizexctq10);
	svcCallPlnsqlio(TRACE_INFO, "xctq10", "1", "1", INL, (char *)&xctq10, ssizexctq10, STXCONT, 1);
	memcpy(xctq10.intchg_pfx, "XI", 2);
	memcpy(xctq10.intchg_no, sccct1OutRec.ctl_no+2, INTCHG_NO_SIZE);
	memcpy(xctq10.qds_ctl_no, sfcjmyInpRec.qds_ctl_no, QDS_CTL_NO_SIZE);
	memcpy(xctq10.jmy_tst_no, "1", 1);
	memcpy(xctq10.jmy_tst_typ, "HRC", 3);
	memcpy(xctq10.rdgpos_1, "J1", 2);
	memcpy(xctq10.jmy_ent_typ_1, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_1, JMY_VAL_1_SIZE, pvtdJ1, JMY_VAL_1_SIZE, JMY_VAL_1_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_2, "J2", 2);
	memcpy(xctq10.jmy_ent_typ_2, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_2, JMY_VAL_2_SIZE, pvtdJ2, JMY_VAL_2_SIZE, JMY_VAL_2_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_3, "J3", 2);
	memcpy(xctq10.jmy_ent_typ_3, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_3, JMY_VAL_3_SIZE, pvtdJ3, JMY_VAL_3_SIZE, JMY_VAL_3_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_4, "J4", 2);
	memcpy(xctq10.jmy_ent_typ_4, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_4, JMY_VAL_4_SIZE, pvtdJ4, JMY_VAL_4_SIZE, JMY_VAL_4_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_5, "J5", 2);
	memcpy(xctq10.jmy_ent_typ_5, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_5, JMY_VAL_5_SIZE, pvtdJ5, JMY_VAL_5_SIZE, JMY_VAL_5_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_6, "J6", 2);
	memcpy(xctq10.jmy_ent_typ_6, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_6, JMY_VAL_6_SIZE, pvtdJ6, JMY_VAL_6_SIZE, JMY_VAL_6_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_7, "J7", 2);
	memcpy(xctq10.jmy_ent_typ_7, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_7, JMY_VAL_7_SIZE, pvtdJ7, JMY_VAL_7_SIZE, JMY_VAL_7_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_8, "J8", 2);
	memcpy(xctq10.jmy_ent_typ_8, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_8, JMY_VAL_8_SIZE, pvtdJ8, JMY_VAL_8_SIZE, JMY_VAL_8_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_9, "J9", 2);
	memcpy(xctq10.jmy_ent_typ_9, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_9, JMY_VAL_9_SIZE, pvtdJ9, JMY_VAL_9_SIZE, JMY_VAL_9_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_10, "J10", 3);
	memcpy(xctq10.jmy_ent_typ_10, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_10, JMY_VAL_10_SIZE, pvtdJ10, JMY_VAL_10_SIZE, JMY_VAL_10_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_11, "J12", 3);
	memcpy(xctq10.jmy_ent_typ_11, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_11, JMY_VAL_11_SIZE, pvtdJ12, JMY_VAL_11_SIZE, JMY_VAL_11_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_12, "J14", 3);
	memcpy(xctq10.jmy_ent_typ_12, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_12, JMY_VAL_12_SIZE, pvtdJ14, JMY_VAL_14_SIZE, JMY_VAL_14_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_13, "J16", 3);
	memcpy(xctq10.jmy_ent_typ_13, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_13, JMY_VAL_13_SIZE, pvtdJ16, JMY_VAL_13_SIZE, JMY_VAL_13_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_14, "J18", 3);
	memcpy(xctq10.jmy_ent_typ_14, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_14, JMY_VAL_14_SIZE, pvtdJ18, JMY_VAL_14_SIZE, JMY_VAL_14_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_15, "J20", 3);
	memcpy(xctq10.jmy_ent_typ_15, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_15, JMY_VAL_15_SIZE, pvtdJ20, JMY_VAL_15_SIZE, JMY_VAL_15_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_16, "J24", 3);
	memcpy(xctq10.jmy_ent_typ_16, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_16, JMY_VAL_16_SIZE, pvtdJ24, JMY_VAL_16_SIZE, JMY_VAL_16_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_17, "J28", 3);
	memcpy(xctq10.jmy_ent_typ_17, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_17, JMY_VAL_17_SIZE, pvtdJ28, JMY_VAL_17_SIZE, JMY_VAL_17_DCML_SIZE, 0);
	memcpy(xctq10.rdgpos_18, "J32", 3);
	memcpy(xctq10.jmy_ent_typ_18, "V", 1);
	svcDblToSrvrValStr(xctq10.jmy_val_18, JMY_VAL_18_SIZE, pvtdJ32, JMY_VAL_18_SIZE, JMY_VAL_18_DCML_SIZE, 0);
	
	svcCallPlnsqlio(TRACE_INFO, "xctq10", "1", "1", INS, (char *)&xctq10, ssizexctq10, STXERR, 1);
	CHK_SQL_IO_STS
	
	COMMIT_WORK
	
	return;
}

static void pvtReadMchqcm(char *sQdsCtlNo)
{
	svcSetDbgInfo(TRACE_INFO, "");
	
	memset(&pvtMchqcm, ' ', sizemchqcm);
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", INL, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	
	memcpy(pvtMchqcm.qds_ctl_no, sQdsCtlNo, QDS_CTL_NO_SIZE);
	
	svcCallPlnsqlio(TRACE_INFO, "mchqcm", "1", "1", RD, (char *)&pvtMchqcm, ssizemchqcm, STXCONT, 1);
	CHK_SQL_IO_STS
	
	return;
}

static void pvtMainLogicEnd(void)
{
	if (*pvtInMainLogicEnd != 'Y')
	{
		SET_OUT_RTN_STS(sfcjmyOutRec)
		memcpy(pvtOutStr, (char *)&sfcjmyOutRec, SFCJMY_OUT_SIZE);
		svcTerminationRtn(pvtInMainLogicEnd, PGM_NM, pvtOutStr, SFCJMY_OUT_SSIZE, pvtiRtnSts, pvtReturnJmp);
	}
	return;

}
