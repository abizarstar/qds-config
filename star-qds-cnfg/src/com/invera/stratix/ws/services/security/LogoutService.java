package com.invera.stratix.ws.services.security;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.invera.stratix.schema.common.datatypes.AuthenticationToken;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.1
 * 
 */
@WebService(name = "LogoutService", targetNamespace = "http://stratix.invera.com/services")
@XmlSeeAlso({ ObjectFactory.class })
public interface LogoutService {

	/**
	 * 
	 * @param authenticationToken
	 * @throws StratixFault
	 */
	@WebMethod(operationName = "Logout", action = "http://stratix.invera.com/security/Logout")
	@RequestWrapper(localName = "Logout", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.ws.services.security.Logout")
	@ResponseWrapper(localName = "LogoutResponse", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.ws.services.security.LogoutResponse")
	public void logout(
			@WebParam(name = "authenticationToken", targetNamespace = "") AuthenticationToken authenticationToken)
			throws StratixFault;

}
