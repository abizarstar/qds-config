package com.invera.stratix.ws.services.security;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.invera.stratix.schema.common.datatypes.ServiceMessages;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.invera.stratix.ws.services.security
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of com.invera.stratix.schema derived interfaces and classes
 * representing the binding of com.invera.stratix.schema type definitions,
 * element declarations and model groups. Factory methods for each of these are
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static QName _ServiceMessagesHeader_QNAME = new QName(
			"http://stratix.invera.com/services", "ServiceMessagesHeader");
	private final static QName _StratixFault_QNAME = new QName(
			"http://stratix.invera.com/services", "StratixFault");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * com.invera.stratix.schema derived classes for package:
	 * com.invera.stratix.ws.services.security
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link Logout }
	 * 
	 */
	public Logout createLogout() {
		return new Logout();
	}

	/**
	 * Create an instance of {@link LogoutResponse }
	 * 
	 */
	public LogoutResponse createLogoutResponse() {
		return new LogoutResponse();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ServiceMessages }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "ServiceMessagesHeader")
	public JAXBElement<ServiceMessages> createServiceMessagesHeader(
			ServiceMessages value) {
		return new JAXBElement<ServiceMessages>(_ServiceMessagesHeader_QNAME,
				ServiceMessages.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "StratixFault")
	public JAXBElement<String> createStratixFault(String value) {
		return new JAXBElement<String>(_StratixFault_QNAME, String.class, null,
				value);
	}
}
