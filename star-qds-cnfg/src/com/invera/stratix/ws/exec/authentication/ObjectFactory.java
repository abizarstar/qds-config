package com.invera.stratix.ws.exec.authentication;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.invera.stratix.schema.common.datatypes.ServiceMessages;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.invera.stratix.ws.exec.authentication
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _ServiceMessagesHeader_QNAME = new QName(
			"http://stratix.invera.com/services", "ServiceMessagesHeader");
	private final static QName _StratixFault_QNAME = new QName(
			"http://stratix.invera.com/services", "StratixFault");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * com.invera.stratix.ws.exec.authentication
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link GatewayLoginResponse }
	 * 
	 */
	public GatewayLoginResponse createGatewayLoginResponse() {
		return new GatewayLoginResponse();
	}

	/**
	 * Create an instance of {@link GatewayLoginRequestType }
	 * 
	 */
	public GatewayLoginRequestType createGatewayLoginRequestType() {
		return new GatewayLoginRequestType();
	}

	/**
	 * Create an instance of {@link GatewayLoginResponseType }
	 * 
	 */
	public GatewayLoginResponseType createGatewayLoginResponseType() {
		return new GatewayLoginResponseType();
	}

	/**
	 * Create an instance of {@link GatewayLogin }
	 * 
	 */
	public GatewayLogin createGatewayLogin() {
		return new GatewayLogin();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ServiceMessages }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "ServiceMessagesHeader")
	public JAXBElement<ServiceMessages> createServiceMessagesHeader(
			ServiceMessages value) {
		return new JAXBElement<ServiceMessages>(_ServiceMessagesHeader_QNAME,
				ServiceMessages.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "StratixFault")
	public JAXBElement<String> createStratixFault(String value) {
		return new JAXBElement<String>(_StratixFault_QNAME, String.class, null,
				value);
	}

}
