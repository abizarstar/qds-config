package com.invera.stratix.ws.exec.authentication;

import javax.xml.ws.WebFault;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.1
 * 
 */
@WebFault(name = "StratixFault", targetNamespace = "http://stratix.invera.com/services")
public class StratixFault extends Exception {

	/**
	 * Java type that goes as soapenv:Fault detail element.
	 * 
	 */
	private String faultInfo;

	/**
	 * 
	 * @param faultInfo
	 * @param message
	 */
	public StratixFault(String message, String faultInfo) {
		super(message);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @param faultInfo
	 * @param message
	 * @param cause
	 */
	public StratixFault(String message, String faultInfo, Throwable cause) {
		super(message, cause);
		this.faultInfo = faultInfo;
	}

	/**
	 * 
	 * @return returns fault bean: java.lang.String
	 */
	public String getFaultInfo() {
		return faultInfo;
	}

}
