package com.invera.stratix.ws.exec.authentication;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://stratix.invera.com/services}GatewayLoginResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "response" })
@XmlRootElement(name = "GatewayLoginResponse")
public class GatewayLoginResponse {

	@XmlElement(required = true)
	protected GatewayLoginResponseType response;

	/**
	 * Gets the value of the response property.
	 * 
	 * @return possible object is {@link GatewayLoginResponseType }
	 * 
	 */
	public GatewayLoginResponseType getResponse() {
		return response;
	}

	/**
	 * Sets the value of the response property.
	 * 
	 * @param value
	 *            allowed object is {@link GatewayLoginResponseType }
	 * 
	 */
	public void setResponse(GatewayLoginResponseType value) {
		this.response = value;
	}

}
