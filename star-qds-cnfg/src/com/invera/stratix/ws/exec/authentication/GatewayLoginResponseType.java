package com.invera.stratix.ws.exec.authentication;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;

/**
 * <p>
 * Java class for GatewayLoginResponseType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GatewayLoginResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authenticationToken" type="{http://stratix.invera.com/schema/common/datatypes}AuthenticationToken"/>
 *         &lt;element name="connectedServer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="connectedPort" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GatewayLoginResponseType", propOrder = {
		"authenticationToken", "connectedServer", "connectedPort" })
public class GatewayLoginResponseType {

	@XmlElement(required = true)
	protected AuthenticationToken authenticationToken;
	@XmlElement(required = true)
	protected String connectedServer;
	protected Integer connectedPort;

	/**
	 * Gets the value of the authenticationToken property.
	 * 
	 * @return possible object is {@link AuthenticationToken }
	 * 
	 */
	public AuthenticationToken getAuthenticationToken() {
		return authenticationToken;
	}

	/**
	 * Sets the value of the authenticationToken property.
	 * 
	 * @param value
	 *            allowed object is {@link AuthenticationToken }
	 * 
	 */
	public void setAuthenticationToken(AuthenticationToken value) {
		this.authenticationToken = value;
	}

	/**
	 * Gets the value of the connectedServer property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getConnectedServer() {
		return connectedServer;
	}

	/**
	 * Sets the value of the connectedServer property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setConnectedServer(String value) {
		this.connectedServer = value;
	}

	/**
	 * Gets the value of the connectedPort property.
	 * 
	 * @return possible object is {@link Integer }
	 * 
	 */
	public Integer getConnectedPort() {
		return connectedPort;
	}

	/**
	 * Sets the value of the connectedPort property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 * 
	 */
	public void setConnectedPort(Integer value) {
		this.connectedPort = value;
	}

}
