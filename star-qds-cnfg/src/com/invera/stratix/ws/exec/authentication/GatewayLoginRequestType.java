package com.invera.stratix.ws.exec.authentication;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for GatewayLoginRequestType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="GatewayLoginRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="connectedAccessType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="environmentName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="environmentClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="forceDisconnect" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GatewayLoginRequestType", propOrder = { "username",
		"password", "companyId", "connectedAccessType", "environmentName",
		"environmentClass", "forceDisconnect" })
public class GatewayLoginRequestType {

	@XmlElement(required = true)
	protected String username;
	@XmlElement(required = true)
	protected String password;
	protected String companyId;
	@XmlElement(required = true)
	protected String connectedAccessType;
	@XmlElement(required = true)
	protected String environmentName;
	@XmlElement(required = true)
	protected String environmentClass;
	protected Boolean forceDisconnect;

	/**
	 * Gets the value of the username property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the value of the username property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUsername(String value) {
		this.username = value;
	}

	/**
	 * Gets the value of the password property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the value of the password property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPassword(String value) {
		this.password = value;
	}

	/**
	 * Gets the value of the companyId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the value of the companyId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCompanyId(String value) {
		this.companyId = value;
	}

	/**
	 * Gets the value of the connectedAccessType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getConnectedAccessType() {
		return connectedAccessType;
	}

	/**
	 * Sets the value of the connectedAccessType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setConnectedAccessType(String value) {
		this.connectedAccessType = value;
	}

	/**
	 * Gets the value of the environmentName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnvironmentName() {
		return environmentName;
	}

	/**
	 * Sets the value of the environmentName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnvironmentName(String value) {
		this.environmentName = value;
	}

	/**
	 * Gets the value of the environmentClass property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnvironmentClass() {
		return environmentClass;
	}

	/**
	 * Sets the value of the environmentClass property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnvironmentClass(String value) {
		this.environmentClass = value;
	}

	/**
	 * Gets the value of the forceDisconnect property.
	 * 
	 * @return possible object is {@link Boolean }
	 * 
	 */
	public Boolean isForceDisconnect() {
		return forceDisconnect;
	}

	/**
	 * Sets the value of the forceDisconnect property.
	 * 
	 * @param value
	 *            allowed object is {@link Boolean }
	 * 
	 */
	public void setForceDisconnect(Boolean value) {
		this.forceDisconnect = value;
	}

}
