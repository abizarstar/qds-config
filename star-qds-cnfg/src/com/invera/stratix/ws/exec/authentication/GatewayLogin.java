package com.invera.stratix.ws.exec.authentication;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="input" type="{http://stratix.invera.com/services}GatewayLoginRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "input" })
@XmlRootElement(name = "GatewayLogin")
public class GatewayLogin {

	@XmlElement(required = true)
	protected GatewayLoginRequestType input;

	/**
	 * Gets the value of the input property.
	 * 
	 * @return possible object is {@link GatewayLoginRequestType }
	 * 
	 */
	public GatewayLoginRequestType getInput() {
		return input;
	}

	/**
	 * Sets the value of the input property.
	 * 
	 * @param value
	 *            allowed object is {@link GatewayLoginRequestType }
	 * 
	 */
	public void setInput(GatewayLoginRequestType value) {
		this.input = value;
	}

}
