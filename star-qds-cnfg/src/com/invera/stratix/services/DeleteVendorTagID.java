
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="deleteMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="vendorTagId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "deleteMode",
    "vendorTagId"
})
@XmlRootElement(name = "DeleteVendorTagID")
public class DeleteVendorTagID {

    protected long qdsNumber;
    protected int deleteMode;
    protected String vendorTagId;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the deleteMode property.
     * 
     */
    public int getDeleteMode() {
        return deleteMode;
    }

    /**
     * Sets the value of the deleteMode property.
     * 
     */
    public void setDeleteMode(int value) {
        this.deleteMode = value;
    }

    /**
     * Gets the value of the vendorTagId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorTagId() {
        return vendorTagId;
    }

    /**
     * Sets the value of the vendorTagId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorTagId(String value) {
        this.vendorTagId = value;
    }

}
