
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updateQdsInput" type="{http://stratix.invera.com/services}UpdateQdsInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateQdsInput"
})
@XmlRootElement(name = "UpdateQds")
public class UpdateQds {

    @XmlElement(required = true)
    protected UpdateQdsInput updateQdsInput;

    /**
     * Gets the value of the updateQdsInput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateQdsInput }
     *     
     */
    public UpdateQdsInput getUpdateQdsInput() {
        return updateQdsInput;
    }

    /**
     * Sets the value of the updateQdsInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateQdsInput }
     *     
     */
    public void setUpdateQdsInput(UpdateQdsInput value) {
        this.updateQdsInput = value;
    }

}
