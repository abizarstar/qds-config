
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateQdsInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateQdsInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mill" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="heat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="whs" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origPoNo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="origPoItem" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="origPoDistribution" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="millOrdNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="venId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apvdFlag" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="origZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uknownHeat" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateQdsInput", propOrder = {
    "mill",
    "heat",
    "type",
    "whs",
    "origPoNo",
    "origPoItem",
    "origPoDistribution",
    "millOrdNo",
    "venId",
    "status",
    "apvdFlag",
    "origZone",
    "uknownHeat"
})
public class CreateQdsInput {

    @XmlElement(required = true)
    protected String mill;
    @XmlElement(required = true)
    protected String heat;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String whs;
    protected Integer origPoNo;
    protected Integer origPoItem;
    protected Integer origPoDistribution;
    protected String millOrdNo;
    protected String venId;
    protected String status;
    protected int apvdFlag;
    protected String origZone;
    protected int uknownHeat;

    /**
     * Gets the value of the mill property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMill() {
        return mill;
    }

    /**
     * Sets the value of the mill property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMill(String value) {
        this.mill = value;
    }

    /**
     * Gets the value of the heat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeat() {
        return heat;
    }

    /**
     * Sets the value of the heat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeat(String value) {
        this.heat = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the whs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhs() {
        return whs;
    }

    /**
     * Sets the value of the whs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhs(String value) {
        this.whs = value;
    }

    /**
     * Gets the value of the origPoNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrigPoNo() {
        return origPoNo;
    }

    /**
     * Sets the value of the origPoNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrigPoNo(Integer value) {
        this.origPoNo = value;
    }

    /**
     * Gets the value of the origPoItem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrigPoItem() {
        return origPoItem;
    }

    /**
     * Sets the value of the origPoItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrigPoItem(Integer value) {
        this.origPoItem = value;
    }

    /**
     * Gets the value of the origPoDistribution property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrigPoDistribution() {
        return origPoDistribution;
    }

    /**
     * Sets the value of the origPoDistribution property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrigPoDistribution(Integer value) {
        this.origPoDistribution = value;
    }

    /**
     * Gets the value of the millOrdNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMillOrdNo() {
        return millOrdNo;
    }

    /**
     * Sets the value of the millOrdNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMillOrdNo(String value) {
        this.millOrdNo = value;
    }

    /**
     * Gets the value of the venId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVenId() {
        return venId;
    }

    /**
     * Sets the value of the venId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVenId(String value) {
        this.venId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the apvdFlag property.
     * 
     */
    public int getApvdFlag() {
        return apvdFlag;
    }

    /**
     * Sets the value of the apvdFlag property.
     * 
     */
    public void setApvdFlag(int value) {
        this.apvdFlag = value;
    }

    /**
     * Gets the value of the origZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigZone() {
        return origZone;
    }

    /**
     * Sets the value of the origZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigZone(String value) {
        this.origZone = value;
    }

    /**
     * Gets the value of the uknownHeat property.
     * 
     */
    public int getUknownHeat() {
        return uknownHeat;
    }

    /**
     * Sets the value of the uknownHeat property.
     * 
     */
    public void setUknownHeat(int value) {
        this.uknownHeat = value;
    }

}
