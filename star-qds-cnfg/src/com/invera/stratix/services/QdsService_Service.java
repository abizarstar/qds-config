
package com.invera.stratix.services;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "QdsService", targetNamespace = "http://stratix.invera.com/services", wsdlLocation = "http://172.20.100.36:60977/webservices/services/gateway/QDS/QdsService.wsdl")
public class QdsService_Service
    extends Service
{

    private final static URL QDSSERVICE_WSDL_LOCATION;
    private final static WebServiceException QDSSERVICE_EXCEPTION;
    private final static QName QDSSERVICE_QNAME = new QName("http://stratix.invera.com/services", "QdsService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://172.20.100.36:60977/webservices/services/gateway/QDS/QdsService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        QDSSERVICE_WSDL_LOCATION = url;
        QDSSERVICE_EXCEPTION = e;
    }

    public QdsService_Service() {
        super(__getWsdlLocation(), QDSSERVICE_QNAME);
    }

    public QdsService_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), QDSSERVICE_QNAME, features);
    }

    public QdsService_Service(URL wsdlLocation) {
        super(wsdlLocation, QDSSERVICE_QNAME);
    }

    public QdsService_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, QDSSERVICE_QNAME, features);
    }

    public QdsService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public QdsService_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns QdsService
     */
    @WebEndpoint(name = "QdsServiceSOAP")
    public QdsService getQdsServiceSOAP() {
        return super.getPort(new QName("http://stratix.invera.com/services", "QdsServiceSOAP"), QdsService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns QdsService
     */
    @WebEndpoint(name = "QdsServiceSOAP")
    public QdsService getQdsServiceSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://stratix.invera.com/services", "QdsServiceSOAP"), QdsService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (QDSSERVICE_EXCEPTION!= null) {
            throw QDSSERVICE_EXCEPTION;
        }
        return QDSSERVICE_WSDL_LOCATION;
    }

}
