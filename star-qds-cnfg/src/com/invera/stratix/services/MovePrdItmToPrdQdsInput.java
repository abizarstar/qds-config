
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MovePrdItmToPrdQdsInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MovePrdItmToPrdQdsInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itmCtlNo" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MovePrdItmToPrdQdsInput", propOrder = {
    "itmCtlNo",
    "qdsNumber"
})
public class MovePrdItmToPrdQdsInput {

    protected long itmCtlNo;
    protected long qdsNumber;

    /**
     * Gets the value of the itmCtlNo property.
     * 
     */
    public long getItmCtlNo() {
        return itmCtlNo;
    }

    /**
     * Sets the value of the itmCtlNo property.
     * 
     */
    public void setItmCtlNo(long value) {
        this.itmCtlNo = value;
    }

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

}
