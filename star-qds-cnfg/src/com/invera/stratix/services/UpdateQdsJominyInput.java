
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsJominyInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsJominyInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jominyPosition" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jominyEntryType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="jominyVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="jominyMinVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="jominyMaxVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsJominyInput", propOrder = {
    "jominyPosition",
    "jominyEntryType",
    "jominyVal",
    "jominyMinVal",
    "jominyMaxVal"
})
public class UpdateQdsJominyInput {

    @XmlElement(required = true)
    protected String jominyPosition;
    @XmlElement(required = true)
    protected String jominyEntryType;
    protected BigDecimal jominyVal;
    protected BigDecimal jominyMinVal;
    protected BigDecimal jominyMaxVal;

    /**
     * Gets the value of the jominyPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJominyPosition() {
        return jominyPosition;
    }

    /**
     * Sets the value of the jominyPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJominyPosition(String value) {
        this.jominyPosition = value;
    }

    /**
     * Gets the value of the jominyEntryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJominyEntryType() {
        return jominyEntryType;
    }

    /**
     * Sets the value of the jominyEntryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJominyEntryType(String value) {
        this.jominyEntryType = value;
    }

    /**
     * Gets the value of the jominyVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJominyVal() {
        return jominyVal;
    }

    /**
     * Sets the value of the jominyVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJominyVal(BigDecimal value) {
        this.jominyVal = value;
    }

    /**
     * Gets the value of the jominyMinVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJominyMinVal() {
        return jominyMinVal;
    }

    /**
     * Sets the value of the jominyMinVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJominyMinVal(BigDecimal value) {
        this.jominyMinVal = value;
    }

    /**
     * Gets the value of the jominyMaxVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJominyMaxVal() {
        return jominyMaxVal;
    }

    /**
     * Sets the value of the jominyMaxVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJominyMaxVal(BigDecimal value) {
        this.jominyMaxVal = value;
    }

}
