
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsHeatTreatmentInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsHeatTreatmentInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="heatTreatment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="temperature" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="temperatureMsr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="coolentMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coolentTemperature" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="coolentTemperatureMsr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsHeatTreatmentInput", propOrder = {
    "heatTreatment",
    "temperature",
    "temperatureMsr",
    "time",
    "coolentMethod",
    "coolentTemperature",
    "coolentTemperatureMsr"
})
public class UpdateQdsHeatTreatmentInput {

    @XmlElement(required = true)
    protected String heatTreatment;
    protected Integer temperature;
    @XmlElement(required = true)
    protected String temperatureMsr;
    protected Integer time;
    @XmlElement(required = true)
    protected String coolentMethod;
    protected Integer coolentTemperature;
    @XmlElement(required = true)
    protected String coolentTemperatureMsr;

    /**
     * Gets the value of the heatTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeatTreatment() {
        return heatTreatment;
    }

    /**
     * Sets the value of the heatTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeatTreatment(String value) {
        this.heatTreatment = value;
    }

    /**
     * Gets the value of the temperature property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTemperature() {
        return temperature;
    }

    /**
     * Sets the value of the temperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTemperature(Integer value) {
        this.temperature = value;
    }

    /**
     * Gets the value of the temperatureMsr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemperatureMsr() {
        return temperatureMsr;
    }

    /**
     * Sets the value of the temperatureMsr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemperatureMsr(String value) {
        this.temperatureMsr = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTime(Integer value) {
        this.time = value;
    }

    /**
     * Gets the value of the coolentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoolentMethod() {
        return coolentMethod;
    }

    /**
     * Sets the value of the coolentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoolentMethod(String value) {
        this.coolentMethod = value;
    }

    /**
     * Gets the value of the coolentTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCoolentTemperature() {
        return coolentTemperature;
    }

    /**
     * Sets the value of the coolentTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCoolentTemperature(Integer value) {
        this.coolentTemperature = value;
    }

    /**
     * Gets the value of the coolentTemperatureMsr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoolentTemperatureMsr() {
        return coolentTemperatureMsr;
    }

    /**
     * Sets the value of the coolentTemperatureMsr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoolentTemperatureMsr(String value) {
        this.coolentTemperatureMsr = value;
    }

}
