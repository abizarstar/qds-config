
package com.invera.stratix.services;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsExternalCertificatesInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsExternalCertificatesInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="certificateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sdo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imageNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="catalogRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorCertifcateReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="externalSystem" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="image" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="imgFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docMgmtGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extDocRef1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extDocRef2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsExternalCertificatesInput", propOrder = {
    "certificateType",
    "sdo",
    "imageNumber",
    "catalogRemark",
    "vendorCertifcateReference",
    "externalSystem",
    "image",
    "imgFormat",
    "docType",
    "docMgmtGroup",
    "extDocRef1",
    "extDocRef2"
})
public class UpdateQdsExternalCertificatesInput {

    @XmlElement(required = true)
    protected String certificateType;
    protected String sdo;
    protected Long imageNumber;
    protected String catalogRemark;
    protected String vendorCertifcateReference;
    protected int externalSystem;
    @XmlMimeType("application/octet-stream")
    protected DataHandler image;
    protected String imgFormat;
    protected String docType;
    protected String docMgmtGroup;
    protected String extDocRef1;
    protected String extDocRef2;

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the sdo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSdo() {
        return sdo;
    }

    /**
     * Sets the value of the sdo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSdo(String value) {
        this.sdo = value;
    }

    /**
     * Gets the value of the imageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getImageNumber() {
        return imageNumber;
    }

    /**
     * Sets the value of the imageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setImageNumber(Long value) {
        this.imageNumber = value;
    }

    /**
     * Gets the value of the catalogRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogRemark() {
        return catalogRemark;
    }

    /**
     * Sets the value of the catalogRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogRemark(String value) {
        this.catalogRemark = value;
    }

    /**
     * Gets the value of the vendorCertifcateReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCertifcateReference() {
        return vendorCertifcateReference;
    }

    /**
     * Sets the value of the vendorCertifcateReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCertifcateReference(String value) {
        this.vendorCertifcateReference = value;
    }

    /**
     * Gets the value of the externalSystem property.
     * 
     */
    public int getExternalSystem() {
        return externalSystem;
    }

    /**
     * Sets the value of the externalSystem property.
     * 
     */
    public void setExternalSystem(int value) {
        this.externalSystem = value;
    }

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setImage(DataHandler value) {
        this.image = value;
    }

    /**
     * Gets the value of the imgFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImgFormat() {
        return imgFormat;
    }

    /**
     * Sets the value of the imgFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImgFormat(String value) {
        this.imgFormat = value;
    }

    /**
     * Gets the value of the docType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the docMgmtGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocMgmtGroup() {
        return docMgmtGroup;
    }

    /**
     * Sets the value of the docMgmtGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocMgmtGroup(String value) {
        this.docMgmtGroup = value;
    }

    /**
     * Gets the value of the extDocRef1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtDocRef1() {
        return extDocRef1;
    }

    /**
     * Sets the value of the extDocRef1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtDocRef1(String value) {
        this.extDocRef1 = value;
    }

    /**
     * Gets the value of the extDocRef2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtDocRef2() {
        return extDocRef2;
    }

    /**
     * Sets the value of the extDocRef2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtDocRef2(String value) {
        this.extDocRef2 = value;
    }

}
