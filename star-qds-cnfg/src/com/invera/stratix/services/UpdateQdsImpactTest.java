
package com.invera.stratix.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="updateQdsImpactTestInput" type="{http://stratix.invera.com/services}UpdateQdsImpactTestInput" maxOccurs="5" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "updateQdsImpactTestInput"
})
@XmlRootElement(name = "UpdateQdsImpactTest")
public class UpdateQdsImpactTest {

    protected long qdsNumber;
    protected List<UpdateQdsImpactTestInput> updateQdsImpactTestInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the updateQdsImpactTestInput property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateQdsImpactTestInput property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateQdsImpactTestInput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateQdsImpactTestInput }
     * 
     * 
     */
    public List<UpdateQdsImpactTestInput> getUpdateQdsImpactTestInput() {
        if (updateQdsImpactTestInput == null) {
            updateQdsImpactTestInput = new ArrayList<UpdateQdsImpactTestInput>();
        }
        return this.updateQdsImpactTestInput;
    }

}
