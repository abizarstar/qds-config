
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsTestInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsTestInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tstDirection" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tstAbbr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tstEntTyp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tstVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tstMinVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tstMaxVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tstAlpha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tstUm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsTestInput", propOrder = {
    "tstDirection",
    "tstAbbr",
    "tstEntTyp",
    "tstVal",
    "tstMinVal",
    "tstMaxVal",
    "tstAlpha",
    "tstUm"
})
public class UpdateQdsTestInput {

    @XmlElement(required = true)
    protected String tstDirection;
    @XmlElement(required = true)
    protected String tstAbbr;
    @XmlElement(required = true)
    protected String tstEntTyp;
    protected BigDecimal tstVal;
    protected BigDecimal tstMinVal;
    protected BigDecimal tstMaxVal;
    protected String tstAlpha;
    protected String tstUm;

    /**
     * Gets the value of the tstDirection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstDirection() {
        return tstDirection;
    }

    /**
     * Sets the value of the tstDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstDirection(String value) {
        this.tstDirection = value;
    }

    /**
     * Gets the value of the tstAbbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstAbbr() {
        return tstAbbr;
    }

    /**
     * Sets the value of the tstAbbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstAbbr(String value) {
        this.tstAbbr = value;
    }

    /**
     * Gets the value of the tstEntTyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstEntTyp() {
        return tstEntTyp;
    }

    /**
     * Sets the value of the tstEntTyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstEntTyp(String value) {
        this.tstEntTyp = value;
    }

    /**
     * Gets the value of the tstVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTstVal() {
        return tstVal;
    }

    /**
     * Sets the value of the tstVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTstVal(BigDecimal value) {
        this.tstVal = value;
    }

    /**
     * Gets the value of the tstMinVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTstMinVal() {
        return tstMinVal;
    }

    /**
     * Sets the value of the tstMinVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTstMinVal(BigDecimal value) {
        this.tstMinVal = value;
    }

    /**
     * Gets the value of the tstMaxVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTstMaxVal() {
        return tstMaxVal;
    }

    /**
     * Sets the value of the tstMaxVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTstMaxVal(BigDecimal value) {
        this.tstMaxVal = value;
    }

    /**
     * Gets the value of the tstAlpha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstAlpha() {
        return tstAlpha;
    }

    /**
     * Sets the value of the tstAlpha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstAlpha(String value) {
        this.tstAlpha = value;
    }

    /**
     * Gets the value of the tstUm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstUm() {
        return tstUm;
    }

    /**
     * Sets the value of the tstUm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstUm(String value) {
        this.tstUm = value;
    }

}
