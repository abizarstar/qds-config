
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="updateQdsMetalStandardInput" type="{http://stratix.invera.com/services}UpdateQdsMetalStandardInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "updateQdsMetalStandardInput"
})
@XmlRootElement(name = "UpdateQdsMetalStandard")
public class UpdateQdsMetalStandard {

    protected long qdsNumber;
    @XmlElement(required = true)
    protected UpdateQdsMetalStandardInput updateQdsMetalStandardInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the updateQdsMetalStandardInput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateQdsMetalStandardInput }
     *     
     */
    public UpdateQdsMetalStandardInput getUpdateQdsMetalStandardInput() {
        return updateQdsMetalStandardInput;
    }

    /**
     * Sets the value of the updateQdsMetalStandardInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateQdsMetalStandardInput }
     *     
     */
    public void setUpdateQdsMetalStandardInput(UpdateQdsMetalStandardInput value) {
        this.updateQdsMetalStandardInput = value;
    }

}
