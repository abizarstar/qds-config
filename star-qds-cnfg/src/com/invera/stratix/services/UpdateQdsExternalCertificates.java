
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="updateQdsExternalCertificatesInput" type="{http://stratix.invera.com/services}UpdateQdsExternalCertificatesInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "updateQdsExternalCertificatesInput"
})
@XmlRootElement(name = "UpdateQdsExternalCertificates")
public class UpdateQdsExternalCertificates {

    protected long qdsNumber;
    @XmlElement(required = true)
    protected UpdateQdsExternalCertificatesInput updateQdsExternalCertificatesInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the updateQdsExternalCertificatesInput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateQdsExternalCertificatesInput }
     *     
     */
    public UpdateQdsExternalCertificatesInput getUpdateQdsExternalCertificatesInput() {
        return updateQdsExternalCertificatesInput;
    }

    /**
     * Sets the value of the updateQdsExternalCertificatesInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateQdsExternalCertificatesInput }
     *     
     */
    public void setUpdateQdsExternalCertificatesInput(UpdateQdsExternalCertificatesInput value) {
        this.updateQdsExternalCertificatesInput = value;
    }

}
