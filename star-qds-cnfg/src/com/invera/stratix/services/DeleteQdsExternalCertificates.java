
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="deleteMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="certificateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sdo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "deleteMode",
    "certificateType",
    "sdo"
})
@XmlRootElement(name = "DeleteQdsExternalCertificates")
public class DeleteQdsExternalCertificates {

    protected long qdsNumber;
    protected int deleteMode;
    protected String certificateType;
    @XmlElement(name = "Sdo")
    protected String sdo;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the deleteMode property.
     * 
     */
    public int getDeleteMode() {
        return deleteMode;
    }

    /**
     * Sets the value of the deleteMode property.
     * 
     */
    public void setDeleteMode(int value) {
        this.deleteMode = value;
    }

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the sdo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSdo() {
        return sdo;
    }

    /**
     * Sets the value of the sdo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSdo(String value) {
        this.sdo = value;
    }

}
