
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsChemistryInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsChemistryInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chmElement" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="chmEntryType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="chmVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chmMinVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chmMaxVal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chmAlpha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsChemistryInput", propOrder = {
    "chmElement",
    "chmEntryType",
    "chmVal",
    "chmMinVal",
    "chmMaxVal",
    "chmAlpha"
})
public class UpdateQdsChemistryInput {

    @XmlElement(required = true)
    protected String chmElement;
    @XmlElement(required = true)
    protected String chmEntryType;
    protected BigDecimal chmVal;
    protected BigDecimal chmMinVal;
    protected BigDecimal chmMaxVal;
    protected String chmAlpha;

    /**
     * Gets the value of the chmElement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChmElement() {
        return chmElement;
    }

    /**
     * Sets the value of the chmElement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChmElement(String value) {
        this.chmElement = value;
    }

    /**
     * Gets the value of the chmEntryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChmEntryType() {
        return chmEntryType;
    }

    /**
     * Sets the value of the chmEntryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChmEntryType(String value) {
        this.chmEntryType = value;
    }

    /**
     * Gets the value of the chmVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChmVal() {
        return chmVal;
    }

    /**
     * Sets the value of the chmVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChmVal(BigDecimal value) {
        this.chmVal = value;
    }

    /**
     * Gets the value of the chmMinVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChmMinVal() {
        return chmMinVal;
    }

    /**
     * Sets the value of the chmMinVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChmMinVal(BigDecimal value) {
        this.chmMinVal = value;
    }

    /**
     * Gets the value of the chmMaxVal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChmMaxVal() {
        return chmMaxVal;
    }

    /**
     * Sets the value of the chmMaxVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChmMaxVal(BigDecimal value) {
        this.chmMaxVal = value;
    }

    /**
     * Gets the value of the chmAlpha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChmAlpha() {
        return chmAlpha;
    }

    /**
     * Sets the value of the chmAlpha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChmAlpha(String value) {
        this.chmAlpha = value;
    }

}
