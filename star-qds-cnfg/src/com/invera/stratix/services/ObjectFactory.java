
package com.invera.stratix.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.schema.common.datatypes.ServiceMessages;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.invera.stratix.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceMessagesHeader_QNAME = new QName("http://stratix.invera.com/services", "ServiceMessagesHeader");
    private final static QName _AuthenticationHeader_QNAME = new QName("http://stratix.invera.com/services", "AuthenticationHeader");
    private final static QName _StratixFault_QNAME = new QName("http://stratix.invera.com/services", "StratixFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.invera.stratix.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateQdsChemistry }
     * 
     */
    public UpdateQdsChemistry createUpdateQdsChemistry() {
        return new UpdateQdsChemistry();
    }

    /**
     * Create an instance of {@link UpdateQdsChemistryInput }
     * 
     */
    public UpdateQdsChemistryInput createUpdateQdsChemistryInput() {
        return new UpdateQdsChemistryInput();
    }

    /**
     * Create an instance of {@link UpdateVendorTagIdResponse }
     * 
     */
    public UpdateVendorTagIdResponse createUpdateVendorTagIdResponse() {
        return new UpdateVendorTagIdResponse();
    }

    /**
     * Create an instance of {@link UpdateVendorTagId }
     * 
     */
    public UpdateVendorTagId createUpdateVendorTagId() {
        return new UpdateVendorTagId();
    }

    /**
     * Create an instance of {@link UpdateVendorTagIdInput }
     * 
     */
    public UpdateVendorTagIdInput createUpdateVendorTagIdInput() {
        return new UpdateVendorTagIdInput();
    }

    /**
     * Create an instance of {@link DeleteVendorTagID }
     * 
     */
    public DeleteVendorTagID createDeleteVendorTagID() {
        return new DeleteVendorTagID();
    }

    /**
     * Create an instance of {@link DeleteQdsMetalStandard }
     * 
     */
    public DeleteQdsMetalStandard createDeleteQdsMetalStandard() {
        return new DeleteQdsMetalStandard();
    }

    /**
     * Create an instance of {@link DeleteQdsMicroinclusionResponse }
     * 
     */
    public DeleteQdsMicroinclusionResponse createDeleteQdsMicroinclusionResponse() {
        return new DeleteQdsMicroinclusionResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsTest }
     * 
     */
    public DeleteQdsTest createDeleteQdsTest() {
        return new DeleteQdsTest();
    }

    /**
     * Create an instance of {@link UpdateQdsInstructions }
     * 
     */
    public UpdateQdsInstructions createUpdateQdsInstructions() {
        return new UpdateQdsInstructions();
    }

    /**
     * Create an instance of {@link UpdateQdsInstructionsInput }
     * 
     */
    public UpdateQdsInstructionsInput createUpdateQdsInstructionsInput() {
        return new UpdateQdsInstructionsInput();
    }

    /**
     * Create an instance of {@link UpdateQdsMicroinclusionResponse }
     * 
     */
    public UpdateQdsMicroinclusionResponse createUpdateQdsMicroinclusionResponse() {
        return new UpdateQdsMicroinclusionResponse();
    }

    /**
     * Create an instance of {@link DeleteVendorTagIDResponse }
     * 
     */
    public DeleteVendorTagIDResponse createDeleteVendorTagIDResponse() {
        return new DeleteVendorTagIDResponse();
    }

    /**
     * Create an instance of {@link CreateQds }
     * 
     */
    public CreateQds createCreateQds() {
        return new CreateQds();
    }

    /**
     * Create an instance of {@link CreateQdsInput }
     * 
     */
    public CreateQdsInput createCreateQdsInput() {
        return new CreateQdsInput();
    }

    /**
     * Create an instance of {@link MovePrdItmToPrdQds }
     * 
     */
    public MovePrdItmToPrdQds createMovePrdItmToPrdQds() {
        return new MovePrdItmToPrdQds();
    }

    /**
     * Create an instance of {@link MovePrdItmToPrdQdsInput }
     * 
     */
    public MovePrdItmToPrdQdsInput createMovePrdItmToPrdQdsInput() {
        return new MovePrdItmToPrdQdsInput();
    }

    /**
     * Create an instance of {@link UpdateQdsHeader }
     * 
     */
    public UpdateQdsHeader createUpdateQdsHeader() {
        return new UpdateQdsHeader();
    }

    /**
     * Create an instance of {@link UpdateQdsHeaderInput }
     * 
     */
    public UpdateQdsHeaderInput createUpdateQdsHeaderInput() {
        return new UpdateQdsHeaderInput();
    }

    /**
     * Create an instance of {@link DeleteQdsImpactTest }
     * 
     */
    public DeleteQdsImpactTest createDeleteQdsImpactTest() {
        return new DeleteQdsImpactTest();
    }

    /**
     * Create an instance of {@link UpdateQdsHeatTreatmentResponse }
     * 
     */
    public UpdateQdsHeatTreatmentResponse createUpdateQdsHeatTreatmentResponse() {
        return new UpdateQdsHeatTreatmentResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsImpactTestResponse }
     * 
     */
    public UpdateQdsImpactTestResponse createUpdateQdsImpactTestResponse() {
        return new UpdateQdsImpactTestResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsMicroinclusion }
     * 
     */
    public DeleteQdsMicroinclusion createDeleteQdsMicroinclusion() {
        return new DeleteQdsMicroinclusion();
    }

    /**
     * Create an instance of {@link UpdateQdsMetalStandard }
     * 
     */
    public UpdateQdsMetalStandard createUpdateQdsMetalStandard() {
        return new UpdateQdsMetalStandard();
    }

    /**
     * Create an instance of {@link UpdateQdsMetalStandardInput }
     * 
     */
    public UpdateQdsMetalStandardInput createUpdateQdsMetalStandardInput() {
        return new UpdateQdsMetalStandardInput();
    }

    /**
     * Create an instance of {@link UpdateQdsExternalCertificatesResponse }
     * 
     */
    public UpdateQdsExternalCertificatesResponse createUpdateQdsExternalCertificatesResponse() {
        return new UpdateQdsExternalCertificatesResponse();
    }

    /**
     * Create an instance of {@link MovePrdItmToPrdQdsResponse }
     * 
     */
    public MovePrdItmToPrdQdsResponse createMovePrdItmToPrdQdsResponse() {
        return new MovePrdItmToPrdQdsResponse();
    }

    /**
     * Create an instance of {@link CreateQdsResponse }
     * 
     */
    public CreateQdsResponse createCreateQdsResponse() {
        return new CreateQdsResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsHeaderResponse }
     * 
     */
    public UpdateQdsHeaderResponse createUpdateQdsHeaderResponse() {
        return new UpdateQdsHeaderResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsJominy }
     * 
     */
    public UpdateQdsJominy createUpdateQdsJominy() {
        return new UpdateQdsJominy();
    }

    /**
     * Create an instance of {@link UpdateQdsJominyInput }
     * 
     */
    public UpdateQdsJominyInput createUpdateQdsJominyInput() {
        return new UpdateQdsJominyInput();
    }

    /**
     * Create an instance of {@link UpdateQdsImpactTest }
     * 
     */
    public UpdateQdsImpactTest createUpdateQdsImpactTest() {
        return new UpdateQdsImpactTest();
    }

    /**
     * Create an instance of {@link UpdateQdsImpactTestInput }
     * 
     */
    public UpdateQdsImpactTestInput createUpdateQdsImpactTestInput() {
        return new UpdateQdsImpactTestInput();
    }

    /**
     * Create an instance of {@link UpdateQdsResponse }
     * 
     */
    public UpdateQdsResponse createUpdateQdsResponse() {
        return new UpdateQdsResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsExternalCertificates }
     * 
     */
    public UpdateQdsExternalCertificates createUpdateQdsExternalCertificates() {
        return new UpdateQdsExternalCertificates();
    }

    /**
     * Create an instance of {@link UpdateQdsExternalCertificatesInput }
     * 
     */
    public UpdateQdsExternalCertificatesInput createUpdateQdsExternalCertificatesInput() {
        return new UpdateQdsExternalCertificatesInput();
    }

    /**
     * Create an instance of {@link UpdateQdsChemistryResponse }
     * 
     */
    public UpdateQdsChemistryResponse createUpdateQdsChemistryResponse() {
        return new UpdateQdsChemistryResponse();
    }

    /**
     * Create an instance of {@link CompareQds }
     * 
     */
    public CompareQds createCompareQds() {
        return new CompareQds();
    }

    /**
     * Create an instance of {@link DeleteQdsChemistryResponse }
     * 
     */
    public DeleteQdsChemistryResponse createDeleteQdsChemistryResponse() {
        return new DeleteQdsChemistryResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsHeatTreatment }
     * 
     */
    public UpdateQdsHeatTreatment createUpdateQdsHeatTreatment() {
        return new UpdateQdsHeatTreatment();
    }

    /**
     * Create an instance of {@link UpdateQdsHeatTreatmentInput }
     * 
     */
    public UpdateQdsHeatTreatmentInput createUpdateQdsHeatTreatmentInput() {
        return new UpdateQdsHeatTreatmentInput();
    }

    /**
     * Create an instance of {@link DeleteQdsJominyResponse }
     * 
     */
    public DeleteQdsJominyResponse createDeleteQdsJominyResponse() {
        return new DeleteQdsJominyResponse();
    }

    /**
     * Create an instance of {@link UpdateQds }
     * 
     */
    public UpdateQds createUpdateQds() {
        return new UpdateQds();
    }

    /**
     * Create an instance of {@link UpdateQdsInput }
     * 
     */
    public UpdateQdsInput createUpdateQdsInput() {
        return new UpdateQdsInput();
    }

    /**
     * Create an instance of {@link UpdateQdsMetalStandardResponse }
     * 
     */
    public UpdateQdsMetalStandardResponse createUpdateQdsMetalStandardResponse() {
        return new UpdateQdsMetalStandardResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsResponse }
     * 
     */
    public DeleteQdsResponse createDeleteQdsResponse() {
        return new DeleteQdsResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsTestResponse }
     * 
     */
    public UpdateQdsTestResponse createUpdateQdsTestResponse() {
        return new UpdateQdsTestResponse();
    }

    /**
     * Create an instance of {@link CompareQdsResponse }
     * 
     */
    public CompareQdsResponse createCompareQdsResponse() {
        return new CompareQdsResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsJominy }
     * 
     */
    public DeleteQdsJominy createDeleteQdsJominy() {
        return new DeleteQdsJominy();
    }

    /**
     * Create an instance of {@link DeleteQdsChemistry }
     * 
     */
    public DeleteQdsChemistry createDeleteQdsChemistry() {
        return new DeleteQdsChemistry();
    }

    /**
     * Create an instance of {@link DeleteQds }
     * 
     */
    public DeleteQds createDeleteQds() {
        return new DeleteQds();
    }

    /**
     * Create an instance of {@link DeleteQdsExternalCertificatesResponse }
     * 
     */
    public DeleteQdsExternalCertificatesResponse createDeleteQdsExternalCertificatesResponse() {
        return new DeleteQdsExternalCertificatesResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsJominyResponse }
     * 
     */
    public UpdateQdsJominyResponse createUpdateQdsJominyResponse() {
        return new UpdateQdsJominyResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsImpactTestResponse }
     * 
     */
    public DeleteQdsImpactTestResponse createDeleteQdsImpactTestResponse() {
        return new DeleteQdsImpactTestResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsHeatTreatmentResponse }
     * 
     */
    public DeleteQdsHeatTreatmentResponse createDeleteQdsHeatTreatmentResponse() {
        return new DeleteQdsHeatTreatmentResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsInstructions }
     * 
     */
    public DeleteQdsInstructions createDeleteQdsInstructions() {
        return new DeleteQdsInstructions();
    }

    /**
     * Create an instance of {@link DeleteQdsMetalStandardResponse }
     * 
     */
    public DeleteQdsMetalStandardResponse createDeleteQdsMetalStandardResponse() {
        return new DeleteQdsMetalStandardResponse();
    }

    /**
     * Create an instance of {@link DeleteQdsInstructionsResponse }
     * 
     */
    public DeleteQdsInstructionsResponse createDeleteQdsInstructionsResponse() {
        return new DeleteQdsInstructionsResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsMicroinclusion }
     * 
     */
    public UpdateQdsMicroinclusion createUpdateQdsMicroinclusion() {
        return new UpdateQdsMicroinclusion();
    }

    /**
     * Create an instance of {@link UpdateQdsMicroinclusionInput }
     * 
     */
    public UpdateQdsMicroinclusionInput createUpdateQdsMicroinclusionInput() {
        return new UpdateQdsMicroinclusionInput();
    }

    /**
     * Create an instance of {@link DeleteQdsHeatTreatment }
     * 
     */
    public DeleteQdsHeatTreatment createDeleteQdsHeatTreatment() {
        return new DeleteQdsHeatTreatment();
    }

    /**
     * Create an instance of {@link DeleteQdsTestResponse }
     * 
     */
    public DeleteQdsTestResponse createDeleteQdsTestResponse() {
        return new DeleteQdsTestResponse();
    }

    /**
     * Create an instance of {@link UpdateQdsTest }
     * 
     */
    public UpdateQdsTest createUpdateQdsTest() {
        return new UpdateQdsTest();
    }

    /**
     * Create an instance of {@link UpdateQdsTestInput }
     * 
     */
    public UpdateQdsTestInput createUpdateQdsTestInput() {
        return new UpdateQdsTestInput();
    }

    /**
     * Create an instance of {@link DeleteQdsExternalCertificates }
     * 
     */
    public DeleteQdsExternalCertificates createDeleteQdsExternalCertificates() {
        return new DeleteQdsExternalCertificates();
    }

    /**
     * Create an instance of {@link UpdateQdsInstructionsResponse }
     * 
     */
    public UpdateQdsInstructionsResponse createUpdateQdsInstructionsResponse() {
        return new UpdateQdsInstructionsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceMessages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "ServiceMessagesHeader")
    public JAXBElement<ServiceMessages> createServiceMessagesHeader(ServiceMessages value) {
        return new JAXBElement<ServiceMessages>(_ServiceMessagesHeader_QNAME, ServiceMessages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "AuthenticationHeader")
    public JAXBElement<AuthenticationToken> createAuthenticationHeader(AuthenticationToken value) {
        return new JAXBElement<AuthenticationToken>(_AuthenticationHeader_QNAME, AuthenticationToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "StratixFault")
    public JAXBElement<String> createStratixFault(String value) {
        return new JAXBElement<String>(_StratixFault_QNAME, String.class, null, value);
    }

}
