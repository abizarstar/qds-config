
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsMetalStandardInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsMetalStandardInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sdo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stdId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="addnlId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matchingChemistry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matchingTest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="matchingJominy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="certifiedByServiceCenter" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsMetalStandardInput", propOrder = {
    "sdo",
    "stdId",
    "addnlId",
    "matchingChemistry",
    "matchingTest",
    "matchingJominy",
    "certifiedByServiceCenter"
})
public class UpdateQdsMetalStandardInput {

    @XmlElement(required = true)
    protected String sdo;
    @XmlElement(required = true)
    protected String stdId;
    @XmlElement(required = true)
    protected String addnlId;
    @XmlElement(required = true)
    protected String matchingChemistry;
    @XmlElement(required = true)
    protected String matchingTest;
    @XmlElement(required = true)
    protected String matchingJominy;
    protected int certifiedByServiceCenter;

    /**
     * Gets the value of the sdo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSdo() {
        return sdo;
    }

    /**
     * Sets the value of the sdo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSdo(String value) {
        this.sdo = value;
    }

    /**
     * Gets the value of the stdId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStdId() {
        return stdId;
    }

    /**
     * Sets the value of the stdId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStdId(String value) {
        this.stdId = value;
    }

    /**
     * Gets the value of the addnlId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddnlId() {
        return addnlId;
    }

    /**
     * Sets the value of the addnlId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddnlId(String value) {
        this.addnlId = value;
    }

    /**
     * Gets the value of the matchingChemistry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchingChemistry() {
        return matchingChemistry;
    }

    /**
     * Sets the value of the matchingChemistry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchingChemistry(String value) {
        this.matchingChemistry = value;
    }

    /**
     * Gets the value of the matchingTest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchingTest() {
        return matchingTest;
    }

    /**
     * Sets the value of the matchingTest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchingTest(String value) {
        this.matchingTest = value;
    }

    /**
     * Gets the value of the matchingJominy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchingJominy() {
        return matchingJominy;
    }

    /**
     * Sets the value of the matchingJominy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchingJominy(String value) {
        this.matchingJominy = value;
    }

    /**
     * Gets the value of the certifiedByServiceCenter property.
     * 
     */
    public int getCertifiedByServiceCenter() {
        return certifiedByServiceCenter;
    }

    /**
     * Sets the value of the certifiedByServiceCenter property.
     * 
     */
    public void setCertifiedByServiceCenter(int value) {
        this.certifiedByServiceCenter = value;
    }

}
