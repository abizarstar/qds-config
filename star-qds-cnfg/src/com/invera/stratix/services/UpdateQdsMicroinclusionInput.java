
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsMicroinclusionInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsMicroinclusionInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="metalStandard" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="thinResultA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thickResultA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thinResultB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thickResultB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thinResultC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thickResultC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thinResultD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="thickResultD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsMicroinclusionInput", propOrder = {
    "metalStandard",
    "thinResultA",
    "thickResultA",
    "thinResultB",
    "thickResultB",
    "thinResultC",
    "thickResultC",
    "thinResultD",
    "thickResultD"
})
public class UpdateQdsMicroinclusionInput {

    @XmlElement(required = true)
    protected String metalStandard;
    protected BigDecimal thinResultA;
    protected BigDecimal thickResultA;
    protected BigDecimal thinResultB;
    protected BigDecimal thickResultB;
    protected BigDecimal thinResultC;
    protected BigDecimal thickResultC;
    protected BigDecimal thinResultD;
    protected BigDecimal thickResultD;

    /**
     * Gets the value of the metalStandard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetalStandard() {
        return metalStandard;
    }

    /**
     * Sets the value of the metalStandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetalStandard(String value) {
        this.metalStandard = value;
    }

    /**
     * Gets the value of the thinResultA property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThinResultA() {
        return thinResultA;
    }

    /**
     * Sets the value of the thinResultA property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThinResultA(BigDecimal value) {
        this.thinResultA = value;
    }

    /**
     * Gets the value of the thickResultA property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThickResultA() {
        return thickResultA;
    }

    /**
     * Sets the value of the thickResultA property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThickResultA(BigDecimal value) {
        this.thickResultA = value;
    }

    /**
     * Gets the value of the thinResultB property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThinResultB() {
        return thinResultB;
    }

    /**
     * Sets the value of the thinResultB property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThinResultB(BigDecimal value) {
        this.thinResultB = value;
    }

    /**
     * Gets the value of the thickResultB property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThickResultB() {
        return thickResultB;
    }

    /**
     * Sets the value of the thickResultB property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThickResultB(BigDecimal value) {
        this.thickResultB = value;
    }

    /**
     * Gets the value of the thinResultC property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThinResultC() {
        return thinResultC;
    }

    /**
     * Sets the value of the thinResultC property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThinResultC(BigDecimal value) {
        this.thinResultC = value;
    }

    /**
     * Gets the value of the thickResultC property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThickResultC() {
        return thickResultC;
    }

    /**
     * Sets the value of the thickResultC property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThickResultC(BigDecimal value) {
        this.thickResultC = value;
    }

    /**
     * Gets the value of the thinResultD property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThinResultD() {
        return thinResultD;
    }

    /**
     * Sets the value of the thinResultD property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThinResultD(BigDecimal value) {
        this.thinResultD = value;
    }

    /**
     * Gets the value of the thickResultD property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThickResultD() {
        return thickResultD;
    }

    /**
     * Sets the value of the thickResultD property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThickResultD(BigDecimal value) {
        this.thickResultD = value;
    }

}
