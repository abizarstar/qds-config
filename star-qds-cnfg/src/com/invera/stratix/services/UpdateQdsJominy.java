
package com.invera.stratix.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="jominyTestNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="jominyTestType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="updateQdsJominyInput" type="{http://stratix.invera.com/services}UpdateQdsJominyInput" maxOccurs="30" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "jominyTestNo",
    "jominyTestType",
    "updateQdsJominyInput"
})
@XmlRootElement(name = "UpdateQdsJominy")
public class UpdateQdsJominy {

    protected long qdsNumber;
    protected int jominyTestNo;
    @XmlElement(required = true)
    protected String jominyTestType;
    protected List<UpdateQdsJominyInput> updateQdsJominyInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the jominyTestNo property.
     * 
     */
    public int getJominyTestNo() {
        return jominyTestNo;
    }

    /**
     * Sets the value of the jominyTestNo property.
     * 
     */
    public void setJominyTestNo(int value) {
        this.jominyTestNo = value;
    }

    /**
     * Gets the value of the jominyTestType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJominyTestType() {
        return jominyTestType;
    }

    /**
     * Sets the value of the jominyTestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJominyTestType(String value) {
        this.jominyTestType = value;
    }

    /**
     * Gets the value of the updateQdsJominyInput property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateQdsJominyInput property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateQdsJominyInput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateQdsJominyInput }
     * 
     * 
     */
    public List<UpdateQdsJominyInput> getUpdateQdsJominyInput() {
        if (updateQdsJominyInput == null) {
            updateQdsJominyInput = new ArrayList<UpdateQdsJominyInput>();
        }
        return this.updateQdsJominyInput;
    }

}
