
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateQdsInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateQdsInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="linkType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="applicableAll" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itmCtlNo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tagNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateQdsInput", propOrder = {
    "linkType",
    "applicableAll",
    "itmCtlNo",
    "tagNo",
    "qdsNumber"
})
public class UpdateQdsInput {

    protected int linkType;
    protected String applicableAll;
    protected Long itmCtlNo;
    protected String tagNo;
    protected Long qdsNumber;

    /**
     * Gets the value of the linkType property.
     * 
     */
    public int getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     */
    public void setLinkType(int value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the applicableAll property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicableAll() {
        return applicableAll;
    }

    /**
     * Sets the value of the applicableAll property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicableAll(String value) {
        this.applicableAll = value;
    }

    /**
     * Gets the value of the itmCtlNo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getItmCtlNo() {
        return itmCtlNo;
    }

    /**
     * Sets the value of the itmCtlNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setItmCtlNo(Long value) {
        this.itmCtlNo = value;
    }

    /**
     * Gets the value of the tagNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagNo() {
        return tagNo;
    }

    /**
     * Sets the value of the tagNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagNo(String value) {
        this.tagNo = value;
    }

    /**
     * Gets the value of the qdsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setQdsNumber(Long value) {
        this.qdsNumber = value;
    }

}
