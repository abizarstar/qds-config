
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="updateQdsMicroinclusionInput" type="{http://stratix.invera.com/services}UpdateQdsMicroinclusionInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "updateQdsMicroinclusionInput"
})
@XmlRootElement(name = "UpdateQdsMicroinclusion")
public class UpdateQdsMicroinclusion {

    protected long qdsNumber;
    @XmlElement(required = true)
    protected UpdateQdsMicroinclusionInput updateQdsMicroinclusionInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the updateQdsMicroinclusionInput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateQdsMicroinclusionInput }
     *     
     */
    public UpdateQdsMicroinclusionInput getUpdateQdsMicroinclusionInput() {
        return updateQdsMicroinclusionInput;
    }

    /**
     * Sets the value of the updateQdsMicroinclusionInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateQdsMicroinclusionInput }
     *     
     */
    public void setUpdateQdsMicroinclusionInput(UpdateQdsMicroinclusionInput value) {
        this.updateQdsMicroinclusionInput = value;
    }

}
