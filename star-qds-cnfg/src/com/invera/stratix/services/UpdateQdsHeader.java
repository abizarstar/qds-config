
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="qdsNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="updateQdsHeaderInput" type="{http://stratix.invera.com/services}UpdateQdsHeaderInput"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qdsNumber",
    "updateQdsHeaderInput"
})
@XmlRootElement(name = "UpdateQdsHeader")
public class UpdateQdsHeader {

    protected long qdsNumber;
    @XmlElement(required = true)
    protected UpdateQdsHeaderInput updateQdsHeaderInput;

    /**
     * Gets the value of the qdsNumber property.
     * 
     */
    public long getQdsNumber() {
        return qdsNumber;
    }

    /**
     * Sets the value of the qdsNumber property.
     * 
     */
    public void setQdsNumber(long value) {
        this.qdsNumber = value;
    }

    /**
     * Gets the value of the updateQdsHeaderInput property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateQdsHeaderInput }
     *     
     */
    public UpdateQdsHeaderInput getUpdateQdsHeaderInput() {
        return updateQdsHeaderInput;
    }

    /**
     * Sets the value of the updateQdsHeaderInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateQdsHeaderInput }
     *     
     */
    public void setUpdateQdsHeaderInput(UpdateQdsHeaderInput value) {
        this.updateQdsHeaderInput = value;
    }

}
