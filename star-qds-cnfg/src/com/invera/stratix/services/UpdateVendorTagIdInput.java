
package com.invera.stratix.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateVendorTagIdInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateVendorTagIdInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vendorTagId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateVendorTagIdInput", propOrder = {
    "vendorTagId"
})
public class UpdateVendorTagIdInput {

    @XmlElement(required = true)
    protected String vendorTagId;

    /**
     * Gets the value of the vendorTagId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorTagId() {
        return vendorTagId;
    }

    /**
     * Sets the value of the vendorTagId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorTagId(String value) {
        this.vendorTagId = value;
    }

}
