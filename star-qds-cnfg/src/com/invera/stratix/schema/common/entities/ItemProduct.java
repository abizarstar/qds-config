
package com.invera.stratix.schema.common.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItemProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemProduct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gradeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numericSize1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="numericSize2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="numericSize3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="numericSize4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="numericSize5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="sizeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="finishId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extendedFinish" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="10" minOccurs="0"/>
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dimensionDesignator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="innerDiameter" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="outerDiameter" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="wallThickness" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="gaugeSize" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="gaugeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="alternateSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="randomDimension1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension6" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension7" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomDimension8" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="randomArea" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="customerVendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entryMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lengthInFeetOrMeters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="formatWithFractions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="orderedLengthType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formatDescriptionWithFractions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="formatDescriptionWithSize" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="partCustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="part" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partNumberRevisionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partAccess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partControlNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="grossWidth" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="grossLength" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="grossPieces" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossWidthInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossWidthNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossWidthDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossLengthInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossLengthNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="grossLengthDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="widthInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="widthNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="widthDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lengthInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lengthNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="lengthDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="innerDiameterInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="innerDiameterNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="innerDiameterDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="outerDiameterInteger" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="outerDiameterNumerator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="outerDiameterDenominator" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="customShape" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemProduct", propOrder = {
    "formId",
    "gradeId",
    "numericSize1",
    "numericSize2",
    "numericSize3",
    "numericSize4",
    "numericSize5",
    "sizeId",
    "finishId",
    "extendedFinish",
    "width",
    "length",
    "dimensionDesignator",
    "innerDiameter",
    "outerDiameter",
    "wallThickness",
    "gaugeSize",
    "gaugeType",
    "alternateSize",
    "randomDimension1",
    "randomDimension2",
    "randomDimension3",
    "randomDimension4",
    "randomDimension5",
    "randomDimension6",
    "randomDimension7",
    "randomDimension8",
    "randomArea",
    "customerVendorId",
    "entryMeasure",
    "lengthInFeetOrMeters",
    "formatWithFractions",
    "orderedLengthType",
    "formatDescriptionWithFractions",
    "formatDescriptionWithSize",
    "partCustomerId",
    "part",
    "partNumberRevisionNumber",
    "partAccess",
    "partControlNumber",
    "grossWidth",
    "grossLength",
    "grossPieces",
    "grossWidthInteger",
    "grossWidthNumerator",
    "grossWidthDenominator",
    "grossLengthInteger",
    "grossLengthNumerator",
    "grossLengthDenominator",
    "widthInteger",
    "widthNumerator",
    "widthDenominator",
    "lengthInteger",
    "lengthNumerator",
    "lengthDenominator",
    "innerDiameterInteger",
    "innerDiameterNumerator",
    "innerDiameterDenominator",
    "outerDiameterInteger",
    "outerDiameterNumerator",
    "outerDiameterDenominator",
    "customShape"
})
public class ItemProduct {

    protected String formId;
    protected String gradeId;
    protected BigDecimal numericSize1;
    protected BigDecimal numericSize2;
    protected BigDecimal numericSize3;
    protected BigDecimal numericSize4;
    protected BigDecimal numericSize5;
    protected String sizeId;
    protected String finishId;
    protected List<String> extendedFinish;
    protected BigDecimal width;
    protected BigDecimal length;
    protected String dimensionDesignator;
    protected BigDecimal innerDiameter;
    protected BigDecimal outerDiameter;
    protected BigDecimal wallThickness;
    protected BigDecimal gaugeSize;
    protected String gaugeType;
    protected String alternateSize;
    protected BigDecimal randomDimension1;
    protected BigDecimal randomDimension2;
    protected BigDecimal randomDimension3;
    protected BigDecimal randomDimension4;
    protected BigDecimal randomDimension5;
    protected BigDecimal randomDimension6;
    protected BigDecimal randomDimension7;
    protected BigDecimal randomDimension8;
    protected BigDecimal randomArea;
    protected String customerVendorId;
    protected String entryMeasure;
    protected Boolean lengthInFeetOrMeters;
    protected Boolean formatWithFractions;
    protected String orderedLengthType;
    protected Boolean formatDescriptionWithFractions;
    protected Boolean formatDescriptionWithSize;
    protected String partCustomerId;
    protected String part;
    protected String partNumberRevisionNumber;
    protected String partAccess;
    protected Long partControlNumber;
    protected BigDecimal grossWidth;
    protected BigDecimal grossLength;
    protected Integer grossPieces;
    protected Integer grossWidthInteger;
    protected Integer grossWidthNumerator;
    protected Integer grossWidthDenominator;
    protected Integer grossLengthInteger;
    protected Integer grossLengthNumerator;
    protected Integer grossLengthDenominator;
    protected Integer widthInteger;
    protected Integer widthNumerator;
    protected Integer widthDenominator;
    protected Integer lengthInteger;
    protected Integer lengthNumerator;
    protected Integer lengthDenominator;
    protected Integer innerDiameterInteger;
    protected Integer innerDiameterNumerator;
    protected Integer innerDiameterDenominator;
    protected Integer outerDiameterInteger;
    protected Integer outerDiameterNumerator;
    protected Integer outerDiameterDenominator;
    protected String customShape;

    /**
     * Gets the value of the formId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of the formId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormId(String value) {
        this.formId = value;
    }

    /**
     * Gets the value of the gradeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGradeId() {
        return gradeId;
    }

    /**
     * Sets the value of the gradeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGradeId(String value) {
        this.gradeId = value;
    }

    /**
     * Gets the value of the numericSize1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumericSize1() {
        return numericSize1;
    }

    /**
     * Sets the value of the numericSize1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumericSize1(BigDecimal value) {
        this.numericSize1 = value;
    }

    /**
     * Gets the value of the numericSize2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumericSize2() {
        return numericSize2;
    }

    /**
     * Sets the value of the numericSize2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumericSize2(BigDecimal value) {
        this.numericSize2 = value;
    }

    /**
     * Gets the value of the numericSize3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumericSize3() {
        return numericSize3;
    }

    /**
     * Sets the value of the numericSize3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumericSize3(BigDecimal value) {
        this.numericSize3 = value;
    }

    /**
     * Gets the value of the numericSize4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumericSize4() {
        return numericSize4;
    }

    /**
     * Sets the value of the numericSize4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumericSize4(BigDecimal value) {
        this.numericSize4 = value;
    }

    /**
     * Gets the value of the numericSize5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNumericSize5() {
        return numericSize5;
    }

    /**
     * Sets the value of the numericSize5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNumericSize5(BigDecimal value) {
        this.numericSize5 = value;
    }

    /**
     * Gets the value of the sizeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSizeId() {
        return sizeId;
    }

    /**
     * Sets the value of the sizeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSizeId(String value) {
        this.sizeId = value;
    }

    /**
     * Gets the value of the finishId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinishId() {
        return finishId;
    }

    /**
     * Sets the value of the finishId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinishId(String value) {
        this.finishId = value;
    }

    /**
     * Gets the value of the extendedFinish property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedFinish property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedFinish().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExtendedFinish() {
        if (extendedFinish == null) {
            extendedFinish = new ArrayList<String>();
        }
        return this.extendedFinish;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWidth(BigDecimal value) {
        this.width = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLength(BigDecimal value) {
        this.length = value;
    }

    /**
     * Gets the value of the dimensionDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDimensionDesignator() {
        return dimensionDesignator;
    }

    /**
     * Sets the value of the dimensionDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDimensionDesignator(String value) {
        this.dimensionDesignator = value;
    }

    /**
     * Gets the value of the innerDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInnerDiameter() {
        return innerDiameter;
    }

    /**
     * Sets the value of the innerDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInnerDiameter(BigDecimal value) {
        this.innerDiameter = value;
    }

    /**
     * Gets the value of the outerDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOuterDiameter() {
        return outerDiameter;
    }

    /**
     * Sets the value of the outerDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOuterDiameter(BigDecimal value) {
        this.outerDiameter = value;
    }

    /**
     * Gets the value of the wallThickness property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWallThickness() {
        return wallThickness;
    }

    /**
     * Sets the value of the wallThickness property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWallThickness(BigDecimal value) {
        this.wallThickness = value;
    }

    /**
     * Gets the value of the gaugeSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGaugeSize() {
        return gaugeSize;
    }

    /**
     * Sets the value of the gaugeSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGaugeSize(BigDecimal value) {
        this.gaugeSize = value;
    }

    /**
     * Gets the value of the gaugeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGaugeType() {
        return gaugeType;
    }

    /**
     * Sets the value of the gaugeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGaugeType(String value) {
        this.gaugeType = value;
    }

    /**
     * Gets the value of the alternateSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateSize() {
        return alternateSize;
    }

    /**
     * Sets the value of the alternateSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateSize(String value) {
        this.alternateSize = value;
    }

    /**
     * Gets the value of the randomDimension1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension1() {
        return randomDimension1;
    }

    /**
     * Sets the value of the randomDimension1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension1(BigDecimal value) {
        this.randomDimension1 = value;
    }

    /**
     * Gets the value of the randomDimension2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension2() {
        return randomDimension2;
    }

    /**
     * Sets the value of the randomDimension2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension2(BigDecimal value) {
        this.randomDimension2 = value;
    }

    /**
     * Gets the value of the randomDimension3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension3() {
        return randomDimension3;
    }

    /**
     * Sets the value of the randomDimension3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension3(BigDecimal value) {
        this.randomDimension3 = value;
    }

    /**
     * Gets the value of the randomDimension4 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension4() {
        return randomDimension4;
    }

    /**
     * Sets the value of the randomDimension4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension4(BigDecimal value) {
        this.randomDimension4 = value;
    }

    /**
     * Gets the value of the randomDimension5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension5() {
        return randomDimension5;
    }

    /**
     * Sets the value of the randomDimension5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension5(BigDecimal value) {
        this.randomDimension5 = value;
    }

    /**
     * Gets the value of the randomDimension6 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension6() {
        return randomDimension6;
    }

    /**
     * Sets the value of the randomDimension6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension6(BigDecimal value) {
        this.randomDimension6 = value;
    }

    /**
     * Gets the value of the randomDimension7 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension7() {
        return randomDimension7;
    }

    /**
     * Sets the value of the randomDimension7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension7(BigDecimal value) {
        this.randomDimension7 = value;
    }

    /**
     * Gets the value of the randomDimension8 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomDimension8() {
        return randomDimension8;
    }

    /**
     * Sets the value of the randomDimension8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomDimension8(BigDecimal value) {
        this.randomDimension8 = value;
    }

    /**
     * Gets the value of the randomArea property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRandomArea() {
        return randomArea;
    }

    /**
     * Sets the value of the randomArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRandomArea(BigDecimal value) {
        this.randomArea = value;
    }

    /**
     * Gets the value of the customerVendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerVendorId() {
        return customerVendorId;
    }

    /**
     * Sets the value of the customerVendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerVendorId(String value) {
        this.customerVendorId = value;
    }

    /**
     * Gets the value of the entryMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryMeasure() {
        return entryMeasure;
    }

    /**
     * Sets the value of the entryMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryMeasure(String value) {
        this.entryMeasure = value;
    }

    /**
     * Gets the value of the lengthInFeetOrMeters property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLengthInFeetOrMeters() {
        return lengthInFeetOrMeters;
    }

    /**
     * Sets the value of the lengthInFeetOrMeters property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLengthInFeetOrMeters(Boolean value) {
        this.lengthInFeetOrMeters = value;
    }

    /**
     * Gets the value of the formatWithFractions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFormatWithFractions() {
        return formatWithFractions;
    }

    /**
     * Sets the value of the formatWithFractions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormatWithFractions(Boolean value) {
        this.formatWithFractions = value;
    }

    /**
     * Gets the value of the orderedLengthType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderedLengthType() {
        return orderedLengthType;
    }

    /**
     * Sets the value of the orderedLengthType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderedLengthType(String value) {
        this.orderedLengthType = value;
    }

    /**
     * Gets the value of the formatDescriptionWithFractions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFormatDescriptionWithFractions() {
        return formatDescriptionWithFractions;
    }

    /**
     * Sets the value of the formatDescriptionWithFractions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormatDescriptionWithFractions(Boolean value) {
        this.formatDescriptionWithFractions = value;
    }

    /**
     * Gets the value of the formatDescriptionWithSize property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFormatDescriptionWithSize() {
        return formatDescriptionWithSize;
    }

    /**
     * Sets the value of the formatDescriptionWithSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormatDescriptionWithSize(Boolean value) {
        this.formatDescriptionWithSize = value;
    }

    /**
     * Gets the value of the partCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartCustomerId() {
        return partCustomerId;
    }

    /**
     * Sets the value of the partCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartCustomerId(String value) {
        this.partCustomerId = value;
    }

    /**
     * Gets the value of the part property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPart() {
        return part;
    }

    /**
     * Sets the value of the part property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPart(String value) {
        this.part = value;
    }

    /**
     * Gets the value of the partNumberRevisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumberRevisionNumber() {
        return partNumberRevisionNumber;
    }

    /**
     * Sets the value of the partNumberRevisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumberRevisionNumber(String value) {
        this.partNumberRevisionNumber = value;
    }

    /**
     * Gets the value of the partAccess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartAccess() {
        return partAccess;
    }

    /**
     * Sets the value of the partAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartAccess(String value) {
        this.partAccess = value;
    }

    /**
     * Gets the value of the partControlNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPartControlNumber() {
        return partControlNumber;
    }

    /**
     * Sets the value of the partControlNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPartControlNumber(Long value) {
        this.partControlNumber = value;
    }

    /**
     * Gets the value of the grossWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrossWidth() {
        return grossWidth;
    }

    /**
     * Sets the value of the grossWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrossWidth(BigDecimal value) {
        this.grossWidth = value;
    }

    /**
     * Gets the value of the grossLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrossLength() {
        return grossLength;
    }

    /**
     * Sets the value of the grossLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrossLength(BigDecimal value) {
        this.grossLength = value;
    }

    /**
     * Gets the value of the grossPieces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossPieces() {
        return grossPieces;
    }

    /**
     * Sets the value of the grossPieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossPieces(Integer value) {
        this.grossPieces = value;
    }

    /**
     * Gets the value of the grossWidthInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossWidthInteger() {
        return grossWidthInteger;
    }

    /**
     * Sets the value of the grossWidthInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossWidthInteger(Integer value) {
        this.grossWidthInteger = value;
    }

    /**
     * Gets the value of the grossWidthNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossWidthNumerator() {
        return grossWidthNumerator;
    }

    /**
     * Sets the value of the grossWidthNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossWidthNumerator(Integer value) {
        this.grossWidthNumerator = value;
    }

    /**
     * Gets the value of the grossWidthDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossWidthDenominator() {
        return grossWidthDenominator;
    }

    /**
     * Sets the value of the grossWidthDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossWidthDenominator(Integer value) {
        this.grossWidthDenominator = value;
    }

    /**
     * Gets the value of the grossLengthInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossLengthInteger() {
        return grossLengthInteger;
    }

    /**
     * Sets the value of the grossLengthInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossLengthInteger(Integer value) {
        this.grossLengthInteger = value;
    }

    /**
     * Gets the value of the grossLengthNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossLengthNumerator() {
        return grossLengthNumerator;
    }

    /**
     * Sets the value of the grossLengthNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossLengthNumerator(Integer value) {
        this.grossLengthNumerator = value;
    }

    /**
     * Gets the value of the grossLengthDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrossLengthDenominator() {
        return grossLengthDenominator;
    }

    /**
     * Sets the value of the grossLengthDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrossLengthDenominator(Integer value) {
        this.grossLengthDenominator = value;
    }

    /**
     * Gets the value of the widthInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidthInteger() {
        return widthInteger;
    }

    /**
     * Sets the value of the widthInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidthInteger(Integer value) {
        this.widthInteger = value;
    }

    /**
     * Gets the value of the widthNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidthNumerator() {
        return widthNumerator;
    }

    /**
     * Sets the value of the widthNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidthNumerator(Integer value) {
        this.widthNumerator = value;
    }

    /**
     * Gets the value of the widthDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidthDenominator() {
        return widthDenominator;
    }

    /**
     * Sets the value of the widthDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidthDenominator(Integer value) {
        this.widthDenominator = value;
    }

    /**
     * Gets the value of the lengthInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLengthInteger() {
        return lengthInteger;
    }

    /**
     * Sets the value of the lengthInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLengthInteger(Integer value) {
        this.lengthInteger = value;
    }

    /**
     * Gets the value of the lengthNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLengthNumerator() {
        return lengthNumerator;
    }

    /**
     * Sets the value of the lengthNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLengthNumerator(Integer value) {
        this.lengthNumerator = value;
    }

    /**
     * Gets the value of the lengthDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLengthDenominator() {
        return lengthDenominator;
    }

    /**
     * Sets the value of the lengthDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLengthDenominator(Integer value) {
        this.lengthDenominator = value;
    }

    /**
     * Gets the value of the innerDiameterInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInnerDiameterInteger() {
        return innerDiameterInteger;
    }

    /**
     * Sets the value of the innerDiameterInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInnerDiameterInteger(Integer value) {
        this.innerDiameterInteger = value;
    }

    /**
     * Gets the value of the innerDiameterNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInnerDiameterNumerator() {
        return innerDiameterNumerator;
    }

    /**
     * Sets the value of the innerDiameterNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInnerDiameterNumerator(Integer value) {
        this.innerDiameterNumerator = value;
    }

    /**
     * Gets the value of the innerDiameterDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInnerDiameterDenominator() {
        return innerDiameterDenominator;
    }

    /**
     * Sets the value of the innerDiameterDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInnerDiameterDenominator(Integer value) {
        this.innerDiameterDenominator = value;
    }

    /**
     * Gets the value of the outerDiameterInteger property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOuterDiameterInteger() {
        return outerDiameterInteger;
    }

    /**
     * Sets the value of the outerDiameterInteger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOuterDiameterInteger(Integer value) {
        this.outerDiameterInteger = value;
    }

    /**
     * Gets the value of the outerDiameterNumerator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOuterDiameterNumerator() {
        return outerDiameterNumerator;
    }

    /**
     * Sets the value of the outerDiameterNumerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOuterDiameterNumerator(Integer value) {
        this.outerDiameterNumerator = value;
    }

    /**
     * Gets the value of the outerDiameterDenominator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOuterDiameterDenominator() {
        return outerDiameterDenominator;
    }

    /**
     * Sets the value of the outerDiameterDenominator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOuterDiameterDenominator(Integer value) {
        this.outerDiameterDenominator = value;
    }

    /**
     * Gets the value of the customShape property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomShape() {
        return customShape;
    }

    /**
     * Sets the value of the customShape property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomShape(String value) {
        this.customShape = value;
    }

}
