
package com.invera.stratix.schema.common.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TestCertificate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestCertificate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="certificateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="standardDevelopmentOrganization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numberOfCopiesWithShipment" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numberOfCopiesToCertificateAddress" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="externalSystem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestCertificate", propOrder = {
    "certificateType",
    "standardDevelopmentOrganization",
    "numberOfCopiesWithShipment",
    "numberOfCopiesToCertificateAddress",
    "externalSystem"
})
public class TestCertificate {

    @XmlElement(required = true)
    protected String certificateType;
    @XmlElement(required = true)
    protected String standardDevelopmentOrganization;
    protected Integer numberOfCopiesWithShipment;
    protected Integer numberOfCopiesToCertificateAddress;
    protected Boolean externalSystem;

    /**
     * Gets the value of the certificateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificateType() {
        return certificateType;
    }

    /**
     * Sets the value of the certificateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificateType(String value) {
        this.certificateType = value;
    }

    /**
     * Gets the value of the standardDevelopmentOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardDevelopmentOrganization() {
        return standardDevelopmentOrganization;
    }

    /**
     * Sets the value of the standardDevelopmentOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardDevelopmentOrganization(String value) {
        this.standardDevelopmentOrganization = value;
    }

    /**
     * Gets the value of the numberOfCopiesWithShipment property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfCopiesWithShipment() {
        return numberOfCopiesWithShipment;
    }

    /**
     * Sets the value of the numberOfCopiesWithShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfCopiesWithShipment(Integer value) {
        this.numberOfCopiesWithShipment = value;
    }

    /**
     * Gets the value of the numberOfCopiesToCertificateAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfCopiesToCertificateAddress() {
        return numberOfCopiesToCertificateAddress;
    }

    /**
     * Sets the value of the numberOfCopiesToCertificateAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfCopiesToCertificateAddress(Integer value) {
        this.numberOfCopiesToCertificateAddress = value;
    }

    /**
     * Gets the value of the externalSystem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExternalSystem() {
        return externalSystem;
    }

    /**
     * Sets the value of the externalSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExternalSystem(Boolean value) {
        this.externalSystem = value;
    }

}
