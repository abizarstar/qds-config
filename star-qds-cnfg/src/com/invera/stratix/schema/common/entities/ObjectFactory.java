
package com.invera.stratix.schema.common.entities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.invera.stratix.schema.common.entities package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.invera.stratix.schema.common.entities
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link NameAndAddress }
     * 
     */
    public NameAndAddress createNameAndAddress() {
        return new NameAndAddress();
    }

    /**
     * Create an instance of {@link Instruction }
     * 
     */
    public Instruction createInstruction() {
        return new Instruction();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link TestCertificate }
     * 
     */
    public TestCertificate createTestCertificate() {
        return new TestCertificate();
    }

    /**
     * Create an instance of {@link ItemProduct }
     * 
     */
    public ItemProduct createItemProduct() {
        return new ItemProduct();
    }

    /**
     * Create an instance of {@link ProductDescription }
     * 
     */
    public ProductDescription createProductDescription() {
        return new ProductDescription();
    }

}
