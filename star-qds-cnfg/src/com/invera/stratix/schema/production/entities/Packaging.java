
package com.invera.stratix.schema.production.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Packaging complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Packaging">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packagingType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="skidType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="piecesPerTag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="minimumPiecesPerTag" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="minimumPackagingWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumPackagingWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Packaging", propOrder = {
    "packagingType",
    "skidType",
    "piecesPerTag",
    "minimumPiecesPerTag",
    "minimumPackagingWeight",
    "maximumPackagingWeight",
    "maximumHeight"
})
public class Packaging {

    @XmlElement(required = true)
    protected String packagingType;
    protected String skidType;
    protected Integer piecesPerTag;
    protected Integer minimumPiecesPerTag;
    protected BigDecimal minimumPackagingWeight;
    protected BigDecimal maximumPackagingWeight;
    protected BigDecimal maximumHeight;

    /**
     * Gets the value of the packagingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagingType() {
        return packagingType;
    }

    /**
     * Sets the value of the packagingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagingType(String value) {
        this.packagingType = value;
    }

    /**
     * Gets the value of the skidType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkidType() {
        return skidType;
    }

    /**
     * Sets the value of the skidType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkidType(String value) {
        this.skidType = value;
    }

    /**
     * Gets the value of the piecesPerTag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPiecesPerTag() {
        return piecesPerTag;
    }

    /**
     * Sets the value of the piecesPerTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPiecesPerTag(Integer value) {
        this.piecesPerTag = value;
    }

    /**
     * Gets the value of the minimumPiecesPerTag property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinimumPiecesPerTag() {
        return minimumPiecesPerTag;
    }

    /**
     * Sets the value of the minimumPiecesPerTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinimumPiecesPerTag(Integer value) {
        this.minimumPiecesPerTag = value;
    }

    /**
     * Gets the value of the minimumPackagingWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumPackagingWeight() {
        return minimumPackagingWeight;
    }

    /**
     * Sets the value of the minimumPackagingWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumPackagingWeight(BigDecimal value) {
        this.minimumPackagingWeight = value;
    }

    /**
     * Gets the value of the maximumPackagingWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumPackagingWeight() {
        return maximumPackagingWeight;
    }

    /**
     * Sets the value of the maximumPackagingWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumPackagingWeight(BigDecimal value) {
        this.maximumPackagingWeight = value;
    }

    /**
     * Gets the value of the maximumHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumHeight() {
        return maximumHeight;
    }

    /**
     * Sets the value of the maximumHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumHeight(BigDecimal value) {
        this.maximumHeight = value;
    }

}
