
package com.invera.stratix.schema.production.entities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.invera.stratix.schema.production.entities package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.invera.stratix.schema.production.entities
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Condition }
     * 
     */
    public Condition createCondition() {
        return new Condition();
    }

    /**
     * Create an instance of {@link CoilPackaging }
     * 
     */
    public CoilPackaging createCoilPackaging() {
        return new CoilPackaging();
    }

    /**
     * Create an instance of {@link Test }
     * 
     */
    public Test createTest() {
        return new Test();
    }

    /**
     * Create an instance of {@link Packaging }
     * 
     */
    public Packaging createPackaging() {
        return new Packaging();
    }

    /**
     * Create an instance of {@link StandardSpecification }
     * 
     */
    public StandardSpecification createStandardSpecification() {
        return new StandardSpecification();
    }

    /**
     * Create an instance of {@link Tolerances }
     * 
     */
    public Tolerances createTolerances() {
        return new Tolerances();
    }

    /**
     * Create an instance of {@link OriginZones }
     * 
     */
    public OriginZones createOriginZones() {
        return new OriginZones();
    }

    /**
     * Create an instance of {@link NarrowStripCoilSpecification }
     * 
     */
    public NarrowStripCoilSpecification createNarrowStripCoilSpecification() {
        return new NarrowStripCoilSpecification();
    }

    /**
     * Create an instance of {@link ChemicalSpecification }
     * 
     */
    public ChemicalSpecification createChemicalSpecification() {
        return new ChemicalSpecification();
    }

}
