
package com.invera.stratix.schema.production.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Tolerances complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tolerances">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="positiveWidthTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeWidthTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="positiveLengthTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeLengthTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="positiveInnerDiameterTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeInnerDiameterTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="positiveOuterDiameterTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeOuterDiameterTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="positiveWallThicknessTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeWallThicknessTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="positiveGaugeTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="negativeGaugeTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="diagonalTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="flatnessTolerance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tolerances", propOrder = {
    "positiveWidthTolerance",
    "negativeWidthTolerance",
    "positiveLengthTolerance",
    "negativeLengthTolerance",
    "positiveInnerDiameterTolerance",
    "negativeInnerDiameterTolerance",
    "positiveOuterDiameterTolerance",
    "negativeOuterDiameterTolerance",
    "positiveWallThicknessTolerance",
    "negativeWallThicknessTolerance",
    "positiveGaugeTolerance",
    "negativeGaugeTolerance",
    "diagonalTolerance",
    "flatnessTolerance"
})
public class Tolerances {

    protected BigDecimal positiveWidthTolerance;
    protected BigDecimal negativeWidthTolerance;
    protected BigDecimal positiveLengthTolerance;
    protected BigDecimal negativeLengthTolerance;
    protected BigDecimal positiveInnerDiameterTolerance;
    protected BigDecimal negativeInnerDiameterTolerance;
    protected BigDecimal positiveOuterDiameterTolerance;
    protected BigDecimal negativeOuterDiameterTolerance;
    protected BigDecimal positiveWallThicknessTolerance;
    protected BigDecimal negativeWallThicknessTolerance;
    protected BigDecimal positiveGaugeTolerance;
    protected BigDecimal negativeGaugeTolerance;
    protected BigDecimal diagonalTolerance;
    protected BigDecimal flatnessTolerance;

    /**
     * Gets the value of the positiveWidthTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveWidthTolerance() {
        return positiveWidthTolerance;
    }

    /**
     * Sets the value of the positiveWidthTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveWidthTolerance(BigDecimal value) {
        this.positiveWidthTolerance = value;
    }

    /**
     * Gets the value of the negativeWidthTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeWidthTolerance() {
        return negativeWidthTolerance;
    }

    /**
     * Sets the value of the negativeWidthTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeWidthTolerance(BigDecimal value) {
        this.negativeWidthTolerance = value;
    }

    /**
     * Gets the value of the positiveLengthTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveLengthTolerance() {
        return positiveLengthTolerance;
    }

    /**
     * Sets the value of the positiveLengthTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveLengthTolerance(BigDecimal value) {
        this.positiveLengthTolerance = value;
    }

    /**
     * Gets the value of the negativeLengthTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeLengthTolerance() {
        return negativeLengthTolerance;
    }

    /**
     * Sets the value of the negativeLengthTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeLengthTolerance(BigDecimal value) {
        this.negativeLengthTolerance = value;
    }

    /**
     * Gets the value of the positiveInnerDiameterTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveInnerDiameterTolerance() {
        return positiveInnerDiameterTolerance;
    }

    /**
     * Sets the value of the positiveInnerDiameterTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveInnerDiameterTolerance(BigDecimal value) {
        this.positiveInnerDiameterTolerance = value;
    }

    /**
     * Gets the value of the negativeInnerDiameterTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeInnerDiameterTolerance() {
        return negativeInnerDiameterTolerance;
    }

    /**
     * Sets the value of the negativeInnerDiameterTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeInnerDiameterTolerance(BigDecimal value) {
        this.negativeInnerDiameterTolerance = value;
    }

    /**
     * Gets the value of the positiveOuterDiameterTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveOuterDiameterTolerance() {
        return positiveOuterDiameterTolerance;
    }

    /**
     * Sets the value of the positiveOuterDiameterTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveOuterDiameterTolerance(BigDecimal value) {
        this.positiveOuterDiameterTolerance = value;
    }

    /**
     * Gets the value of the negativeOuterDiameterTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeOuterDiameterTolerance() {
        return negativeOuterDiameterTolerance;
    }

    /**
     * Sets the value of the negativeOuterDiameterTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeOuterDiameterTolerance(BigDecimal value) {
        this.negativeOuterDiameterTolerance = value;
    }

    /**
     * Gets the value of the positiveWallThicknessTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveWallThicknessTolerance() {
        return positiveWallThicknessTolerance;
    }

    /**
     * Sets the value of the positiveWallThicknessTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveWallThicknessTolerance(BigDecimal value) {
        this.positiveWallThicknessTolerance = value;
    }

    /**
     * Gets the value of the negativeWallThicknessTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeWallThicknessTolerance() {
        return negativeWallThicknessTolerance;
    }

    /**
     * Sets the value of the negativeWallThicknessTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeWallThicknessTolerance(BigDecimal value) {
        this.negativeWallThicknessTolerance = value;
    }

    /**
     * Gets the value of the positiveGaugeTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPositiveGaugeTolerance() {
        return positiveGaugeTolerance;
    }

    /**
     * Sets the value of the positiveGaugeTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPositiveGaugeTolerance(BigDecimal value) {
        this.positiveGaugeTolerance = value;
    }

    /**
     * Gets the value of the negativeGaugeTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNegativeGaugeTolerance() {
        return negativeGaugeTolerance;
    }

    /**
     * Sets the value of the negativeGaugeTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNegativeGaugeTolerance(BigDecimal value) {
        this.negativeGaugeTolerance = value;
    }

    /**
     * Gets the value of the diagonalTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiagonalTolerance() {
        return diagonalTolerance;
    }

    /**
     * Sets the value of the diagonalTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiagonalTolerance(BigDecimal value) {
        this.diagonalTolerance = value;
    }

    /**
     * Gets the value of the flatnessTolerance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFlatnessTolerance() {
        return flatnessTolerance;
    }

    /**
     * Sets the value of the flatnessTolerance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFlatnessTolerance(BigDecimal value) {
        this.flatnessTolerance = value;
    }

}
