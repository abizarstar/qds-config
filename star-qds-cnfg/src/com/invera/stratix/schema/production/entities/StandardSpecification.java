
package com.invera.stratix.schema.production.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardSpecification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="standardDevelopmentOrganization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="standardId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="additionalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardSpecification", propOrder = {
    "standardDevelopmentOrganization",
    "standardId",
    "additionalId"
})
public class StandardSpecification {

    @XmlElement(required = true)
    protected String standardDevelopmentOrganization;
    @XmlElement(required = true)
    protected String standardId;
    protected String additionalId;

    /**
     * Gets the value of the standardDevelopmentOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardDevelopmentOrganization() {
        return standardDevelopmentOrganization;
    }

    /**
     * Sets the value of the standardDevelopmentOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardDevelopmentOrganization(String value) {
        this.standardDevelopmentOrganization = value;
    }

    /**
     * Gets the value of the standardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandardId() {
        return standardId;
    }

    /**
     * Sets the value of the standardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandardId(String value) {
        this.standardId = value;
    }

    /**
     * Gets the value of the additionalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalId() {
        return additionalId;
    }

    /**
     * Sets the value of the additionalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalId(String value) {
        this.additionalId = value;
    }

}
