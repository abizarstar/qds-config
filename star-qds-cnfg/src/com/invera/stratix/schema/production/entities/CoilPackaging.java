
package com.invera.stratix.schema.production.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoilPackaging complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoilPackaging">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="internalDiameter" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minimumOuterDiameter" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumOuterDiameter" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minimumCoilWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumCoilWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="minimumCoilLength" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumCoilLength" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoilPackaging", propOrder = {
    "internalDiameter",
    "minimumOuterDiameter",
    "maximumOuterDiameter",
    "minimumCoilWeight",
    "maximumCoilWeight",
    "minimumCoilLength",
    "maximumCoilLength"
})
public class CoilPackaging {

    protected BigDecimal internalDiameter;
    protected BigDecimal minimumOuterDiameter;
    protected BigDecimal maximumOuterDiameter;
    protected BigDecimal minimumCoilWeight;
    protected BigDecimal maximumCoilWeight;
    protected BigDecimal minimumCoilLength;
    protected BigDecimal maximumCoilLength;

    /**
     * Gets the value of the internalDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInternalDiameter() {
        return internalDiameter;
    }

    /**
     * Sets the value of the internalDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInternalDiameter(BigDecimal value) {
        this.internalDiameter = value;
    }

    /**
     * Gets the value of the minimumOuterDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumOuterDiameter() {
        return minimumOuterDiameter;
    }

    /**
     * Sets the value of the minimumOuterDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumOuterDiameter(BigDecimal value) {
        this.minimumOuterDiameter = value;
    }

    /**
     * Gets the value of the maximumOuterDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumOuterDiameter() {
        return maximumOuterDiameter;
    }

    /**
     * Sets the value of the maximumOuterDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumOuterDiameter(BigDecimal value) {
        this.maximumOuterDiameter = value;
    }

    /**
     * Gets the value of the minimumCoilWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumCoilWeight() {
        return minimumCoilWeight;
    }

    /**
     * Sets the value of the minimumCoilWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumCoilWeight(BigDecimal value) {
        this.minimumCoilWeight = value;
    }

    /**
     * Gets the value of the maximumCoilWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumCoilWeight() {
        return maximumCoilWeight;
    }

    /**
     * Sets the value of the maximumCoilWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumCoilWeight(BigDecimal value) {
        this.maximumCoilWeight = value;
    }

    /**
     * Gets the value of the minimumCoilLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumCoilLength() {
        return minimumCoilLength;
    }

    /**
     * Sets the value of the minimumCoilLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumCoilLength(BigDecimal value) {
        this.minimumCoilLength = value;
    }

    /**
     * Gets the value of the maximumCoilLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumCoilLength() {
        return maximumCoilLength;
    }

    /**
     * Sets the value of the maximumCoilLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumCoilLength(BigDecimal value) {
        this.maximumCoilLength = value;
    }

}
