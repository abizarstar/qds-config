
package com.invera.stratix.schema.production.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NarrowStripCoilSpecification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NarrowStripCoilSpecification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="faceWidth" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="maximumWeldsApplicable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="maximumWelds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="mixedHeats" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NarrowStripCoilSpecification", propOrder = {
    "faceWidth",
    "maximumWeldsApplicable",
    "maximumWelds",
    "mixedHeats"
})
public class NarrowStripCoilSpecification {

    protected BigDecimal faceWidth;
    protected boolean maximumWeldsApplicable;
    protected int maximumWelds;
    protected boolean mixedHeats;

    /**
     * Gets the value of the faceWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFaceWidth() {
        return faceWidth;
    }

    /**
     * Sets the value of the faceWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFaceWidth(BigDecimal value) {
        this.faceWidth = value;
    }

    /**
     * Gets the value of the maximumWeldsApplicable property.
     * 
     */
    public boolean isMaximumWeldsApplicable() {
        return maximumWeldsApplicable;
    }

    /**
     * Sets the value of the maximumWeldsApplicable property.
     * 
     */
    public void setMaximumWeldsApplicable(boolean value) {
        this.maximumWeldsApplicable = value;
    }

    /**
     * Gets the value of the maximumWelds property.
     * 
     */
    public int getMaximumWelds() {
        return maximumWelds;
    }

    /**
     * Sets the value of the maximumWelds property.
     * 
     */
    public void setMaximumWelds(int value) {
        this.maximumWelds = value;
    }

    /**
     * Gets the value of the mixedHeats property.
     * 
     */
    public boolean isMixedHeats() {
        return mixedHeats;
    }

    /**
     * Sets the value of the mixedHeats property.
     * 
     */
    public void setMixedHeats(boolean value) {
        this.mixedHeats = value;
    }

}
