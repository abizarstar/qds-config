
package com.invera.stratix.schema.costing.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItemCost complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemCost">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="costNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="costDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="costClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="costUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="costQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="costQuantityUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="exchangeRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchaseOrderPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="purchaseOrderItemNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="otherReferencePrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="otherReferenceItemNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="vendorReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemCost", propOrder = {
    "costNumber",
    "costDescription",
    "costClass",
    "cost",
    "costUnitOfMeasure",
    "costQuantity",
    "costQuantityUnitOfMeasure",
    "currencyCode",
    "exchangeRate",
    "exchangeRateType",
    "vendorId",
    "vendorName",
    "purchaseOrderPrefix",
    "purchaseOrderNumber",
    "purchaseOrderItemNumber",
    "otherReferencePrefix",
    "otherReferenceNumber",
    "otherReferenceItemNumber",
    "vendorReference"
})
public class ItemCost {

    protected Integer costNumber;
    protected String costDescription;
    protected String costClass;
    protected BigDecimal cost;
    protected String costUnitOfMeasure;
    protected BigDecimal costQuantity;
    protected String costQuantityUnitOfMeasure;
    protected String currencyCode;
    protected BigDecimal exchangeRate;
    protected String exchangeRateType;
    protected String vendorId;
    protected String vendorName;
    protected String purchaseOrderPrefix;
    protected BigDecimal purchaseOrderNumber;
    protected BigDecimal purchaseOrderItemNumber;
    protected String otherReferencePrefix;
    protected BigDecimal otherReferenceNumber;
    protected BigDecimal otherReferenceItemNumber;
    protected String vendorReference;

    /**
     * Gets the value of the costNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCostNumber() {
        return costNumber;
    }

    /**
     * Sets the value of the costNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCostNumber(Integer value) {
        this.costNumber = value;
    }

    /**
     * Gets the value of the costDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostDescription() {
        return costDescription;
    }

    /**
     * Sets the value of the costDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostDescription(String value) {
        this.costDescription = value;
    }

    /**
     * Gets the value of the costClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostClass() {
        return costClass;
    }

    /**
     * Sets the value of the costClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostClass(String value) {
        this.costClass = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCost(BigDecimal value) {
        this.cost = value;
    }

    /**
     * Gets the value of the costUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostUnitOfMeasure() {
        return costUnitOfMeasure;
    }

    /**
     * Sets the value of the costUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostUnitOfMeasure(String value) {
        this.costUnitOfMeasure = value;
    }

    /**
     * Gets the value of the costQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCostQuantity() {
        return costQuantity;
    }

    /**
     * Sets the value of the costQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCostQuantity(BigDecimal value) {
        this.costQuantity = value;
    }

    /**
     * Gets the value of the costQuantityUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostQuantityUnitOfMeasure() {
        return costQuantityUnitOfMeasure;
    }

    /**
     * Sets the value of the costQuantityUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostQuantityUnitOfMeasure(String value) {
        this.costQuantityUnitOfMeasure = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the exchangeRateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateType() {
        return exchangeRateType;
    }

    /**
     * Sets the value of the exchangeRateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateType(String value) {
        this.exchangeRateType = value;
    }

    /**
     * Gets the value of the vendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * Sets the value of the vendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorId(String value) {
        this.vendorId = value;
    }

    /**
     * Gets the value of the vendorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * Sets the value of the vendorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorName(String value) {
        this.vendorName = value;
    }

    /**
     * Gets the value of the purchaseOrderPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderPrefix() {
        return purchaseOrderPrefix;
    }

    /**
     * Sets the value of the purchaseOrderPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderPrefix(String value) {
        this.purchaseOrderPrefix = value;
    }

    /**
     * Gets the value of the purchaseOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * Sets the value of the purchaseOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaseOrderNumber(BigDecimal value) {
        this.purchaseOrderNumber = value;
    }

    /**
     * Gets the value of the purchaseOrderItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaseOrderItemNumber() {
        return purchaseOrderItemNumber;
    }

    /**
     * Sets the value of the purchaseOrderItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaseOrderItemNumber(BigDecimal value) {
        this.purchaseOrderItemNumber = value;
    }

    /**
     * Gets the value of the otherReferencePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherReferencePrefix() {
        return otherReferencePrefix;
    }

    /**
     * Sets the value of the otherReferencePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherReferencePrefix(String value) {
        this.otherReferencePrefix = value;
    }

    /**
     * Gets the value of the otherReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOtherReferenceNumber() {
        return otherReferenceNumber;
    }

    /**
     * Sets the value of the otherReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOtherReferenceNumber(BigDecimal value) {
        this.otherReferenceNumber = value;
    }

    /**
     * Gets the value of the otherReferenceItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOtherReferenceItemNumber() {
        return otherReferenceItemNumber;
    }

    /**
     * Sets the value of the otherReferenceItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOtherReferenceItemNumber(BigDecimal value) {
        this.otherReferenceItemNumber = value;
    }

    /**
     * Gets the value of the vendorReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorReference() {
        return vendorReference;
    }

    /**
     * Sets the value of the vendorReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorReference(String value) {
        this.vendorReference = value;
    }

}
