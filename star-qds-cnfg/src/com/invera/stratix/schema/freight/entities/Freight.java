
package com.invera.stratix.schema.freight.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Freight complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Freight">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tradeTermCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deliveryMethodCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightVendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="carrierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="designatedShipToWhs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transitWarehouseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="exchangeRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transportRoute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="costUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fuelSurchargeCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fuelSurchargeCostUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="charge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="chargeUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fuelSurchargeCharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="fuelSurchargeChargeUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Freight", propOrder = {
    "tradeTermCode",
    "deliveryMethodCode",
    "freightVendorId",
    "carrierName",
    "portId",
    "destination",
    "currencyCode",
    "designatedShipToWhs",
    "transitWarehouseId",
    "exchangeRate",
    "exchangeRateType",
    "vendorReference",
    "transportRoute",
    "cost",
    "costUnitOfMeasure",
    "fuelSurchargeCost",
    "fuelSurchargeCostUnitOfMeasure",
    "charge",
    "chargeUnitOfMeasure",
    "fuelSurchargeCharge",
    "fuelSurchargeChargeUnitOfMeasure"
})
public class Freight {

    protected String tradeTermCode;
    protected String deliveryMethodCode;
    protected String freightVendorId;
    protected String carrierName;
    protected String portId;
    protected String destination;
    protected String currencyCode;
    protected String designatedShipToWhs;
    protected String transitWarehouseId;
    protected BigDecimal exchangeRate;
    protected String exchangeRateType;
    protected String vendorReference;
    protected String transportRoute;
    protected BigDecimal cost;
    protected String costUnitOfMeasure;
    protected BigDecimal fuelSurchargeCost;
    protected String fuelSurchargeCostUnitOfMeasure;
    protected BigDecimal charge;
    protected String chargeUnitOfMeasure;
    protected BigDecimal fuelSurchargeCharge;
    protected String fuelSurchargeChargeUnitOfMeasure;

    /**
     * Gets the value of the tradeTermCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeTermCode() {
        return tradeTermCode;
    }

    /**
     * Sets the value of the tradeTermCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeTermCode(String value) {
        this.tradeTermCode = value;
    }

    /**
     * Gets the value of the deliveryMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryMethodCode() {
        return deliveryMethodCode;
    }

    /**
     * Sets the value of the deliveryMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryMethodCode(String value) {
        this.deliveryMethodCode = value;
    }

    /**
     * Gets the value of the freightVendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightVendorId() {
        return freightVendorId;
    }

    /**
     * Sets the value of the freightVendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightVendorId(String value) {
        this.freightVendorId = value;
    }

    /**
     * Gets the value of the carrierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierName() {
        return carrierName;
    }

    /**
     * Sets the value of the carrierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierName(String value) {
        this.carrierName = value;
    }

    /**
     * Gets the value of the portId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortId() {
        return portId;
    }

    /**
     * Sets the value of the portId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortId(String value) {
        this.portId = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the designatedShipToWhs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesignatedShipToWhs() {
        return designatedShipToWhs;
    }

    /**
     * Sets the value of the designatedShipToWhs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesignatedShipToWhs(String value) {
        this.designatedShipToWhs = value;
    }

    /**
     * Gets the value of the transitWarehouseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitWarehouseId() {
        return transitWarehouseId;
    }

    /**
     * Sets the value of the transitWarehouseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitWarehouseId(String value) {
        this.transitWarehouseId = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the exchangeRateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateType() {
        return exchangeRateType;
    }

    /**
     * Sets the value of the exchangeRateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateType(String value) {
        this.exchangeRateType = value;
    }

    /**
     * Gets the value of the vendorReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorReference() {
        return vendorReference;
    }

    /**
     * Sets the value of the vendorReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorReference(String value) {
        this.vendorReference = value;
    }

    /**
     * Gets the value of the transportRoute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportRoute() {
        return transportRoute;
    }

    /**
     * Sets the value of the transportRoute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportRoute(String value) {
        this.transportRoute = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCost(BigDecimal value) {
        this.cost = value;
    }

    /**
     * Gets the value of the costUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostUnitOfMeasure() {
        return costUnitOfMeasure;
    }

    /**
     * Sets the value of the costUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostUnitOfMeasure(String value) {
        this.costUnitOfMeasure = value;
    }

    /**
     * Gets the value of the fuelSurchargeCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFuelSurchargeCost() {
        return fuelSurchargeCost;
    }

    /**
     * Sets the value of the fuelSurchargeCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFuelSurchargeCost(BigDecimal value) {
        this.fuelSurchargeCost = value;
    }

    /**
     * Gets the value of the fuelSurchargeCostUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelSurchargeCostUnitOfMeasure() {
        return fuelSurchargeCostUnitOfMeasure;
    }

    /**
     * Sets the value of the fuelSurchargeCostUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelSurchargeCostUnitOfMeasure(String value) {
        this.fuelSurchargeCostUnitOfMeasure = value;
    }

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCharge(BigDecimal value) {
        this.charge = value;
    }

    /**
     * Gets the value of the chargeUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeUnitOfMeasure() {
        return chargeUnitOfMeasure;
    }

    /**
     * Sets the value of the chargeUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeUnitOfMeasure(String value) {
        this.chargeUnitOfMeasure = value;
    }

    /**
     * Gets the value of the fuelSurchargeCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFuelSurchargeCharge() {
        return fuelSurchargeCharge;
    }

    /**
     * Sets the value of the fuelSurchargeCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFuelSurchargeCharge(BigDecimal value) {
        this.fuelSurchargeCharge = value;
    }

    /**
     * Gets the value of the fuelSurchargeChargeUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelSurchargeChargeUnitOfMeasure() {
        return fuelSurchargeChargeUnitOfMeasure;
    }

    /**
     * Sets the value of the fuelSurchargeChargeUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelSurchargeChargeUnitOfMeasure(String value) {
        this.fuelSurchargeChargeUnitOfMeasure = value;
    }

}
