
package com.invera.stratix.schema.purchaseorders.entities;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.invera.stratix.schema.common.entities.Status;


/**
 * <p>Java class for Distribution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Distribution">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="refItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="internalReleaseNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="distributionNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="distributionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipToWarehouseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="boughtForCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="boughtForCustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="boughtForPart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="boughtForSalesPersonId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderedPieces" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderedPiecesType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderedMeasure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="orderedMeasureType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderedWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="orderedWeightType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transitDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="freightCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="freightCostUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightVendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="freightExchangeRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inquiryCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="inquiryCostUpdated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="inquiryRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transportCompletionStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receivingStatus" type="{http://stratix.invera.com/schema/common/entities}Status" minOccurs="0"/>
 *         &lt;element name="transactionStatus" type="{http://stratix.invera.com/schema/common/entities}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Distribution", propOrder = {
    "identity",
    "refPrefix",
    "refNumber",
    "refItemNumber",
    "internalReleaseNumber",
    "distributionNumber",
    "distributionType",
    "shipToWarehouseId",
    "boughtForCode",
    "boughtForCustomerId",
    "boughtForPart",
    "boughtForSalesPersonId",
    "orderPrefix",
    "orderNumber",
    "orderItemNumber",
    "orderedPieces",
    "orderedPiecesType",
    "orderedMeasure",
    "orderedMeasureType",
    "orderedWeight",
    "orderedWeightType",
    "transitDays",
    "freightCost",
    "freightCostUnitOfMeasure",
    "freightVendorId",
    "freightCurrencyCode",
    "freightExchangeRate",
    "freightExchangeRateType",
    "inquiryCost",
    "inquiryCostUpdated",
    "inquiryRemark",
    "transportCompletionStatus",
    "receivingStatus",
    "transactionStatus"
})
public class Distribution {

    protected String identity;
    protected String refPrefix;
    protected Integer refNumber;
    protected Integer refItemNumber;
    protected Integer internalReleaseNumber;
    protected Integer distributionNumber;
    protected String distributionType;
    protected String shipToWarehouseId;
    @XmlElement(required = true)
    protected String boughtForCode;
    protected String boughtForCustomerId;
    protected String boughtForPart;
    protected String boughtForSalesPersonId;
    protected String orderPrefix;
    protected Integer orderNumber;
    protected Integer orderItemNumber;
    protected Integer orderedPieces;
    protected String orderedPiecesType;
    protected BigDecimal orderedMeasure;
    protected String orderedMeasureType;
    protected BigDecimal orderedWeight;
    protected String orderedWeightType;
    protected Integer transitDays;
    protected BigDecimal freightCost;
    protected String freightCostUnitOfMeasure;
    protected String freightVendorId;
    protected String freightCurrencyCode;
    protected BigDecimal freightExchangeRate;
    protected String freightExchangeRateType;
    protected BigDecimal inquiryCost;
    @XmlElement(required = true)
    protected String inquiryCostUpdated;
    protected String inquiryRemark;
    protected String transportCompletionStatus;
    protected Status receivingStatus;
    protected Status transactionStatus;

    /**
     * Gets the value of the identity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Sets the value of the identity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

    /**
     * Gets the value of the refPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefPrefix() {
        return refPrefix;
    }

    /**
     * Sets the value of the refPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefPrefix(String value) {
        this.refPrefix = value;
    }

    /**
     * Gets the value of the refNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefNumber() {
        return refNumber;
    }

    /**
     * Sets the value of the refNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefNumber(Integer value) {
        this.refNumber = value;
    }

    /**
     * Gets the value of the refItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefItemNumber() {
        return refItemNumber;
    }

    /**
     * Sets the value of the refItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefItemNumber(Integer value) {
        this.refItemNumber = value;
    }

    /**
     * Gets the value of the internalReleaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInternalReleaseNumber() {
        return internalReleaseNumber;
    }

    /**
     * Sets the value of the internalReleaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInternalReleaseNumber(Integer value) {
        this.internalReleaseNumber = value;
    }

    /**
     * Gets the value of the distributionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDistributionNumber() {
        return distributionNumber;
    }

    /**
     * Sets the value of the distributionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDistributionNumber(Integer value) {
        this.distributionNumber = value;
    }

    /**
     * Gets the value of the distributionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionType() {
        return distributionType;
    }

    /**
     * Sets the value of the distributionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionType(String value) {
        this.distributionType = value;
    }

    /**
     * Gets the value of the shipToWarehouseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToWarehouseId() {
        return shipToWarehouseId;
    }

    /**
     * Sets the value of the shipToWarehouseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToWarehouseId(String value) {
        this.shipToWarehouseId = value;
    }

    /**
     * Gets the value of the boughtForCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoughtForCode() {
        return boughtForCode;
    }

    /**
     * Sets the value of the boughtForCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoughtForCode(String value) {
        this.boughtForCode = value;
    }

    /**
     * Gets the value of the boughtForCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoughtForCustomerId() {
        return boughtForCustomerId;
    }

    /**
     * Sets the value of the boughtForCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoughtForCustomerId(String value) {
        this.boughtForCustomerId = value;
    }

    /**
     * Gets the value of the boughtForPart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoughtForPart() {
        return boughtForPart;
    }

    /**
     * Sets the value of the boughtForPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoughtForPart(String value) {
        this.boughtForPart = value;
    }

    /**
     * Gets the value of the boughtForSalesPersonId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoughtForSalesPersonId() {
        return boughtForSalesPersonId;
    }

    /**
     * Sets the value of the boughtForSalesPersonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoughtForSalesPersonId(String value) {
        this.boughtForSalesPersonId = value;
    }

    /**
     * Gets the value of the orderPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPrefix() {
        return orderPrefix;
    }

    /**
     * Sets the value of the orderPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPrefix(String value) {
        this.orderPrefix = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the orderItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderItemNumber() {
        return orderItemNumber;
    }

    /**
     * Sets the value of the orderItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderItemNumber(Integer value) {
        this.orderItemNumber = value;
    }

    /**
     * Gets the value of the orderedPieces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderedPieces() {
        return orderedPieces;
    }

    /**
     * Sets the value of the orderedPieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderedPieces(Integer value) {
        this.orderedPieces = value;
    }

    /**
     * Gets the value of the orderedPiecesType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderedPiecesType() {
        return orderedPiecesType;
    }

    /**
     * Sets the value of the orderedPiecesType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderedPiecesType(String value) {
        this.orderedPiecesType = value;
    }

    /**
     * Gets the value of the orderedMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderedMeasure() {
        return orderedMeasure;
    }

    /**
     * Sets the value of the orderedMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderedMeasure(BigDecimal value) {
        this.orderedMeasure = value;
    }

    /**
     * Gets the value of the orderedMeasureType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderedMeasureType() {
        return orderedMeasureType;
    }

    /**
     * Sets the value of the orderedMeasureType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderedMeasureType(String value) {
        this.orderedMeasureType = value;
    }

    /**
     * Gets the value of the orderedWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderedWeight() {
        return orderedWeight;
    }

    /**
     * Sets the value of the orderedWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderedWeight(BigDecimal value) {
        this.orderedWeight = value;
    }

    /**
     * Gets the value of the orderedWeightType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderedWeightType() {
        return orderedWeightType;
    }

    /**
     * Sets the value of the orderedWeightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderedWeightType(String value) {
        this.orderedWeightType = value;
    }

    /**
     * Gets the value of the transitDays property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransitDays() {
        return transitDays;
    }

    /**
     * Sets the value of the transitDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransitDays(Integer value) {
        this.transitDays = value;
    }

    /**
     * Gets the value of the freightCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreightCost() {
        return freightCost;
    }

    /**
     * Sets the value of the freightCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreightCost(BigDecimal value) {
        this.freightCost = value;
    }

    /**
     * Gets the value of the freightCostUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightCostUnitOfMeasure() {
        return freightCostUnitOfMeasure;
    }

    /**
     * Sets the value of the freightCostUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightCostUnitOfMeasure(String value) {
        this.freightCostUnitOfMeasure = value;
    }

    /**
     * Gets the value of the freightVendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightVendorId() {
        return freightVendorId;
    }

    /**
     * Sets the value of the freightVendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightVendorId(String value) {
        this.freightVendorId = value;
    }

    /**
     * Gets the value of the freightCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightCurrencyCode() {
        return freightCurrencyCode;
    }

    /**
     * Sets the value of the freightCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightCurrencyCode(String value) {
        this.freightCurrencyCode = value;
    }

    /**
     * Gets the value of the freightExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreightExchangeRate() {
        return freightExchangeRate;
    }

    /**
     * Sets the value of the freightExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreightExchangeRate(BigDecimal value) {
        this.freightExchangeRate = value;
    }

    /**
     * Gets the value of the freightExchangeRateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightExchangeRateType() {
        return freightExchangeRateType;
    }

    /**
     * Sets the value of the freightExchangeRateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightExchangeRateType(String value) {
        this.freightExchangeRateType = value;
    }

    /**
     * Gets the value of the inquiryCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInquiryCost() {
        return inquiryCost;
    }

    /**
     * Sets the value of the inquiryCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInquiryCost(BigDecimal value) {
        this.inquiryCost = value;
    }

    /**
     * Gets the value of the inquiryCostUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInquiryCostUpdated() {
        return inquiryCostUpdated;
    }

    /**
     * Sets the value of the inquiryCostUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInquiryCostUpdated(String value) {
        this.inquiryCostUpdated = value;
    }

    /**
     * Gets the value of the inquiryRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInquiryRemark() {
        return inquiryRemark;
    }

    /**
     * Sets the value of the inquiryRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInquiryRemark(String value) {
        this.inquiryRemark = value;
    }

    /**
     * Gets the value of the transportCompletionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportCompletionStatus() {
        return transportCompletionStatus;
    }

    /**
     * Sets the value of the transportCompletionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportCompletionStatus(String value) {
        this.transportCompletionStatus = value;
    }

    /**
     * Gets the value of the receivingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getReceivingStatus() {
        return receivingStatus;
    }

    /**
     * Sets the value of the receivingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setReceivingStatus(Status value) {
        this.receivingStatus = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setTransactionStatus(Status value) {
        this.transactionStatus = value;
    }

}
