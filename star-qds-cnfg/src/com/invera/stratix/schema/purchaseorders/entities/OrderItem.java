
package com.invera.stratix.schema.purchaseorders.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.invera.stratix.schema.common.entities.Instruction;
import com.invera.stratix.schema.common.entities.ItemProduct;
import com.invera.stratix.schema.common.entities.ProductDescription;
import com.invera.stratix.schema.common.entities.Status;
import com.invera.stratix.schema.common.entities.TestCertificate;
import com.invera.stratix.schema.costing.entities.ItemCost;
import com.invera.stratix.schema.production.entities.ChemicalSpecification;
import com.invera.stratix.schema.production.entities.CoilPackaging;
import com.invera.stratix.schema.production.entities.Condition;
import com.invera.stratix.schema.production.entities.NarrowStripCoilSpecification;
import com.invera.stratix.schema.production.entities.Packaging;
import com.invera.stratix.schema.production.entities.StandardSpecification;
import com.invera.stratix.schema.production.entities.Test;
import com.invera.stratix.schema.production.entities.Tolerances;


/**
 * <p>Java class for OrderItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderBranchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderWeightUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="blanketPieces" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="blanketMeasure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="blanketWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="blanketQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="blanketWeightUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consignment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="purchaseCategoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sourceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allowPartialShipment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="overshipPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="undershipPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="qualityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="alloySurchargeApplicationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="designatedShipToWarehouseId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorPricingSourceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="priceInEffect" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="releases" type="{http://stratix.invera.com/schema/purchaseOrders/entities}Release" maxOccurs="99"/>
 *         &lt;element name="instructions" type="{http://stratix.invera.com/schema/common/entities}Instruction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="product" type="{http://stratix.invera.com/schema/common/entities}ItemProduct"/>
 *         &lt;element name="productDescriptions" type="{http://stratix.invera.com/schema/common/entities}ProductDescription" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="costs" type="{http://stratix.invera.com/schema/costing/entities}ItemCost" maxOccurs="99" minOccurs="0"/>
 *         &lt;element name="conditions" type="{http://stratix.invera.com/schema/production/entities}Condition" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tolerances" type="{http://stratix.invera.com/schema/production/entities}Tolerances" minOccurs="0"/>
 *         &lt;element name="coilPackagings" type="{http://stratix.invera.com/schema/production/entities}CoilPackaging" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="narrowStripCoilSpecification" type="{http://stratix.invera.com/schema/production/entities}NarrowStripCoilSpecification" minOccurs="0"/>
 *         &lt;element name="packaging" type="{http://stratix.invera.com/schema/production/entities}Packaging" minOccurs="0"/>
 *         &lt;element name="standardSpecs" type="{http://stratix.invera.com/schema/production/entities}StandardSpecification" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tests" type="{http://stratix.invera.com/schema/production/entities}Test" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="chemicalSpecifications" type="{http://stratix.invera.com/schema/production/entities}ChemicalSpecification" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="testCertificates" type="{http://stratix.invera.com/schema/common/entities}TestCertificate" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="statusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionStatus" type="{http://stratix.invera.com/schema/common/entities}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItem", propOrder = {
    "identity",
    "orderPrefix",
    "orderNumber",
    "orderItemNumber",
    "orderBranchId",
    "orderWeightUnitOfMeasure",
    "blanketPieces",
    "blanketMeasure",
    "blanketWeight",
    "blanketQuantity",
    "blanketWeightUnitOfMeasure",
    "consignment",
    "purchaseCategoryCode",
    "sourceCode",
    "allowPartialShipment",
    "overshipPercentage",
    "undershipPercentage",
    "qualityCode",
    "alloySurchargeApplicationCode",
    "designatedShipToWarehouseId",
    "vendorPricingSourceCode",
    "priceInEffect",
    "releases",
    "instructions",
    "product",
    "productDescriptions",
    "costs",
    "conditions",
    "tolerances",
    "coilPackagings",
    "narrowStripCoilSpecification",
    "packaging",
    "standardSpecs",
    "tests",
    "chemicalSpecifications",
    "testCertificates",
    "statusCode",
    "transactionStatus"
})
public class OrderItem {

    protected String identity;
    protected String orderPrefix;
    protected Integer orderNumber;
    protected Integer orderItemNumber;
    protected String orderBranchId;
    protected String orderWeightUnitOfMeasure;
    protected Integer blanketPieces;
    protected BigDecimal blanketMeasure;
    protected BigDecimal blanketWeight;
    protected BigDecimal blanketQuantity;
    protected String blanketWeightUnitOfMeasure;
    protected Boolean consignment;
    protected String purchaseCategoryCode;
    protected String sourceCode;
    protected Boolean allowPartialShipment;
    protected BigDecimal overshipPercentage;
    protected BigDecimal undershipPercentage;
    protected String qualityCode;
    protected String alloySurchargeApplicationCode;
    protected String designatedShipToWarehouseId;
    protected String vendorPricingSourceCode;
    protected Boolean priceInEffect;
    @XmlElement(required = true)
    protected List<Release> releases;
    @XmlElement(nillable = true)
    protected List<Instruction> instructions;
    @XmlElement(required = true)
    protected ItemProduct product;
    protected List<ProductDescription> productDescriptions;
    protected List<ItemCost> costs;
    protected List<Condition> conditions;
    protected Tolerances tolerances;
    protected List<CoilPackaging> coilPackagings;
    protected NarrowStripCoilSpecification narrowStripCoilSpecification;
    protected Packaging packaging;
    protected List<StandardSpecification> standardSpecs;
    protected List<Test> tests;
    protected List<ChemicalSpecification> chemicalSpecifications;
    protected List<TestCertificate> testCertificates;
    protected String statusCode;
    protected Status transactionStatus;

    /**
     * Gets the value of the identity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Sets the value of the identity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

    /**
     * Gets the value of the orderPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPrefix() {
        return orderPrefix;
    }

    /**
     * Sets the value of the orderPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPrefix(String value) {
        this.orderPrefix = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the orderItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderItemNumber() {
        return orderItemNumber;
    }

    /**
     * Sets the value of the orderItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderItemNumber(Integer value) {
        this.orderItemNumber = value;
    }

    /**
     * Gets the value of the orderBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBranchId() {
        return orderBranchId;
    }

    /**
     * Sets the value of the orderBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBranchId(String value) {
        this.orderBranchId = value;
    }

    /**
     * Gets the value of the orderWeightUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderWeightUnitOfMeasure() {
        return orderWeightUnitOfMeasure;
    }

    /**
     * Sets the value of the orderWeightUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderWeightUnitOfMeasure(String value) {
        this.orderWeightUnitOfMeasure = value;
    }

    /**
     * Gets the value of the blanketPieces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBlanketPieces() {
        return blanketPieces;
    }

    /**
     * Sets the value of the blanketPieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBlanketPieces(Integer value) {
        this.blanketPieces = value;
    }

    /**
     * Gets the value of the blanketMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBlanketMeasure() {
        return blanketMeasure;
    }

    /**
     * Sets the value of the blanketMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBlanketMeasure(BigDecimal value) {
        this.blanketMeasure = value;
    }

    /**
     * Gets the value of the blanketWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBlanketWeight() {
        return blanketWeight;
    }

    /**
     * Sets the value of the blanketWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBlanketWeight(BigDecimal value) {
        this.blanketWeight = value;
    }

    /**
     * Gets the value of the blanketQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBlanketQuantity() {
        return blanketQuantity;
    }

    /**
     * Sets the value of the blanketQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBlanketQuantity(BigDecimal value) {
        this.blanketQuantity = value;
    }

    /**
     * Gets the value of the blanketWeightUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlanketWeightUnitOfMeasure() {
        return blanketWeightUnitOfMeasure;
    }

    /**
     * Sets the value of the blanketWeightUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlanketWeightUnitOfMeasure(String value) {
        this.blanketWeightUnitOfMeasure = value;
    }

    /**
     * Gets the value of the consignment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConsignment() {
        return consignment;
    }

    /**
     * Sets the value of the consignment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConsignment(Boolean value) {
        this.consignment = value;
    }

    /**
     * Gets the value of the purchaseCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseCategoryCode() {
        return purchaseCategoryCode;
    }

    /**
     * Sets the value of the purchaseCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseCategoryCode(String value) {
        this.purchaseCategoryCode = value;
    }

    /**
     * Gets the value of the sourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceCode() {
        return sourceCode;
    }

    /**
     * Sets the value of the sourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceCode(String value) {
        this.sourceCode = value;
    }

    /**
     * Gets the value of the allowPartialShipment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPartialShipment() {
        return allowPartialShipment;
    }

    /**
     * Sets the value of the allowPartialShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPartialShipment(Boolean value) {
        this.allowPartialShipment = value;
    }

    /**
     * Gets the value of the overshipPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOvershipPercentage() {
        return overshipPercentage;
    }

    /**
     * Sets the value of the overshipPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOvershipPercentage(BigDecimal value) {
        this.overshipPercentage = value;
    }

    /**
     * Gets the value of the undershipPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUndershipPercentage() {
        return undershipPercentage;
    }

    /**
     * Sets the value of the undershipPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUndershipPercentage(BigDecimal value) {
        this.undershipPercentage = value;
    }

    /**
     * Gets the value of the qualityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualityCode() {
        return qualityCode;
    }

    /**
     * Sets the value of the qualityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualityCode(String value) {
        this.qualityCode = value;
    }

    /**
     * Gets the value of the alloySurchargeApplicationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlloySurchargeApplicationCode() {
        return alloySurchargeApplicationCode;
    }

    /**
     * Sets the value of the alloySurchargeApplicationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlloySurchargeApplicationCode(String value) {
        this.alloySurchargeApplicationCode = value;
    }

    /**
     * Gets the value of the designatedShipToWarehouseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesignatedShipToWarehouseId() {
        return designatedShipToWarehouseId;
    }

    /**
     * Sets the value of the designatedShipToWarehouseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesignatedShipToWarehouseId(String value) {
        this.designatedShipToWarehouseId = value;
    }

    /**
     * Gets the value of the vendorPricingSourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorPricingSourceCode() {
        return vendorPricingSourceCode;
    }

    /**
     * Sets the value of the vendorPricingSourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorPricingSourceCode(String value) {
        this.vendorPricingSourceCode = value;
    }

    /**
     * Gets the value of the priceInEffect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPriceInEffect() {
        return priceInEffect;
    }

    /**
     * Sets the value of the priceInEffect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPriceInEffect(Boolean value) {
        this.priceInEffect = value;
    }

    /**
     * Gets the value of the releases property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releases property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleases().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Release }
     * 
     * 
     */
    public List<Release> getReleases() {
        if (releases == null) {
            releases = new ArrayList<Release>();
        }
        return this.releases;
    }

    /**
     * Gets the value of the instructions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instructions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstructions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Instruction }
     * 
     * 
     */
    public List<Instruction> getInstructions() {
        if (instructions == null) {
            instructions = new ArrayList<Instruction>();
        }
        return this.instructions;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ItemProduct }
     *     
     */
    public ItemProduct getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemProduct }
     *     
     */
    public void setProduct(ItemProduct value) {
        this.product = value;
    }

    /**
     * Gets the value of the productDescriptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productDescriptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductDescriptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductDescription }
     * 
     * 
     */
    public List<ProductDescription> getProductDescriptions() {
        if (productDescriptions == null) {
            productDescriptions = new ArrayList<ProductDescription>();
        }
        return this.productDescriptions;
    }

    /**
     * Gets the value of the costs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCosts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemCost }
     * 
     * 
     */
    public List<ItemCost> getCosts() {
        if (costs == null) {
            costs = new ArrayList<ItemCost>();
        }
        return this.costs;
    }

    /**
     * Gets the value of the conditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Condition }
     * 
     * 
     */
    public List<Condition> getConditions() {
        if (conditions == null) {
            conditions = new ArrayList<Condition>();
        }
        return this.conditions;
    }

    /**
     * Gets the value of the tolerances property.
     * 
     * @return
     *     possible object is
     *     {@link Tolerances }
     *     
     */
    public Tolerances getTolerances() {
        return tolerances;
    }

    /**
     * Sets the value of the tolerances property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tolerances }
     *     
     */
    public void setTolerances(Tolerances value) {
        this.tolerances = value;
    }

    /**
     * Gets the value of the coilPackagings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coilPackagings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoilPackagings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoilPackaging }
     * 
     * 
     */
    public List<CoilPackaging> getCoilPackagings() {
        if (coilPackagings == null) {
            coilPackagings = new ArrayList<CoilPackaging>();
        }
        return this.coilPackagings;
    }

    /**
     * Gets the value of the narrowStripCoilSpecification property.
     * 
     * @return
     *     possible object is
     *     {@link NarrowStripCoilSpecification }
     *     
     */
    public NarrowStripCoilSpecification getNarrowStripCoilSpecification() {
        return narrowStripCoilSpecification;
    }

    /**
     * Sets the value of the narrowStripCoilSpecification property.
     * 
     * @param value
     *     allowed object is
     *     {@link NarrowStripCoilSpecification }
     *     
     */
    public void setNarrowStripCoilSpecification(NarrowStripCoilSpecification value) {
        this.narrowStripCoilSpecification = value;
    }

    /**
     * Gets the value of the packaging property.
     * 
     * @return
     *     possible object is
     *     {@link Packaging }
     *     
     */
    public Packaging getPackaging() {
        return packaging;
    }

    /**
     * Sets the value of the packaging property.
     * 
     * @param value
     *     allowed object is
     *     {@link Packaging }
     *     
     */
    public void setPackaging(Packaging value) {
        this.packaging = value;
    }

    /**
     * Gets the value of the standardSpecs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the standardSpecs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStandardSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StandardSpecification }
     * 
     * 
     */
    public List<StandardSpecification> getStandardSpecs() {
        if (standardSpecs == null) {
            standardSpecs = new ArrayList<StandardSpecification>();
        }
        return this.standardSpecs;
    }

    /**
     * Gets the value of the tests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Test }
     * 
     * 
     */
    public List<Test> getTests() {
        if (tests == null) {
            tests = new ArrayList<Test>();
        }
        return this.tests;
    }

    /**
     * Gets the value of the chemicalSpecifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chemicalSpecifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChemicalSpecifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChemicalSpecification }
     * 
     * 
     */
    public List<ChemicalSpecification> getChemicalSpecifications() {
        if (chemicalSpecifications == null) {
            chemicalSpecifications = new ArrayList<ChemicalSpecification>();
        }
        return this.chemicalSpecifications;
    }

    /**
     * Gets the value of the testCertificates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the testCertificates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTestCertificates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TestCertificate }
     * 
     * 
     */
    public List<TestCertificate> getTestCertificates() {
        if (testCertificates == null) {
            testCertificates = new ArrayList<TestCertificate>();
        }
        return this.testCertificates;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setTransactionStatus(Status value) {
        this.transactionStatus = value;
    }

}
