
package com.invera.stratix.schema.purchaseorders.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Release complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Release">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="refItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="internalReleaseNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="releaseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="readyPieces" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="readyMeasure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="readyWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="readyQuantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="readyDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="defaultDueDateQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="defaultDueDateFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dueDateEntry" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dueFromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dueToDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dueDateWeekNo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dueDateYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rollingSchedule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acknowledgementReceived" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="acknowledgementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="millOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="distributions" type="{http://stratix.invera.com/schema/purchaseOrders/entities}Distribution" maxOccurs="99"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Release", propOrder = {
    "identity",
    "refPrefix",
    "refNumber",
    "refItemNumber",
    "internalReleaseNumber",
    "releaseNumber",
    "readyPieces",
    "readyMeasure",
    "readyWeight",
    "readyQuantity",
    "readyDate",
    "defaultDueDateQualifier",
    "defaultDueDateFormat",
    "dueDateEntry",
    "dueFromDate",
    "dueToDate",
    "dueDateWeekNo",
    "dueDateYear",
    "rollingSchedule",
    "acknowledgementReceived",
    "acknowledgementDate",
    "millOrderNumber",
    "distributions"
})
public class Release {

    protected String identity;
    protected String refPrefix;
    protected Integer refNumber;
    protected Integer refItemNumber;
    protected Integer internalReleaseNumber;
    protected String releaseNumber;
    protected Integer readyPieces;
    protected BigDecimal readyMeasure;
    protected BigDecimal readyWeight;
    protected BigDecimal readyQuantity;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar readyDate;
    protected String defaultDueDateQualifier;
    protected String defaultDueDateFormat;
    protected Integer dueDateEntry;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueFromDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dueToDate;
    protected Integer dueDateWeekNo;
    protected Integer dueDateYear;
    protected String rollingSchedule;
    protected Boolean acknowledgementReceived;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar acknowledgementDate;
    protected String millOrderNumber;
    @XmlElement(required = true)
    protected List<Distribution> distributions;

    /**
     * Gets the value of the identity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Sets the value of the identity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

    /**
     * Gets the value of the refPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefPrefix() {
        return refPrefix;
    }

    /**
     * Sets the value of the refPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefPrefix(String value) {
        this.refPrefix = value;
    }

    /**
     * Gets the value of the refNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefNumber() {
        return refNumber;
    }

    /**
     * Sets the value of the refNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefNumber(Integer value) {
        this.refNumber = value;
    }

    /**
     * Gets the value of the refItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefItemNumber() {
        return refItemNumber;
    }

    /**
     * Sets the value of the refItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefItemNumber(Integer value) {
        this.refItemNumber = value;
    }

    /**
     * Gets the value of the internalReleaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInternalReleaseNumber() {
        return internalReleaseNumber;
    }

    /**
     * Sets the value of the internalReleaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInternalReleaseNumber(Integer value) {
        this.internalReleaseNumber = value;
    }

    /**
     * Gets the value of the releaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseNumber() {
        return releaseNumber;
    }

    /**
     * Sets the value of the releaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseNumber(String value) {
        this.releaseNumber = value;
    }

    /**
     * Gets the value of the readyPieces property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReadyPieces() {
        return readyPieces;
    }

    /**
     * Sets the value of the readyPieces property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReadyPieces(Integer value) {
        this.readyPieces = value;
    }

    /**
     * Gets the value of the readyMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReadyMeasure() {
        return readyMeasure;
    }

    /**
     * Sets the value of the readyMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReadyMeasure(BigDecimal value) {
        this.readyMeasure = value;
    }

    /**
     * Gets the value of the readyWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReadyWeight() {
        return readyWeight;
    }

    /**
     * Sets the value of the readyWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReadyWeight(BigDecimal value) {
        this.readyWeight = value;
    }

    /**
     * Gets the value of the readyQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReadyQuantity() {
        return readyQuantity;
    }

    /**
     * Sets the value of the readyQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReadyQuantity(BigDecimal value) {
        this.readyQuantity = value;
    }

    /**
     * Gets the value of the readyDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReadyDate() {
        return readyDate;
    }

    /**
     * Sets the value of the readyDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReadyDate(XMLGregorianCalendar value) {
        this.readyDate = value;
    }

    /**
     * Gets the value of the defaultDueDateQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDueDateQualifier() {
        return defaultDueDateQualifier;
    }

    /**
     * Sets the value of the defaultDueDateQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDueDateQualifier(String value) {
        this.defaultDueDateQualifier = value;
    }

    /**
     * Gets the value of the defaultDueDateFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDueDateFormat() {
        return defaultDueDateFormat;
    }

    /**
     * Sets the value of the defaultDueDateFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDueDateFormat(String value) {
        this.defaultDueDateFormat = value;
    }

    /**
     * Gets the value of the dueDateEntry property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDueDateEntry() {
        return dueDateEntry;
    }

    /**
     * Sets the value of the dueDateEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDueDateEntry(Integer value) {
        this.dueDateEntry = value;
    }

    /**
     * Gets the value of the dueFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueFromDate() {
        return dueFromDate;
    }

    /**
     * Sets the value of the dueFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueFromDate(XMLGregorianCalendar value) {
        this.dueFromDate = value;
    }

    /**
     * Gets the value of the dueToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueToDate() {
        return dueToDate;
    }

    /**
     * Sets the value of the dueToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueToDate(XMLGregorianCalendar value) {
        this.dueToDate = value;
    }

    /**
     * Gets the value of the dueDateWeekNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDueDateWeekNo() {
        return dueDateWeekNo;
    }

    /**
     * Sets the value of the dueDateWeekNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDueDateWeekNo(Integer value) {
        this.dueDateWeekNo = value;
    }

    /**
     * Gets the value of the dueDateYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDueDateYear() {
        return dueDateYear;
    }

    /**
     * Sets the value of the dueDateYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDueDateYear(Integer value) {
        this.dueDateYear = value;
    }

    /**
     * Gets the value of the rollingSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRollingSchedule() {
        return rollingSchedule;
    }

    /**
     * Sets the value of the rollingSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRollingSchedule(String value) {
        this.rollingSchedule = value;
    }

    /**
     * Gets the value of the acknowledgementReceived property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcknowledgementReceived() {
        return acknowledgementReceived;
    }

    /**
     * Sets the value of the acknowledgementReceived property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcknowledgementReceived(Boolean value) {
        this.acknowledgementReceived = value;
    }

    /**
     * Gets the value of the acknowledgementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAcknowledgementDate() {
        return acknowledgementDate;
    }

    /**
     * Sets the value of the acknowledgementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAcknowledgementDate(XMLGregorianCalendar value) {
        this.acknowledgementDate = value;
    }

    /**
     * Gets the value of the millOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMillOrderNumber() {
        return millOrderNumber;
    }

    /**
     * Sets the value of the millOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMillOrderNumber(String value) {
        this.millOrderNumber = value;
    }

    /**
     * Gets the value of the distributions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the distributions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDistributions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Distribution }
     * 
     * 
     */
    public List<Distribution> getDistributions() {
        if (distributions == null) {
            distributions = new ArrayList<Distribution>();
        }
        return this.distributions;
    }

}
