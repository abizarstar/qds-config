
package com.invera.stratix.schema.purchaseorders.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.invera.stratix.schema.common.entities.Contact;
import com.invera.stratix.schema.common.entities.Instruction;
import com.invera.stratix.schema.common.entities.NameAndAddress;
import com.invera.stratix.schema.common.entities.Status;
import com.invera.stratix.schema.freight.entities.Freight;


/**
 * <p>Java class for PurchaseOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="orderBranchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipFromId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="shipFromAddress" type="{http://stratix.invera.com/schema/common/entities}NameAndAddress" minOccurs="0"/>
 *         &lt;element name="millId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceMailToBranchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentTermCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="discountTermCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="buyerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="exchangeRateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipItemsTogether" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="acknowledgementRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="orderPlacedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="salesContact" type="{http://stratix.invera.com/schema/common/entities}Contact" minOccurs="0"/>
 *         &lt;element name="freight" type="{http://stratix.invera.com/schema/freight/entities}Freight" minOccurs="0"/>
 *         &lt;element name="instructions" type="{http://stratix.invera.com/schema/common/entities}Instruction" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="items" type="{http://stratix.invera.com/schema/purchaseOrders/entities}OrderItem" maxOccurs="300" minOccurs="0"/>
 *         &lt;element name="authorizationStatus" type="{http://stratix.invera.com/schema/common/entities}Status" minOccurs="0"/>
 *         &lt;element name="transactionStatus" type="{http://stratix.invera.com/schema/common/entities}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrder", propOrder = {
    "identity",
    "orderPrefix",
    "orderNumber",
    "orderBranchId",
    "vendorId",
    "shipFromId",
    "shipFromAddress",
    "millId",
    "orderType",
    "invoiceMailToBranchId",
    "paymentTermCode",
    "discountTermCode",
    "buyerId",
    "expediterId",
    "currencyCode",
    "exchangeRate",
    "exchangeRateType",
    "shipItemsTogether",
    "acknowledgementRequired",
    "orderPlacedDate",
    "salesContact",
    "freight",
    "instructions",
    "items",
    "authorizationStatus",
    "transactionStatus"
})
public class PurchaseOrder {

    protected String identity;
    protected String orderPrefix;
    protected Integer orderNumber;
    protected String orderBranchId;
    protected String vendorId;
    protected Integer shipFromId;
    protected NameAndAddress shipFromAddress;
    protected String millId;
    protected String orderType;
    protected String invoiceMailToBranchId;
    protected Integer paymentTermCode;
    protected Integer discountTermCode;
    protected String buyerId;
    protected String expediterId;
    protected String currencyCode;
    protected BigDecimal exchangeRate;
    protected String exchangeRateType;
    protected Boolean shipItemsTogether;
    protected Boolean acknowledgementRequired;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar orderPlacedDate;
    protected Contact salesContact;
    protected Freight freight;
    @XmlElement(nillable = true)
    protected List<Instruction> instructions;
    @XmlElement(nillable = true)
    protected List<OrderItem> items;
    protected Status authorizationStatus;
    protected Status transactionStatus;

    /**
     * Gets the value of the identity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Sets the value of the identity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

    /**
     * Gets the value of the orderPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPrefix() {
        return orderPrefix;
    }

    /**
     * Sets the value of the orderPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPrefix(String value) {
        this.orderPrefix = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the orderBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBranchId() {
        return orderBranchId;
    }

    /**
     * Sets the value of the orderBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBranchId(String value) {
        this.orderBranchId = value;
    }

    /**
     * Gets the value of the vendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * Sets the value of the vendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorId(String value) {
        this.vendorId = value;
    }

    /**
     * Gets the value of the shipFromId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShipFromId() {
        return shipFromId;
    }

    /**
     * Sets the value of the shipFromId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShipFromId(Integer value) {
        this.shipFromId = value;
    }

    /**
     * Gets the value of the shipFromAddress property.
     * 
     * @return
     *     possible object is
     *     {@link NameAndAddress }
     *     
     */
    public NameAndAddress getShipFromAddress() {
        return shipFromAddress;
    }

    /**
     * Sets the value of the shipFromAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameAndAddress }
     *     
     */
    public void setShipFromAddress(NameAndAddress value) {
        this.shipFromAddress = value;
    }

    /**
     * Gets the value of the millId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMillId() {
        return millId;
    }

    /**
     * Sets the value of the millId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMillId(String value) {
        this.millId = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the invoiceMailToBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceMailToBranchId() {
        return invoiceMailToBranchId;
    }

    /**
     * Sets the value of the invoiceMailToBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceMailToBranchId(String value) {
        this.invoiceMailToBranchId = value;
    }

    /**
     * Gets the value of the paymentTermCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaymentTermCode() {
        return paymentTermCode;
    }

    /**
     * Sets the value of the paymentTermCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaymentTermCode(Integer value) {
        this.paymentTermCode = value;
    }

    /**
     * Gets the value of the discountTermCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDiscountTermCode() {
        return discountTermCode;
    }

    /**
     * Sets the value of the discountTermCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDiscountTermCode(Integer value) {
        this.discountTermCode = value;
    }

    /**
     * Gets the value of the buyerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerId() {
        return buyerId;
    }

    /**
     * Sets the value of the buyerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerId(String value) {
        this.buyerId = value;
    }

    /**
     * Gets the value of the expediterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpediterId() {
        return expediterId;
    }

    /**
     * Sets the value of the expediterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpediterId(String value) {
        this.expediterId = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the exchangeRateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateType() {
        return exchangeRateType;
    }

    /**
     * Sets the value of the exchangeRateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateType(String value) {
        this.exchangeRateType = value;
    }

    /**
     * Gets the value of the shipItemsTogether property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShipItemsTogether() {
        return shipItemsTogether;
    }

    /**
     * Sets the value of the shipItemsTogether property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipItemsTogether(Boolean value) {
        this.shipItemsTogether = value;
    }

    /**
     * Gets the value of the acknowledgementRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcknowledgementRequired() {
        return acknowledgementRequired;
    }

    /**
     * Sets the value of the acknowledgementRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcknowledgementRequired(Boolean value) {
        this.acknowledgementRequired = value;
    }

    /**
     * Gets the value of the orderPlacedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrderPlacedDate() {
        return orderPlacedDate;
    }

    /**
     * Sets the value of the orderPlacedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrderPlacedDate(XMLGregorianCalendar value) {
        this.orderPlacedDate = value;
    }

    /**
     * Gets the value of the salesContact property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getSalesContact() {
        return salesContact;
    }

    /**
     * Sets the value of the salesContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setSalesContact(Contact value) {
        this.salesContact = value;
    }

    /**
     * Gets the value of the freight property.
     * 
     * @return
     *     possible object is
     *     {@link Freight }
     *     
     */
    public Freight getFreight() {
        return freight;
    }

    /**
     * Sets the value of the freight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Freight }
     *     
     */
    public void setFreight(Freight value) {
        this.freight = value;
    }

    /**
     * Gets the value of the instructions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instructions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstructions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Instruction }
     * 
     * 
     */
    public List<Instruction> getInstructions() {
        if (instructions == null) {
            instructions = new ArrayList<Instruction>();
        }
        return this.instructions;
    }

    /**
     * Gets the value of the items property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the items property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderItem }
     * 
     * 
     */
    public List<OrderItem> getItems() {
        if (items == null) {
            items = new ArrayList<OrderItem>();
        }
        return this.items;
    }

    /**
     * Gets the value of the authorizationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getAuthorizationStatus() {
        return authorizationStatus;
    }

    /**
     * Sets the value of the authorizationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setAuthorizationStatus(Status value) {
        this.authorizationStatus = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setTransactionStatus(Status value) {
        this.transactionStatus = value;
    }

}
