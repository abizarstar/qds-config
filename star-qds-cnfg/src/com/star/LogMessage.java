package com.star;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class LogMessage {

	private static String LOG_FILE = "";

	public static void setLogFile(String logFlNm) {
		LOG_FILE = logFlNm;
	}
	
	public static void generateLogs() {
		try {
			File file = new File(LOG_FILE);
			if (file.createNewFile()) {
				System.out.println("File has been created.");
			}
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeLogs(String logMsg) {

		try {

			FileWriter fw = new FileWriter(LOG_FILE, true);
			BufferedWriter bw = new BufferedWriter(fw);

			bw.write(logMsg + "\n");

			bw.close();

		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
