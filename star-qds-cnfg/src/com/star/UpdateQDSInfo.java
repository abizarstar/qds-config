package com.star;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.services.QdsService;
import com.invera.stratix.services.UpdateQdsHeaderInput;
import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MessagesSOAPHandler;
import com.ws.samples.handler.SecuritySOAPHandler;

public class UpdateQDSInfo {

	public List<String> updateQDS(String host, String port, String authToken, String userId, ServiceReturn response, String qdsNo, QdsHeaderInput headerInput,String nginxURL)
	{
		List<String> msgList = new ArrayList<String>();
		MessagesSOAPHandler messageHandler = new MessagesSOAPHandler();
		SecuritySOAPHandler securityHandler = null;		
		
		try {
		
		ServiceHelper.setWpaEndpointProtocol("https",nginxURL);

		ServiceHelper.setStxEndpointHostAndPort(host, Integer.parseInt(port),nginxURL);

		messageHandler = new MessagesSOAPHandler();
		AuthenticationToken authenticationToken = new AuthenticationToken();

		authenticationToken.setUsername(userId);
		authenticationToken.setValue(authToken);
		
		
		securityHandler = new SecuritySOAPHandler(authenticationToken);
		
		QdsService qdsService = ServiceHelper.qdsService(messageHandler, securityHandler);
		
		UpdateQdsHeaderInput updateQdsHeaderInput = new UpdateQdsHeaderInput();
		
		updateQdsHeaderInput.setMill(headerInput.getMill().trim());
		updateQdsHeaderInput.setHeat("Z" + headerInput.getHeat().trim());
		updateQdsHeaderInput.setWhs(headerInput.getWhs().trim());
	/*	updateQdsHeaderInput.setOrigPoNo(headerInput.getOrigPoNo());
		updateQdsHeaderInput.setOrigPoItem(headerInput.getOrigPoItem());
		updateQdsHeaderInput.setOrigPoDistribution(headerInput.getOrigPoDistribution());*/
		/*updateQdsHeaderInput.setMillOrdNo(headerInput.getMillOrdNo());
		updateQdsHeaderInput.setVenId(headerInput.getVenId());*/
		/*if(headerInput.getStatus() != null)
		{
			updateQdsHeaderInput.setStatus(headerInput.getStatus());
		}
		else
		{
			updateQdsHeaderInput.setStatus("");
		}		*/
		updateQdsHeaderInput.setApvdFlag(0);
		
		/*if(headerInput.getOrigZone() != null)
		{
			updateQdsHeaderInput.setOrigZone(headerInput.getOrigZone());
		}
		else
		{
			updateQdsHeaderInput.setOrigZone("");
		}*/
		updateQdsHeaderInput.setUknownHeat(0);
	
		
		qdsService.updateQdsHeader(Long.parseLong(qdsNo), updateQdsHeaderInput);
	
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		} 
		catch (ServerSOAPFaultException e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		}
		
		finally {
			response.setAuthToekn(securityHandler.getAuthenticationToken().getValue());
		}
		
		return msgList;
		
	}
	
	public List<String> deleteQDS(String host, String port, String authToken, String userId, ServiceReturn response, String qdsNo,String nginxURL)
	{
		List<String> msgList = new ArrayList<String>();
		MessagesSOAPHandler messageHandler = new MessagesSOAPHandler();
		SecuritySOAPHandler securityHandler = null;		
		
		try {
		
		ServiceHelper.setWpaEndpointProtocol("https",nginxURL);

		ServiceHelper.setStxEndpointHostAndPort(host, Integer.parseInt(port),nginxURL);

		messageHandler = new MessagesSOAPHandler();
		AuthenticationToken authenticationToken = new AuthenticationToken();

		authenticationToken.setUsername(userId);
		authenticationToken.setValue(authToken);
		
		
		securityHandler = new SecuritySOAPHandler(authenticationToken);
		
		QdsService qdsService = ServiceHelper.qdsService(messageHandler, securityHandler);
			
		qdsService.deleteQds(Long.parseLong(qdsNo));
	
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		} 
		catch (ServerSOAPFaultException e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			msgList.add(e.getMessage());
			e.printStackTrace();
		}
		
		finally {
			response.setAuthToekn(securityHandler.getAuthenticationToken().getValue());
		}
		
		return msgList;
		
	}
}
