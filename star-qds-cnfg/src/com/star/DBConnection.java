package com.star;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

	public static Connection getDBConnection() {

		String URL = "";
		String dbUsername = "";
		String dbPassword = "";

		try {

			if (System.getenv("SERVER") != null) {
				if (System.getenv("SERVER").equals("POSTGRES")) {
					Class.forName("org.postgresql.Driver");

					String ipAddr = System.getenv("PGHOST");
					String port = System.getenv("PGPORT");
					String db = System.getenv("PGDATABASE");

					URL = "jdbc:postgresql://" + ipAddr + ":" + port + "/" + db;

					dbUsername = "postgres";
					dbPassword = "postgres";

				} else if (System.getenv("SERVER").equals("INFORMIX")) {
					Class.forName("com.informix.jdbc.IfxDriver");

					String ipAddr = System.getenv("PRI_SRVR");
					String port = System.getenv("DBPORT");
					String db = System.getenv("DATABASE");
					String server = System.getenv("INFORMIXSERVER");

					URL = "jdbc:informix-sqli://" + ipAddr + ":" + port + "/" + db + ":INFORMIXSERVER=" + server;

					dbUsername = System.getenv("USER");
					dbPassword = System.getenv("USER");

				}
			}

			else {

				Class.forName("org.postgresql.Driver");

				URL = "jdbc:postgresql://172.20.35.15:12010/sq11pr08";
				dbUsername = "sq1108";
				dbPassword = "sq1108";

				URL = "jdbc:postgresql://172.20.35.15:12016/ai012pr09";
				dbUsername = "ai01209";
				dbPassword = "ai01209";
				
				URL = "jdbc:postgresql://172.20.35.15:12012/ap11pr09";
				dbUsername = "ap1109";
				dbPassword = "ap1109";
				
				URL = "jdbc:postgresql://172.20.35.48:12020/il021pr07";
				dbUsername = "il02107";
				dbPassword = "il02107";
				
				URL = "jdbc:postgresql://172.20.35.15:12018/iq01prdemo";
				dbUsername = "iq01demo";
				dbPassword = "iq01demo";
				
				URL = "jdbc:postgresql://172.20.35.48:12020/il030pr07";
				dbUsername = "il03007";
				dbPassword = "il03007";

			}

			String dbConnectionURL = URL;

			Connection connection = DriverManager.getConnection(dbConnectionURL, dbUsername, dbPassword);

			connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);

			return connection;
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			} else {
				throw new RuntimeException(e);
			}
		}
	}
}
