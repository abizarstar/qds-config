package com.star.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.ws.exec.authentication.GatewayLoginResponseType;
import com.invera.stratix.ws.services.security.StratixFault;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MessagesSOAPHandler;
import com.ws.samples.handler.SecuritySOAPHandler;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet("/Authenticate")
public class AuthenticateQDS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthenticateQDS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String userId = "";
		String password = "";
		String token = "";
		
		GatewayLoginResponseType gatewayLoginResponseType = null;
		
		String reqTyp = request.getParameter("reqTyp");
		
		String execPort = "";
		execPort = getExecInfo();
		
		JsonObject jsonObject = new JsonObject();
		
		if(reqTyp.equals("login"))
		{
			 userId = request.getParameter("user");
			 password = request.getParameter("pass");
			 
			 Authenticate authenticate = new Authenticate();
				
			//gatewayLoginResponseType = authenticate.stxAuthToken(userId, password, execPort,"","","");
			
			jsonObject.addProperty("login-user", userId);
			
			if(gatewayLoginResponseType != null)
	    	{	
				jsonObject.addProperty("authToken", gatewayLoginResponseType.getAuthenticationToken().getValue());
				
				jsonObject.addProperty("appHost", gatewayLoginResponseType.getConnectedServer().trim());
				jsonObject.addProperty("appPort", gatewayLoginResponseType.getConnectedPort());
	    	}
	    	else
	    	{
	    		jsonObject.addProperty("authToken", "");
	    	}
			
			
			
			 
		}
		else if(reqTyp.equals("logout"))
		{
			AuthenticationToken authenticationToken = new AuthenticationToken();
			
			Authenticate authenticateSTX = new Authenticate();
			
			String execServer = System.getenv("PRI_EXECSRVR");
			
			ServiceHelper.setWpaEndpointProtocol("http","");
			
			System.err.println("EXEC SERVER - " + execServer);
			System.err.println("EXEC PORT - " + execPort);
			
			
			if(execServer != null)
			{
				ServiceHelper.setStxEndpointHostAndPort(execServer, Integer.parseInt(execPort),"");
			}
			else
			{
				ServiceHelper.setStxEndpointHostAndPort("172.20.100.72", Integer.parseInt(execPort),"");
			}
			
			userId = request.getParameter("user");
			token = request.getParameter("token");
		
			try {
				authenticationToken.setUsername(userId);
				authenticationToken.setValue(token);
				
				SecuritySOAPHandler securityHandler = new SecuritySOAPHandler(authenticationToken);
				
				MessagesSOAPHandler messageHandler = new MessagesSOAPHandler();
				
				authenticateSTX.logout(messageHandler, securityHandler);
				
			} catch (StratixFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("DATA INPUT----->" + request.getParameter("user"));
		System.out.println("DATA INPUT----->" + request.getParameter("pass"));
		
		PrintWriter printWriter = response.getWriter();
		
		
		
		printWriter.write(jsonObject.toString());
	}
	
	protected String getExecInfo()
	{
		/* CHAND EXEC PORT - 172.20.35.65 35234 ap10203 T12*/
		/* SATURN ap10409 EXEC PORT - 172.20.100.24 55000 ap10409 TST*/
		
		String line = null;
		
		String portNumber = "";
		
		InputStream input = getServletContext().getResourceAsStream("/WEB-INF/exec_port.ini");
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
		
		try {
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println("READ EXEC INI FOR PORT - " + line);
				portNumber = line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return portNumber;
	}
	

}
