package com.star.auth;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invera.stratix.ws.exec.authentication.AuthenticationService;
import com.invera.stratix.ws.exec.authentication.GatewayLoginRequestType;
import com.invera.stratix.ws.exec.authentication.GatewayLoginResponseType;
import com.invera.stratix.ws.services.security.LogoutService;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MessagesSOAPHandler;
import com.ws.samples.handler.SecuritySOAPHandler;

public class Authenticate {

	/**
	 * @param args
	 */
	public String stxAuth(String userId, String password, String execPort,String nginxURL) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		String execServer = System.getenv("PRI_EXECSRVR");
		
		ServiceHelper.setWpaEndpointProtocol("https",nginxURL);
		
		System.err.println("EXEC SERVER - " + execServer);
		System.err.println("EXEC PORT - " + execPort);
		
		
		if(execServer != null)
		{
			ServiceHelper.setWpaEndpointHostAndPort(execServer, Integer.parseInt(execPort),nginxURL);
		}
		else
		{
			ServiceHelper.setWpaEndpointHostAndPort("172.20.35.73", Integer.parseInt(execPort),nginxURL);
		}

		MessagesSOAPHandler messageHandler = new MessagesSOAPHandler();
			
		int loginFlg = 0;
		List<String> messages = new ArrayList<String>();
		
		try {
			
			validateLogin(messageHandler, userId, password);
			
			loginFlg = 1;
		
		} catch (com.invera.stratix.ws.exec.authentication.StratixFault fault) {
			
			messages =  printMessages(messageHandler);
			
			loginFlg = 0;
		}
		
		Gson gson = new GsonBuilder().create();
        JsonArray myCustomArray = gson.toJsonTree(messages).getAsJsonArray();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("msg", myCustomArray);
        
        jsonObject.addProperty("sts", loginFlg);
        jsonObject.addProperty("role", "NA");
        
		return jsonObject.toString();
		
	}
	
	public GatewayLoginResponseType stxAuthToken(MessagesSOAPHandler messageHandler, String userId, String password, String execPort, String envClass, String envSrvr, String host,String nginxURL) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		GatewayLoginResponseType gatewayLoginResponseType = null;
		
		String execServer = System.getenv("PRI_EXECSRVR");
		
		ServiceHelper.setWpaEndpointProtocol("https",nginxURL);
		
		System.err.println("EXEC SERVER - " + execServer);
		System.err.println("EXEC PORT - " + execPort);
		
		
		if(execServer != null)
		{
			ServiceHelper.setWpaEndpointHostAndPort(execServer, Integer.parseInt(execPort),nginxURL);
		}
		else
		{
			ServiceHelper.setWpaEndpointHostAndPort(host, Integer.parseInt(execPort),nginxURL);
		}

		List<String> messages = new ArrayList<String>();
		
		try {
			
			gatewayLoginResponseType = login(messageHandler, userId, password, envClass, envSrvr);
		
			System.err.println("EXEC TOKEN - " + gatewayLoginResponseType.getAuthenticationToken().getValue());
			
		} catch (com.invera.stratix.ws.exec.authentication.StratixFault fault) {
			
			messages =  printMessages(messageHandler);
		}
        
		return gatewayLoginResponseType;
		
	}
	
	private void validateLogin(
			MessagesSOAPHandler messageHandler,String userId, String password) throws MalformedURLException,
			com.invera.stratix.ws.exec.authentication.StratixFault {

		AuthenticationService authenticationService = ServiceHelper
				.createAuthenticationService(messageHandler);
		
		GatewayLoginRequestType input = new GatewayLoginRequestType();
		
		input.setUsername(userId);
		input.setUsername(password);
		
		
		authenticationService.validate(userId, password);

	}
	
	private GatewayLoginResponseType login(
			MessagesSOAPHandler messageHandler,String userId, String password, String envClass, String envSrvr) throws MalformedURLException,
			com.invera.stratix.ws.exec.authentication.StratixFault {

		AuthenticationService authenticationService = ServiceHelper
				.createAuthenticationService(messageHandler);
		
		
		String USER = System.getenv("USER");
		
		String ENV_CLASS = System.getenv("ENVCLASS");
		
		GatewayLoginRequestType loginRequest = new GatewayLoginRequestType();
		
		if(USER ==null )
		{
			loginRequest.setEnvironmentName(envSrvr);
		}
		else
		{
			loginRequest.setEnvironmentName(USER);
		}
		
		if(ENV_CLASS == null)
		{
			loginRequest.setEnvironmentClass(envClass);
		}
		else
		{
			loginRequest.setEnvironmentClass(ENV_CLASS);
		}
		
		
		

		// Network Access - I: Internal, E: External
		loginRequest.setConnectedAccessType("I");

		loginRequest.setUsername(userId);
		loginRequest.setPassword(password);
		// Optional: Company ID
		// loginRequest.setCompanyId("APW");

		loginRequest.setForceDisconnect(Boolean.TRUE);

		return authenticationService.gatewayLogin(loginRequest);
	}
	
	private static List<String> printMessages(MessagesSOAPHandler messageHandler) {

		List<String> messages = messageHandler.getMessageList();
		for (String message : messages) {
			if (message != null) {
				
				System.out.println("IN PRINT MESSAGE");
				System.out.println(message);
			}
		}
		return messages;

	}
	
	public void logout(MessagesSOAPHandler messageHandler,
			SecuritySOAPHandler securityHandler) throws MalformedURLException,
			com.invera.stratix.ws.services.security.StratixFault {

		LogoutService logoutService = ServiceHelper.createLogoutService(
				messageHandler, securityHandler);
		logoutService.logout(securityHandler.getAuthenticationToken());
	}

}
