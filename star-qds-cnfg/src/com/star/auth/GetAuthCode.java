package com.star.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GetAuthCode {

	public static final String JSON_HANDLER_URL = "/stratix-info";
	// Invera HTTP headers
	private static final String HEADER_REQUEST_ID = "invera-request-id";
	private static final String HEADER_USERNAME = "invera-username";
	private static final String HEADER_CLIENT_PROGRAM_NAME = "invera-client-program-name";
	private static final String HEADER_CLIENT_TYPE = "invera-client-type";
	private static final String HEADER_CLIENT_PLATFORM = "invera-client-platform";

	private static final String HEADER_CONTEXT_ID = "invera-context-id";
	private static final String HEADER_CHARSET = "invera-charset";
	private static final String HEADER_SESSION = "invera-session";
	private static final String CONTENT_TYPE = "content-type";

	private static final String REQUEST_INPUT_TYPE = "application/json; charset=utf-8";

	public String getPlainToken(String address, String port, String usrId, String authToken, String nginxURL) {
		// System.out.println("****** getPlainToken ***** ");

		String requestId = String.valueOf(new Date().getTime());
		String plainTextCode = "";
		JsonObject jsonResponse = null;
		URL url;

		try {

			String uri = null;

			uri = JSON_HANDLER_URL + "/" + "common/session/SessionService/getExtendedSessionInformation";

			if(nginxURL != null && nginxURL.length() > 0) {
				uri = nginxURL + uri;
			} else {
				uri = "http://" + address + ":" + port + "/" + uri;
			}

			// System.out.println("getPlainToken >> uri >> " + uri);

			url = new URL(uri);

			// System.out.println(" getPlainToken >> url >> " + url);

			// System.out.println("getPlainToken >> AUTH--->" + authToken);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty(CONTENT_TYPE, REQUEST_INPUT_TYPE);
			con.setRequestProperty(HEADER_CHARSET, "ISO-8859-4");
			con.setRequestProperty(HEADER_CLIENT_PLATFORM, "Windows");
			con.setRequestProperty(HEADER_CLIENT_PROGRAM_NAME, "WMN");
			con.setRequestProperty(HEADER_CLIENT_TYPE, "desktop");
			con.setRequestProperty(HEADER_CONTEXT_ID, "XX000000null");
			con.setRequestProperty(HEADER_REQUEST_ID, requestId);
			con.setRequestProperty(HEADER_USERNAME, usrId);
			con.setRequestProperty(HEADER_SESSION, authToken);

			String jsonInputString = "WMN";

			con.setDoOutput(true);
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
				os.flush();
				os.close();
			}

			int responseCode = con.getResponseCode();
			// System.out.println("POST Response Code :: " + responseCode);

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				// print result
				System.out.println(response.toString());

				if (response.toString().length() > 0) {
					JsonParser parser = new JsonParser();
					jsonResponse = (JsonObject) parser.parse(response.toString());
				}

			} else {
				System.out.println("POST request not worked");
				// System.out.println("jsonResponse >> " + jsonResponse);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (jsonResponse != null) {
			if (jsonResponse.has("output")) {
				JsonObject jsonObject = jsonResponse.getAsJsonObject("output");

				plainTextCode = jsonObject.get("sessionToken").getAsString();

			}

			return jsonResponse.toString();

		} else {
			return null;
		}

	}

}
