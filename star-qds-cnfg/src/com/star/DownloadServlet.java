package com.star;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
// import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// @WebServlet("/DownloadServlet")


public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String filePath;
	private String fileNm;
	
	
	public DownloadServlet(String filePath, String fileNm) {
		this.filePath = filePath;
		this.fileNm = fileNm;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			File downloadFile = new File(filePath + fileNm);

			boolean exists = downloadFile.exists();

			if (exists) {
				FileInputStream inStream = new FileInputStream(downloadFile);

				// modifies response
				response.setContentLength((int) downloadFile.length());

				// forces download
				response.setContentType("APPLICATION/OCTET-STREAM");
				response.setHeader("Content-Disposition", "attachment; filename=" + fileNm);

				// obtains response's output stream
				OutputStream outStream = response.getOutputStream();

				byte[] buffer = new byte[4096];
				int bytesRead = -1;

				while ((bytesRead = inStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}

				inStream.close();
				outStream.close();
			} else {

			}
		} catch (Exception e) {
			System.err.println(">>>>> ERROR: " + e.getMessage());
		} finally {

		}
	}

}
