package com.star;

public class QdsHeaderInput {

	    protected String mill;
	    protected String heat;
	    protected String whs;
	    protected Integer origPoNo;
	    protected Integer origPoItem;
	    protected Integer origPoDistribution;
	    protected String millOrdNo;
	    protected String venId;
	    protected String status;
	    protected int apvdFlag;
	    protected String origZone;
	    protected int uknownHeat;
		public String getMill() {
			return mill;
		}
		public void setMill(String mill) {
			this.mill = mill;
		}
		public String getHeat() {
			return heat;
		}
		public void setHeat(String heat) {
			this.heat = heat;
		}
		public String getWhs() {
			return whs;
		}
		public void setWhs(String whs) {
			this.whs = whs;
		}
		public Integer getOrigPoNo() {
			return origPoNo;
		}
		public void setOrigPoNo(Integer origPoNo) {
			this.origPoNo = origPoNo;
		}
		public Integer getOrigPoItem() {
			return origPoItem;
		}
		public void setOrigPoItem(Integer origPoItem) {
			this.origPoItem = origPoItem;
		}
		public Integer getOrigPoDistribution() {
			return origPoDistribution;
		}
		public void setOrigPoDistribution(Integer origPoDistribution) {
			this.origPoDistribution = origPoDistribution;
		}
		public String getMillOrdNo() {
			return millOrdNo;
		}
		public void setMillOrdNo(String millOrdNo) {
			this.millOrdNo = millOrdNo;
		}
		public String getVenId() {
			return venId;
		}
		public void setVenId(String venId) {
			this.venId = venId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public int getApvdFlag() {
			return apvdFlag;
		}
		public void setApvdFlag(int apvdFlag) {
			this.apvdFlag = apvdFlag;
		}
		public String getOrigZone() {
			return origZone;
		}
		public void setOrigZone(String origZone) {
			this.origZone = origZone;
		}
		public int getUknownHeat() {
			return uknownHeat;
		}
		public void setUknownHeat(int uknownHeat) {
			this.uknownHeat = uknownHeat;
		}
	    
	    
	    
}
