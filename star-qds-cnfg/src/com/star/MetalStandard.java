package com.star;

public class MetalStandard {

	private String mssCtlNo;
	private String mssSdo;
	private String mssStdId;
	private String mssAddnlId;
	
	public MetalStandard(String mssCtlNo, String mssSdo, String mssStdId, String mssAddnlId) {
		super();
		this.mssCtlNo = mssCtlNo;
		this.mssSdo = mssSdo;
		this.mssStdId = mssStdId;
		this.mssAddnlId = mssAddnlId;
	}

	public String getMssCtlNo() {
		return mssCtlNo;
	}

	public void setMssCtlNo(String mssCtlNo) {
		this.mssCtlNo = mssCtlNo;
	}

	public String getMssSdo() {
		return mssSdo;
	}

	public void setMssSdo(String mssSdo) {
		this.mssSdo = mssSdo;
	}

	public String getMssStdId() {
		return mssStdId;
	}

	public void setMssStdId(String mssStdId) {
		this.mssStdId = mssStdId;
	}

	public String getMssAddnlId() {
		return mssAddnlId;
	}

	public void setMssAddnlId(String mssAddnlId) {
		this.mssAddnlId = mssAddnlId;
	}

	@Override
	public String toString() {
		return "MetalStandard [mssCtlNo=" + mssCtlNo + ", mssSdo=" + mssSdo + ", mssStdId=" + mssStdId + ", mssAddnlId="
				+ mssAddnlId + "]";
	}

}
