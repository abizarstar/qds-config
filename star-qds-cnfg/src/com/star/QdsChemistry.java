package com.star;

public class QdsChemistry {

	private String chmEl;
	private String chmVl;

	
	public QdsChemistry(String chmEl, String chmVl) {
		super();
		this.chmEl = chmEl;
		this.chmVl = chmVl;
	}

	public String getChmEl() {
		return chmEl;
	}

	public void setChmEl(String chmEl) {
		this.chmEl = chmEl;
	}

	public String getChmVl() {
		return chmVl;
	}

	public void setChmVl(String chmVl) {
		this.chmVl = chmVl;
	}



	@Override
	public String toString() {
		return "QdsChemistry [chmEl=" + chmEl + ", chmVl=" + chmVl  + "]";
	}

	
	
}
