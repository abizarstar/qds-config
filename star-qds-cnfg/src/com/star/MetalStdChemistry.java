package com.star;

public class MetalStdChemistry {

	private String chmEl;
	private String chmMinVl;
	private String chmMaxVl;

	public MetalStdChemistry(String chmEl, String chmMinVl, String chmMaxVl) {
		super();
		this.chmEl = chmEl;
		this.chmMinVl = chmMinVl;
		this.chmMaxVl = chmMaxVl;
	}

	public String getChmEl() {
		return chmEl;
	}

	public void setChmEl(String chmEl) {
		this.chmEl = chmEl;
	}

	public String getChmMinVl() {
		return chmMinVl;
	}

	public void setChmMinVl(String chmMinVl) {
		this.chmMinVl = chmMinVl;
	}

	public String getChmMaxVl() {
		return chmMaxVl;
	}

	public void setChmMaxVl(String chmMaxVl) {
		this.chmMaxVl = chmMaxVl;
	}

	@Override
	public String toString() {
		return "MetalStdChemistry [chmEl=" + chmEl + ", chmMinVl=" + chmMinVl + ", chmMaxVl=" + chmMaxVl + "]";
	}

}
