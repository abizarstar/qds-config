package com.star;

import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.services.UpdateQdsMetalStandardInput;
import com.star.auth.GetAuthCode;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MessagesSOAPHandler;
import com.ws.samples.handler.SecuritySOAPHandler;

public class GetDataFromDB {

	public static String getQDSDetails(String cmpyId, String userId, String qdsCtlNo, String param, String host,
			String port, String nginxURL, String authToken) {

		String outputVal = "";
		SendExternalReq externalReq = new SendExternalReq();

		try {

			/** Chemistry */
			if (param.equals("CHM")) {

				JsonObject inpReq = new JsonObject();
				inpReq.addProperty("prsMd", "P");
				inpReq.addProperty("qdsFltr", "4");
				inpReq.addProperty("frstTmFlg", "1");
				inpReq.addProperty("qdsCtlNo", qdsCtlNo);
				inpReq.addProperty("mssCtlNo", "0");
				inpReq.addProperty("cmtTmpl", "");
				inpReq.addProperty("cmtFlg", "0");
				inpReq.addProperty("fmtFlg", "0");
				inpReq.addProperty("selChmel", "");
				inpReq.addProperty("chmLnSize", "0");

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"45\",\"frstTmFlg\":1,\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"mssCtlNo\":\"0\",\"cmtTmpl\":\"\",\"cmtFlg\":\"0\",\"fmtFlg\":0,\"selChmel\":\"\",\"chmLnSize\":0}}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqcm", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);

			}

			else if (param.equals("HT")) {
				JsonObject inpReq = new JsonObject();

				inpReq.addProperty("prsMd", "P");
				inpReq.addProperty("qdsCtlNo", qdsCtlNo);

				String jsonInp = "{\"prsMd\":\"P\",\"qdsCtlNo\":" + qdsCtlNo + "}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqht", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			else if (param.equals("IMP")) {
				JsonObject inpReq = new JsonObject();

				inpReq.addProperty("prsMd", "P");
				inpReq.addProperty("qdsFltr", "2");
				inpReq.addProperty("qdsCtlNo", qdsCtlNo);

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"2\",\"qdsCtlNo\":" + qdsCtlNo + "}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqmp", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			else if (param.equals("JMY")) {

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"4\",\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"mssCtlNo\":0,\"jmyTstNo\":1,\"jmyTmpl\":\"\",\"jmyFlg\":0}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqjm", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			else if (param.equals("MIC")) {

				String jsonInp = "{\"prsMd\":\"P\",\"qdsCtlNo\":" + qdsCtlNo + "}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqmi", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			/* GET JOMINY 2 */
			else if (param.equals("JMY2")) {

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"4\",\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"mssCtlNo\":0,\"jmyTstNo\":2,\"jmyTmpl\":\"\",\"jmyFlg\":0}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqjm", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			/* GET TEST SPECIFICATIONS */
			else if (param.equals("TST")) {

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"C\",\"frstTmFlg\":1,\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"mssCtlNo\":0,\"tstTmpl\":\"\",\"tstFlg\":0,\"selTstAbbr\":\"\"}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqdt", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			/* GET METAL STANDARDS */
			else if (param.equals("MTL")) {

				String jsonInp = "{\"cpsFlg\":0,\"partSts\":\"\",\"partCtlNo\":0,\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"refPfx\":\"\",\"refNo\":0,\"refItm\":0}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "scbpss", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			/* GET EXTERNAL CERTIFICATES */
			else if (param.equals("CERT")) {

				String jsonInp = "{\"prsMd\":\"P\",\"qdsFltr\":\"5\",\"frstTmFlg\":1,\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"certTyp\":\"\",\"sdo\":\"\",\"refPfx\":\"\",\"refNo\":0,\"itmCtlNo\":0,\"rawtSeqNo\":0}";

				outputVal = externalReq.getQDSInformation(host, port, authToken, "P", "mcbqec", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}

			else if (param.equals("QDS")) {
				JsonObject inpReq = new JsonObject();

				inpReq.addProperty("prsMd", "P");
				inpReq.addProperty("qdsFltr", "4");
				inpReq.addProperty("frstTmFlg", "1");
				inpReq.addProperty("qdsCtlNo", qdsCtlNo);
				inpReq.addProperty("mssCtlNo", "0");
				inpReq.addProperty("cmtTmpl", "");
				inpReq.addProperty("cmtFlg", "0");
				inpReq.addProperty("fmtFlg", "0");
				inpReq.addProperty("selChmel", "");
				inpReq.addProperty("chmLnSize", "0");

				String jsonInp = "{\"prsMd\":\"4\",\"errFlg\":0,\"unknwFlg\":0,\"qdsCtlNo\":" + qdsCtlNo
						+ ",\"itmCtlNo\":0,\"tagNo\":\"\",\"mssCtlNo\":0,\"altCtlNo\":0,\"whs\":\"\",\"mill\":\"\",\"heat\":\"\",\"origZn\":\"\",\"mfrOrigZn\":\"\",\"mltdOrigZn\":\"\",\"unknwHeat\":0,\"millOrdNo\":\"\",\"venId\":\"\",\"qdsSts\":\"\",\"qdsTyp\":\"\",\"apvdFlg\":0,\"certRcvd\":0,\"origPfx\":\"\",\"origNo\":0,\"origItm\":0,\"origSbitm\":0,\"crtdPfx\":\"\",\"crtdNo\":0,\"crtdItm\":0,\"crtdSbitm\":0,\"origPoPfx\":\"\",\"origPoNo\":0,\"origPoItm\":0,\"origPoDist\":0,\"apvdLgnId\":\"\",\"comprFlg\":\"\"}";

				/* GET CHEMISTRY INFORMATION */
				outputVal = externalReq.getQDSInformation(host, port, authToken, "4", "mccqds", "QDS", userId, cmpyId,
						"en", "USA", jsonInp, nginxURL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return outputVal;
	}

	public static String callLogin(String cmpyId, String userId, String qdsCtlNo, String auth, String appUrl) {

		// System.out.println("********** callLogin ************ ");

		List<String> usrInfo = getUsrInfo(userId);
		// System.out.println("usrInfo >> " + usrInfo);
		String usrNm = usrInfo.get(0);
		String usrLng = usrInfo.get(1);
		String usrCty = usrInfo.get(2);

		String nginxURL = "";
		String host = "";
		String port = "";

		JsonObject mainObj = new JsonObject();
		JsonArray msgArray = new JsonArray();

		if (appUrl.contains("localhost")) {
			host = "172.20.35.60";
			port = "60208";
			//nginxURL = "https://exildemopl2demo.invex.cloud/il01dem1-T25";
			nginxURL = "https://excli030pl2qa1.invex.cloud/il03007-T23";

			// https://jadesterlingsteel.invex.cloud/tstjad-TST
		}

		else if (appUrl.contains("invex.cloud") || appUrl.contains("auxinvex")) {

			if (appUrl.contains("https")) {
				nginxURL = appUrl;
			} else {
				nginxURL = "https://" + appUrl;
			}

		}

		else {
			String urlArr[] = appUrl.split(":");
			host = urlArr[0];
			port = urlArr[1];
		}

		// System.out.println("local host >> " + host);
		// System.out.println("local port >> " + port);
		// System.out.println("local url >> " + nginxURL);

		GetAuthCode authCode = new GetAuthCode();
		String jsonStr = authCode.getPlainToken(host, port, userId, auth, nginxURL);
		// System.out.println("jsonStr >> " + jsonStr);

		if (jsonStr != null) {

			JsonParser parser = new JsonParser();
			JsonObject jsonResponse = (JsonObject) parser.parse(jsonStr.toString());
			// System.out.println("jsonResponse >> " + jsonResponse);

			if (jsonResponse.has("output")) {
				JsonObject jsonObject = jsonResponse.getAsJsonObject("output");
				mainObj.addProperty("authToken", jsonObject.get("sessionToken").getAsString());
				mainObj.addProperty("cmpy", jsonObject.get("companyId").getAsString());
				mainObj.addProperty("userNm", jsonObject.get("displayName").getAsString());
				mainObj.addProperty("userId", jsonObject.get("userId").getAsString());
				mainObj.addProperty("baseLanguage", jsonObject.get("baseLanguage").getAsString());
				mainObj.addProperty("baseCountry", jsonObject.get("baseCountry").getAsString());

				mainObj.addProperty("server", host);
				mainObj.addProperty("port", port);
				mainObj.addProperty("nginxURL", nginxURL);
				mainObj.addProperty("qdsCtlNo", qdsCtlNo);
				mainObj.addProperty("rtnSts", "0");

				mainObj.add("msgList", msgArray);

				return mainObj.toString();
			}
		}

		mainObj.addProperty("authToken", auth);
		mainObj.addProperty("cmpy", cmpyId);
		mainObj.addProperty("userNm", usrNm);
		mainObj.addProperty("userId", userId);
		mainObj.addProperty("baseLanguage", usrLng);
		mainObj.addProperty("baseCountry", usrCty);

		mainObj.addProperty("server", host);
		mainObj.addProperty("port", port);
		mainObj.addProperty("nginxURL", nginxURL);
		mainObj.addProperty("qdsCtlNo", qdsCtlNo);
		mainObj.addProperty("rtnSts", "0");

		mainObj.add("msgList", msgArray);

		return mainObj.toString();
	}

	public static String getCustomerList(String cmpyId) {
		// System.out.println("****** getCustomerList ******* ");

		Connection con = DBConnection.getDBConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		JsonObject mainObject = new JsonObject();
		JsonArray jsonArray = new JsonArray();

		try {
			String query = "select cus_cus_id, cus_cus_nm from arrcus_rec where cus_cmpy_id=?"
					+ " order by cus_cmpy_id, cus_cus_nm  ";
			ps = con.prepareStatement(query);
			ps.setString(1, cmpyId);
			// System.out.println("Query >> " + ps.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("cusId2", checkNull(rs.getString(1)));
				jsonObject.addProperty("cusNm", checkNull(rs.getString(2)));
				jsonArray.add(jsonObject);
			}
			mainObject.add("cusFldTbl", jsonArray);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			throw new RuntimeException(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return mainObject.toString();
	}

	public static List<String> getUsrInfo(String userId) {
		// System.out.println("****** getUsrInfo ******* ");
		Connection con = DBConnection.getDBConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> list = new ArrayList<>();
		try {
			String query = "select usr_nm,usr_lng,usr_cty from mxrusr_rec where usr_lgn_id=? and usr_actv='1';";
			ps = con.prepareStatement(query);
			ps.setString(1, userId);
			// System.out.println("Query >> " + ps.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				list.add(checkNull(rs.getString(1)));
				list.add(checkNull(rs.getString(2)));
				list.add(checkNull(rs.getString(3)));
			} else {
				list.add("");
				list.add("");
				list.add("");
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			throw new RuntimeException(sqle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<QdsChemistry> getQdsChemistry(String cmpyId, String qdsCtlNo, Connection con) {
		System.out.println(" ********* getQdsChemistry ********** ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<QdsChemistry> list = new ArrayList<>();

		// boolean isLocalEnv = true;
		//
		// if (System.getenv("SERVER") != null && cmpyId.equalsIgnoreCase("JAD")) {
		// isLocalEnv = false;
		// }

		try {
			String query = "SELECT qcm_chmel_1, qcm_chm_val_1, qcm_chm_alpha_1,\r\n"
					+ "    qcm_chmel_2, qcm_chm_val_2, qcm_chm_alpha_2,\r\n"
					+ "    qcm_chmel_3, qcm_chm_val_3, qcm_chm_alpha_3,\r\n"
					+ "    qcm_chmel_4, qcm_chm_val_4, qcm_chm_alpha_4,\r\n"
					+ "    qcm_chmel_5, qcm_chm_val_5, qcm_chm_alpha_5,\r\n"
					+ "    qcm_chmel_6, qcm_chm_val_6, qcm_chm_alpha_6,\r\n"
					+ "    qcm_chmel_7, qcm_chm_val_7, qcm_chm_alpha_7,\r\n"
					+ "    qcm_chmel_8, qcm_chm_val_8, qcm_chm_alpha_8,\r\n"
					+ "    qcm_chmel_9, qcm_chm_val_9, qcm_chm_alpha_9,\r\n"
					+ "    qcm_chmel_10, qcm_chm_val_10, qcm_chm_alpha_10,\r\n"
					+ "    qcm_chmel_11, qcm_chm_val_11, qcm_chm_alpha_11,\r\n"
					+ "    qcm_chmel_12, qcm_chm_val_12, qcm_chm_alpha_12,\r\n"
					+ "    qcm_chmel_13, qcm_chm_val_13, qcm_chm_alpha_13,\r\n"
					+ "    qcm_chmel_14, qcm_chm_val_14, qcm_chm_alpha_14,\r\n"
					+ "    qcm_chmel_15, qcm_chm_val_15, qcm_chm_alpha_15,\r\n"
					+ "    qcm_chmel_16, qcm_chm_val_16, qcm_chm_alpha_16,\r\n"
					+ "    qcm_chmel_17, qcm_chm_val_17, qcm_chm_alpha_17,\r\n"
					+ "    qcm_chmel_18, qcm_chm_val_18, qcm_chm_alpha_18,\r\n"
					+ "    qcm_chmel_19, qcm_chm_val_19, qcm_chm_alpha_19,\r\n"
					+ "    qcm_chmel_20, qcm_chm_val_20, qcm_chm_alpha_20 FROM mchqcm_rec\r\n"
					+ "	   WHERE qcm_cmpy_id = ?   AND qcm_qds_ctl_no = ? limit 1";
			ps = con.prepareStatement(query);
			ps.setString(1, cmpyId);
			ps.setInt(2, Integer.parseInt(qdsCtlNo));
			// System.out.println("Query >> " + ps.toString());
			rs = ps.executeQuery();

			String chmEl = "", chmVl = "", chmAlpha = "";
			if (rs.next()) {
				for (int i = 1; i <= 60; i = i + 3) {
					chmEl = checkNull(rs.getString(i));
					chmVl = checkNull2(rs.getString(i + 1));
					chmAlpha = checkNull(rs.getString(i + 2));

					if (Double.parseDouble(chmVl) > 0) {
						QdsChemistry qdsChm = new QdsChemistry(chmEl, chmVl);
						list.add(qdsChm);
					}

					else if (chmAlpha.length() > 0 && !chmAlpha.equals("-")) {
						QdsChemistry qdsChm = new QdsChemistry(chmEl, chmAlpha);
						list.add(qdsChm);
					}

					// Additional condition based on Tom comment
					else if (chmEl.equals("Ni") || chmEl.equals("Cr") || chmEl.equals("Mo") || chmEl.equals("Cu")
							|| chmEl.equals("B") || chmEl.equals("Ti") || chmEl.equals("V") || chmEl.equals("Pb")
							|| chmEl.equals("Bi")) {

						if (Double.parseDouble(chmVl) == 0) {
							QdsChemistry qdsChm = new QdsChemistry(chmEl, chmVl);
							list.add(qdsChm);
						}

						else if (chmAlpha.length() > 0 && chmAlpha.equals("-")) {
							chmAlpha = "0";
							QdsChemistry qdsChm = new QdsChemistry(chmEl, chmAlpha);
							list.add(qdsChm);
						}
					}
				}
			}
		} catch (SQLException sqle) {
			System.out.println("SQLException--getQdsChemistry---->" + sqle.getLocalizedMessage());
		} catch (Exception e) {
			System.out.println("Exception--getQdsChemistry---->" + e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	public static JsonArray getMtlStdAndChemistry(Connection con) {
		System.out.println(" ********* getMtlStdAndChemistry ********** ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		JsonArray jsnMtlStdChmArr = new JsonArray();
		try {
			String query = "SELECT  mss_mss_ctl_no, mss_sdo, mss_std_id, mss_addnl_id,\r\n"
					+ "    cms_chmel_1, cms_min_val_1, cms_max_val_1,\r\n"
					+ "    cms_chmel_2, cms_min_val_2, cms_max_val_2,\r\n"
					+ "    cms_chmel_3, cms_min_val_3, cms_max_val_3,\r\n"
					+ "    cms_chmel_4, cms_min_val_4, cms_max_val_4,\r\n"
					+ "    cms_chmel_5, cms_min_val_5, cms_max_val_5,\r\n"
					+ "    cms_chmel_6, cms_min_val_6, cms_max_val_6,\r\n"
					+ "    cms_chmel_7, cms_min_val_7, cms_max_val_7,\r\n"
					+ "    cms_chmel_8, cms_min_val_8, cms_max_val_8,\r\n"
					+ "    cms_chmel_9, cms_min_val_9, cms_max_val_9,\r\n"
					+ "    cms_chmel_10, cms_min_val_10, cms_max_val_10,\r\n"
					+ "    cms_chmel_11, cms_min_val_11, cms_max_val_11,\r\n"
					+ "    cms_chmel_12, cms_min_val_12, cms_max_val_12,\r\n"
					+ "    cms_chmel_13, cms_min_val_13, cms_max_val_13,\r\n"
					+ "    cms_chmel_14, cms_min_val_14, cms_max_val_14,\r\n"
					+ "    cms_chmel_15, cms_min_val_15, cms_max_val_15,\r\n"
					+ "    cms_chmel_16, cms_min_val_16, cms_max_val_16,\r\n"
					+ "    cms_chmel_17, cms_min_val_17, cms_max_val_17,\r\n"
					+ "    cms_chmel_18, cms_min_val_18, cms_max_val_18,\r\n"
					+ "    cms_chmel_19, cms_min_val_19, cms_max_val_19,\r\n"
					+ "    cms_chmel_20, cms_min_val_20, cms_max_val_20 FROM \r\n"
					+ "    mcrmss_rec, mcrcms_rec WHERE  mss_mss_ctl_no = cms_mss_ctl_no \r\n"
					+ "    AND mss_actv = '1' order by mss_mss_ctl_no";
			ps = con.prepareStatement(query);
			// System.out.println("Query >> " + ps.toString());
			rs = ps.executeQuery();

			String chmEl = "", chmMinVl = "", chmMaxVl = "";
			while (rs.next()) {
				int mtlStdChmCount = 0;

				JsonObject jsnMtlStdObj = new JsonObject();
				jsnMtlStdObj.addProperty("mssCtlNo", checkNull(rs.getString(1)));
				jsnMtlStdObj.addProperty("mssSdo", checkNull(rs.getString(2)));
				jsnMtlStdObj.addProperty("mssStdId", checkNull(rs.getString(3)));
				jsnMtlStdObj.addProperty("mssAddnlId", checkNull(rs.getString(4)));
				jsnMtlStdChmArr.add(jsnMtlStdObj);

				for (int i = 5; i <= 62; i = i + 3) {

					chmEl = checkNull(rs.getString(i));
					chmMinVl = checkNull2(rs.getString(i + 1));
					chmMaxVl = checkNull2(rs.getString(i + 2));

					if (Double.parseDouble(chmMinVl) > 0 || Double.parseDouble(chmMaxVl) > 0) {

						JsonObject jsnMtlStdChmObj = new JsonObject();

						jsnMtlStdChmObj.addProperty("chmEl", chmEl);
						jsnMtlStdChmObj.addProperty("chmMinVl", chmMinVl);
						jsnMtlStdChmObj.addProperty("chmMaxVl", chmMaxVl);

						jsnMtlStdChmArr.add(jsnMtlStdChmObj);

						mtlStdChmCount++;

					}
				}
			}
		} catch (SQLException sqle) {
			System.out.println("SQLException--getMtlStdAndChemistry---->" + sqle.getLocalizedMessage());
		} catch (Exception e) {
			System.out.println("Exception--getMtlStdAndChemistry---->" + e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return jsnMtlStdChmArr;
	}

	public static JsonArray filterMetalStandards(String cmpyId, String qdsCtlNo, String param) {
		LogMessage.writeLogs("********* filterMetalStandards ************ ");
		Connection con = DBConnection.getDBConnection();
		String[] arr = param.split("/");
		String chmFlg = arr[0].trim();
		String tstSpecFlg = arr[1].trim();

		LogMessage.writeLogs("chmFlg >> " + chmFlg);
		LogMessage.writeLogs("tstSpecFlg >> " + tstSpecFlg);

		JsonArray resultArray = new JsonArray();
		JsonObject currentMetalStandard = null;

		List<String> specificElements = new ArrayList<>();
		specificElements.add("Ni");
		specificElements.add("Cr");
		specificElements.add("Mo");
		specificElements.add("Cu");
		specificElements.add("B");
		specificElements.add("Ti");
		specificElements.add("V");
		specificElements.add("Pb");
		specificElements.add("Bi");

		try {
			if (chmFlg.equals("1")) {
				List<QdsChemistry> qdsChmList = getQdsChemistry(cmpyId, qdsCtlNo, con);
				LogMessage.writeLogs("qdsChmList >> " + qdsChmList);

				JsonArray jsonArray = getMtlStdAndChemistry(con);
				LogMessage.writeLogs("jsonArray >> " + jsonArray);

				int mtlStdChmCount = 0, chmElMatchedCount = 0;

				for (int i = 0; i < jsonArray.size(); i++) {
					JsonObject entry = jsonArray.get(i).getAsJsonObject();

					// Check if this is a new metal standard
					if (entry.has("mssCtlNo")) {
						if (currentMetalStandard != null && mtlStdChmCount == chmElMatchedCount) {
							LogMessage.writeLogs("Match found for standard: " + currentMetalStandard);
							resultArray.add(currentMetalStandard);
						}

						// Initialize for the new standard
						currentMetalStandard = entry;
						mtlStdChmCount = 0;
						chmElMatchedCount = 0;
					}
					// Process chemical element entries
					else if (entry.has("chmEl") && currentMetalStandard != null) {
						mtlStdChmCount++;
						String chmEl = entry.get("chmEl").getAsString().trim();
						double minVal = entry.get("chmMinVl").getAsDouble();
						double maxVal = entry.get("chmMaxVl").getAsDouble();

						// Initialize variables
						double qdsValue = 0;
						boolean isReported = false;

						for (QdsChemistry qdsChem : qdsChmList) {
							if (chmEl.equalsIgnoreCase(qdsChem.getChmEl().trim())) {
								qdsValue = Double.parseDouble(qdsChem.getChmVl().trim());
								isReported = true;
								break;
							}
						}

						/*
						 * if (chmEl.equalsIgnoreCase("B")) { double tiValue =
						 * getChemistryValue(qdsChmList, "Ti");
						 * 
						 * // If Ti > 0.002, Boron must meet the standard (≤ 0.0005) if (tiValue >
						 * 0.002) {
						 * 
						 * if (qdsValue <= 0.0005) { chmElMatchedCount++; } else { mtlStdChmCount--; } }
						 * 
						 * else { // If Ti ≤ 0.002 or not reported, Boron’s max does not apply //
						 * Instead, Ti should meet its max (≤ 0.02) if (tiValue <= 0.02) {
						 * chmElMatchedCount++; } else { mtlStdChmCount--; } }
						 * 
						 * continue; // Move to the next element }
						 * 
						 * else if (chmEl.equalsIgnoreCase("Ti")) { double bValue =
						 * getChemistryValue(qdsChmList, "B");
						 * 
						 * if (bValue > 0 && bValue <= 0.005) { // If Boron (B) is present and ≤ 0.005,
						 * Ti must be ≤ 0.02 if (qdsValue <= 0.02) { chmElMatchedCount++; } else {
						 * mtlStdChmCount--; } } else { // General case for Ti when B is not reported or
						 * does not apply if (qdsValue <= 0.02) { chmElMatchedCount++; } else {
						 * mtlStdChmCount--; } }
						 * 
						 * continue; // Move to the next element }
						 */

						// Treat specific non-reported elements as zero if they have a defined max value
						// if (specificElements.contains(chmEl)) {
						// if (!isReported && maxVal > 0) {
						// chmElMatchedCount++;
						// continue;
						// }
						// }

						// Treat specific non-reported elements as zero if they have a defined max value
						if (specificElements.contains(chmEl) && !isReported) {
							qdsValue = 0; // if not added then considering it to 0
							if ((maxVal > 0 && qdsValue >= minVal && qdsValue <= maxVal)
									|| (maxVal == 0 && qdsValue >= minVal)) {
								chmElMatchedCount++;
								continue;
							}
						}

						// Regular value range check for reported elements
						if (isReported && (maxVal > 0 && qdsValue >= minVal && qdsValue <= maxVal)
								|| (maxVal == 0 && qdsValue >= minVal)) {
							chmElMatchedCount++;
						}

					}
				}

				// Add the last metal standard if it matches
				if (currentMetalStandard != null && mtlStdChmCount == chmElMatchedCount) {
					LogMessage.writeLogs("Last Match found for standard: " + currentMetalStandard);
					resultArray.add(currentMetalStandard);
				}
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return resultArray;
	}

	/**
	 * Helper method to fetch chemistry value by element name.
	 */
	private static double getChemistryValue(List<QdsChemistry> qdsChmList, String element) {
		for (QdsChemistry qdsChem : qdsChmList) {
			if (qdsChem.getChmEl().trim().equalsIgnoreCase(element)) {
				return Double.parseDouble(qdsChem.getChmVl().trim());
			}
		}
		return 0; // Treat non-reported values as zero
	}

	public static JsonArray filterMetalStandardsOld(String cmpyId, String qdsCtlNo, String param) {
		LogMessage.writeLogs("********* filterMetalStandards ************ ");
		Connection con = DBConnection.getDBConnection();
		String[] arr = param.split("/");
		String chmFlg = arr[0].trim();
		String tstSpecFlg = arr[1].trim();

		LogMessage.writeLogs("chmFlg >> " + chmFlg);
		LogMessage.writeLogs("tstSpecFlg >> " + tstSpecFlg);

		JsonArray resultArray = new JsonArray();
		JsonObject currentMetalStandard = null;

		try {
			if (chmFlg.equals("1")) {

				List<QdsChemistry> qdsChmList = getQdsChemistry(cmpyId, qdsCtlNo, con);
				LogMessage.writeLogs("qdsChmList >> " + qdsChmList);

				int totQdsCount = qdsChmList.size();
				LogMessage.writeLogs("totQdsCount >> " + totQdsCount);

				JsonArray jsonArray = getMtlStdAndChemistry(con);
				LogMessage.writeLogs("jsonArray >> " + jsonArray);

				LogMessage.writeLogs("\n ************************************ ");

				int count = 0, qdsMatchedCount = 0, qdsElMatchedCount = 0;

				// Loop through the input JSON
				for (int i = 0; i < jsonArray.size(); i++) {
					JsonObject entry = jsonArray.get(i).getAsJsonObject();

					// Check if this is a metal standard
					if (entry.has("mssCtlNo")) {

						// If all elements match, add this standard to the result
						if (count > 0 && (totQdsCount == qdsMatchedCount) && (totQdsCount == qdsElMatchedCount)) {
							LogMessage.writeLogs("Match found for standard: " + currentMetalStandard);
							resultArray.add(currentMetalStandard);
						}

						currentMetalStandard = entry;
						// LogMessage.writeLogs("currentMetalStandard >> " + currentMetalStandard);
						qdsMatchedCount = 0;
						qdsElMatchedCount = 0;
						count = 1;
					}

					// Process chemical element entries
					else if (entry.has("chmEl") && currentMetalStandard != null) {

						qdsElMatchedCount++;

						// Compare each element in `qdsChmList` with the `jsonArray`
						for (QdsChemistry qdsChem : qdsChmList) {
							String element = qdsChem.getChmEl().trim();
							double value = Double.parseDouble(qdsChem.getChmVl().trim());

							if (entry.get("chmEl").getAsString().trim().equalsIgnoreCase(element)) {
								double minVal = entry.get("chmMinVl").getAsDouble();
								double maxVal = entry.get("chmMaxVl").getAsDouble();

								// Debug: Print the matching process
								// LogMessage.writeLogs("Element: " + element + ", Value: " + value + ", Min: "
								// + minVal
								// + ", Max: " + maxVal);

								if (element.equalsIgnoreCase("B") && maxVal <= 0.0005) {

								}

								else if (value >= minVal && value <= maxVal) {
									qdsMatchedCount++;
									// LogMessage.writeLogs("qdsMatchedCount >> " + qdsMatchedCount);
									break;
								}
							}
						}
					}
				}

				// Adding last entry if it's match the value
				if (count > 0 && (totQdsCount == qdsMatchedCount) && (totQdsCount == qdsElMatchedCount)) {
					LogMessage.writeLogs("Last Match found for standard: " + currentMetalStandard);
					resultArray.add(currentMetalStandard);
				}

			}

			// LogMessage.writeLogs("resultArray >> " + resultArray);

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return resultArray;
	}

	public static String updateMtlStdInInvex(String cmpyId, String userId, String authToken, String qdsCtlNo,
			String param, String host, String port, String nginxURL) {
		int count = 0;
		String TMP_PATH = "";
		if (System.getenv("SERVER") != null) {
			TMP_PATH = System.getenv("HOME") + "/bin/STAR_LOGS/";
		} else {
			TMP_PATH = "C:\\Users\\Nawab-pc\\Desktop\\tmp\\";
		}

		String logFlNm = TMP_PATH + cmpyId + "_" + userId + "_mtl_upd_" + new Date().getTime() + ".log";
		LogMessage.setLogFile(logFlNm);
		LogMessage.generateLogs();

		LogMessage.writeLogs("********** updateMtlStdInInvex *************");

		JsonObject mainObj = new JsonObject();

		JsonArray jsonArray = filterMetalStandards(cmpyId, qdsCtlNo, param);
		LogMessage.writeLogs("updateMtlStdInInvex >> jsonArray >> " + jsonArray.toString());

		if (host.trim().length() == 0) {
			host = "0";
		}

		if (port.trim().length() == 0) {
			port = "0";
		}

		if (jsonArray.size() > 0) {

			LogMessage.writeLogs("updateMtlStdInInvex >>> jsonArray.size() > 0 ");

			LogMessage.writeLogs("nginxURL >> " + nginxURL);

			ServiceHelper.setWpaEndpointProtocol("https", nginxURL);

			LogMessage.writeLogs("After >>> ");

			ServiceHelper.setStxEndpointHostAndPort(host, Integer.parseInt(port), nginxURL);

			MessagesSOAPHandler messageHandler = new MessagesSOAPHandler();
			AuthenticationToken authenticationToken = new AuthenticationToken();

			authenticationToken.setUsername(userId);
			authenticationToken.setValue(authToken);

			SecuritySOAPHandler securityHandler = new SecuritySOAPHandler(authenticationToken);

			try {

				com.invera.stratix.services.QdsService qdsService1;

				qdsService1 = ServiceHelper.qdsService(messageHandler, securityHandler);

				for (int i = 0; i < jsonArray.size(); i++) {
					JsonObject entry = jsonArray.get(i).getAsJsonObject();

					String sdo = entry.get("mssSdo").getAsString();
					String stdId = entry.get("mssStdId").getAsString();
					String addnlId = entry.get("mssAddnlId").getAsString();

					UpdateQdsMetalStandardInput updQdsMtlStdInpt = new UpdateQdsMetalStandardInput();
					updQdsMtlStdInpt.setSdo(sdo);
					updQdsMtlStdInpt.setStdId(stdId);
					updQdsMtlStdInpt.setAddnlId(addnlId);
					updQdsMtlStdInpt.setMatchingChemistry("Y");
					updQdsMtlStdInpt.setMatchingTest("NA");
					updQdsMtlStdInpt.setMatchingJominy("NA");
					updQdsMtlStdInpt.setCertifiedByServiceCenter(1);

					qdsService1.updateQdsMetalStandard(Integer.parseInt(qdsCtlNo), updQdsMtlStdInpt);

					mainObj.addProperty("updateStatus", 0);
					mainObj.addProperty("msg", "");

					count++;
				}

				LogMessage.writeLogs("count >> " + count);

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				String errMsg = "";

				LogMessage.writeLogs("File---->" + messageHandler.getMessageList().size());

				for (int i = 0; i < messageHandler.getMessageList().size(); i++) {
					LogMessage.writeLogs("Logs---->" + messageHandler.getMessageList().get(i));

					errMsg = errMsg + messageHandler.getMessageList().get(i) + "<br>";
				}

				mainObj.addProperty("updateStatus", 1);
				mainObj.addProperty("msg", errMsg);
				count++;

			} catch (com.invera.stratix.services.StratixFault e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				String errMsg = "";
				LogMessage.writeLogs("File---->" + messageHandler.getMessageList().size());

				for (int i = 0; i < messageHandler.getMessageList().size(); i++) {
					LogMessage.writeLogs("Logs---->" + messageHandler.getMessageList().get(i));

					errMsg = errMsg + messageHandler.getMessageList().get(i) + "<br>";
				}

				mainObj.addProperty("updateStatus", 1);
				mainObj.addProperty("msg", errMsg);
				count++;
			}
		}

		else {

			LogMessage.writeLogs("updateMtlStdInInvex in else ");

		}

		LogMessage.writeLogs("count >> " + count);
		LogMessage.writeLogs("jsonArray.size() >> " + jsonArray.size());

		if (count == 0 || jsonArray.size() == 0) {
			mainObj.addProperty("updateStatus", 2);
			mainObj.addProperty("msg",
					"The QDS chemistry provided does not match any entries in the metal standards chemistry.");
		}

		LogMessage.writeLogs("mainObj >> " + mainObj.toString());
		return mainObj.toString();
	}

	public static String checkNull(String v) {
		return v == null ? "" : v.trim();
	}

	public static String checkNull2(String v) {
		return v == null ? "0" : v.trim();
	}

}
