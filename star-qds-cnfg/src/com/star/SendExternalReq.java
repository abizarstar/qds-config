package com.star;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SendExternalReq {

	public static final String JSON_HANDLER_URL = "/stratix-info";
	// Invera HTTP headers
	private static final String HEADER_REQUEST_ID = "invera-request-id";
	private static final String HEADER_USERNAME = "invera-username";
	private static final String HEADER_CLIENT_PROGRAM_NAME = "invera-client-program-name";
	private static final String HEADER_CLIENT_TYPE = "invera-client-type";
	private static final String HEADER_CLIENT_PLATFORM = "invera-client-platform";

	private static final String HEADER_CONTEXT_ID = "invera-context-id";
	private static final String HEADER_SERVICE_NAME = "invera-service-name";
	private static final String HEADER_SERVICE_CALL_TYPE = "invera-service-call-type";
	private static final String HEADER_SERVICE_ROUTING = "invera-service-route";
	private static final String HEADER_CHARSET = "invera-charset";
	private static final String HEADER_SESSION = "invera-session";
	private static final String CONTENT_TYPE = "content-type";

	private static final String HEADER_SERVICE_STATUS = "invera-service-status";
	private static final String HEADER_SERVICE_MESSAGES = "invera-service-messages";

	private static final String REQUEST_INPUT_TYPE = "application/json; charset=utf-8";

	private static final String RESPONSE_OUTPUT = "output";
	private static final String RESPONSE_OUTPUT_RTN_STS = "rtnSts"; // use output.RtnSts as a fallback
	private static final String RESPONSE_OUTPUT_STATUS = "status";
	private static final String RESPONSE_CONTENT_TYPE = "application/json";

	private static final String SERVICE_CALL_TYPE_ASYNC = "A";
	private static final String PRS_MD = "1";

	public String getQDSInformation(String address, String port, String authToken, String prsMd, String serviceName,
			String pgmNm, String usrId, String cmpyId, String baseLng, String baseCntry, String jsonString,
			String nginxURL) throws Exception {

		// System.out.println("****** getQDSInformation ***");
		// System.out.println("serviceName >> "+serviceName);

		String requestId = String.valueOf(new Date().getTime());
		String outputVal = "";

		/* GET PLAIN TOKEN BASED ON OCTET STREAM */

		String plainToken = getPlainToken(address, port, usrId, authToken, nginxURL);

		// System.out.println("plainToken >> " + plainToken);

		URL url;

		String uri = null;

		uri = JSON_HANDLER_URL + "/" + serviceName + "/" + (prsMd == null ? "-" : prsMd.trim()) + "/" + pgmNm + "/"
				+ "XX000000null" + "/" + usrId + "/" + cmpyId + "/" + baseLng + "_" + baseCntry + "/" + requestId + "/"
				+ serviceName.toUpperCase() + (prsMd != null ? "-" + prsMd.trim() : "");

		if (nginxURL != null && nginxURL.trim().length() > 0) {
			uri = nginxURL + uri;
		} else {
			uri = "http://" + address + ":" + port + "/" + uri;
		}

		url = new URL(uri);

		// System.out.println("url >> " + url);

		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty(CONTENT_TYPE, REQUEST_INPUT_TYPE);
		con.setRequestProperty(HEADER_CHARSET, "ISO-8859-4");
		con.setRequestProperty(HEADER_CLIENT_PLATFORM, "Windows");
		con.setRequestProperty(HEADER_CLIENT_PROGRAM_NAME, pgmNm);
		con.setRequestProperty(HEADER_CLIENT_TYPE, "desktop");
		con.setRequestProperty(HEADER_CONTEXT_ID, "XX000000null");
		con.setRequestProperty(HEADER_REQUEST_ID, requestId);
		con.setRequestProperty(HEADER_SERVICE_NAME, serviceName);
		// con.setRequestProperty(HEADER_SERVICE_ROUTING, "PR");
		con.setRequestProperty(HEADER_USERNAME, usrId);
		con.setRequestProperty(HEADER_SESSION, plainToken);

		String jsonInputString = jsonString;

		con.setDoOutput(true);
		try (OutputStream os = con.getOutputStream()) {
			byte[] input = jsonInputString.getBytes("utf-8");
			os.write(input, 0, input.length);
			os.flush();
			os.close();
		}

		int responseCode = con.getResponseCode();
		// System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK || responseCode == 206) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			outputVal = response.toString();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}

		String serviceMsg = con.getHeaderField(HEADER_SERVICE_MESSAGES);
		String serviceStatus = con.getHeaderField(HEADER_SERVICE_STATUS);

		if (serviceStatus.equals("ERROR")) {
			JsonArray jsonArray = new JsonParser().parse(serviceMsg).getAsJsonArray();

			for (int i = 0; i < jsonArray.size(); i++) {

				JsonObject object = jsonArray.get(i).getAsJsonObject();

			}

			throw new Exception("Error while Reconciliation");
		}

		return outputVal;
	}

	public String callService(String address, String port, String authToken, String prsMd, String serviceName,
			String pgmNm, String usrId, String cmpyId, String baseLng, String baseCntry, String jsonString,
			String nginxURL) throws Exception {

		System.out.println("******** callService ******** ");

		String requestId = String.valueOf(new Date().getTime());
		String outputVal = "";

		/* GET PLAIN TOKEN BASED ON OCTET STREAM */
		String plainToken = getPlainToken(address, port, usrId, authToken, nginxURL);

		URL url;

		String uri = null;

		uri = JSON_HANDLER_URL + "/" + serviceName + "/" + (prsMd == null ? "-" : prsMd.trim()) + "/" + pgmNm + "/"
				+ "XX000000null" + "/" + usrId + "/" + cmpyId + "/" + baseLng + "_" + baseCntry + "/" + requestId + "/"
				+ serviceName.toUpperCase() + (prsMd != null ? "-" + prsMd.trim() : "");

		if (nginxURL!=null && nginxURL.trim().length() > 0) {
			uri =  nginxURL + uri;
		} else {
			uri = "http://" + address + ":" + port + "/" + uri;
		}

		System.out.println(pgmNm + " >> callService >> uri >> " + uri);

		url = new URL(uri);

		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty(CONTENT_TYPE, REQUEST_INPUT_TYPE);
		con.setRequestProperty(HEADER_CHARSET, "ISO-8859-4");
		con.setRequestProperty(HEADER_CLIENT_PLATFORM, "Windows");
		con.setRequestProperty(HEADER_CLIENT_PROGRAM_NAME, pgmNm);
		con.setRequestProperty(HEADER_CLIENT_TYPE, "desktop");
		con.setRequestProperty(HEADER_CONTEXT_ID, "XX000000null");
		con.setRequestProperty(HEADER_REQUEST_ID, requestId);
		con.setRequestProperty(HEADER_SERVICE_NAME, serviceName);
		con.setRequestProperty(HEADER_SERVICE_ROUTING, "PR");
		con.setRequestProperty(HEADER_USERNAME, usrId);
		con.setRequestProperty(HEADER_SESSION, plainToken);

		String jsonInputString = jsonString;

		if (jsonInputString.trim().length() > 0) {
			con.setDoOutput(true);
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
				os.flush();
				os.close();
			}
		}

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			outputVal = response.toString();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}

		String serviceMsg = con.getHeaderField(HEADER_SERVICE_MESSAGES);
		String serviceStatus = con.getHeaderField(HEADER_SERVICE_STATUS);

		System.out.println("serviceMsg >> " + serviceMsg);
		System.out.println("serviceStatus >> " + serviceStatus);

		if (serviceStatus != null && serviceStatus.equals("ERROR")) {
			JsonArray jsonArray = new JsonParser().parse(serviceMsg).getAsJsonArray();

			for (int i = 0; i < jsonArray.size(); i++) {

				JsonObject object = jsonArray.get(i).getAsJsonObject();

				JsonObject jsonObjectMain = new JsonObject();

				JsonObject jsonObject = new JsonObject();

				jsonObject.addProperty("rtnSts", 1);
				jsonObject.addProperty("msg", object.get("msg").toString());

				jsonObjectMain.add("output", jsonObject);

				outputVal = jsonObjectMain.toString();

			}
		}

		return outputVal;
	}

	public String getPlainToken(String address, String port, String usrId, String authToken, String nginxURL) {
		String requestId = String.valueOf(new Date().getTime());
		String plainTextCode = "";
		JsonObject jsonResponse = null;

		URL url;
		try {

			String uri = null;

			uri = JSON_HANDLER_URL + "/" + "common/session/SessionService/getExtendedSessionInformation";

			if (nginxURL != null && nginxURL.length() > 0) {
				uri = nginxURL + uri;
			} else {
				uri = "http://" + address + ":" + port + "/" + uri;
			}

			// System.out.println("getPlainToken >> uri >> " + uri);

			url = new URL(uri);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty(CONTENT_TYPE, REQUEST_INPUT_TYPE);
			con.setRequestProperty(HEADER_CHARSET, "ISO-8859-4");
			con.setRequestProperty(HEADER_CLIENT_PLATFORM, "Windows");
			con.setRequestProperty(HEADER_CLIENT_PROGRAM_NAME, "WMN");
			con.setRequestProperty(HEADER_CLIENT_TYPE, "desktop");
			con.setRequestProperty(HEADER_CONTEXT_ID, "XX000000null");
			con.setRequestProperty(HEADER_REQUEST_ID, requestId);
			con.setRequestProperty(HEADER_USERNAME, usrId);
			con.setRequestProperty(HEADER_SESSION, authToken);

			String jsonInputString = "WMN";

			con.setDoOutput(true);
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
				os.flush();
				os.close();
			}

			int responseCode = con.getResponseCode();
			// System.out.println("POST Response Code :: " + responseCode);

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// print result
				System.out.println(response.toString());

				if (response.toString().length() > 0) {
					JsonParser parser = new JsonParser();
					jsonResponse = (JsonObject) parser.parse(response.toString());
				}

			} else {
				System.out.println("POST request not worked");
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (jsonResponse != null) {
			if (jsonResponse.has("output")) {
				JsonObject jsonObject = jsonResponse.getAsJsonObject("output");

				plainTextCode = jsonObject.get("sessionToken").getAsString();

			}
		}

		return plainTextCode;
	}

}
