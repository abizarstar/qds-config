/*
 *
 * PROPRIETARY PROGRAM MATERIAL 
 * 
 * This material is proprietary to Invera Inc. and is not
 * to be reproduced, used or disclosed except in accordance
 * with the program license or upon written authorization from
 * Invera Inc., 4333 St-Catherine Street West, Westmount
 * Quebec Canada H3Z 1P9
 *
 * Copyright (C) 2018, Invera Inc.
 */

package com.ws.samples;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.invera.stratix.services.QdsService;
import com.invera.stratix.services.QdsService_Service;
import com.invera.stratix.ws.exec.authentication.AuthenticationService;
import com.invera.stratix.ws.exec.authentication.AuthenticationService_Service;
import com.invera.stratix.ws.services.security.LogoutService;
import com.invera.stratix.ws.services.security.LogoutService_Service;
import com.star.LogMessage;
import com.ws.samples.handler.MessagesSOAPHandler;
import com.ws.samples.handler.SecuritySOAPHandler;
import com.ws.samples.util.WSUtils;

public abstract class ServiceHelper {

	private static final String STRATIX_NAMESPACE_ROOT = "http://stratix.invera.com/services";

	// EXEC Web Product Authentication Server
	private static final String WPA_ENDPOINT_BASE_URI = "/webservices";
	private static String wpaEndpointProtocol = "http";
	private static String wpaEndpointHostAndPort;
	private static String wpaEndpointUrlRoot;
	// private static final String WPA_ENDPOINT_URL_ROOT =
	// "http://hermes:35731/webservices";

	// STRATIX Server
	private static final String STX_ENDPOINT_BASE_URI = "/webservices";
	private static String stxEndpointProtocol = "http";
	private static String stxEndpointHostAndPort;
	private static String stxEndpointUrlRoot;
	// private static final String STX_ENDPOINT_URL_ROOT =
	// "http://saturn:60851/webservices";

	// Authentication service URI on the EXEC/WPA server
	private static final String AUTHENTICATION_SERVICE_ENDPOINT_URI = "/exec/AuthenticationService";

	// QDS service URI on the STRATIX Server
	private static final String QDS_SERVICE_ENDPOINT_URI = "/gateway/QDS/QdsService";
	private static final String PO_SERVICE_ENDPOINT_URI = "/gateway/purchaseOrders/PurchaseOrderService";
	private static final String PO_ITM_SERVICE_ENDPOINT_URI = "/gateway/purchaseOrders/PurchaseOrderItemService";
	// Logout service URI on the STRATIX Server
	private static final String LOGOUT_SERVICE_ENDPOINT_URI = "/security/LogoutService";

	public static void setWpaEndpointProtocol(String httpOrHttps, String nginxURL) {

		// nginxURL = https and Ip/port = http
		if (httpOrHttps != null) {
			wpaEndpointProtocol = httpOrHttps.trim();
			if (wpaEndpointHostAndPort != null && (nginxURL == null || nginxURL.length() == 0)) {
				wpaEndpointUrlRoot = wpaEndpointProtocol + "://" + wpaEndpointHostAndPort + WPA_ENDPOINT_BASE_URI;

			}

			else {
				wpaEndpointUrlRoot = nginxURL + WPA_ENDPOINT_BASE_URI;
			}
		}

		LogMessage.writeLogs("setWpaEndpointProtocol >> Authentication Server Url: " + wpaEndpointUrlRoot);

	}

	public static void setWpaEndpointHostAndPort(String host, int port, String nginxURL) {
		if (nginxURL == null || nginxURL.length() == 0) {

			if (host != null) {
				wpaEndpointHostAndPort = host.trim() + ":" + port;
				if ((wpaEndpointProtocol != null) && (host != null)) {
					wpaEndpointUrlRoot = wpaEndpointProtocol + "://" + wpaEndpointHostAndPort + WPA_ENDPOINT_BASE_URI;
					System.out.println("Authentication Server Url: " + wpaEndpointUrlRoot);
				}
			}
		}

		else {
			wpaEndpointUrlRoot = nginxURL + WPA_ENDPOINT_BASE_URI;
			System.out.println("Authentication Server Url: " + wpaEndpointUrlRoot);

		}
	}

	private static String getWpaEndpointUrlRoot() {
		return wpaEndpointUrlRoot;
	}

	public static void setStxEndpointProtocol(String httpOrHttps, String nginxURL) {
		if (httpOrHttps != null) {
			stxEndpointProtocol = httpOrHttps.trim();

			if (stxEndpointHostAndPort != null && (nginxURL == null || nginxURL.length() == 0)) {
				stxEndpointUrlRoot = stxEndpointProtocol + "://" + stxEndpointHostAndPort + STX_ENDPOINT_BASE_URI;
			}

			else {
				stxEndpointUrlRoot = nginxURL + STX_ENDPOINT_BASE_URI;
			}
		}

		LogMessage.writeLogs("setStxEndpointProtocol >> STRATIX Server Url: " + stxEndpointUrlRoot);

	}

	public static void setStxEndpointHostAndPort(String host, int port, String nginxURL) {

		if (nginxURL == null || nginxURL.length() == 0) {
			if (host != null) {
				stxEndpointHostAndPort = host.trim() + ":" + port;
				if ((stxEndpointProtocol != null) && (host != null)) {
					stxEndpointUrlRoot = stxEndpointProtocol + "://" + stxEndpointHostAndPort + STX_ENDPOINT_BASE_URI;
				}
			}
		} else {
			// INVEX 2.0
			stxEndpointUrlRoot = nginxURL + STX_ENDPOINT_BASE_URI;

		}

		LogMessage.writeLogs("setStxEndpointHostAndPort >> STRATIX Server Url: " + stxEndpointUrlRoot);

	}

	private static String getStxEndpointUrlRoot() {
		return stxEndpointUrlRoot;
	}

	public static AuthenticationService createAuthenticationService(MessagesSOAPHandler messageHandler)
			throws MalformedURLException {

		AuthenticationService service = new AuthenticationService_Service(
				new URL(getWpaEndpointUrlRoot() + "/services/exec/AuthenticationService.wsdl"),
				new QName(STRATIX_NAMESPACE_ROOT, "AuthenticationService")).getAuthenticationServiceSOAP();

		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getWpaEndpointUrlRoot() + AUTHENTICATION_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);

		return service;
	}

	public static QdsService qdsService(MessagesSOAPHandler messageHandler, SecuritySOAPHandler securityHandler)
			throws MalformedURLException {

		QdsService service = new QdsService_Service(
				new URL(getStxEndpointUrlRoot() + "/services/gateway/QDS/QdsService.wsdl"),
				new QName(STRATIX_NAMESPACE_ROOT, "QdsService")).getQdsServiceSOAP();

		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + QDS_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}

	public static LogoutService createLogoutService(MessagesSOAPHandler messageHandler,
			SecuritySOAPHandler securityHandler) throws MalformedURLException {

		LogoutService service = new LogoutService_Service(
				new URL(getStxEndpointUrlRoot() + "/services/security/LogoutService.wsdl"),
				new QName(STRATIX_NAMESPACE_ROOT, "LogoutService")).getLogoutServiceSOAP();

		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + LOGOUT_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}

}
