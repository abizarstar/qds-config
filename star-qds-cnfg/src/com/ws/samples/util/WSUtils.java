/*
 *
 * PROPRIETARY PROGRAM MATERIAL 
 * 
 * This material is proprietary to Invera Inc. and is not
 * to be reproduced, used or disclosed except in accordance
 * with the program license or upon written authorization from
 * Invera Inc., 4333 St-Catherine Street West, Westmount
 * Quebec Canada H3Z 1P9
 *
 * Copyright (C) 2018, Invera Inc.
 */

package com.ws.samples.util;

import java.util.List;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.soap.SOAPHandler;

public class WSUtils {

	public static void bind(BindingProvider bindingProvider,
			String endpointPublishUrlRoot, String endpointPublishUri) {
		bind(bindingProvider, endpointPublishUrlRoot + endpointPublishUri);
	}

	public static void bind(BindingProvider bindingProvider,
			String endpointPublishUrl) {

		bindingProvider.getRequestContext().put(
				BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointPublishUrl);

	}

	public static void addSOAPHandler(BindingProvider bindingProvider,
			SOAPHandler soapHandler) {

		Binding binding = bindingProvider.getBinding();

		List<Handler> handlerList = binding.getHandlerChain();
		handlerList.add(soapHandler);

		binding.setHandlerChain(handlerList);

	}

}
