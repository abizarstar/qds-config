/*
 *
 * PROPRIETARY PROGRAM MATERIAL 
 * 
 * This material is proprietary to Invera Inc. and is not
 * to be reproduced, used or disclosed except in accordance
 * with the program license or upon written authorization from
 * Invera Inc., 4333 St-Catherine Street West, Westmount
 * Quebec Canada H3Z 1P9
 *
 * Copyright (C) 2018, Invera Inc.
 */

package com.ws.samples.handler;

import java.util.Iterator;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;

import com.invera.stratix.schema.common.datatypes.AuthenticationToken;

public class SecuritySOAPHandler implements SOAPHandler<SOAPMessageContext> {

	private AuthenticationToken authenticationToken = null;

	private static final StratixNamespaceContext stratixNamespaceContext;
	private static final JAXBContext jaxbContext;
	private static final String AUTHENTICATION_HEADER_ROOT_ELEMENT_NAME = "AuthenticationHeader";
	private static final String STRATIX_SERVICES_NAMESPACE = "http://stratix.invera.com/services";

	private static class StratixNamespaceContext implements NamespaceContext {
		public StratixNamespaceContext() {
			super();
		}

		public String getNamespaceURI(String prefix) {
			if (prefix == null) {
				throw new NullPointerException("Null prefix");
			} else if ("stx".equals(prefix)) {
				return STRATIX_SERVICES_NAMESPACE;
			} else if ("xml".equals(prefix)) {
				return XMLConstants.XML_NS_URI;
			}

			return null;
		}

		public String getPrefix(String namespace) {
			throw new UnsupportedOperationException();
		}

		public Iterator getPrefixes(String namespace) {
			throw new UnsupportedOperationException();
		}
	}

	static {
		try {
			jaxbContext = JAXBContext.newInstance(AuthenticationToken.class);
			stratixNamespaceContext = new StratixNamespaceContext();
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public SecuritySOAPHandler() {
		super();
	}

	public SecuritySOAPHandler(AuthenticationToken authenticationToken) {
		this();
		setAuthenticationToken(authenticationToken);
	}

	public Set<QName> getHeaders() {
		return null;
	}

	private void addAuthenticationTokenToRequestHeader(SOAPMessageContext smc) {
		if (getAuthenticationToken() != null)
			try {
				SOAPMessage sm = smc.getMessage();
				SOAPHeader header = sm.getSOAPHeader();

				if (header == null) {
					sm.getSOAPPart().getEnvelope().addHeader();
					header = sm.getSOAPHeader();
				}

				JAXBElement<AuthenticationToken> jaxbElement = new JAXBElement<AuthenticationToken>(
						new QName(STRATIX_SERVICES_NAMESPACE,
								AUTHENTICATION_HEADER_ROOT_ELEMENT_NAME),
						AuthenticationToken.class, getAuthenticationToken());
				Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.marshal(jaxbElement, header);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
	}

	public void readAuthenticationTokenFromResponseHeader(SOAPMessageContext smc) {
		try {
			SOAPMessage sm = smc.getMessage();
			SOAPHeader header = sm.getSOAPHeader();
			if (header != null) {
				header = sm.getSOAPHeader();

				XPathFactory factory = XPathFactory.newInstance();
				XPath xpath = factory.newXPath();
				xpath.setNamespaceContext(stratixNamespaceContext);

				XPathExpression expr = xpath.compile("//stx:"
						+ AUTHENTICATION_HEADER_ROOT_ELEMENT_NAME);
				Node domNode = (Node) expr
						.evaluate(header, XPathConstants.NODE);

				if (domNode != null) {
					Unmarshaller unmarshaller = jaxbContext
							.createUnmarshaller();

					JAXBElement<AuthenticationToken> jaxbElement = unmarshaller
							.unmarshal(domNode, AuthenticationToken.class);
					setAuthenticationToken(jaxbElement.getValue());
				}
			}
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			}
			throw new RuntimeException(e);
		}
	}

	public boolean handleMessage(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		boolean isAuthenticated = true;

		if (outboundProperty.booleanValue()) {
			if (getAuthenticationToken() != null) {
				addAuthenticationTokenToRequestHeader(smc);
			} else {
				isAuthenticated = false;
			}
		} else if (!outboundProperty.booleanValue()) {
			readAuthenticationTokenFromResponseHeader(smc);
		}

		return isAuthenticated;
	}

	public boolean handleFault(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (!outboundProperty.booleanValue()) {
			readAuthenticationTokenFromResponseHeader(smc);
		}

		return true;
	}

	public void close(MessageContext messageContext) {
		// nothing to clean up
	}

	public AuthenticationToken getAuthenticationToken() {
		return this.authenticationToken;
	}

	public void setAuthenticationToken(AuthenticationToken authenticationToken) {
		this.authenticationToken = authenticationToken;
	}
}