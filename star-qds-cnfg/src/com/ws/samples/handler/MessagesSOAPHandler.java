/*
 *
 * PROPRIETARY PROGRAM MATERIAL 
 * 
 * This material is proprietary to Invera Inc. and is not
 * to be reproduced, used or disclosed except in accordance
 * with the program license or upon written authorization from
 * Invera Inc., 4333 St-Catherine Street West, Westmount
 * Quebec Canada H3Z 1P9
 *
 * Copyright (C) 2018, Invera Inc.
 */

package com.ws.samples.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;

import com.invera.stratix.schema.common.datatypes.DataElementMessageList;
import com.invera.stratix.schema.common.datatypes.ServiceMessages;

public class MessagesSOAPHandler implements SOAPHandler<SOAPMessageContext> {

	private ServiceMessages serviceMessages = null;

	private static final StratixNamespaceContext stratixNamespaceContext;
	private static final JAXBContext jaxbContext;
	private static final String MESSAGES_HEADER_ROOT_ELEMENT_NAME = "ServiceMessagesHeader";
	private static final String STRATIX_SERVICES_NAMESPACE = "http://stratix.invera.com/services";

	private static class StratixNamespaceContext implements NamespaceContext {
		public StratixNamespaceContext() {
			super();
		}

		public String getNamespaceURI(String prefix) {
			if (prefix == null) {
				throw new NullPointerException("Null prefix");
			} else if ("stx".equals(prefix)) {
				return STRATIX_SERVICES_NAMESPACE;
			} else if ("xml".equals(prefix)) {
				return XMLConstants.XML_NS_URI;
			}

			return null;
		}

		public String getPrefix(String namespace) {
			throw new UnsupportedOperationException();
		}

		public Iterator getPrefixes(String namespace) {
			throw new UnsupportedOperationException();
		}
	}

	static {
		try {
			jaxbContext = JAXBContext.newInstance(ServiceMessages.class);
			stratixNamespaceContext = new StratixNamespaceContext();
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public MessagesSOAPHandler() {
		super();
		this.serviceMessages = new ServiceMessages();
	}

	public Set<QName> getHeaders() {
		return null;
	}

	public void readMessagesFromResponseHeader(SOAPMessageContext smc) {
		try {
			SOAPMessage sm = smc.getMessage();
			SOAPHeader header = sm.getSOAPHeader();
			if (header != null) {
				header = sm.getSOAPHeader();

				XPathFactory factory = XPathFactory.newInstance();
				XPath xpath = factory.newXPath();
				xpath.setNamespaceContext(stratixNamespaceContext);

				XPathExpression expr = xpath.compile("//stx:"
						+ MESSAGES_HEADER_ROOT_ELEMENT_NAME);
				Node domNode = (Node) expr
						.evaluate(header, XPathConstants.NODE);

				if (domNode != null) {
					Unmarshaller unmarshaller = jaxbContext
							.createUnmarshaller();

					JAXBElement<ServiceMessages> jaxbElement = unmarshaller
							.unmarshal(domNode, ServiceMessages.class);
					this.serviceMessages = jaxbElement.getValue();
				} else {
					// clear values from previous invocation
					this.serviceMessages = new ServiceMessages();
				}
			}
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			}
			throw new RuntimeException(e);
		}
	}

	public boolean handleMessage(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (!outboundProperty.booleanValue()) {
			readMessagesFromResponseHeader(smc);
		}

		return true;
	}

	public boolean handleFault(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (!outboundProperty.booleanValue()) {
			readMessagesFromResponseHeader(smc);
		}

		return true;
	}

	public void close(MessageContext messageContext) {
		// nothing to clean up
	}

	public void clearMessages() {
		this.serviceMessages = new ServiceMessages();
	}

	public List<String> getMessageList() {
		List<String> fullMessageList = new ArrayList<String>();

		fullMessageList.addAll(this.serviceMessages.getMessageList());

		// The STRATIX ServiceMessages class is compatible with the
		// ServiceMessages returned by Exec.
		// Only STRATIX Web services returns this collection
		// EXEC Web Services do NOT return his collection
		Iterator<DataElementMessageList> dataElementMessages = this.serviceMessages
				.getDataElementMessageList().iterator();
		if (dataElementMessages != null) {
			while (dataElementMessages.hasNext()) {
				fullMessageList.addAll(dataElementMessages.next()
						.getMessageList());
			}
		}

		return fullMessageList;
	}
}